__author__ = 'Denis Romanov'


class UnitPoints():
    def __init__(self):
        self.id = 'points'
        self.value = 0

    def set(self, val):
        self.value = val

    def get(self):
        return self.value
