from builder.core import build_points_string, id_filter, node_load
from builder.core.options import GenericOption

__author__ = 'Denis Romanov'


class Count(GenericOption):

    def __init__(self, id, name, min, max, points, help=None):
        GenericOption.__init__(self, id, name)
        self.unit_points = points
        if help is not None:
            self.help = help
        else:
            self.help = build_points_string(points)

        self._min = self.observable('min', min)
        self._max = self.observable('max', max)
        self._cur = self.observable('cur', min)

    def points(self):
        if not self.is_used():
            return 0
        return self._cur.get() * self.unit_points

    @id_filter
    def update(self, data):
        self._cur.set(data['value'])

    def update_range(self, new_min, new_max):
        self._min.set(max(0, new_min))
        self._max.set(max(0, new_max))
        if self._cur.get() < self._min.get():
            self.set(self._min.get())
        elif self._cur.get() > self._max.get():
            self.set(self._max.get())

    def set(self, val):
        self._cur.set(val)

    def get(self):
        return self._cur.get() if self.is_used() else 0

    def get_max(self):
        return self._max.get()

    def get_min(self):
        return self._min.get()

    def dump(self):
        res = GenericOption.dump(self)
        res.update(
            {
                'type': 'count',
                'name': self.name,
                'min': self._min.get(),
                'max': self._max.get(),
                'cur': self._cur.get(),
                'help': self.help,
            }
        )
        return res

    def get_selected(self):
        return {'name': self.name, 'count': self._cur.get()} if self.is_used() else None

    def dump_save(self):
        res = GenericOption.dump(self)
        res.update(
            {
                'min': self._min.get(),
                'max': self._max.get(),
                'cur': self._cur.get(),
            }
        )
        return res

    @node_load
    def load(self, data):
        if 'min' in data.keys():
            self._min.set(data['min'])
        if 'max' in data.keys():
            self._max.set(data['max'])
        if 'cur' in data.keys():
            self._cur.set(data['cur'])

    def set_active(self, val):
        GenericOption.set_active(self, val)
        GenericOption.set_visible(self, val)
