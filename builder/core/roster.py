from builder.core import Node


__author__ = 'Denis Romanov'


class Roster(Node):
    def __init__(self, sections, limit=None):
        Node.__init__(self)
        self._sections = self.observable_array('sections', sections)
        self.sections_map = {}
        for s in self._sections.get():
            self.sections_map[s.id] = s
        self._errors = self.observable('errors', [])
        self._points = self.observable('points', 0)
        self._limit = self.observable('limit', limit)
        self._description = self.observable('description', '')

    def dump_settings(self):
        return dict(
            name=self.name,
            limit=self._limit.get(),
            description=self._description.get()
        )

    def update(self, data):
        if 'sections' in data.keys():
            for section in data['sections']:
                self.sections_map[section['id']].update(section)
        if 'settings' in data.keys():
            self._limit.set(data['settings'].get('limit', None))
            self._description.set(data['settings'].get('description', ''))

    def dump(self):
        res = Node.dump(self)
        res['version'] = 1
        res['name'] = self.name
        res['points'] = self._points.get()
        res['limit'] = self._limit.get()
        res['sections'] = map(lambda section: section.dump(), self._sections.get())
        res['errors'] = self._errors.get()
        res['description'] = self._description.get()
        res['settings'] = self.dump_settings()
        return res

    def save(self):
        return self.dump_save()

    def dump_save(self):
        res = {}
        res['army_id'] = self.army_id
        res['limit'] = self._limit.get()
        res['points'] = self._points.get()
        res['sections'] = map(lambda section: section.dump_save(), self._sections.get())
        res['description'] = self._description.get()
        return res

    def load(self, data):
        self._limit.set(data.get('limit', self._limit.get()))
        self._description.set(data.get('description', self._description.get()))
        for sec_data in data.get('sections', []):
            for s in self._sections.get():
                if s.id != sec_data['id']:
                    continue
                s.load(sec_data)

    def error(self, message):
        if not isinstance(message, (list, tuple)):
            message = [message]
        self._errors.set(self._errors.get() + [e for e in message if e])

    def _unique_check(self, names_getter, message):
        unique_map = {}
        for sec in self._sections.get():
            for unit in sec.get_units():
                un = names_getter(unit)
                if un is None:
                    continue
                if not isinstance(un, (list, type)):
                    un = [un]
                for name in un:
                    if name in unique_map.keys():
                        if unique_map[name] == 1:
                            self.error(message.format(name))
                        unique_map[name] += 1
                    else:
                        unique_map[name] = 1

    def unique_check(self):
        self._unique_check(lambda unit: unit.get_unique(), '{0} must be unique in army')
        self._unique_check(lambda unit: unit.get_unique_gear(), '{0} must be unique in army')

    def sections_check(self):
        points = 0
        for sec in self._sections.get():
            self.error(sec.get_errors())
            points += sec.get_points()
        self._points.set(points)
        if self._limit.get() and self._points.get() > self._limit.get():
            self.error('Roster is over limit ({0})'.format(self._limit.get()))

    def check_rules_chain(self):
        Node.check_rules_chain(self)
        self._errors.set([])
        self.sections_check()
        self.check_rules()
        self.unique_check()

    def check_rules(self):
        pass

    def count_unit(self, type):
        return reduce(lambda val, s: val + s.count_unit(type), self._sections.get(), 0)

    def get_points(self):
        return self._points.get()

    def get_limit(self):
        return self._limit.get()

    def get_errors(self):
        return self._errors.get()

    def get_sections(self):
        return self.sections

    @property
    def points(self):
        return self.get_points()

    @property
    def errors(self):
        return self.get_errors()

    @errors.setter
    def errors(self, value):
        self._errors.set(value)

    @property
    def sections(self):
        return self._sections.get()
