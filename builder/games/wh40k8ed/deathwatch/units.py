Aggressors = ('Aggressors', 21)
Bikers = ('Bikers', 21)
Chaplain = ('Chaplain', 72)
CorvusBlackstar = ('Corvus Blackstar', 135)
Dreadnought = ('Dreadnought', 60)
DropPod = ('Drop Pod', 63)
Hellblasters = ('Hellblasters', 18)
Inceptors = ('Inceptors', 25)
Intercessors = ('Intercessors', 17)
JumpChaplain = ('Chaplain with Jump Pack', 90)
JumpLibrarian = ('Librarian with Jump Pack', 112)
JumpWatchCaptain = ('Watch Captain with Jump Pack', 93)
KillTeamAggressors = ('Aggressors', 21)
KillTeamBikers = ('Bikers', 21)
KillTeamBlackShield = ('Black Shield', 16)
KillTeamHellblasters = ('Hellblasters', 18)
KillTeamInceptors = ('Inceptors', 25)
KillTeamReivers = ('Reivers', 18)
KillTeamTerminators = ('Terminators', 23)
KillTeamVanguardVeterans = ('Vanguard Veterans', 17)
LandRaider = ('Land Raider', 200)
LandRaiderCrusader = ('Land Raider Crusader', 200)
LandRaiderRedeemer = ('Land Raider Redeemer', 180)
Librarian = ('Librarian', 88)
PrimarisApothecary = ('Primaris Apothecary', 68)
PrimarisChaplain = ('Primaris Chaplain', 77)
PrimarisLibrarian = ('Primaris Librarian', 93)
PrimarisWatchCaptain = ('Primaris Watch Captain', 78)
Razorback = ('Razorback', 70)
RedemptorDreadnought = ('Redemptor Dreadnought', 105)
Reivers = ('Reivers', 18)
Repulsor = ('Repulsor', 185)
Rhino = ('Rhino', 70)
TDAChaplain = ('Chaplain in Terminator Armour', 90)
TDALibrarian = ('Librarian in Terminator Armour', 102)
TDAWatchCaptain = ('Watch Captain in Terminator Armour', 95)
Terminators = ('Terminators', 23)
VanguardVeterans = ('Vanguard Veterans', 17)
VenerableDreadnought = ('Venerable Dreadnought', 80)
Veterans = ('Veterans', 14)
WatchCaptain = ('Watch Captain', 74)
WatchCaptainArtemis = ('Watch Captain Artemis', 130)
WatchMaster = ('Watch Master', 130)
