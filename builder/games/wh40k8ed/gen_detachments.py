from rosters import *

from necrons import *
from adepta_sororitas import *
from craftworld import *
from chaos_other import *
from heretic_astartes import *
from drukhari import *
from aeldari_other import *
from chaos_demons import *
from imperium_other import *
from astra_militarum import *
from adeptus_mechanicum import *
from tau import *
from space_marines import *
from blood_angels import *
from dark_angels import *
from space_wolves import *
from deathwatch import *
from tyranids import *
from orks import *
from grey_knights import *
from genestealer_cult import *
from death_guard import *
from adeptus_custodes import *
from thousand_sons import *
from imperial_knights import *

detachments = []

#################################################################
# Code below is generated
#################################################################


class DetachAuxilary_canoptek(DetachAuxilary):
    army_name = u'Canoptek (Auxilary Support Detachment)'
    faction_base = u'CANOPTEK'
    alternate_factions = []
    army_id = u'Auxilary_canoptek'
    army_factions = [u'CANOPTEK']

    def __init__(self, parent=None):
        super(DetachAuxilary_canoptek, self).__init__(*[], **{u'heavy': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([Spyders])
        self.fast.add_classes([Scarabs, Wraiths])
        return None


class DetachPatrol__sept_(DetachPatrol):
    army_name = u'<Sept> (Patrol detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]
    army_id = u'patrol__sept_'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachPatrol__sept_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachBatallion__sept_(DetachBatallion):
    army_name = u'<Sept> (Batallion detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]
    army_id = u'batallion__sept_'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachBatallion__sept_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachBrigade__sept_(DetachBrigade):
    army_name = u'<Sept> (Brigade detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]
    army_id = u'brigade__sept_'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachBrigade__sept_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachVanguard__sept_(DetachVanguard):
    army_name = u'<Sept> (Vanguard detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]
    army_id = u'vanguard__sept_'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachVanguard__sept_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachSpearhead__sept_(DetachSpearhead):
    army_name = u'<Sept> (Spearhead detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]
    army_id = u'spearhead__sept_'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachSpearhead__sept_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachOutrider__sept_(DetachOutrider):
    army_name = u'<Sept> (Outrider detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]
    army_id = u'outrider__sept_'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachOutrider__sept_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachCommand__sept_(DetachCommand):
    army_name = u'<Sept> (Supreme command detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]
    army_id = u'command__sept_'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachCommand__sept_, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Stormsurge, TheEight])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.transports.add_classes([Devilfish])
        return None


class DetachSuperHeavy__sept_(DetachSuperHeavy):
    army_name = u'<Sept> (Super-Heavy detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]
    army_id = u'super_heavy__sept_'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachSuperHeavy__sept_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Stormsurge, TheEight])
        return None


class DetachSuperHeavyAux__sept_(DetachSuperHeavyAux):
    army_name = u'<Sept> (Super-Heavy auxilary detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]
    army_id = u'super_heavy_aux__sept_'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__sept_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Stormsurge, TheEight])
        return None


class DetachAirWing__sept_(DetachAirWing):
    army_name = u'<Sept> (Air Wing detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]
    army_id = u'air_wing__sept_'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachAirWing__sept_, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([RazorShark, SunShark])
        return None


class DetachFort__sept_(DetachFort):
    army_name = u'<Sept> (Fortification Network)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]
    army_id = u'fort__sept_'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]

    def __init__(self, parent=None):
        super(DetachFort__sept_, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([Shieldline, Droneport, Gunrig])
        return None


class DetachAuxilary__sept_(DetachAuxilary):
    army_name = u'<Sept> (Auxilary Support Detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]
    army_id = u'Auxilary__sept_'
    army_factions = [u'<SEPT>']

    def __init__(self, parent=None):
        super(DetachAuxilary__sept_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachVanguard_ravenwing(DetachVanguard):
    army_name = u'Ravenwing (Vanguard detachment)'
    faction_base = u'RAVENWING'
    alternate_factions = []
    army_id = u'vanguard_ravenwing'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'DARK ANGELS', u'RAVENWING', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachVanguard_ravenwing, self).__init__(*[], **{u'hq': True, u'elite': True, u'fast': True, u'fliers': True, u'parent': parent, })
        self.hq.add_classes([Sammael, SpeederSammael, RavenwingTalonmaster])
        self.elite.add_classes([RWAncient, RWApothecary, RWChampion])
        self.fast.add_classes([RavenwingBikeSquad, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, LandSpeederVengeance])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        return None


class DetachOutrider_ravenwing(DetachOutrider):
    army_name = u'Ravenwing (Outrider detachment)'
    faction_base = u'RAVENWING'
    alternate_factions = []
    army_id = u'outrider_ravenwing'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'DARK ANGELS', u'RAVENWING', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachOutrider_ravenwing, self).__init__(*[], **{u'hq': True, u'elite': True, u'fast': True, u'fliers': True, u'parent': parent, })
        self.hq.add_classes([Sammael, SpeederSammael, RavenwingTalonmaster])
        self.elite.add_classes([RWAncient, RWApothecary, RWChampion])
        self.fast.add_classes([RavenwingBikeSquad, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, LandSpeederVengeance])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        return None


class DetachCommand_ravenwing(DetachCommand):
    army_name = u'Ravenwing (Supreme command detachment)'
    faction_base = u'RAVENWING'
    alternate_factions = []
    army_id = u'command_ravenwing'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'DARK ANGELS', u'RAVENWING', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachCommand_ravenwing, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Sammael, SpeederSammael, RavenwingTalonmaster])
        self.elite.add_classes([RWAncient, RWApothecary, RWChampion])
        return None


class DetachAirWing_ravenwing(DetachAirWing):
    army_name = u'Ravenwing (Air Wing detachment)'
    faction_base = u'RAVENWING'
    alternate_factions = []
    army_id = u'air_wing_ravenwing'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'DARK ANGELS', u'RAVENWING', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_ravenwing, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        return None


class DetachAuxilary_ravenwing(DetachAuxilary):
    army_name = u'Ravenwing (Auxilary Support Detachment)'
    faction_base = u'RAVENWING'
    alternate_factions = []
    army_id = u'Auxilary_ravenwing'
    army_factions = [u'RAVENWING']

    def __init__(self, parent=None):
        super(DetachAuxilary_ravenwing, self).__init__(*[], **{u'hq': True, u'elite': True, u'fast': True, u'fliers': True, u'parent': parent, })
        self.hq.add_classes([Sammael, SpeederSammael, RavenwingTalonmaster])
        self.elite.add_classes([RWAncient, RWApothecary, RWChampion])
        self.fast.add_classes([RavenwingBikeSquad, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, LandSpeederVengeance])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        return None


class DetachPatrol_blood_angels(DetachPatrol):
    army_name = u'Blood Angels (Patrol detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'patrol_blood_angels'
    army_factions = [u'IMPERIUM', u'FLESH TEARERS', u'BLOOD ANGELS', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPatrol_blood_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachBatallion_blood_angels(DetachBatallion):
    army_name = u'Blood Angels (Batallion detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'batallion_blood_angels'
    army_factions = [u'IMPERIUM', u'FLESH TEARERS', u'BLOOD ANGELS', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachBatallion_blood_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachBrigade_blood_angels(DetachBrigade):
    army_name = u'Blood Angels (Brigade detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'brigade_blood_angels'
    army_factions = [u'IMPERIUM', u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachBrigade_blood_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachVanguard_blood_angels(DetachVanguard):
    army_name = u'Blood Angels (Vanguard detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'vanguard_blood_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'DEATH COMPANY', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachVanguard_blood_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachSpearhead_blood_angels(DetachSpearhead):
    army_name = u'Blood Angels (Spearhead detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'spearhead_blood_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSpearhead_blood_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachOutrider_blood_angels(DetachOutrider):
    army_name = u'Blood Angels (Outrider detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'outrider_blood_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachOutrider_blood_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachCommand_blood_angels(DetachCommand):
    army_name = u'Blood Angels (Supreme command detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'command_blood_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'DEATH COMPANY', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachCommand_blood_angels, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'lords': True, u'parent': parent, })
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavy_blood_angels(DetachSuperHeavy):
    army_name = u'Blood Angels (Super-Heavy detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'super_heavy_blood_angels'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'ULTRAMARINES', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', 'FLESH TEARERS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_blood_angels, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavyAux_blood_angels(DetachSuperHeavyAux):
    army_name = u'Blood Angels (Super-Heavy auxilary detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'super_heavy_aux_blood_angels'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'ULTRAMARINES', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', 'FLESH TEARERS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_blood_angels, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachAirWing_blood_angels(DetachAirWing):
    army_name = u'Blood Angels (Air Wing detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'air_wing_blood_angels'
    army_factions = [u'IMPERIUM', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachAirWing_blood_angels, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([StormravenGunship])
        return None


class DetachAuxilary_blood_angels(DetachAuxilary):
    army_name = u'Blood Angels (Auxilary Support Detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'Auxilary_blood_angels'
    army_factions = [u'BLOOD ANGELS']

    def __init__(self, parent=None):
        super(DetachAuxilary_blood_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPatrol_asuryani(DetachPatrol):
    army_name = u'Asuryani (Patrol detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'patrol_asuryani'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachPatrol_asuryani, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachBatallion_asuryani(DetachBatallion):
    army_name = u'Asuryani (Batallion detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'batallion_asuryani'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachBatallion_asuryani, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachBrigade_asuryani(DetachBrigade):
    army_name = u'Asuryani (Brigade detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'brigade_asuryani'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachBrigade_asuryani, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachVanguard_asuryani(DetachVanguard):
    army_name = u'Asuryani (Vanguard detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'vanguard_asuryani'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachVanguard_asuryani, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachSpearhead_asuryani(DetachSpearhead):
    army_name = u'Asuryani (Spearhead detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'spearhead_asuryani'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachSpearhead_asuryani, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachOutrider_asuryani(DetachOutrider):
    army_name = u'Asuryani (Outrider detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'outrider_asuryani'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachOutrider_asuryani, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachCommand_asuryani(DetachCommand):
    army_name = u'Asuryani (Supreme command detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'command_asuryani'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachCommand_asuryani, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachSuperHeavy_asuryani(DetachSuperHeavy):
    army_name = u'Asuryani (Super-Heavy detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'super_heavy_asuryani'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_asuryani, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachSuperHeavyAux_asuryani(DetachSuperHeavyAux):
    army_name = u'Asuryani (Super-Heavy auxilary detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'super_heavy_aux_asuryani'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_asuryani, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachAirWing_asuryani(DetachAirWing):
    army_name = u'Asuryani (Air Wing detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'air_wing_asuryani'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachAirWing_asuryani, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        return None


class DetachAuxilary_asuryani(DetachAuxilary):
    army_name = u'Asuryani (Auxilary Support Detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'Auxilary_asuryani'
    army_factions = [u'ASURYANI']

    def __init__(self, parent=None):
        super(DetachAuxilary_asuryani, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachPatrol_slaanesh(DetachPatrol):
    army_name = u'Slaanesh (Patrol detachment)'
    faction_base = u'SLAANESH'
    alternate_factions = []
    army_id = u'patrol_slaanesh'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'HERETIC ASTARTES', u'SLAANESH', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPatrol_slaanesh, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBatallion_slaanesh(DetachBatallion):
    army_name = u'Slaanesh (Batallion detachment)'
    faction_base = u'SLAANESH'
    alternate_factions = []
    army_id = u'batallion_slaanesh'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'HERETIC ASTARTES', u'SLAANESH', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachBatallion_slaanesh, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBrigade_slaanesh(DetachBrigade):
    army_name = u'Slaanesh (Brigade detachment)'
    faction_base = u'SLAANESH'
    alternate_factions = []
    army_id = u'brigade_slaanesh'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u"EMPEROR'S CHILDREN", u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'HERETIC ASTARTES', u'SLAANESH', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachBrigade_slaanesh, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachVanguard_slaanesh(DetachVanguard):
    army_name = u'Slaanesh (Vanguard detachment)'
    faction_base = u'SLAANESH'
    alternate_factions = []
    army_id = u'vanguard_slaanesh'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'HERETIC ASTARTES', u'SLAANESH', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachVanguard_slaanesh, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSpearhead_slaanesh(DetachSpearhead):
    army_name = u'Slaanesh (Spearhead detachment)'
    faction_base = u'SLAANESH'
    alternate_factions = []
    army_id = u'spearhead_slaanesh'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'HERETIC ASTARTES', u'SLAANESH', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachSpearhead_slaanesh, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachOutrider_slaanesh(DetachOutrider):
    army_name = u'Slaanesh (Outrider detachment)'
    faction_base = u'SLAANESH'
    alternate_factions = []
    army_id = u'outrider_slaanesh'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'HERETIC ASTARTES', u'SLAANESH', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachOutrider_slaanesh, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachCommand_slaanesh(DetachCommand):
    army_name = u'Slaanesh (Supreme command detachment)'
    faction_base = u'SLAANESH'
    alternate_factions = []
    army_id = u'command_slaanesh'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'SLAANESH', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachCommand_slaanesh, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        return None


class DetachAirWing_slaanesh(DetachAirWing):
    army_name = u'Slaanesh (Air Wing detachment)'
    faction_base = u'SLAANESH'
    alternate_factions = []
    army_id = u'air_wing_slaanesh'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'RED CORSAIRS', u'SLAANESH', u'WORD BEARERS', u'IRON WARRIORS', u'HERETIC ASTARTES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachAirWing_slaanesh, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary_slaanesh(DetachAuxilary):
    army_name = u'Slaanesh (Auxilary Support Detachment)'
    faction_base = u'SLAANESH'
    alternate_factions = []
    army_id = u'Auxilary_slaanesh'
    army_factions = [u'SLAANESH']

    def __init__(self, parent=None):
        super(DetachAuxilary_slaanesh, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachAuxilary_skitarii(DetachAuxilary):
    army_name = u'Skitarii (Auxilary Support Detachment)'
    faction_base = u'SKITARII'
    alternate_factions = []
    army_id = u'Auxilary_skitarii'
    army_factions = [u'SKITARII']

    def __init__(self, parent=None):
        super(DetachAuxilary_skitarii, self).__init__(*[], **{u'heavy': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([Dunecrawler])
        self.elite.add_classes([Infiltrators, Ruststalkers])
        self.troops.add_classes([SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachVanguard_deathwing(DetachVanguard):
    army_name = u'Deathwing (Vanguard detachment)'
    faction_base = u'DEATHWING'
    alternate_factions = []
    army_id = u'vanguard_deathwing'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'DARK ANGELS', u'DEATHWING']

    hq_sec = DWCommand
    elite_sec = DWElites
    heavy_sec = DWHeavy

    def __init__(self, parent=None):
        super(DetachVanguard_deathwing, self).__init__(*[], heavy=True, **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([BikeLibrarian, Librarian, TermoLibrarian, CompanyMaster, Azrael, Belial, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, DATermoLibrarian])
        self.elite.add_classes([VenDreadnought, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad])
        self.heavy.add_classes([])
        return None


class DetachSpearhead_deathwing(DetachSpearhead):
    army_name = u'Deathwing (Spearhead detachment)'
    faction_base = u'DEATHWING'
    alternate_factions = []
    army_id = u'spearhead_deathwing'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'DARK ANGELS', u'DEATHWING']

    hq_sec = DWCommand
    elite_sec = DWElites
    heavy_sec = DWHeavy

    def __init__(self, parent=None):
        super(DetachSpearhead_deathwing, self).__init__(*[], heavy=True, **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([BikeLibrarian, Librarian, TermoLibrarian, CompanyMaster, Azrael, Belial, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, DATermoLibrarian])
        self.elite.add_classes([VenDreadnought, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad])
        self.heavy.add_classes([])
        return None


class DetachCommand_deathwing(DetachCommand):
    army_name = u'Deathwing (Supreme command detachment)'
    faction_base = u'DEATHWING'
    alternate_factions = []
    army_id = u'command_deathwing'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'DARK ANGELS', u'DEATHWING']

    hq_sec = DWCommand
    elite_sec = DWElites

    def __init__(self, parent=None):
        super(DetachCommand_deathwing, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([BikeLibrarian, Librarian, TermoLibrarian, CompanyMaster, Azrael, Belial, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, DATermoLibrarian])
        self.elite.add_classes([VenDreadnought, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad])
        return None


class DetachAuxilary_deathwing(DetachAuxilary):
    army_name = u'Deathwing (Auxilary Support Detachment)'
    faction_base = u'DEATHWING'
    alternate_factions = []
    army_id = u'Auxilary_deathwing'
    army_factions = [u'DEATHWING']

    def __init__(self, parent=None):
        super(DetachAuxilary_deathwing, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([BikeLibrarian, Librarian, TermoLibrarian, CompanyMaster, Azrael, Belial, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, DATermoLibrarian])
        self.elite.add_classes([VenDreadnought, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad])
        return None


class DetachVanguard_astra_telepathica(DetachVanguard):
    army_name = u'Astra Telepathica (Vanguard detachment)'
    faction_base = u'ASTRA TELEPATHICA'
    alternate_factions = []
    army_id = u'vanguard_astra_telepathica'
    army_factions = [u'SCHOLASTICA PSYKANA', u'ASTRA TELEPATHICA', u'IMPERIUM', u'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachVanguard_astra_telepathica, self).__init__(*[], **{u'elite': True, u'hq': True, u'transports': True, u'parent': parent, })
        self.elite.add_classes([Prosecutors, Vigilators, Witchseekers, Wyrdvanes, Astropath])
        self.hq.add_classes([Primaris])
        self.transports.add_classes([NullRhino])
        return None


class DetachCommand_astra_telepathica(DetachCommand):
    army_name = u'Astra Telepathica (Supreme command detachment)'
    faction_base = u'ASTRA TELEPATHICA'
    alternate_factions = []
    army_id = u'command_astra_telepathica'
    army_factions = [u'SCHOLASTICA PSYKANA', u'ASTRA TELEPATHICA', u'IMPERIUM', u'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachCommand_astra_telepathica, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([NullRhino])
        self.hq.add_classes([Primaris])
        self.elite.add_classes([Prosecutors, Vigilators, Witchseekers, Wyrdvanes, Astropath])
        return None


class DetachAuxilary_astra_telepathica(DetachAuxilary):
    army_name = u'Astra Telepathica (Auxilary Support Detachment)'
    faction_base = u'ASTRA TELEPATHICA'
    alternate_factions = []
    army_id = u'Auxilary_astra_telepathica'
    army_factions = [u'ASTRA TELEPATHICA']

    def __init__(self, parent=None):
        super(DetachAuxilary_astra_telepathica, self).__init__(*[], **{u'elite': True, u'hq': True, u'transports': True, u'parent': parent, })
        self.elite.add_classes([Prosecutors, Vigilators, Witchseekers, Wyrdvanes, Astropath])
        self.hq.add_classes([Primaris])
        self.transports.add_classes([NullRhino])
        return None


class DetachVanguard_incubi(DetachVanguard):
    army_name = u'Incubi (Vanguard detachment)'
    faction_base = u'INCUBI'
    alternate_factions = []
    army_id = u'vanguard_incubi'
    army_factions = [u'INCUBI', u'DRUKHARI', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachVanguard_incubi, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Drazhar])
        self.elite.add_classes([Incubi])
        return None


class DetachCommand_incubi(DetachCommand):
    army_name = u'Incubi (Supreme command detachment)'
    faction_base = u'INCUBI'
    alternate_factions = []
    army_id = u'command_incubi'
    army_factions = [u'DRUKHARI', u'INCUBI', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachCommand_incubi, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Drazhar])
        self.elite.add_classes([Incubi])
        return None


class DetachAuxilary_incubi(DetachAuxilary):
    army_name = u'Incubi (Auxilary Support Detachment)'
    faction_base = u'INCUBI'
    alternate_factions = []
    army_id = u'Auxilary_incubi'
    army_factions = [u'INCUBI']

    def __init__(self, parent=None):
        super(DetachAuxilary_incubi, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Drazhar])
        self.elite.add_classes([Incubi])
        return None


class DetachPatrol_astra_militarum(DetachPatrol):
    army_name = u'Astra Militarum (Patrol detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'patrol_astra_militarum'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON', u'MILITARUM TEMPESTUS', u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachPatrol_astra_militarum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachBatallion_astra_militarum(DetachBatallion):
    army_name = u'Astra Militarum (Batallion detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'batallion_astra_militarum'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON', u'MILITARUM TEMPESTUS', u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachBatallion_astra_militarum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachBrigade_astra_militarum(DetachBrigade):
    army_name = u'Astra Militarum (Brigade detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'brigade_astra_militarum'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON', u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachBrigade_astra_militarum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachVanguard_astra_militarum(DetachVanguard):
    army_name = u'Astra Militarum (Vanguard detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'vanguard_astra_militarum'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'AGRIPINAA', u'MORDIAN', u'METALICA', u'TALLARN', u'ARMAGEDDON', u'GRAIA', u'ASTRA TELEPATHICA', u'CULT MECHANICUS', u'STYGIES VIII', u'ASTRA MILITARUM', u'LUCIUS', u'<REGIMENT>', u'VOSTROYAN', u'RYZA', u'CADIAN', u'SCHOLASTICA PSYKANA', u'<FORGE WORLD>', u'ADEPTUS MECHANICUS', u'CATACHAN', u'BLOOD BROTHERS', u'MARS', u'VALHALLAN', u'OFFICIO PREFECTUS', u'MILITARUM TEMPESTUS', u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachVanguard_astra_militarum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachSpearhead_astra_militarum(DetachSpearhead):
    army_name = u'Astra Militarum (Spearhead detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'spearhead_astra_militarum'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON', u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachSpearhead_astra_militarum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachOutrider_astra_militarum(DetachOutrider):
    army_name = u'Astra Militarum (Outrider detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'outrider_astra_militarum'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON', u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachOutrider_astra_militarum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachCommand_astra_militarum(DetachCommand):
    army_name = u'Astra Militarum (Supreme command detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'command_astra_militarum'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'AGRIPINAA', u'MORDIAN', u'METALICA', u'TALLARN', u'ARMAGEDDON', u'GRAIA', u'ASTRA TELEPATHICA', u'CULT MECHANICUS', u'STYGIES VIII', u'ASTRA MILITARUM', u'LUCIUS', u'<REGIMENT>', u'VOSTROYAN', u'RYZA', u'CADIAN', u'SCHOLASTICA PSYKANA', u'<FORGE WORLD>', u'ADEPTUS MECHANICUS', u'CATACHAN', u'BLOOD BROTHERS', u'MARS', u'VALHALLAN', u'OFFICIO PREFECTUS', u'MILITARUM TEMPESTUS', u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachCommand_astra_militarum, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachSuperHeavy_astra_militarum(DetachSuperHeavy):
    army_name = u'Astra Militarum (Super-Heavy detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'super_heavy_astra_militarum'
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON', u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_astra_militarum, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword])
        return None


class DetachSuperHeavyAux_astra_militarum(DetachSuperHeavyAux):
    army_name = u'Astra Militarum (Super-Heavy auxilary detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'super_heavy_aux_astra_militarum'
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON', u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_astra_militarum, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword])
        return None


class DetachAirWing_astra_militarum(DetachAirWing):
    army_name = u'Astra Militarum (Air Wing detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'air_wing_astra_militarum'
    army_factions = [u'AERONAUTICA IMPERIALIS', u'IMPERIUM', u'ASTRA MILITARUM', u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachAirWing_astra_militarum, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Valkyries])
        return None


class DetachAuxilary_astra_militarum(DetachAuxilary):
    army_name = u'Astra Militarum (Auxilary Support Detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'Auxilary_astra_militarum'
    army_factions = [u'ASTRA MILITARUM', u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachAuxilary_astra_militarum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachPatrol__regiment_(DetachPatrol):
    army_name = u'<Regiment> (Patrol detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'patrol__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPatrol__regiment_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachBatallion__regiment_(DetachBatallion):
    army_name = u'<Regiment> (Batallion detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'batallion__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachBatallion__regiment_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachBrigade__regiment_(DetachBrigade):
    army_name = u'<Regiment> (Brigade detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'brigade__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachBrigade__regiment_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachVanguard__regiment_(DetachVanguard):
    army_name = u'<Regiment> (Vanguard detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'vanguard__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachVanguard__regiment_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachSpearhead__regiment_(DetachSpearhead):
    army_name = u'<Regiment> (Spearhead detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'spearhead__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachSpearhead__regiment_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachOutrider__regiment_(DetachOutrider):
    army_name = u'<Regiment> (Outrider detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'outrider__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachOutrider__regiment_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachCommand__regiment_(DetachCommand):
    army_name = u'<Regiment> (Supreme command detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'command__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachCommand__regiment_, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        self.transports.add_classes([Chimera, Taurox])
        return None


class DetachSuperHeavy__regiment_(DetachSuperHeavy):
    army_name = u'<Regiment> (Super-Heavy detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'super_heavy__regiment_'
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__regiment_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword])
        return None


class DetachSuperHeavyAux__regiment_(DetachSuperHeavyAux):
    army_name = u'<Regiment> (Super-Heavy auxilary detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'super_heavy_aux__regiment_'
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__regiment_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword])
        return None


class DetachAuxilary__regiment_(DetachAuxilary):
    army_name = u'<Regiment> (Auxilary Support Detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'Auxilary__regiment_'
    army_factions = [u'<REGIMENT>']

    def __init__(self, parent=None):
        super(DetachAuxilary__regiment_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachAuxilary_legion_of_the_damned(DetachAuxilary):
    army_name = u'Legion Of The Damned (Auxilary Support Detachment)'
    faction_base = u'LEGION OF THE DAMNED'
    alternate_factions = []
    army_id = u'Auxilary_legion_of_the_damned'
    army_factions = [u'LEGION OF THE DAMNED']

    def __init__(self, parent=None):
        super(DetachAuxilary_legion_of_the_damned, self).__init__(*[], **{u'elite': True, u'parent': parent, })
        self.elite.add_classes([TheDamned])
        return None


class DetachPatrol__mascue_(DetachPatrol):
    army_name = u'<Mascue> (Patrol detachment)'
    faction_base = u'<MASCUE>'
    alternate_factions = []
    army_id = u'patrol__mascue_'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachPatrol__mascue_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachBatallion__mascue_(DetachBatallion):
    army_name = u'<Mascue> (Batallion detachment)'
    faction_base = u'<MASCUE>'
    alternate_factions = []
    army_id = u'batallion__mascue_'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachBatallion__mascue_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachBrigade__mascue_(DetachBrigade):
    army_name = u'<Mascue> (Brigade detachment)'
    faction_base = u'<MASCUE>'
    alternate_factions = []
    army_id = u'brigade__mascue_'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachBrigade__mascue_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachVanguard__mascue_(DetachVanguard):
    army_name = u'<Mascue> (Vanguard detachment)'
    faction_base = u'<MASCUE>'
    alternate_factions = []
    army_id = u'vanguard__mascue_'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachVanguard__mascue_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachSpearhead__mascue_(DetachSpearhead):
    army_name = u'<Mascue> (Spearhead detachment)'
    faction_base = u'<MASCUE>'
    alternate_factions = []
    army_id = u'spearhead__mascue_'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachSpearhead__mascue_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachOutrider__mascue_(DetachOutrider):
    army_name = u'<Mascue> (Outrider detachment)'
    faction_base = u'<MASCUE>'
    alternate_factions = []
    army_id = u'outrider__mascue_'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachOutrider__mascue_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachCommand__mascue_(DetachCommand):
    army_name = u'<Mascue> (Supreme command detachment)'
    faction_base = u'<MASCUE>'
    alternate_factions = []
    army_id = u'command__mascue_'
    army_factions = [u'<MASCUE>', u'YNNARI', u'HARLEQUINS', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachCommand__mascue_, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachAuxilary__mascue_(DetachAuxilary):
    army_name = u'<Mascue> (Auxilary Support Detachment)'
    faction_base = u'<MASCUE>'
    alternate_factions = []
    army_id = u'Auxilary__mascue_'
    army_factions = [u'<MASCUE>']

    def __init__(self, parent=None):
        super(DetachAuxilary__mascue_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachPatrol_deathwatch(DetachPatrol):
    army_name = u'Deathwatch (Patrol detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'patrol_deathwatch'
    army_factions = [u'DEATHWATCH', u'IMPERIUM', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPatrol_deathwatch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachBatallion_deathwatch(DetachBatallion):
    army_name = u'Deathwatch (Batallion detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'batallion_deathwatch'
    army_factions = [u'DEATHWATCH', u'IMPERIUM', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachBatallion_deathwatch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachBrigade_deathwatch(DetachBrigade):
    army_name = u'Deathwatch (Brigade detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'brigade_deathwatch'
    army_factions = [u'DEATHWATCH', u'IMPERIUM', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachBrigade_deathwatch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachVanguard_deathwatch(DetachVanguard):
    army_name = u'Deathwatch (Vanguard detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'vanguard_deathwatch'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachVanguard_deathwatch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachSpearhead_deathwatch(DetachSpearhead):
    army_name = u'Deathwatch (Spearhead detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'spearhead_deathwatch'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSpearhead_deathwatch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachOutrider_deathwatch(DetachOutrider):
    army_name = u'Deathwatch (Outrider detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'outrider_deathwatch'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachOutrider_deathwatch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachCommand_deathwatch(DetachCommand):
    army_name = u'Deathwatch (Supreme command detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'command_deathwatch'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachCommand_deathwatch, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'lords': True, u'parent': parent, })
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavy_deathwatch(DetachSuperHeavy):
    army_name = u'Deathwatch (Super-Heavy detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'super_heavy_deathwatch'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'DEATHWATCH']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_deathwatch, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavyAux_deathwatch(DetachSuperHeavyAux):
    army_name = u'Deathwatch (Super-Heavy auxilary detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'super_heavy_aux_deathwatch'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'DEATHWATCH']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_deathwatch, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachAirWing_deathwatch(DetachAirWing):
    army_name = u'Deathwatch (Air Wing detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'air_wing_deathwatch'
    army_factions = [u'DEATHWATCH', u'IMPERIUM', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_deathwatch, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Corvus])
        return None


class DetachAuxilary_deathwatch(DetachAuxilary):
    army_name = u'Deathwatch (Auxilary Support Detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'Auxilary_deathwatch'
    army_factions = [u'DEATHWATCH']

    def __init__(self, parent=None):
        super(DetachAuxilary_deathwatch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachVanguard_officio_prefectus(DetachVanguard):
    army_name = u'Officio Prefectus (Vanguard detachment)'
    faction_base = u'OFFICIO PREFECTUS'
    alternate_factions = []
    army_id = u'vanguard_officio_prefectus'
    army_factions = [u'IMPERIUM', u'OFFICIO PREFECTUS', u'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachVanguard_officio_prefectus, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([LordCommissar, Yarrik])
        self.elite.add_classes([Commissar])
        return None


class DetachCommand_officio_prefectus(DetachCommand):
    army_name = u'Officio Prefectus (Supreme command detachment)'
    faction_base = u'OFFICIO PREFECTUS'
    alternate_factions = []
    army_id = u'command_officio_prefectus'
    army_factions = [u'IMPERIUM', u'OFFICIO PREFECTUS', u'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachCommand_officio_prefectus, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([LordCommissar, Yarrik])
        self.elite.add_classes([Commissar])
        return None


class DetachAuxilary_officio_prefectus(DetachAuxilary):
    army_name = u'Officio Prefectus (Auxilary Support Detachment)'
    faction_base = u'OFFICIO PREFECTUS'
    alternate_factions = []
    army_id = u'Auxilary_officio_prefectus'
    army_factions = [u'OFFICIO PREFECTUS']

    def __init__(self, parent=None):
        super(DetachAuxilary_officio_prefectus, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([LordCommissar, Yarrik])
        self.elite.add_classes([Commissar])
        return None


class DetachPatrol_space_wolves(DetachPatrol):
    army_name = u'Space Wolves (Patrol detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'patrol_space_wolves'
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPatrol_space_wolves, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors])
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class DetachBatallion_space_wolves(DetachBatallion):
    army_name = u'Space Wolves (Batallion detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'batallion_space_wolves'
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachBatallion_space_wolves, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors])
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class DetachBrigade_space_wolves(DetachBrigade):
    army_name = u'Space Wolves (Brigade detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'brigade_space_wolves'
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachBrigade_space_wolves, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors])
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class DetachVanguard_space_wolves(DetachVanguard):
    army_name = u'Space Wolves (Vanguard detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'vanguard_space_wolves'
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachVanguard_space_wolves, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors])
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class DetachSpearhead_space_wolves(DetachSpearhead):
    army_name = u'Space Wolves (Spearhead detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'spearhead_space_wolves'
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachSpearhead_space_wolves, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors])
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class DetachOutrider_space_wolves(DetachOutrider):
    army_name = u'Space Wolves (Outrider detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'outrider_space_wolves'
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachOutrider_space_wolves, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors])
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class DetachCommand_space_wolves(DetachCommand):
    army_name = u'Space Wolves (Supreme command detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'command_space_wolves'
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachCommand_space_wolves, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'lords': True, u'parent': parent, })
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought, RelicContemptor, RelicDeredeo])
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavy_space_wolves(DetachSuperHeavy):
    army_name = u'Space Wolves (Super-Heavy detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'super_heavy_space_wolves'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'SPACE WOLVES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_space_wolves, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavyAux_space_wolves(DetachSuperHeavyAux):
    army_name = u'Space Wolves (Super-Heavy auxilary detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'super_heavy_aux_space_wolves'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'SPACE WOLVES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_space_wolves, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachAirWing_space_wolves(DetachAirWing):
    army_name = u'Space Wolves (Air Wing detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'air_wing_space_wolves'
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_space_wolves, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        return None


class DetachAuxilary_space_wolves(DetachAuxilary):
    army_name = u'Space Wolves (Auxilary Support Detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'Auxilary_space_wolves'
    army_factions = [u'SPACE WOLVES']

    def __init__(self, parent=None):
        super(DetachAuxilary_space_wolves, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors])
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class DetachPatrol_tzeentch(DetachPatrol):
    army_name = u'Tzeentch (Patrol detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'patrol_tzeentch'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'HERETIC ASTARTES', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPatrol_tzeentch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBatallion_tzeentch(DetachBatallion):
    army_name = u'Tzeentch (Batallion detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'batallion_tzeentch'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'HERETIC ASTARTES', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachBatallion_tzeentch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBrigade_tzeentch(DetachBrigade):
    army_name = u'Tzeentch (Brigade detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'brigade_tzeentch'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'HERETIC ASTARTES', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachBrigade_tzeentch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachVanguard_tzeentch(DetachVanguard):
    army_name = u'Tzeentch (Vanguard detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'vanguard_tzeentch'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'HERETIC ASTARTES', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachVanguard_tzeentch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSpearhead_tzeentch(DetachSpearhead):
    army_name = u'Tzeentch (Spearhead detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'spearhead_tzeentch'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'HERETIC ASTARTES', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachSpearhead_tzeentch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachOutrider_tzeentch(DetachOutrider):
    army_name = u'Tzeentch (Outrider detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'outrider_tzeentch'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'HERETIC ASTARTES', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachOutrider_tzeentch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachCommand_tzeentch(DetachCommand):
    army_name = u'Tzeentch (Supreme command detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'command_tzeentch'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'THOUSAND SONS', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachCommand_tzeentch, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Magnus])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavyAux_tzeentch(DetachSuperHeavyAux):
    army_name = u'Tzeentch (Super-Heavy auxilary detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'super_heavy_aux_tzeentch'
    army_factions = [u'THOUSAND SONS', u'TZEENTCH', u'CHAOS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_tzeentch, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Magnus])
        return None


class DetachAirWing_tzeentch(DetachAirWing):
    army_name = u'Tzeentch (Air Wing detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'air_wing_tzeentch'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'RED CORSAIRS', u'THOUSAND SONS', u'WORD BEARERS', u'IRON WARRIORS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_tzeentch, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary_tzeentch(DetachAuxilary):
    army_name = u'Tzeentch (Auxilary Support Detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'Auxilary_tzeentch'
    army_factions = [u'TZEENTCH']

    def __init__(self, parent=None):
        super(DetachAuxilary_tzeentch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPatrol_drukhari(DetachPatrol):
    army_name = u'Drukhari (Patrol detachment)'
    faction_base = u'DRUKHARI'
    alternate_factions = []
    army_id = u'patrol_drukhari'
    army_factions = [u'DRUKHARI', u'AELDARI', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'YNNARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'<HAEMUNCULUS COVEN>', u'<WYCH CULT>']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachPatrol_drukhari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachBatallion_drukhari(DetachBatallion):
    army_name = u'Drukhari (Batallion detachment)'
    faction_base = u'DRUKHARI'
    alternate_factions = []
    army_id = u'batallion_drukhari'
    army_factions = [u'DRUKHARI', u'AELDARI', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'YNNARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'<HAEMUNCULUS COVEN>', u'<WYCH CULT>']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachBatallion_drukhari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachBrigade_drukhari(DetachBrigade):
    army_name = u'Drukhari (Brigade detachment)'
    faction_base = u'DRUKHARI'
    alternate_factions = []
    army_id = u'brigade_drukhari'
    army_factions = [u'YNNARI', u'DRUKHARI', u'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachBrigade_drukhari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachVanguard_drukhari(DetachVanguard):
    army_name = u'Drukhari (Vanguard detachment)'
    faction_base = u'DRUKHARI'
    alternate_factions = []
    army_id = u'vanguard_drukhari'
    army_factions = [u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'<WYCH CULT>', u'AELDARI', u'INCUBI', u'YNNARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'<HAEMUNCULUS COVEN>', u'DRUKHARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachVanguard_drukhari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachSpearhead_drukhari(DetachSpearhead):
    army_name = u'Drukhari (Spearhead detachment)'
    faction_base = u'DRUKHARI'
    alternate_factions = []
    army_id = u'spearhead_drukhari'
    army_factions = [u'AELDARI', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'YNNARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'<HAEMUNCULUS COVEN>', u'DRUKHARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachSpearhead_drukhari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachOutrider_drukhari(DetachOutrider):
    army_name = u'Drukhari (Outrider detachment)'
    faction_base = u'DRUKHARI'
    alternate_factions = []
    army_id = u'outrider_drukhari'
    army_factions = [u'<WYCH CULT>', u'DRUKHARI', u'YNNARI', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachOutrider_drukhari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachCommand_drukhari(DetachCommand):
    army_name = u'Drukhari (Supreme command detachment)'
    faction_base = u'DRUKHARI'
    alternate_factions = []
    army_id = u'command_drukhari'
    army_factions = [u'AELDARI', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'INCUBI', u'<WYCH CULT>', u'YNNARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'<HAEMUNCULUS COVEN>', u'DRUKHARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachCommand_drukhari, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        return None


class DetachAirWing_drukhari(DetachAirWing):
    army_name = u'Drukhari (Air Wing detachment)'
    faction_base = u'DRUKHARI'
    alternate_factions = []
    army_id = u'air_wing_drukhari'
    army_factions = [u'AELDARI', u'DRUKHARI', u'<WYCH CULT>', u'YNNARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'WYCH CULT OF STRIFE']

    def __init__(self, parent=None):
        super(DetachAirWing_drukhari, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Razorwing, Voidraven])
        return None


class DetachAuxilary_drukhari(DetachAuxilary):
    army_name = u'Drukhari (Auxilary Support Detachment)'
    faction_base = u'DRUKHARI'
    alternate_factions = []
    army_id = u'Auxilary_drukhari'
    army_factions = [u'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachAuxilary_drukhari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachPatrol_heretic_astartes(DetachPatrol):
    army_name = u'Heretic Astartes (Patrol detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'patrol_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPatrol_heretic_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBatallion_heretic_astartes(DetachBatallion):
    army_name = u'Heretic Astartes (Batallion detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'batallion_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachBatallion_heretic_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBrigade_heretic_astartes(DetachBrigade):
    army_name = u'Heretic Astartes (Brigade detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'brigade_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachBrigade_heretic_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachVanguard_heretic_astartes(DetachVanguard):
    army_name = u'Heretic Astartes (Vanguard detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'vanguard_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachVanguard_heretic_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSpearhead_heretic_astartes(DetachSpearhead):
    army_name = u'Heretic Astartes (Spearhead detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'spearhead_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachSpearhead_heretic_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachOutrider_heretic_astartes(DetachOutrider):
    army_name = u'Heretic Astartes (Outrider detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'outrider_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'KHORNE', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachOutrider_heretic_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachCommand_heretic_astartes(DetachCommand):
    army_name = u'Heretic Astartes (Supreme command detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'command_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'SLAANESH', u'THOUSAND SONS', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachCommand_heretic_astartes, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([LordOfSkulls, Mortarion, Magnus])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavy_heretic_astartes(DetachSuperHeavy):
    army_name = u'Heretic Astartes (Super-Heavy detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'super_heavy_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_heretic_astartes, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([LordOfSkulls, Mortarion, Magnus])
        return None


class DetachSuperHeavyAux_heretic_astartes(DetachSuperHeavyAux):
    army_name = u'Heretic Astartes (Super-Heavy auxilary detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'super_heavy_aux_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_heretic_astartes, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([LordOfSkulls, Mortarion, Magnus])
        return None


class DetachAirWing_heretic_astartes(DetachAirWing):
    army_name = u'Heretic Astartes (Air Wing detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'air_wing_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'RED CORSAIRS', u'SLAANESH', u'THOUSAND SONS', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'KHORNE', u'HERETIC ASTARTES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachAirWing_heretic_astartes, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary_heretic_astartes(DetachAuxilary):
    army_name = u'Heretic Astartes (Auxilary Support Detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'Auxilary_heretic_astartes'
    army_factions = [u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachAuxilary_heretic_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPatrol_thousand_sons(DetachPatrol):
    army_name = u'Thousand Sons (Patrol detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'patrol_thousand_sons'
    army_factions = [u'CHAOS', u'TZEENTCH', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPatrol_thousand_sons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBatallion_thousand_sons(DetachBatallion):
    army_name = u'Thousand Sons (Batallion detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'batallion_thousand_sons'
    army_factions = [u'CHAOS', u'TZEENTCH', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachBatallion_thousand_sons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBrigade_thousand_sons(DetachBrigade):
    army_name = u'Thousand Sons (Brigade detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'brigade_thousand_sons'
    army_factions = [u'CHAOS', u'TZEENTCH', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachBrigade_thousand_sons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachVanguard_thousand_sons(DetachVanguard):
    army_name = u'Thousand Sons (Vanguard detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'vanguard_thousand_sons'
    army_factions = [u'CHAOS', u'TZEENTCH', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachVanguard_thousand_sons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSpearhead_thousand_sons(DetachSpearhead):
    army_name = u'Thousand Sons (Spearhead detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'spearhead_thousand_sons'
    army_factions = [u'CHAOS', u'TZEENTCH', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSpearhead_thousand_sons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachOutrider_thousand_sons(DetachOutrider):
    army_name = u'Thousand Sons (Outrider detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'outrider_thousand_sons'
    army_factions = [u'CHAOS', u'TZEENTCH', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachOutrider_thousand_sons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachCommand_thousand_sons(DetachCommand):
    army_name = u'Thousand Sons (Supreme command detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'command_thousand_sons'
    army_factions = [u'CHAOS', u'TZEENTCH', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachCommand_thousand_sons, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Magnus])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavy_thousand_sons(DetachSuperHeavy):
    army_name = u'Thousand Sons (Super-Heavy detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'super_heavy_thousand_sons'
    army_factions = [u'THOUSAND SONS', u'TZEENTCH', u'CHAOS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_thousand_sons, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Magnus])
        return None


class DetachSuperHeavyAux_thousand_sons(DetachSuperHeavyAux):
    army_name = u'Thousand Sons (Super-Heavy auxilary detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'super_heavy_aux_thousand_sons'
    army_factions = [u'THOUSAND SONS', u'TZEENTCH', u'CHAOS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_thousand_sons, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Magnus])
        return None


class DetachAirWing_thousand_sons(DetachAirWing):
    army_name = u'Thousand Sons (Air Wing detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'air_wing_thousand_sons'
    army_factions = [u'CHAOS', u'TZEENTCH', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachAirWing_thousand_sons, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary_thousand_sons(DetachAuxilary):
    army_name = u'Thousand Sons (Auxilary Support Detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'Auxilary_thousand_sons'
    army_factions = [u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachAuxilary_thousand_sons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPatrol__legion_(DetachPatrol):
    army_name = u'<Legion> (Patrol detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'patrol__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPatrol__legion_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionRubrics, Tzaangors, LegionPlagues, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBatallion__legion_(DetachBatallion):
    army_name = u'<Legion> (Batallion detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'batallion__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachBatallion__legion_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionRubrics, Tzaangors, LegionPlagues, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBrigade__legion_(DetachBrigade):
    army_name = u'<Legion> (Brigade detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'brigade__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachBrigade__legion_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionRubrics, Tzaangors, LegionPlagues, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachVanguard__legion_(DetachVanguard):
    army_name = u'<Legion> (Vanguard detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'vanguard__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachVanguard__legion_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionRubrics, Tzaangors, LegionPlagues, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSpearhead__legion_(DetachSpearhead):
    army_name = u'<Legion> (Spearhead detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'spearhead__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachSpearhead__legion_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionRubrics, Tzaangors, LegionPlagues, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachOutrider__legion_(DetachOutrider):
    army_name = u'<Legion> (Outrider detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'outrider__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'RED CORSAIRS', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'KHORNE', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachOutrider__legion_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionRubrics, Tzaangors, LegionPlagues, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachCommand__legion_(DetachCommand):
    army_name = u'<Legion> (Supreme command detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'command__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'KHORNE', u'SLAANESH', u'THOUSAND SONS', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachCommand__legion_, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([LordOfSkulls, Magnus])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavy__legion_(DetachSuperHeavy):
    army_name = u'<Legion> (Super-Heavy detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'super_heavy__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'KHORNE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__legion_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([LordOfSkulls, Magnus])
        return None


class DetachSuperHeavyAux__legion_(DetachSuperHeavyAux):
    army_name = u'<Legion> (Super-Heavy auxilary detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'super_heavy_aux__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'KHORNE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__legion_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([LordOfSkulls, Magnus])
        return None


class DetachAirWing__legion_(DetachAirWing):
    army_name = u'<Legion> (Air Wing detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'air_wing__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'RED CORSAIRS', u'SLAANESH', u'THOUSAND SONS', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'KHORNE', u'HERETIC ASTARTES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachAirWing__legion_, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary__legion_(DetachAuxilary):
    army_name = u'<Legion> (Auxilary Support Detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'Auxilary__legion_'
    army_factions = [u'<LEGION>']

    def __init__(self, parent=None):
        super(DetachAuxilary__legion_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionRubrics, Tzaangors, LegionPlagues, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPatrol_dark_angels(DetachPatrol):
    army_name = u'Dark Angels (Patrol detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'patrol_dark_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPatrol_dark_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, CodexVindicator, Whirlwind, CodexWhirlwind, CodexDevastators, CodexPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachBatallion_dark_angels(DetachBatallion):
    army_name = u'Dark Angels (Batallion detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'batallion_dark_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachBatallion_dark_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, CodexVindicator, Whirlwind, CodexWhirlwind, CodexDevastators, CodexPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachBrigade_dark_angels(DetachBrigade):
    army_name = u'Dark Angels (Brigade detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'brigade_dark_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachBrigade_dark_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, CodexVindicator, Whirlwind, CodexWhirlwind, CodexDevastators, CodexPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachVanguard_dark_angels(DetachVanguard):
    army_name = u'Dark Angels (Vanguard detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'vanguard_dark_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'RAVENWING', u'IMPERIAL FISTS', u'DEATHWING', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachVanguard_dark_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, CodexVindicator, Whirlwind, CodexWhirlwind, CodexDevastators, CodexPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachSpearhead_dark_angels(DetachSpearhead):
    army_name = u'Dark Angels (Spearhead detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'spearhead_dark_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSpearhead_dark_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, CodexVindicator, Whirlwind, CodexWhirlwind, CodexDevastators, CodexPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachOutrider_dark_angels(DetachOutrider):
    army_name = u'Dark Angels (Outrider detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'outrider_dark_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'RAVENWING', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachOutrider_dark_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, CodexVindicator, Whirlwind, CodexWhirlwind, CodexDevastators, CodexPredator, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachCommand_dark_angels(DetachCommand):
    army_name = u'Dark Angels (Supreme command detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'command_dark_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'RAVENWING', u'IMPERIAL FISTS', u'DEATHWING', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachCommand_dark_angels, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'lords': True, u'parent': parent, })
        self.transports.add_classes([Razorback, CodexRazorback, Rhino, DropPod, CodexDropPod, Repulsor])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, RelicContemptor, RelicDeredeo])
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavy_dark_angels(DetachSuperHeavy):
    army_name = u'Dark Angels (Super-Heavy detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'super_heavy_dark_angels'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'<DARK ANGELS SUCCESSORS>', u'DARK ANGELS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_dark_angels, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavyAux_dark_angels(DetachSuperHeavyAux):
    army_name = u'Dark Angels (Super-Heavy auxilary detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'super_heavy_aux_dark_angels'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'<DARK ANGELS SUCCESSORS>', u'DARK ANGELS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_dark_angels, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachAirWing_dark_angels(DetachAirWing):
    army_name = u'Dark Angels (Air Wing detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'air_wing_dark_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'DARK ANGELS', u'RAVENWING', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_dark_angels, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        return None


class DetachAuxilary_dark_angels(DetachAuxilary):
    army_name = u'Dark Angels (Auxilary Support Detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'Auxilary_dark_angels'
    army_factions = [u'DARK ANGELS']

    def __init__(self, parent=None):
        super(DetachAuxilary_dark_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, CodexVindicator, Whirlwind, CodexWhirlwind, CodexDevastators, CodexPredator])
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPatrol_khorne(DetachPatrol):
    army_name = u'Khorne (Patrol detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'patrol_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'WORLD EATERS', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPatrol_khorne, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBatallion_khorne(DetachBatallion):
    army_name = u'Khorne (Batallion detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'batallion_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'WORLD EATERS', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachBatallion_khorne, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBrigade_khorne(DetachBrigade):
    army_name = u'Khorne (Brigade detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'brigade_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'WORLD EATERS', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachBrigade_khorne, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachVanguard_khorne(DetachVanguard):
    army_name = u'Khorne (Vanguard detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'vanguard_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'WORLD EATERS', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachVanguard_khorne, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSpearhead_khorne(DetachSpearhead):
    army_name = u'Khorne (Spearhead detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'spearhead_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'WORLD EATERS', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachSpearhead_khorne, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachOutrider_khorne(DetachOutrider):
    army_name = u'Khorne (Outrider detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'outrider_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'WORLD EATERS', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachOutrider_khorne, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachCommand_khorne(DetachCommand):
    army_name = u'Khorne (Supreme command detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'command_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'KHORNE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachCommand_khorne, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([LordOfSkulls])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavy_khorne(DetachSuperHeavy):
    army_name = u'Khorne (Super-Heavy detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'super_heavy_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'KHORNE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_khorne, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([LordOfSkulls])
        return None


class DetachSuperHeavyAux_khorne(DetachSuperHeavyAux):
    army_name = u'Khorne (Super-Heavy auxilary detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'super_heavy_aux_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'KHORNE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_khorne, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([LordOfSkulls])
        return None


class DetachAirWing_khorne(DetachAirWing):
    army_name = u'Khorne (Air Wing detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'air_wing_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'RED CORSAIRS', u'WORD BEARERS', u'IRON WARRIORS', u'KHORNE', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_khorne, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary_khorne(DetachAuxilary):
    army_name = u'Khorne (Auxilary Support Detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'Auxilary_khorne'
    army_factions = [u'KHORNE']

    def __init__(self, parent=None):
        super(DetachAuxilary_khorne, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavy_questor_traitoris(KnightSuperHeavy):
    army_name = u'Questor Traitoris (Super-Heavy detachment)'
    faction_base = u'QUESTOR TRAITORIS'
    alternate_factions = []
    army_id = u'super_heavy_questor_traitoris'
    army_factions = [u'QUESTOR TRAITORIS', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_questor_traitoris, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RenegadeKnight, RenegadeDominus, RenegadeArmigers])
        return None


class DetachSuperHeavyAux_questor_traitoris(DetachSuperHeavyAux):
    army_name = u'Questor Traitoris (Super-Heavy auxilary detachment)'
    faction_base = u'QUESTOR TRAITORIS'
    alternate_factions = []
    army_id = u'super_heavy_aux_questor_traitoris'
    army_factions = [u'QUESTOR TRAITORIS', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_questor_traitoris, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RenegadeKnight, RenegadeDominus, RenegadeArmigers])
        return None


class DetachVanguard_scholastica_psykana(DetachVanguard):
    army_name = u'Scholastica Psykana (Vanguard detachment)'
    faction_base = u'SCHOLASTICA PSYKANA'
    alternate_factions = []
    army_id = u'vanguard_scholastica_psykana'
    army_factions = [u'SCHOLASTICA PSYKANA', u'ASTRA TELEPATHICA', u'IMPERIUM', u'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachVanguard_scholastica_psykana, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Primaris])
        self.elite.add_classes([Wyrdvanes, Astropath])
        return None


class DetachCommand_scholastica_psykana(DetachCommand):
    army_name = u'Scholastica Psykana (Supreme command detachment)'
    faction_base = u'SCHOLASTICA PSYKANA'
    alternate_factions = []
    army_id = u'command_scholastica_psykana'
    army_factions = [u'SCHOLASTICA PSYKANA', u'ASTRA TELEPATHICA', u'IMPERIUM', u'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachCommand_scholastica_psykana, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Primaris])
        self.elite.add_classes([Wyrdvanes, Astropath])
        return None


class DetachAuxilary_scholastica_psykana(DetachAuxilary):
    army_name = u'Scholastica Psykana (Auxilary Support Detachment)'
    faction_base = u'SCHOLASTICA PSYKANA'
    alternate_factions = []
    army_id = u'Auxilary_scholastica_psykana'
    army_factions = [u'SCHOLASTICA PSYKANA']

    def __init__(self, parent=None):
        super(DetachAuxilary_scholastica_psykana, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Primaris])
        self.elite.add_classes([Wyrdvanes, Astropath])
        return None


class DetachPatrol__forge_world_(DetachPatrol):
    army_name = u'<Forge World> (Patrol detachment)'
    faction_base = u'<FORGE WORLD>'
    alternate_factions = [u'MARS', u'GRAIA', u'METALICA', u'LUCIUS', u'AGRIPINAA', u'STYGIES VIII', u'RYZA']
    army_id = u'patrol__forge_world_'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPatrol__forge_world_, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachBatallion__forge_world_(DetachBatallion):
    army_name = u'<Forge World> (Batallion detachment)'
    faction_base = u'<FORGE WORLD>'
    alternate_factions = [u'MARS', u'GRAIA', u'METALICA', u'LUCIUS', u'AGRIPINAA', u'STYGIES VIII', u'RYZA']
    army_id = u'batallion__forge_world_'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachBatallion__forge_world_, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachBrigade__forge_world_(DetachBrigade):
    army_name = u'<Forge World> (Brigade detachment)'
    faction_base = u'<FORGE WORLD>'
    alternate_factions = [u'MARS', u'GRAIA', u'METALICA', u'LUCIUS', u'AGRIPINAA', u'STYGIES VIII', u'RYZA']
    army_id = u'brigade__forge_world_'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachBrigade__forge_world_, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachVanguard__forge_world_(DetachVanguard):
    army_name = u'<Forge World> (Vanguard detachment)'
    faction_base = u'<FORGE WORLD>'
    alternate_factions = [u'MARS', u'GRAIA', u'METALICA', u'LUCIUS', u'AGRIPINAA', u'STYGIES VIII', u'RYZA']
    army_id = u'vanguard__forge_world_'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'ASTRA MILITARUM', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachVanguard__forge_world_, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachSpearhead__forge_world_(DetachSpearhead):
    army_name = u'<Forge World> (Spearhead detachment)'
    faction_base = u'<FORGE WORLD>'
    alternate_factions = [u'MARS', u'GRAIA', u'METALICA', u'LUCIUS', u'AGRIPINAA', u'STYGIES VIII', u'RYZA']
    army_id = u'spearhead__forge_world_'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachSpearhead__forge_world_, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachOutrider__forge_world_(DetachOutrider):
    army_name = u'<Forge World> (Outrider detachment)'
    faction_base = u'<FORGE WORLD>'
    alternate_factions = [u'MARS', u'GRAIA', u'METALICA', u'LUCIUS', u'AGRIPINAA', u'STYGIES VIII', u'RYZA']
    army_id = u'outrider__forge_world_'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachOutrider__forge_world_, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachCommand__forge_world_(DetachCommand):
    army_name = u'<Forge World> (Supreme command detachment)'
    faction_base = u'<FORGE WORLD>'
    alternate_factions = [u'MARS', u'GRAIA', u'METALICA', u'LUCIUS', u'AGRIPINAA', u'STYGIES VIII', u'RYZA']
    army_id = u'command__forge_world_'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'ASTRA MILITARUM', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachCommand__forge_world_, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        return None


class DetachAuxilary__forge_world_(DetachAuxilary):
    army_name = u'<Forge World> (Auxilary Support Detachment)'
    faction_base = u'<FORGE WORLD>'
    alternate_factions = [u'MARS', u'GRAIA', u'METALICA', u'LUCIUS', u'AGRIPINAA', u'STYGIES VIII', u'RYZA']
    army_id = u'Auxilary__forge_world_'
    army_factions = [u'<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachAuxilary__forge_world_, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachPatrol_necrons(DetachPatrol):
    army_name = u'Necrons (Patrol detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'patrol_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'MAYNARKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachPatrol_necrons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachBatallion_necrons(DetachBatallion):
    army_name = u'Necrons (Batallion detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'batallion_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'MAYNARKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachBatallion_necrons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachBrigade_necrons(DetachBrigade):
    army_name = u'Necrons (Brigade detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'brigade_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'MAYNARKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachBrigade_necrons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachVanguard_necrons(DetachVanguard):
    army_name = u'Necrons (Vanguard detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'vanguard_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'MAYNARKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachVanguard_necrons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombSentinel])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachSpearhead_necrons(DetachSpearhead):
    army_name = u'Necrons (Spearhead detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'spearhead_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'MAYNARKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachSpearhead_necrons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachOutrider_necrons(DetachOutrider):
    army_name = u'Necrons (Outrider detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'outrider_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'MAYNARKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachOutrider_necrons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachCommand_necrons(DetachCommand):
    army_name = u'Necrons (Supreme command detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'command_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'SAUTEKH', u'MAYNARKH', u'<DYNASTY>', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachCommand_necrons, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk, GaussPylon])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.transports.add_classes([GhostArk])
        return None


class DetachSuperHeavy_necrons(DetachSuperHeavy):
    army_name = u'Necrons (Super-Heavy detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'super_heavy_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'SAUTEKH', u'MAYNARKH', u'<DYNASTY>', u'NECRONS', u"C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavy_necrons, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk, GaussPylon])
        return None


class DetachSuperHeavyAux_necrons(DetachSuperHeavyAux):
    army_name = u'Necrons (Super-Heavy auxilary detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'super_heavy_aux_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'SAUTEKH', u'MAYNARKH', u'<DYNASTY>', u'NECRONS', u"C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_necrons, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk, GaussPylon])
        return None


class DetachAirWing_necrons(DetachAirWing):
    army_name = u'Necrons (Air Wing detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'air_wing_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'SAUTEKH', u'MAYNARKH', u'<DYNASTY>', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachAirWing_necrons, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        return None


class DetachAuxilary_necrons(DetachAuxilary):
    army_name = u'Necrons (Auxilary Support Detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'Auxilary_necrons'
    army_factions = [u'NECRONS']

    def __init__(self, parent=None):
        super(DetachAuxilary_necrons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, ZahndrekhKutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachVanguard_fallen(DetachVanguard):
    army_name = u'Fallen (Vanguard detachment)'
    faction_base = u'FALLEN'
    alternate_factions = []
    army_id = u'vanguard_fallen'
    army_factions = [u'IMPERIUM', u'FALLEN', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachVanguard_fallen, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Cypher])
        self.elite.add_classes([Fallen])
        return None


class DetachCommand_fallen(DetachCommand):
    army_name = u'Fallen (Supreme command detachment)'
    faction_base = u'FALLEN'
    alternate_factions = []
    army_id = u'command_fallen'
    army_factions = [u'IMPERIUM', u'FALLEN', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachCommand_fallen, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Cypher])
        self.elite.add_classes([Fallen])
        return None


class DetachAuxilary_fallen(DetachAuxilary):
    army_name = u'Fallen (Auxilary Support Detachment)'
    faction_base = u'FALLEN'
    alternate_factions = []
    army_id = u'Auxilary_fallen'
    army_factions = [u'FALLEN']

    def __init__(self, parent=None):
        super(DetachAuxilary_fallen, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Cypher])
        self.elite.add_classes([Fallen])
        return None


class DetachAuxilary_kroot(DetachAuxilary):
    army_name = u'Kroot (Auxilary Support Detachment)'
    faction_base = u'KROOT'
    alternate_factions = []
    army_id = u'Auxilary_kroot'
    army_factions = [u'KROOT']

    def __init__(self, parent=None):
        super(DetachAuxilary_kroot, self).__init__(*[], **{u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.elite.add_classes([Shaper, KrootoxSquad])
        self.troops.add_classes([KrootSquad])
        self.fast.add_classes([KroothoundSquad])
        return None


class DetachPatrol_adeptus_ministorum(DetachPatrol):
    army_name = u'Adeptus Ministorum (Patrol detachment)'
    faction_base = u'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = u'patrol_adeptus_ministorum'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachPatrol_adeptus_ministorum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachBatallion_adeptus_ministorum(DetachBatallion):
    army_name = u'Adeptus Ministorum (Batallion detachment)'
    faction_base = u'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = u'batallion_adeptus_ministorum'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachBatallion_adeptus_ministorum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachBrigade_adeptus_ministorum(DetachBrigade):
    army_name = u'Adeptus Ministorum (Brigade detachment)'
    faction_base = u'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = u'brigade_adeptus_ministorum'
    army_factions = [u'IMPERIUM', u'ADEPTUS MINISTORUM', u'ADEPTA SORORITAS', u'<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachBrigade_adeptus_ministorum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachVanguard_adeptus_ministorum(DetachVanguard):
    army_name = u'Adeptus Ministorum (Vanguard detachment)'
    faction_base = u'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = u'vanguard_adeptus_ministorum'
    army_factions = [u'IMPERIUM', u'ADEPTUS MINISTORUM', u'ADEPTA SORORITAS', u'ASTRA MILITARUM', u'<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachVanguard_adeptus_ministorum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachSpearhead_adeptus_ministorum(DetachSpearhead):
    army_name = u'Adeptus Ministorum (Spearhead detachment)'
    faction_base = u'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = u'spearhead_adeptus_ministorum'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachSpearhead_adeptus_ministorum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachOutrider_adeptus_ministorum(DetachOutrider):
    army_name = u'Adeptus Ministorum (Outrider detachment)'
    faction_base = u'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = u'outrider_adeptus_ministorum'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachOutrider_adeptus_ministorum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachCommand_adeptus_ministorum(DetachCommand):
    army_name = u'Adeptus Ministorum (Supreme command detachment)'
    faction_base = u'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = u'command_adeptus_ministorum'
    army_factions = [u'IMPERIUM', u'ASTRA MILITARUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachCommand_adeptus_ministorum, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachAuxilary_adeptus_ministorum(DetachAuxilary):
    army_name = u'Adeptus Ministorum (Auxilary Support Detachment)'
    faction_base = u'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = u'Auxilary_adeptus_ministorum'
    army_factions = [u'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachAuxilary_adeptus_ministorum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachPatrol_imperium(DetachPatrol):
    army_name = u'Imperium (Patrol detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'patrol_imperium'
    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'BLACK TEMPLARS', u'AGRIPINAA', u'SALAMANDERS', u'MORDIAN', u'METALICA', u'CRIMSON FISTS', u'TALLARN', u'ARMAGEDDON', u'GRAIA', u'CULT MECHANICUS', u'BLOOD ANGELS', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'STYGIES VIII', u'DARK ANGELS', u'ASTRA MILITARUM', u'LUCIUS', u'<REGIMENT>', u'FLESH TEARERS', u'VOSTROYAN', u'IMPERIAL FISTS', u'ADEPTA SORORITAS', u'RYZA', u'<DARK ANGELS SUCCESSORS>', u'<ORDER>', u'CADIAN', u'MARS', u'RAVEN GUARD', u'ADEPTUS CUSTODES', u'<CHAPTER>', u'<FORGE WORLD>', u'GREY KNIGHTS', u'ADEPTUS MECHANICUS', u'CATACHAN', u'BLOOD BROTHERS', u'DEATHWATCH', u'VALHALLAN', u'WHITE SCARS', u'ADEPTUS MINISTORUM', u'MILITARUM TEMPESTUS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPatrol_imperium, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachBatallion_imperium(DetachBatallion):
    army_name = u'Imperium (Batallion detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'batallion_imperium'
    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'BLACK TEMPLARS', u'AGRIPINAA', u'SALAMANDERS', u'MORDIAN', u'METALICA', u'CRIMSON FISTS', u'TALLARN', u'ARMAGEDDON', u'GRAIA', u'CULT MECHANICUS', u'BLOOD ANGELS', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'STYGIES VIII', u'DARK ANGELS', u'ASTRA MILITARUM', u'LUCIUS', u'<REGIMENT>', u'FLESH TEARERS', u'VOSTROYAN', u'IMPERIAL FISTS', u'ADEPTA SORORITAS', u'RYZA', u'<DARK ANGELS SUCCESSORS>', u'<ORDER>', u'CADIAN', u'MARS', u'RAVEN GUARD', u'ADEPTUS CUSTODES', u'<CHAPTER>', u'<FORGE WORLD>', u'GREY KNIGHTS', u'ADEPTUS MECHANICUS', u'CATACHAN', u'BLOOD BROTHERS', u'DEATHWATCH', u'VALHALLAN', u'WHITE SCARS', u'ADEPTUS MINISTORUM', u'MILITARUM TEMPESTUS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachBatallion_imperium, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachBrigade_imperium(DetachBrigade):
    army_name = u'Imperium (Brigade detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'brigade_imperium'
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'BLACK TEMPLARS', u'AGRIPINAA', u'SALAMANDERS', u'MORDIAN', u'METALICA', u'CRIMSON FISTS', u'TALLARN', u'ARMAGEDDON', u'GRAIA', u'BLOOD ANGELS', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'STYGIES VIII', u'DARK ANGELS', u'ASTRA MILITARUM', u'LUCIUS', u'<REGIMENT>', u'FLESH TEARERS', u'VOSTROYAN', u'ULTRAMARINES', u'IMPERIAL FISTS', u'ADEPTA SORORITAS', u'RYZA', u'<DARK ANGELS SUCCESSORS>', u'<ORDER>', u'CADIAN', u'RAVEN GUARD', u'ADEPTUS CUSTODES', u'<CHAPTER>', u'<FORGE WORLD>', u'GREY KNIGHTS', u'ADEPTUS MECHANICUS', u'CATACHAN', u'BLOOD BROTHERS', u'MARS', u'VALHALLAN', u'WHITE SCARS', u'DEATHWATCH', u'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachBrigade_imperium, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachVanguard_imperium(DetachVanguard):
    army_name = u'Imperium (Vanguard detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'vanguard_imperium'
    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'BLOOD BROTHERS', u'<CHAPTER>', u'BLOOD ANGELS', u'CHAOS', u'BLACK TEMPLARS', u'AGRIPINAA', u'SALAMANDERS', u'MORDIAN', u'METALICA', u'CRIMSON FISTS', u'TALLARN', u'DEATHWING', u'GRAIA', u'ASTRA TELEPATHICA', u'ARMAGEDDON', u'<ORDO>', u'INQUISITION', u'SPACE WOLVES', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'STYGIES VIII', u'DARK ANGELS', u'ASTRA MILITARUM', u'CULT MECHANICUS', u'<REGIMENT>', u'FLESH TEARERS', u'LUCIUS', u'VOSTROYAN', u'IMPERIAL FISTS', u'ADEPTA SORORITAS', u'RYZA', u'DEATH COMPANY', u'<DARK ANGELS SUCCESSORS>', u'<ORDER>', u'CADIAN', u'MARS', u'RAVEN GUARD', u'ORDO XENOS', u'ADEPTUS CUSTODES', u'RAVENWING', u'ORDO HERETICUS', u'SCHOLASTICA PSYKANA', u'<FORGE WORLD>', u'GREY KNIGHTS', u'OFFICIO PREFECTUS', u'ADEPTUS MECHANICUS', u'ADEPTUS MINISTORUM', u'CATACHAN', u'ORDO MALLEUS', u'DEATHWATCH', u'VALHALLAN', u'WHITE SCARS', u'ULTRAMARINES', u'FALLEN', u'MILITARUM TEMPESTUS']

    def __init__(self, parent=None):
        super(DetachVanguard_imperium, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachSpearhead_imperium(DetachSpearhead):
    army_name = u'Imperium (Spearhead detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'spearhead_imperium'
    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'BLACK TEMPLARS', u'AGRIPINAA', u'SALAMANDERS', u'MORDIAN', u'METALICA', u'CRIMSON FISTS', u'TALLARN', u'ARMAGEDDON', u'GRAIA', u'CULT MECHANICUS', u'BLOOD ANGELS', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'STYGIES VIII', u'DARK ANGELS', u'ASTRA MILITARUM', u'LUCIUS', u'<REGIMENT>', u'FLESH TEARERS', u'VOSTROYAN', u'IMPERIAL FISTS', u'ADEPTA SORORITAS', u'RYZA', u'<DARK ANGELS SUCCESSORS>', u'<ORDER>', u'CADIAN', u'MARS', u'RAVEN GUARD', u'ADEPTUS CUSTODES', u'<CHAPTER>', u'<FORGE WORLD>', u'GREY KNIGHTS', u'ADEPTUS MECHANICUS', u'CATACHAN', u'BLOOD BROTHERS', u'DEATHWATCH', u'VALHALLAN', u'WHITE SCARS', u'ADEPTUS MINISTORUM', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSpearhead_imperium, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachOutrider_imperium(DetachOutrider):
    army_name = u'Imperium (Outrider detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'outrider_imperium'
    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = [u'IMPERIUM', u'RAVENWING', u'SPACE WOLVES', u'BLACK TEMPLARS', u'AGRIPINAA', u'SALAMANDERS', u'MORDIAN', u'METALICA', u'CRIMSON FISTS', u'TALLARN', u'ARMAGEDDON', u'GRAIA', u'BLOOD ANGELS', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'STYGIES VIII', u'DARK ANGELS', u'ASTRA MILITARUM', u'LUCIUS', u'<REGIMENT>', u'FLESH TEARERS', u'VOSTROYAN', u'IMPERIAL FISTS', u'ADEPTA SORORITAS', u'RYZA', u'<DARK ANGELS SUCCESSORS>', u'<ORDER>', u'CADIAN', u'MARS', u'RAVEN GUARD', u'ADEPTUS CUSTODES', u'<CHAPTER>', u'<FORGE WORLD>', u'GREY KNIGHTS', u'ADEPTUS MECHANICUS', u'CATACHAN', u'BLOOD BROTHERS', u'DEATHWATCH', u'VALHALLAN', u'WHITE SCARS', u'ADEPTUS MINISTORUM', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachOutrider_imperium, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachCommand_imperium(DetachCommand):
    army_name = u'Imperium (Supreme command detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'command_imperium'
    army_factions = [u'IMPERIUM', u'RAVENWING', u'BLOOD ANGELS', u'CHAOS', u'BLACK TEMPLARS', u'MILITARUM TEMPESTUS', u'AGRIPINAA', u'SALAMANDERS', u'MORDIAN', u'METALICA', u'CRIMSON FISTS', u'TALLARN', u'ARMAGEDDON', u'GRAIA', u'ASTRA TELEPATHICA', u'CULT MECHANICUS', u'<ORDO>', u'INQUISITION', u'DEATHWING', u'SPACE WOLVES', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'STYGIES VIII', u'DARK ANGELS', u'ASTRA MILITARUM', u'LUCIUS', u'<REGIMENT>', u'ORDO MALLEUS', u'VOSTROYAN', u'ULTRAMARINES', u'IMPERIAL FISTS', u'ADEPTA SORORITAS', u'RYZA', u'DEATH COMPANY', u'<DARK ANGELS SUCCESSORS>', u'<ORDER>', u'CADIAN', u'FLESH TEARERS', u'RAVEN GUARD', u'ORDO XENOS', u'ADEPTUS CUSTODES', u'<CHAPTER>', u'ADEPTUS MECHANICUS', u'SCHOLASTICA PSYKANA', u'<FORGE WORLD>', u'GREY KNIGHTS', u'ORDO HERETICUS', u'CATACHAN', u'BLOOD BROTHERS', u'DEATHWATCH', u'MARS', u'VALHALLAN', u'WHITE SCARS', u'OFFICIO PREFECTUS', u'FALLEN', u'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachCommand_imperium, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword, KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, Guilliman, TerminusUltra])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachSuperHeavy_imperium(DetachSuperHeavy):
    army_name = u'Imperium (Super-Heavy detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'super_heavy_imperium'
    army_factions = [u'<HOUSEHOLD>', u'IMPERIUM', u'QUESTOR IMPERIALIS', u'ADEPTUS ASTARTES', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'QUESTOR MECHANICUS', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_imperium, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword, KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, Guilliman, TerminusUltra])
        return None


class DetachSuperHeavyAux_imperium(DetachSuperHeavyAux):
    army_name = u'Imperium (Super-Heavy auxilary detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'super_heavy_aux_imperium'
    army_factions = [u'<HOUSEHOLD>', u'IMPERIUM', u'QUESTOR IMPERIALIS', u'ADEPTUS ASTARTES', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'QUESTOR MECHANICUS', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_imperium, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword, KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, Guilliman, TerminusUltra])
        return None


class DetachAirWing_imperium(DetachAirWing):
    army_name = u'Imperium (Air Wing detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'air_wing_imperium'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'AERONAUTICA IMPERIALIS', u'RAVEN GUARD', u'GREY KNIGHTS', u'DARK ANGELS', u'ASTRA MILITARUM', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'DEATHWATCH', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'RAVENWING', u'IMPERIAL FISTS', u'SPACE WOLVES', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachAirWing_imperium, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        return None


class DetachAuxilary_imperium(DetachAuxilary):
    army_name = u'Imperium (Auxilary Support Detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'Auxilary_imperium'
    army_factions = [u'IMPERIUM']

    def __init__(self, parent=None):
        super(DetachAuxilary_imperium, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachSuperHeavy_questor_imperialis(DetachSuperHeavy):
    army_name = u'Questor Imperialis (Super-Heavy detachment)'
    faction_base = u'QUESTOR IMPERIALIS'
    alternate_factions = []
    army_id = u'super_heavy_questor_imperialis'
    army_factions = [u'<HOUSEHOLD>', u'IMPERIUM', u'QUESTOR IMPERIALIS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_questor_imperialis, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives])
        return None


class DetachSuperHeavyAux_questor_imperialis(DetachSuperHeavyAux):
    army_name = u'Questor Imperialis (Super-Heavy auxilary detachment)'
    faction_base = u'QUESTOR IMPERIALIS'
    alternate_factions = []
    army_id = u'super_heavy_aux_questor_imperialis'
    army_factions = [u'<HOUSEHOLD>', u'IMPERIUM', u'QUESTOR IMPERIALIS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_questor_imperialis, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives])
        return None


class DetachAirWing_aeronautica_imperialis(DetachAirWing):
    army_name = u'Aeronautica Imperialis (Air Wing detachment)'
    faction_base = u'AERONAUTICA IMPERIALIS'
    alternate_factions = []
    army_id = u'air_wing_aeronautica_imperialis'
    army_factions = [u'AERONAUTICA IMPERIALIS', u'IMPERIUM', u'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachAirWing_aeronautica_imperialis, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Valkyries])
        return None


class DetachAuxilary_aeronautica_imperialis(DetachAuxilary):
    army_name = u'Aeronautica Imperialis (Auxilary Support Detachment)'
    faction_base = u'AERONAUTICA IMPERIALIS'
    alternate_factions = []
    army_id = u'Auxilary_aeronautica_imperialis'
    army_factions = [u'AERONAUTICA IMPERIALIS']

    def __init__(self, parent=None):
        super(DetachAuxilary_aeronautica_imperialis, self).__init__(*[], **{u'elite': True, u'fliers': True, u'parent': parent, })
        self.elite.add_classes([FleetOfficer])
        self.fliers.add_classes([Valkyries])
        return None

    
class DetachPatrol_chaos(DetachPatrol):
    army_name = u'Chaos (Patrol detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'patrol_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'KHORNE', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPatrol_chaos, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBatallion_chaos(DetachBatallion):
    army_name = u'Chaos (Batallion detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'batallion_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'KHORNE', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachBatallion_chaos, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBrigade_chaos(DetachBrigade):
    army_name = u'Chaos (Brigade detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'brigade_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'SLAANESH', u"EMPEROR'S CHILDREN", u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'KHORNE', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachBrigade_chaos, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachVanguard_chaos(DetachVanguard):
    army_name = u'Chaos (Vanguard detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'vanguard_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'IMPERIUM', u'RED CORSAIRS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'DAEMON', u'DEATH GUARD', u'CHAOS', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'KHORNE', u'FALLEN', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachVanguard_chaos, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSpearhead_chaos(DetachSpearhead):
    army_name = u'Chaos (Spearhead detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'spearhead_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'KHORNE', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachSpearhead_chaos, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachOutrider_chaos(DetachOutrider):
    army_name = u'Chaos (Outrider detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'outrider_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'KHORNE', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachOutrider_chaos, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachCommand_chaos(DetachCommand):
    army_name = u'Chaos (Supreme command detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'command_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'IMPERIUM', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'DAEMON', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'SLAANESH', u'THOUSAND SONS', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'FALLEN', u'HERETIC ASTARTES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachCommand_chaos, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([RenegadeKnight, LordOfSkulls, Mortarion, Magnus])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavy_chaos(DetachSuperHeavy):
    army_name = u'Chaos (Super-Heavy detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'super_heavy_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'QUESTOR TRAITORIS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_chaos, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RenegadeKnight, LordOfSkulls, Mortarion, Magnus])
        return None


class DetachSuperHeavyAux_chaos(DetachSuperHeavyAux):
    army_name = u'Chaos (Super-Heavy auxilary detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'super_heavy_aux_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'QUESTOR TRAITORIS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_chaos, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([RenegadeKnight, LordOfSkulls, Mortarion, Magnus])
        return None


class DetachAirWing_chaos(DetachAirWing):
    army_name = u'Chaos (Air Wing detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'air_wing_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'RED CORSAIRS', u'SLAANESH', u'THOUSAND SONS', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'KHORNE', u'HERETIC ASTARTES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachAirWing_chaos, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachFort_chaos(DetachFort):
    army_name = u'Chaos (Fortification Network)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'fort_chaos'
    army_factions = [u'DAEMON', u'CHAOS', u'NURGLE']

    def __init__(self, parent=None):
        super(DetachFort_chaos, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([ChaosBastion, FeculentGnarlmaws])
        return None


class DetachAuxilary_chaos(DetachAuxilary):
    army_name = u'Chaos (Auxilary Support Detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'Auxilary_chaos'
    army_factions = [u'CHAOS']

    def __init__(self, parent=None):
        super(DetachAuxilary_chaos, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavy_c_tan_shards(DetachSuperHeavy):
    army_name = u"C'tan Shards (Super-Heavy detachment)"
    faction_base = u"C'TAN SHARDS"
    alternate_factions = []
    army_id = u'super_heavy_c_tan_shards'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'SAUTEKH', u'<DYNASTY>', u'NECRONS', u"C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavy_c_tan_shards, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([TesseractVault])
        return None


class DetachSuperHeavyAux_c_tan_shards(DetachSuperHeavyAux):
    army_name = u"C'tan Shards (Super-Heavy auxilary detachment)"
    faction_base = u"C'TAN SHARDS"
    alternate_factions = []
    army_id = u'super_heavy_aux_c_tan_shards'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'SAUTEKH', u'<DYNASTY>', u'NECRONS', u"C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_c_tan_shards, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([TesseractVault])
        return None


class DetachAuxilary_c_tan_shards(DetachAuxilary):
    army_name = u"C'tan Shards (Auxilary Support Detachment)"
    faction_base = u"C'TAN SHARDS"
    alternate_factions = []
    army_id = u'Auxilary_c_tan_shards'
    army_factions = [u"C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachAuxilary_c_tan_shards, self).__init__(*[], **{u'heavy': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([TranscendentCtan])
        self.elite.add_classes([Deciever, Nightbringer])
        return None


class DetachVanguard_inquisition(DetachVanguard):
    army_name = u'Inquisition (Vanguard detachment)'
    faction_base = u'INQUISITION'
    alternate_factions = []
    army_id = u'vanguard_inquisition'
    army_factions = [u'<ORDO>', u'INQUISITION', u'IMPERIUM', u'ORDO HERETICUS', u'ORDO XENOS', u'ORDO MALLEUS']

    def __init__(self, parent=None):
        super(DetachVanguard_inquisition, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus])
        self.elite.add_classes([Acolytes, Jokaero, Daemonhost])
        return None


class DetachCommand_inquisition(DetachCommand):
    army_name = u'Inquisition (Supreme command detachment)'
    faction_base = u'INQUISITION'
    alternate_factions = []
    army_id = u'command_inquisition'
    army_factions = [u'<ORDO>', u'INQUISITION', u'IMPERIUM', u'ORDO HERETICUS', u'ORDO XENOS', u'ORDO MALLEUS']

    def __init__(self, parent=None):
        super(DetachCommand_inquisition, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus])
        self.elite.add_classes([Acolytes, Jokaero, Daemonhost])
        return None


class DetachAuxilary_inquisition(DetachAuxilary):
    army_name = u'Inquisition (Auxilary Support Detachment)'
    faction_base = u'INQUISITION'
    alternate_factions = []
    army_id = u'Auxilary_inquisition'
    army_factions = [u'INQUISITION']

    def __init__(self, parent=None):
        super(DetachAuxilary_inquisition, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus])
        self.elite.add_classes([Acolytes, Jokaero, Daemonhost])
        return None


class DetachPatrol_tyranids(DetachPatrol):
    army_name = u'Tyranids (Patrol detachment)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'patrol_tyranids'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GENESTEALER CULTS', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'KRONOS', u'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachPatrol_tyranids, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachBatallion_tyranids(DetachBatallion):
    army_name = u'Tyranids (Batallion detachment)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'batallion_tyranids'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GENESTEALER CULTS', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'KRONOS', u'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachBatallion_tyranids, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachBrigade_tyranids(DetachBrigade):
    army_name = u'Tyranids (Brigade detachment)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'brigade_tyranids'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GENESTEALER CULTS', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'KRONOS', u'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachBrigade_tyranids, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachVanguard_tyranids(DetachVanguard):
    army_name = u'Tyranids (Vanguard detachment)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'vanguard_tyranids'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GENESTEALER CULTS', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'KRONOS', u'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachVanguard_tyranids, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachSpearhead_tyranids(DetachSpearhead):
    army_name = u'Tyranids (Spearhead detachment)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'spearhead_tyranids'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GENESTEALER CULTS', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'KRONOS', u'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachSpearhead_tyranids, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachOutrider_tyranids(DetachOutrider):
    army_name = u'Tyranids (Outrider detachment)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'outrider_tyranids'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GENESTEALER CULTS', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'KRONOS', u'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachOutrider_tyranids, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachCommand_tyranids(DetachCommand):
    army_name = u'Tyranids (Supreme command detachment)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'command_tyranids'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GENESTEALER CULTS', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'KRONOS', u'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachCommand_tyranids, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        return None


class DetachAirWing_tyranids(DetachAirWing):
    army_name = u'Tyranids (Air Wing detachment)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'air_wing_tyranids'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'KRONOS', u'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachAirWing_tyranids, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Harpy, HiveCrone])
        return None


class DetachFort_tyranids(DetachFort):
    army_name = u'Tyranids (Fortification Network)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'fort_tyranids'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'KRONOS', u'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachFort_tyranids, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([Sporocyst])
        return None


class DetachAuxilary_tyranids(DetachAuxilary):
    army_name = u'Tyranids (Auxilary Support Detachment)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'Auxilary_tyranids'
    army_factions = [u'TYRANIDS']

    def __init__(self, parent=None):
        super(DetachAuxilary_tyranids, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachPatrol_t_au_empire(DetachPatrol):
    army_name = u"T'au Empire (Patrol detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'patrol_t_au_empire'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachPatrol_t_au_empire, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachBatallion_t_au_empire(DetachBatallion):
    army_name = u"T'au Empire (Batallion detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'batallion_t_au_empire'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachBatallion_t_au_empire, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachBrigade_t_au_empire(DetachBrigade):
    army_name = u"T'au Empire (Brigade detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'brigade_t_au_empire'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachBrigade_t_au_empire, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachVanguard_t_au_empire(DetachVanguard):
    army_name = u"T'au Empire (Vanguard detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'vanguard_t_au_empire'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachVanguard_t_au_empire, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachSpearhead_t_au_empire(DetachSpearhead):
    army_name = u"T'au Empire (Spearhead detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'spearhead_t_au_empire'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachSpearhead_t_au_empire, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachOutrider_t_au_empire(DetachOutrider):
    army_name = u"T'au Empire (Outrider detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'outrider_t_au_empire'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachOutrider_t_au_empire, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachCommand_t_au_empire(DetachCommand):
    army_name = u"T'au Empire (Supreme command detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'command_t_au_empire'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(DetachCommand_t_au_empire, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Stormsurge, TheEight])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.transports.add_classes([Devilfish])
        return None


class DetachSuperHeavy_t_au_empire(DetachSuperHeavy):
    army_name = u"T'au Empire (Super-Heavy detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'super_heavy_t_au_empire'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachSuperHeavy_t_au_empire, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Stormsurge, TheEight])
        return None


class DetachSuperHeavyAux_t_au_empire(DetachSuperHeavyAux):
    army_name = u"T'au Empire (Super-Heavy auxilary detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'super_heavy_aux_t_au_empire'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_t_au_empire, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Stormsurge, TheEight])
        return None


class DetachAirWing_t_au_empire(DetachAirWing):
    army_name = u"T'au Empire (Air Wing detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'air_wing_t_au_empire'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachAirWing_t_au_empire, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([RazorShark, SunShark])
        return None


class DetachFort_t_au_empire(DetachFort):
    army_name = u"T'au Empire (Fortification Network)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'fort_t_au_empire'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachFort_t_au_empire, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([Shieldline, Droneport, Gunrig])
        return None


class DetachAuxilary_t_au_empire(DetachAuxilary):
    army_name = u"T'au Empire (Auxilary Support Detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'Auxilary_t_au_empire'
    army_factions = [u"T'AU EMPIRE"]

    def __init__(self, parent=None):
        super(DetachAuxilary_t_au_empire, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachPatrol_adepta_sororitas(DetachPatrol):
    army_name = u'Adepta Sororitas (Patrol detachment)'
    faction_base = u'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = u'patrol_adepta_sororitas'
    army_factions = [u'IMPERIUM', u'ADEPTUS MINISTORUM', u'ADEPTA SORORITAS', u'<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachPatrol_adepta_sororitas, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachBatallion_adepta_sororitas(DetachBatallion):
    army_name = u'Adepta Sororitas (Batallion detachment)'
    faction_base = u'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = u'batallion_adepta_sororitas'
    army_factions = [u'IMPERIUM', u'ADEPTUS MINISTORUM', u'ADEPTA SORORITAS', u'<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachBatallion_adepta_sororitas, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachBrigade_adepta_sororitas(DetachBrigade):
    army_name = u'Adepta Sororitas (Brigade detachment)'
    faction_base = u'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = u'brigade_adepta_sororitas'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachBrigade_adepta_sororitas, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachVanguard_adepta_sororitas(DetachVanguard):
    army_name = u'Adepta Sororitas (Vanguard detachment)'
    faction_base = u'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = u'vanguard_adepta_sororitas'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachVanguard_adepta_sororitas, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachSpearhead_adepta_sororitas(DetachSpearhead):
    army_name = u'Adepta Sororitas (Spearhead detachment)'
    faction_base = u'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = u'spearhead_adepta_sororitas'
    army_factions = [u'IMPERIUM', u'ADEPTUS MINISTORUM', u'ADEPTA SORORITAS', u'<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachSpearhead_adepta_sororitas, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachOutrider_adepta_sororitas(DetachOutrider):
    army_name = u'Adepta Sororitas (Outrider detachment)'
    faction_base = u'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = u'outrider_adepta_sororitas'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachOutrider_adepta_sororitas, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachCommand_adepta_sororitas(DetachCommand):
    army_name = u'Adepta Sororitas (Supreme command detachment)'
    faction_base = u'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = u'command_adepta_sororitas'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachCommand_adepta_sororitas, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachAuxilary_adepta_sororitas(DetachAuxilary):
    army_name = u'Adepta Sororitas (Auxilary Support Detachment)'
    faction_base = u'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = u'Auxilary_adepta_sororitas'
    army_factions = [u'ADEPTA SORORITAS']

    def __init__(self, parent=None):
        super(DetachAuxilary_adepta_sororitas, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachPatrol__dynasty_(DetachPatrol):
    army_name = u'<Dynasty> (Patrol detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'MAYNARKH']
    army_id = u'patrol__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'MAYNARKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachPatrol__dynasty_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachBatallion__dynasty_(DetachBatallion):
    army_name = u'<Dynasty> (Batallion detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'MAYNARKH']
    army_id = u'batallion__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'NECRONS', u'MAYNARKH']

    def __init__(self, parent=None):
        super(DetachBatallion__dynasty_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArc])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachBrigade__dynasty_(DetachBrigade):
    army_name = u'<Dynasty> (Brigade detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'MAYNARKH']
    army_id = u'brigade__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'MAYNARKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachBrigade__dynasty_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachVanguard__dynasty_(DetachVanguard):
    army_name = u'<Dynasty> (Vanguard detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'MAYNARKH']
    army_id = u'vanguard__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'MAYNARKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachVanguard__dynasty_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachSpearhead__dynasty_(DetachSpearhead):
    army_name = u'<Dynasty> (Spearhead detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'MAYNARKH']
    army_id = u'spearhead__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'MAYNARKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachSpearhead__dynasty_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachOutrider__dynasty_(DetachOutrider):
    army_name = u'<Dynasty> (Outrider detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'MAYNARKH']
    army_id = u'outrider__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'MAYNARKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachOutrider__dynasty_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachCommand__dynasty_(DetachCommand):
    army_name = u'<Dynasty> (Supreme command detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'MAYNARKH']
    army_id = u'command__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'SAUTEKH', u'MAYNARKH', u'<DYNASTY>', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachCommand__dynasty_, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk, GaussPylon])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.transports.add_classes([GhostArk])
        return None


class DetachSuperHeavy__dynasty_(DetachSuperHeavy):
    army_name = u'<Dynasty> (Super-Heavy detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'MAYNARKH']
    army_id = u'super_heavy__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'SAUTEKH', u'MAYNARKH', u'<DYNASTY>', u'NECRONS', u"C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavy__dynasty_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk])
        return None


class DetachSuperHeavyAux__dynasty_(DetachSuperHeavyAux):
    army_name = u'<Dynasty> (Super-Heavy auxilary detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'MAYNARKH']
    army_id = u'super_heavy_aux__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'SAUTEKH', u'MAYNARKH', u'<DYNASTY>', u'NECRONS', u"C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__dynasty_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk, GaussPylon])
        return None


class DetachAirWing__dynasty_(DetachAirWing):
    army_name = u'<Dynasty> (Air Wing detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'MAYNARKH']
    army_id = u'air_wing__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'SAUTEKH', u'MAYNARKH', u'<DYNASTY>', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachAirWing__dynasty_, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        return None


class DetachAuxilary__dynasty_(DetachAuxilary):
    army_name = u'<Dynasty> (Auxilary Support Detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'MAYNARKH']
    army_id = u'Auxilary__dynasty_'
    army_factions = [u'<DYNASTY>']

    def __init__(self, parent=None):
        super(DetachAuxilary__dynasty_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachPatrol_aeldari(DetachPatrol):
    army_name = u'Aeldari (Patrol detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'patrol_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'DRUKHARI', u'SAIM-HANN', u'ASPECT WARRIOR', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'<HAEMUNCULUS COVEN>', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'<WYCH CULT>', u'IYANDEN']

    def __init__(self, parent=None):
        super(DetachPatrol_aeldari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachBatallion_aeldari(DetachBatallion):
    army_name = u'Aeldari (Batallion detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'batallion_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'DRUKHARI', u'SAIM-HANN', u'ASPECT WARRIOR', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'<HAEMUNCULUS COVEN>', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'<WYCH CULT>', u'IYANDEN']

    def __init__(self, parent=None):
        super(DetachBatallion_aeldari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachBrigade_aeldari(DetachBrigade):
    army_name = u'Aeldari (Brigade detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'brigade_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'SAIM-HANN', u'AELDARI', u'<MASCUE>', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'HARLEQUINS', u'ASPECT WARRIOR', u'BIEL-TAN', u'WARHOST', u'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachBrigade_aeldari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachVanguard_aeldari(DetachVanguard):
    army_name = u'Aeldari (Vanguard detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'vanguard_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'SAIM-HANN', u'SPIRIT HOST', u'ASPECT WARRIOR', u'AELDARI', u'DRUKHARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'INCUBI', u'<CRAFTWORLD>', u'ALATOIC', u'<WYCH CULT>', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'<HAEMUNCULUS COVEN>', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'WYCH CULT OF STRIFE']

    def __init__(self, parent=None):
        super(DetachVanguard_aeldari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachSpearhead_aeldari(DetachSpearhead):
    army_name = u'Aeldari (Spearhead detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'spearhead_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'DRUKHARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'<HAEMUNCULUS COVEN>', u'ALATOIC', u'WARHOST', u'<MASCUE>', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSpearhead_aeldari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachOutrider_aeldari(DetachOutrider):
    army_name = u'Aeldari (Outrider detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'outrider_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'DRUKHARI', u'SAIM-HANN', u'IYANDEN', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'HARLEQUINS', u'ALATOIC', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachOutrider_aeldari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachCommand_aeldari(DetachCommand):
    army_name = u'Aeldari (Supreme command detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'command_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    army_factions = [u'<HAEMUNCULUS COVEN>', u'SAIM-HANN', u'SPIRIT HOST', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'AELDARI', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'INCUBI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'<WYCH CULT>', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'ASPECT WARRIOR', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachCommand_aeldari, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachSuperHeavy_aeldari(DetachSuperHeavy):
    army_name = u'Aeldari (Super-Heavy detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'super_heavy_aeldari'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_aeldari, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachSuperHeavyAux_aeldari(DetachSuperHeavyAux):
    army_name = u'Aeldari (Super-Heavy auxilary detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'super_heavy_aux_aeldari'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_aeldari, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachAirWing_aeldari(DetachAirWing):
    army_name = u'Aeldari (Air Wing detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'air_wing_aeldari'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'DRUKHARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'ASPECT WARRIOR', u'BIEL-TAN', u'<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachAirWing_aeldari, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        return None


class DetachAuxilary_aeldari(DetachAuxilary):
    army_name = u'Aeldari (Auxilary Support Detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'Auxilary_aeldari'
    army_factions = [u'AELDARI']

    def __init__(self, parent=None):
        super(DetachAuxilary_aeldari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachFort_aeldari(DetachFort):
    army_name = u'Aeldari (Fortification Network)'
    faction_base = u'AELDARI'
    army_id = u'fort_aeldari'
    army_factions = [u"AELDARI"]

    def __init__(self, parent=None):
        super(DetachFort_aeldari, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([WebwayGate])
        return None


class DetachPatrol__hive_fleet_(DetachPatrol):
    army_name = u'<Hive Fleet> (Patrol detachment)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'patrol__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachPatrol__hive_fleet_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachBatallion__hive_fleet_(DetachBatallion):
    army_name = u'<Hive Fleet> (Batallion detachment)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'batallion__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachBatallion__hive_fleet_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachBrigade__hive_fleet_(DetachBrigade):
    army_name = u'<Hive Fleet> (Brigade detachment)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'brigade__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachBrigade__hive_fleet_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachVanguard__hive_fleet_(DetachVanguard):
    army_name = u'<Hive Fleet> (Vanguard detachment)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'vanguard__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachVanguard__hive_fleet_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachSpearhead__hive_fleet_(DetachSpearhead):
    army_name = u'<Hive Fleet> (Spearhead detachment)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'spearhead__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachSpearhead__hive_fleet_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachOutrider__hive_fleet_(DetachOutrider):
    army_name = u'<Hive Fleet> (Outrider detachment)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'outrider__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachOutrider__hive_fleet_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachCommand__hive_fleet_(DetachCommand):
    army_name = u'<Hive Fleet> (Supreme command detachment)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'command__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachCommand__hive_fleet_, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([Tyrannocyte])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        return None


class DetachAirWing__hive_fleet_(DetachAirWing):
    army_name = u'<Hive Fleet> (Air Wing detachment)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'air_wing__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachAirWing__hive_fleet_, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Harpy, HiveCrone])
        return None


class DetachFort__hive_fleet_(DetachFort):
    army_name = u'<Hive Fleet> (Fortification Network)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'fort__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachFort__hive_fleet_, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([Sporocyst])
        return None


class DetachAuxilary__hive_fleet_(DetachAuxilary):
    army_name = u'<Hive Fleet> (Auxilary Support Detachment)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'Auxilary__hive_fleet_'
    army_factions = [u'<HIVE FLEET>']

    def __init__(self, parent=None):
        super(DetachAuxilary__hive_fleet_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachPatrol__chapter_(DetachPatrol):
    army_name = u'<Chapter> (Patrol detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'patrol__chapter_'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPatrol__chapter_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachBatallion__chapter_(DetachBatallion):
    army_name = u'<Chapter> (Batallion detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'batallion__chapter_'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachBatallion__chapter_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachBrigade__chapter_(DetachBrigade):
    army_name = u'<Chapter> (Brigade detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'brigade__chapter_'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachBrigade__chapter_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachVanguard__chapter_(DetachVanguard):
    army_name = u'<Chapter> (Vanguard detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'vanguard__chapter_'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachVanguard__chapter_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachSpearhead__chapter_(DetachSpearhead):
    army_name = u'<Chapter> (Spearhead detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'spearhead__chapter_'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSpearhead__chapter_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachOutrider__chapter_(DetachOutrider):
    army_name = u'<Chapter> (Outrider detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'outrider__chapter_'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachOutrider__chapter_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachCommand__chapter_(DetachCommand):
    army_name = u'<Chapter> (Supreme command detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'command__chapter_'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachCommand__chapter_, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Guilliman, TerminusUltra, RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachSuperHeavy__chapter_(DetachSuperHeavy):
    army_name = u'<Chapter> (Super-Heavy detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'super_heavy__chapter_'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__chapter_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Guilliman, TerminusUltra, RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavyAux__chapter_(DetachSuperHeavyAux):
    army_name = u'<Chapter> (Super-Heavy auxilary detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'super_heavy_aux__chapter_'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__chapter_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Guilliman, TerminusUltra, RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachAirWing__chapter_(DetachAirWing):
    army_name = u'<Chapter> (Air Wing detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'air_wing__chapter_'
    army_factions = [u'IMPERIUM', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachAirWing__chapter_, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        return None


class DetachAuxilary__chapter_(DetachAuxilary):
    army_name = u'<Chapter> (Auxilary Support Detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'Auxilary__chapter_'
    army_factions = [u'<CHAPTER>']

    def __init__(self, parent=None):
        super(DetachAuxilary__chapter_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachSuperHeavy_questor_mechanicus(DetachSuperHeavy):
    army_name = u'Questor Mechanicus (Super-Heavy detachment)'
    faction_base = u'QUESTOR MECHANICUS'
    alternate_factions = []
    army_id = u'super_heavy_questor_mechanicus'
    army_factions = [u'<HOUSEHOLD>', u'IMPERIUM', u'QUESTOR MECHANICUS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_questor_mechanicus, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, ArmigerWarglaives])
        return None


class DetachSuperHeavyAux_questor_mechanicus(DetachSuperHeavyAux):
    army_name = u'Questor Mechanicus (Super-Heavy auxilary detachment)'
    faction_base = u'QUESTOR MECHANICUS'
    alternate_factions = []
    army_id = u'super_heavy_aux_questor_mechanicus'
    army_factions = [u'<HOUSEHOLD>', u'IMPERIUM', u'QUESTOR MECHANICUS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_questor_mechanicus, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, ArmigerWarglaives])
        return None


class DetachPatrol_death_guard(DetachPatrol):
    army_name = u'Death Guard (Patrol detachment)'
    faction_base = u'DEATH GUARD'
    alternate_factions = []
    army_id = u'patrol_death_guard'
    army_factions = [u'NIGHT LORDS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPatrol_death_guard, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachBatallion_death_guard(DetachBatallion):
    army_name = u'Death Guard (Batallion detachment)'
    faction_base = u'DEATH GUARD'
    alternate_factions = []
    army_id = u'batallion_death_guard'
    army_factions = [u'NIGHT LORDS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachBatallion_death_guard, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachBrigade_death_guard(DetachBrigade):
    army_name = u'Death Guard (Brigade detachment)'
    faction_base = u'DEATH GUARD'
    alternate_factions = []
    army_id = u'brigade_death_guard'
    army_factions = [u'NIGHT LORDS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachBrigade_death_guard, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachVanguard_death_guard(DetachVanguard):
    army_name = u'Death Guard (Vanguard detachment)'
    faction_base = u'DEATH GUARD'
    alternate_factions = []
    army_id = u'vanguard_death_guard'
    army_factions = [u'NIGHT LORDS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachVanguard_death_guard, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachSpearhead_death_guard(DetachSpearhead):
    army_name = u'Death Guard (Spearhead detachment)'
    faction_base = u'DEATH GUARD'
    alternate_factions = []
    army_id = u'spearhead_death_guard'
    army_factions = [u'NIGHT LORDS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachSpearhead_death_guard, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachOutrider_death_guard(DetachOutrider):
    army_name = u'Death Guard (Outrider detachment)'
    faction_base = u'DEATH GUARD'
    alternate_factions = []
    army_id = u'outrider_death_guard'
    army_factions = [u'NIGHT LORDS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachOutrider_death_guard, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachCommand_death_guard(DetachCommand):
    army_name = u'Death Guard (Supreme command detachment)'
    faction_base = u'DEATH GUARD'
    alternate_factions = []
    army_id = u'command_death_guard'
    army_factions = [u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachCommand_death_guard, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Mortarion])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavyAux_death_guard(DetachSuperHeavyAux):
    army_name = u'Death Guard (Super-Heavy auxilary detachment)'
    faction_base = u'DEATH GUARD'
    alternate_factions = []
    army_id = u'super_heavy_aux_death_guard'
    army_factions = [u'HERETIC ASTARTES', u'NURGLE', u'DEATH GUARD', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_death_guard, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Mortarion])
        return None


class DetachAuxilary_death_guard(DetachAuxilary):
    army_name = u'Death Guard (Auxilary Support Detachment)'
    faction_base = u'DEATH GUARD'
    alternate_factions = []
    army_id = u'Auxilary_death_guard'
    army_factions = [u'DEATH GUARD']

    def __init__(self, parent=None):
        super(DetachAuxilary_death_guard, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachAuxilary_vespid(DetachAuxilary):
    army_name = u'Vespid (Auxilary Support Detachment)'
    faction_base = u'VESPID'
    alternate_factions = []
    army_id = u'Auxilary_vespid'
    army_factions = [u'VESPID']

    def __init__(self, parent=None):
        super(DetachAuxilary_vespid, self).__init__(*[], **{u'fast': True, u'parent': parent, })
        self.fast.add_classes([Vespids])
        return None


class DetachPatrol_harlequins(DetachPatrol):
    army_name = u'Harlequins (Patrol detachment)'
    faction_base = u'HARLEQUINS'
    alternate_factions = []
    army_id = u'patrol_harlequins'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachPatrol_harlequins, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachBatallion_harlequins(DetachBatallion):
    army_name = u'Harlequins (Batallion detachment)'
    faction_base = u'HARLEQUINS'
    alternate_factions = []
    army_id = u'batallion_harlequins'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachBatallion_harlequins, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachBrigade_harlequins(DetachBrigade):
    army_name = u'Harlequins (Brigade detachment)'
    faction_base = u'HARLEQUINS'
    alternate_factions = []
    army_id = u'brigade_harlequins'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachBrigade_harlequins, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachVanguard_harlequins(DetachVanguard):
    army_name = u'Harlequins (Vanguard detachment)'
    faction_base = u'HARLEQUINS'
    alternate_factions = []
    army_id = u'vanguard_harlequins'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachVanguard_harlequins, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachSpearhead_harlequins(DetachSpearhead):
    army_name = u'Harlequins (Spearhead detachment)'
    faction_base = u'HARLEQUINS'
    alternate_factions = []
    army_id = u'spearhead_harlequins'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachSpearhead_harlequins, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachOutrider_harlequins(DetachOutrider):
    army_name = u'Harlequins (Outrider detachment)'
    faction_base = u'HARLEQUINS'
    alternate_factions = []
    army_id = u'outrider_harlequins'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachOutrider_harlequins, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachCommand_harlequins(DetachCommand):
    army_name = u'Harlequins (Supreme command detachment)'
    faction_base = u'HARLEQUINS'
    alternate_factions = []
    army_id = u'command_harlequins'
    army_factions = [u'<MASCUE>', u'YNNARI', u'HARLEQUINS', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachCommand_harlequins, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachAuxilary_harlequins(DetachAuxilary):
    army_name = u'Harlequins (Auxilary Support Detachment)'
    faction_base = u'HARLEQUINS'
    alternate_factions = []
    army_id = u'Auxilary_harlequins'
    army_factions = [u'HARLEQUINS']

    def __init__(self, parent=None):
        super(DetachAuxilary_harlequins, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachPatrol_ork(DetachPatrol):
    army_name = u'Ork (Patrol detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'patrol_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPatrol_ork, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachBatallion_ork(DetachBatallion):
    army_name = u'Ork (Batallion detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'batallion_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachBatallion_ork, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachBrigade_ork(DetachBrigade):
    army_name = u'Ork (Brigade detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'brigade_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachBrigade_ork, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachVanguard_ork(DetachVanguard):
    army_name = u'Ork (Vanguard detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'vanguard_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachVanguard_ork, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachSpearhead_ork(DetachSpearhead):
    army_name = u'Ork (Spearhead detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'spearhead_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachSpearhead_ork, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachOutrider_ork(DetachOutrider):
    army_name = u'Ork (Outrider detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'outrider_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachOutrider_ork, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachCommand_ork(DetachCommand):
    army_name = u'Ork (Supreme command detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'command_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ', u'ORK', u'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachCommand_ork, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Stompa])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.transports.add_classes([Trukk])
        return None


class DetachSuperHeavy_ork(DetachSuperHeavy):
    army_name = u'Ork (Super-Heavy detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'super_heavy_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ', u'ORK', u'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_ork, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Stompa])
        return None


class DetachSuperHeavyAux_ork(DetachSuperHeavyAux):
    army_name = u'Ork (Super-Heavy auxilary detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'super_heavy_aux_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ', u'ORK', u'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_ork, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Stompa])
        return None


class DetachFort_ork(DetachFort):
    army_name = u'Ork (Fortification Network)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'fort_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ', u'ORK', u'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachFort_ork, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([MekboyWorkshop])
        return None


class DetachAirWing_ork(DetachAirWing):
    army_name = u'Ork (Air Wing detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'air_wing_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ', u'ORK', u'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachAirWing_ork, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        return None


class DetachAuxilary_ork(DetachAuxilary):
    army_name = u'Ork (Auxilary Support Detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'Auxilary_ork'
    army_factions = [u'ORK']

    def __init__(self, parent=None):
        super(DetachAuxilary_ork, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachPatrol_militarum_tempestus(DetachPatrol):
    army_name = u'Militarum Tempestus (Patrol detachment)'
    faction_base = u'MILITARUM TEMPESTUS'
    alternate_factions = []
    army_id = u'patrol_militarum_tempestus'
    army_factions = [u'IMPERIUM', u'ASTRA MILITARUM', u'MILITARUM TEMPESTUS']

    hq_sec = GuardHQ
    elite_sec = GuardElites

    def __init__(self, parent=None):
        super(DetachPatrol_militarum_tempestus, self).__init__(*[], **{u'hq': True, u'elite': True, u'troops': True, u'parent': parent, })
        self.hq.add_classes([TempestorPrime])
        self.elite.add_classes([TempCommandSquad])
        self.troops.add_classes([TempestusSquad])
        return None


class DetachBatallion_militarum_tempestus(DetachBatallion):
    army_name = u'Militarum Tempestus (Batallion detachment)'
    faction_base = u'MILITARUM TEMPESTUS'
    alternate_factions = []
    army_id = u'batallion_militarum_tempestus'
    army_factions = [u'IMPERIUM', u'ASTRA MILITARUM', u'MILITARUM TEMPESTUS']

    hq_sec = GuardHQ
    elite_sec = GuardElites

    def __init__(self, parent=None):
        super(DetachBatallion_militarum_tempestus, self).__init__(*[], **{u'hq': True, u'elite': True, u'troops': True, u'parent': parent, })
        self.hq.add_classes([TempestorPrime])
        self.elite.add_classes([TempCommandSquad])
        self.troops.add_classes([TempestusSquad])
        return None


class DetachCommand_militarum_tempestus(DetachCommand):
    army_name = u'Militarum Tempestus (Supreme command detachment)'
    faction_base = u'MILITARUM TEMPESTUS'
    alternate_factions = []
    army_id = u'command_militarum_tempestus'
    army_factions = [u'IMPERIUM', u'ASTRA MILITARUM', u'MILITARUM TEMPESTUS']

    hq_sec = GuardHQ
    elite_sec = GuardElites

    def __init__(self, parent=None):
        super(DetachCommand_militarum_tempestus, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([TempestorPrime])
        self.elite.add_classes([TempCommandSquad])
        return None


class DetachAuxilary_militarum_tempestus(DetachAuxilary):
    army_name = u'Militarum Tempestus (Auxilary Support Detachment)'
    faction_base = u'MILITARUM TEMPESTUS'
    alternate_factions = []
    army_id = u'Auxilary_militarum_tempestus'
    army_factions = [u'MILITARUM TEMPESTUS']

    def __init__(self, parent=None):
        super(DetachAuxilary_militarum_tempestus, self).__init__(*[], **{u'hq': True, u'elite': True, u'troops': True, u'parent': parent, })
        self.hq.add_classes([TempestorPrime])
        self.elite.add_classes([TempCommandSquad])
        self.troops.add_classes([TempestusSquad])
        return None


class DetachPatrol_genestealer_cults(DetachPatrol):
    army_name = u'Genestealer Cults (Patrol detachment)'
    faction_base = u'GENESTEALER CULTS'
    alternate_factions = []
    army_id = u'patrol_genestealer_cults'
    army_factions = [u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachPatrol_genestealer_cults, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([AcolyteHybrids, NeophyteHybrids])
        self.transports.add_classes([GoliathTruck, CultChimera])
        self.hq.add_classes([Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.elite.add_classes([HybridMetamorphs, Aberrants, PurestrainGenestealers])
        return None


class DetachBatallion_genestealer_cults(DetachBatallion):
    army_name = u'Genestealer Cults (Batallion detachment)'
    faction_base = u'GENESTEALER CULTS'
    alternate_factions = []
    army_id = u'batallion_genestealer_cults'
    army_factions = [u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachBatallion_genestealer_cults, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([AcolyteHybrids, NeophyteHybrids])
        self.transports.add_classes([GoliathTruck, CultChimera])
        self.hq.add_classes([Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.elite.add_classes([HybridMetamorphs, Aberrants, PurestrainGenestealers])
        return None


class DetachBrigade_genestealer_cults(DetachBrigade):
    army_name = u'Genestealer Cults (Brigade detachment)'
    faction_base = u'GENESTEALER CULTS'
    alternate_factions = []
    army_id = u'brigade_genestealer_cults'
    army_factions = [u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachBrigade_genestealer_cults, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([AcolyteHybrids, NeophyteHybrids])
        self.transports.add_classes([GoliathTruck, CultChimera])
        self.hq.add_classes([Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.elite.add_classes([HybridMetamorphs, Aberrants, PurestrainGenestealers])
        return None


class DetachVanguard_genestealer_cults(DetachVanguard):
    army_name = u'Genestealer Cults (Vanguard detachment)'
    faction_base = u'GENESTEALER CULTS'
    alternate_factions = []
    army_id = u'vanguard_genestealer_cults'
    army_factions = [u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachVanguard_genestealer_cults, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([AcolyteHybrids, NeophyteHybrids])
        self.transports.add_classes([GoliathTruck, CultChimera])
        self.hq.add_classes([Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.elite.add_classes([HybridMetamorphs, Aberrants, PurestrainGenestealers])
        return None


class DetachSpearhead_genestealer_cults(DetachSpearhead):
    army_name = u'Genestealer Cults (Spearhead detachment)'
    faction_base = u'GENESTEALER CULTS'
    alternate_factions = []
    army_id = u'spearhead_genestealer_cults'
    army_factions = [u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachSpearhead_genestealer_cults, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([AcolyteHybrids, NeophyteHybrids])
        self.transports.add_classes([GoliathTruck, CultChimera])
        self.hq.add_classes([Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.elite.add_classes([HybridMetamorphs, Aberrants, PurestrainGenestealers])
        return None


class DetachOutrider_genestealer_cults(DetachOutrider):
    army_name = u'Genestealer Cults (Outrider detachment)'
    faction_base = u'GENESTEALER CULTS'
    alternate_factions = []
    army_id = u'outrider_genestealer_cults'
    army_factions = [u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachOutrider_genestealer_cults, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([AcolyteHybrids, NeophyteHybrids])
        self.transports.add_classes([GoliathTruck, CultChimera])
        self.hq.add_classes([Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.elite.add_classes([HybridMetamorphs, Aberrants, PurestrainGenestealers])
        return None


class DetachCommand_genestealer_cults(DetachCommand):
    army_name = u'Genestealer Cults (Supreme command detachment)'
    faction_base = u'GENESTEALER CULTS'
    alternate_factions = []
    army_id = u'command_genestealer_cults'
    army_factions = [u'TYRANIDS', u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachCommand_genestealer_cults, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([GoliathTruck, CultChimera])
        self.hq.add_classes([Patriarch, Magus, Primus, Iconward, Abominant])
        self.elite.add_classes([HybridMetamorphs, Aberrants, PurestrainGenestealers])
        return None


class DetachAuxilary_genestealer_cults(DetachAuxilary):
    army_name = u'Genestealer Cults (Auxilary Support Detachment)'
    faction_base = u'GENESTEALER CULTS'
    alternate_factions = []
    army_id = u'Auxilary_genestealer_cults'
    army_factions = [u'GENESTEALER CULTS']

    def __init__(self, parent=None):
        super(DetachAuxilary_genestealer_cults, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([AcolyteHybrids, NeophyteHybrids])
        self.transports.add_classes([GoliathTruck, CultChimera])
        self.hq.add_classes([Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.elite.add_classes([HybridMetamorphs, Aberrants, PurestrainGenestealers])
        return None


class DetachPatrol__craftworld_(DetachPatrol):
    army_name = u'<Craftworld> (Patrol detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'patrol__craftworld_'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachPatrol__craftworld_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachBatallion__craftworld_(DetachBatallion):
    army_name = u'<Craftworld> (Batallion detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'batallion__craftworld_'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachBatallion__craftworld_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachBrigade__craftworld_(DetachBrigade):
    army_name = u'<Craftworld> (Brigade detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'brigade__craftworld_'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachBrigade__craftworld_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachVanguard__craftworld_(DetachVanguard):
    army_name = u'<Craftworld> (Vanguard detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'vanguard__craftworld_'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachVanguard__craftworld_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachSpearhead__craftworld_(DetachSpearhead):
    army_name = u'<Craftworld> (Spearhead detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'spearhead__craftworld_'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachSpearhead__craftworld_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachOutrider__craftworld_(DetachOutrider):
    army_name = u'<Craftworld> (Outrider detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'outrider__craftworld_'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachOutrider__craftworld_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachCommand__craftworld_(DetachCommand):
    army_name = u'<Craftworld> (Supreme command detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'command__craftworld_'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachCommand__craftworld_, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachSuperHeavy__craftworld_(DetachSuperHeavy):
    army_name = u'<Craftworld> (Super-Heavy detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'super_heavy__craftworld_'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__craftworld_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachSuperHeavyAux__craftworld_(DetachSuperHeavyAux):
    army_name = u'<Craftworld> (Super-Heavy auxilary detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'super_heavy_aux__craftworld_'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__craftworld_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachAirWing__craftworld_(DetachAirWing):
    army_name = u'<Craftworld> (Air Wing detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'air_wing__craftworld_'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachAirWing__craftworld_, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        return None


class DetachAuxilary__craftworld_(DetachAuxilary):
    army_name = u'<Craftworld> (Auxilary Support Detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'Auxilary__craftworld_'
    army_factions = [u'<CRAFTWORLD>']

    def __init__(self, parent=None):
        super(DetachAuxilary__craftworld_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachPatrol__kabal_(DetachPatrol):
    army_name = u'<Kabal> (Patrol detachment)'
    faction_base = u'<KABAL>'
    alternate_factions = [u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE']
    army_id = u'patrol__kabal_'
    army_factions = [u'YNNARI', u'DRUKHARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE', u'<KABAL>', u'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachPatrol__kabal_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachBatallion__kabal_(DetachBatallion):
    army_name = u'<Kabal> (Batallion detachment)'
    faction_base = u'<KABAL>'
    alternate_factions = [u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE']
    army_id = u'batallion__kabal_'
    army_factions = [u'YNNARI', u'DRUKHARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE', u'<KABAL>', u'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachBatallion__kabal_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachVanguard__kabal_(DetachVanguard):
    army_name = u'<Kabal> (Vanguard detachment)'
    faction_base = u'<KABAL>'
    alternate_factions = [u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE']
    army_id = u'vanguard__kabal_'
    army_factions = [u'YNNARI', u'DRUKHARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE', u'<KABAL>', u'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachVanguard__kabal_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachSpearhead__kabal_(DetachSpearhead):
    army_name = u'<Kabal> (Spearhead detachment)'
    faction_base = u'<KABAL>'
    alternate_factions = [u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE']
    army_id = u'spearhead__kabal_'
    army_factions = [u'YNNARI', u'DRUKHARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE', u'<KABAL>', u'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachSpearhead__kabal_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachCommand__kabal_(DetachCommand):
    army_name = u'<Kabal> (Supreme command detachment)'
    faction_base = u'<KABAL>'
    alternate_factions = [u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE']
    army_id = u'command__kabal_'
    army_factions = [u'YNNARI', u'DRUKHARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE', u'<KABAL>', u'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachCommand__kabal_, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachAirWing__kabal_(DetachAirWing):
    army_name = u'<Kabal> (Air Wing detachment)'
    faction_base = u'<KABAL>'
    alternate_factions = [u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE']
    army_id = u'air_wing__kabal_'
    army_factions = [u'AELDARI', u'DRUKHARI', u'<WYCH CULT>', u'YNNARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE', u'<KABAL>', u'WYCH CULT OF STRIFE']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachAirWing__kabal_, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Razorwing, Voidraven])
        return None


class DetachAuxilary__kabal_(DetachAuxilary):
    army_name = u'<Kabal> (Auxilary Support Detachment)'
    faction_base = u'<KABAL>'
    alternate_factions = [u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE']
    army_id = u'Auxilary__kabal_'
    army_factions = [u'<KABAL>']

    def __init__(self, parent=None):
        super(DetachAuxilary__kabal_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachPatrol__clan_(DetachPatrol):
    army_name = u'<Clan> (Patrol detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'patrol__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPatrol__clan_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachBatallion__clan_(DetachBatallion):
    army_name = u'<Clan> (Batallion detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'batallion__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachBatallion__clan_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachBrigade__clan_(DetachBrigade):
    army_name = u'<Clan> (Brigade detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'brigade__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachBrigade__clan_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachVanguard__clan_(DetachVanguard):
    army_name = u'<Clan> (Vanguard detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'vanguard__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachVanguard__clan_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachSpearhead__clan_(DetachSpearhead):
    army_name = u'<Clan> (Spearhead detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'spearhead__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachSpearhead__clan_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachOutrider__clan_(DetachOutrider):
    army_name = u'<Clan> (Outrider detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'outrider__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachOutrider__clan_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachCommand__clan_(DetachCommand):
    army_name = u'<Clan> (Supreme command detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'command__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ', u'ORK', u'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachCommand__clan_, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Stompa])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.transports.add_classes([Trukk])
        return None


class DetachSuperHeavy__clan_(DetachSuperHeavy):
    army_name = u'<Clan> (Super-Heavy detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'super_heavy__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ', u'ORK', u'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__clan_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Stompa])
        return None


class DetachSuperHeavyAux__clan_(DetachSuperHeavyAux):
    army_name = u'<Clan> (Super-Heavy auxilary detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'super_heavy_aux__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ', u'ORK', u'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__clan_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Stompa])
        return None


class DetachAirWing__clan_(DetachAirWing):
    army_name = u'<Clan> (Air Wing detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'air_wing__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ', u'ORK', u'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachAirWing__clan_, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        return None


class DetachAuxilary__clan_(DetachAuxilary):
    army_name = u'<Clan> (Auxilary Support Detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'Auxilary__clan_'
    army_factions = [u'<CLAN>']

    def __init__(self, parent=None):
        super(DetachAuxilary__clan_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachFort__clan_(DetachFort):
    army_name = u'<Clan> (Fortification Network)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'fort__clan_'
    army_factions = [u'<CLAN>']

    def __init__(self, parent=None):
        super(DetachFort__clan_, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([MekboyWorkshop])
        return None


class DetachVanguard__ordo_(DetachVanguard):
    army_name = u'<Ordo> (Vanguard detachment)'
    faction_base = u'<ORDO>'
    alternate_factions = [u'ORDO MALLEUS', u'ORDO HERETICUS', u'ORDO XENOS']
    army_id = u'vanguard__ordo_'
    army_factions = [u'<ORDO>', u'INQUISITION', u'IMPERIUM', u'ORDO HERETICUS', u'ORDO XENOS', u'ORDO MALLEUS']

    def __init__(self, parent=None):
        super(DetachVanguard__ordo_, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor])
        self.elite.add_classes([Acolytes, Jokaero])
        return None


class DetachCommand__ordo_(DetachCommand):
    army_name = u'<Ordo> (Supreme command detachment)'
    faction_base = u'<ORDO>'
    alternate_factions = [u'ORDO MALLEUS', u'ORDO HERETICUS', u'ORDO XENOS']
    army_id = u'command__ordo_'
    army_factions = [u'<ORDO>', u'INQUISITION', u'IMPERIUM', u'ORDO HERETICUS', u'ORDO XENOS', u'ORDO MALLEUS']

    def __init__(self, parent=None):
        super(DetachCommand__ordo_, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor])
        self.elite.add_classes([Acolytes, Jokaero])
        return None


class DetachAuxilary__ordo_(DetachAuxilary):
    army_name = u'<Ordo> (Auxilary Support Detachment)'
    faction_base = u'<ORDO>'
    alternate_factions = [u'ORDO MALLEUS', u'ORDO HERETICUS', u'ORDO XENOS']
    army_id = u'Auxilary__ordo_'
    army_factions = [u'<ORDO>']

    def __init__(self, parent=None):
        super(DetachAuxilary__ordo_, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor])
        self.elite.add_classes([Acolytes, Jokaero])
        return None


class DetachPatrol_adeptus_astartes(DetachPatrol):
    army_name = u'Adeptus Astartes (Patrol detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'patrol_adeptus_astartes'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'ULTRAMARINES', u'IMPERIAL FISTS', u'SPACE WOLVES', u'GREY KNIGHTS']

    def __init__(self, parent=None):
        super(DetachPatrol_adeptus_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachBatallion_adeptus_astartes(DetachBatallion):
    army_name = u'Adeptus Astartes (Batallion detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'batallion_adeptus_astartes'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'ULTRAMARINES', u'IMPERIAL FISTS', u'SPACE WOLVES', u'GREY KNIGHTS']

    def __init__(self, parent=None):
        super(DetachBatallion_adeptus_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachBrigade_adeptus_astartes(DetachBrigade):
    army_name = u'Adeptus Astartes (Brigade detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'brigade_adeptus_astartes'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'GREY KNIGHTS', u'IMPERIAL FISTS', u'SPACE WOLVES', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachBrigade_adeptus_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachVanguard_adeptus_astartes(DetachVanguard):
    army_name = u'Adeptus Astartes (Vanguard detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'vanguard_adeptus_astartes'
    army_factions = [u'ULTRAMARINES', u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'DEATH COMPANY', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'RAVENWING', u'IMPERIAL FISTS', u'DEATHWING', u'SPACE WOLVES', u'GREY KNIGHTS']

    def __init__(self, parent=None):
        super(DetachVanguard_adeptus_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachSpearhead_adeptus_astartes(DetachSpearhead):
    army_name = u'Adeptus Astartes (Spearhead detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'spearhead_adeptus_astartes'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'ULTRAMARINES', u'IMPERIAL FISTS', u'SPACE WOLVES', u'GREY KNIGHTS']

    def __init__(self, parent=None):
        super(DetachSpearhead_adeptus_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachOutrider_adeptus_astartes(DetachOutrider):
    army_name = u'Adeptus Astartes (Outrider detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'outrider_adeptus_astartes'
    army_factions = [u'ULTRAMARINES', u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'RAVENWING', u'IMPERIAL FISTS', u'SPACE WOLVES', u'GREY KNIGHTS']

    def __init__(self, parent=None):
        super(DetachOutrider_adeptus_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachCommand_adeptus_astartes(DetachCommand):
    army_name = u'Adeptus Astartes (Supreme command detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'command_adeptus_astartes'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'GREY KNIGHTS', u'DARK ANGELS', u'BLACK TEMPLARS', u'DEATH COMPANY', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'RAVENWING', u'IMPERIAL FISTS', u'DEATHWING', u'SPACE WOLVES', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachCommand_adeptus_astartes, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Guilliman, TerminusUltra, RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachSuperHeavy_adeptus_astartes(DetachSuperHeavy):
    army_name = u'Adeptus Astartes (Super-Heavy detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'super_heavy_adeptus_astartes'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_adeptus_astartes, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Guilliman, TerminusUltra, RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavyAux_adeptus_astartes(DetachSuperHeavyAux):
    army_name = u'Adeptus Astartes (Super-Heavy auxilary detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'super_heavy_aux_adeptus_astartes'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_adeptus_astartes, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Guilliman, TerminusUltra, RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachAirWing_adeptus_astartes(DetachAirWing):
    army_name = u'Adeptus Astartes (Air Wing detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'air_wing_adeptus_astartes'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'GREY KNIGHTS', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'RAVENWING', u'IMPERIAL FISTS', u'SPACE WOLVES', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachAirWing_adeptus_astartes, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        return None


class DetachAuxilary_adeptus_astartes(DetachAuxilary):
    army_name = u'Adeptus Astartes (Auxilary Support Detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'Auxilary_adeptus_astartes'
    army_factions = [u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAuxilary_adeptus_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachPatrol_cult_mechanicus(DetachPatrol):
    army_name = u'Cult Mechanicus (Patrol detachment)'
    faction_base = u'CULT MECHANICUS'
    alternate_factions = []
    army_id = u'patrol_cult_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPatrol_cult_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachBatallion_cult_mechanicus(DetachBatallion):
    army_name = u'Cult Mechanicus (Batallion detachment)'
    faction_base = u'CULT MECHANICUS'
    alternate_factions = []
    army_id = u'batallion_cult_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachBatallion_cult_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachVanguard_cult_mechanicus(DetachVanguard):
    army_name = u'Cult Mechanicus (Vanguard detachment)'
    faction_base = u'CULT MECHANICUS'
    alternate_factions = []
    army_id = u'vanguard_cult_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'ASTRA MILITARUM', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachVanguard_cult_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachSpearhead_cult_mechanicus(DetachSpearhead):
    army_name = u'Cult Mechanicus (Spearhead detachment)'
    faction_base = u'CULT MECHANICUS'
    alternate_factions = []
    army_id = u'spearhead_cult_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachSpearhead_cult_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachCommand_cult_mechanicus(DetachCommand):
    army_name = u'Cult Mechanicus (Supreme command detachment)'
    faction_base = u'CULT MECHANICUS'
    alternate_factions = []
    army_id = u'command_cult_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'ASTRA MILITARUM', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachCommand_cult_mechanicus, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        return None


class DetachAuxilary_cult_mechanicus(DetachAuxilary):
    army_name = u'Cult Mechanicus (Auxilary Support Detachment)'
    faction_base = u'CULT MECHANICUS'
    alternate_factions = []
    army_id = u'Auxilary_cult_mechanicus'
    army_factions = [u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachAuxilary_cult_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachAuxilary_sisters_of_silence(DetachAuxilary):
    army_name = u'Sisters Of Silence (Auxilary Support Detachment)'
    faction_base = u'SISTERS OF SILENCE'
    alternate_factions = []
    army_id = u'Auxilary_sisters_of_silence'
    army_factions = [u'SISTERS OF SILENCE']

    def __init__(self, parent=None):
        super(DetachAuxilary_sisters_of_silence, self).__init__(*[], **{u'elite': True, u'transports': True, u'parent': parent, })
        self.elite.add_classes([Prosecutors, Vigilators, Witchseekers])
        self.transports.add_classes([NullRhino])
        return None


class DetachPatrol_ynnari(DetachPatrol):
    army_name = u'Ynnari (Patrol detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'patrol_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'DRUKHARI', u'SAIM-HANN', u'IYANDEN', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'ALATOIC', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachPatrol_ynnari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachBatallion_ynnari(DetachBatallion):
    army_name = u'Ynnari (Batallion detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'batallion_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'DRUKHARI', u'SAIM-HANN', u'IYANDEN', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'ALATOIC', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachBatallion_ynnari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachBrigade_ynnari(DetachBrigade):
    army_name = u'Ynnari (Brigade detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'brigade_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'SAIM-HANN', u'AELDARI', u'<MASCUE>', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'HARLEQUINS', u'ALATOIC', u'BIEL-TAN', u'WARHOST', u'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachBrigade_ynnari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachVanguard_ynnari(DetachVanguard):
    army_name = u'Ynnari (Vanguard detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'vanguard_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'DRUKHARI', u'SAIM-HANN', u'SPIRIT HOST', u'IYANDEN', u'<WYCH CULT>', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'ALATOIC', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'WYCH CULT OF STRIFE']

    def __init__(self, parent=None):
        super(DetachVanguard_ynnari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachSpearhead_ynnari(DetachSpearhead):
    army_name = u'Ynnari (Spearhead detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'spearhead_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'IYANDEN', u'DRUKHARI', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'ASPECT WARRIOR', u'BIEL-TAN', u'WARHOST', u'<MASCUE>']

    def __init__(self, parent=None):
        super(DetachSpearhead_ynnari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachOutrider_ynnari(DetachOutrider):
    army_name = u'Ynnari (Outrider detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'outrider_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = [u'DRUKHARI', u'SAIM-HANN', u'IYANDEN', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'HARLEQUINS', u'ALATOIC', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachOutrider_ynnari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachCommand_ynnari(DetachCommand):
    army_name = u'Ynnari (Supreme command detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'command_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'<WYCH CULT>', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'ASPECT WARRIOR', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachCommand_ynnari, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachSuperHeavy_ynnari(DetachSuperHeavy):
    army_name = u'Ynnari (Super-Heavy detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'super_heavy_ynnari'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_ynnari, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachSuperHeavyAux_ynnari(DetachSuperHeavyAux):
    army_name = u'Ynnari (Super-Heavy auxilary detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'super_heavy_aux_ynnari'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_ynnari, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachAirWing_ynnari(DetachAirWing):
    army_name = u'Ynnari (Air Wing detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'air_wing_ynnari'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'DRUKHARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'ASPECT WARRIOR', u'BIEL-TAN', u'<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachAirWing_ynnari, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachAuxilary_ynnari(DetachAuxilary):
    army_name = u'Ynnari (Auxilary Support Detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'Auxilary_ynnari'
    army_factions = [u'YNNARI']

    def __init__(self, parent=None):
        super(DetachAuxilary_ynnari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachPatrol_nurgle(DetachPatrol):
    army_name = u'Nurgle (Patrol detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'patrol_nurgle'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'HERETIC ASTARTES', u'NURGLE', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPatrol_nurgle, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBatallion_nurgle(DetachBatallion):
    army_name = u'Nurgle (Batallion detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'batallion_nurgle'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'HERETIC ASTARTES', u'NURGLE', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachBatallion_nurgle, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBrigade_nurgle(DetachBrigade):
    army_name = u'Nurgle (Brigade detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'brigade_nurgle'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'NURGLE', u'IRON WARRIORS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachBrigade_nurgle, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachVanguard_nurgle(DetachVanguard):
    army_name = u'Nurgle (Vanguard detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'vanguard_nurgle'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'NURGLE', u'IRON WARRIORS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachVanguard_nurgle, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSpearhead_nurgle(DetachSpearhead):
    army_name = u'Nurgle (Spearhead detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'spearhead_nurgle'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'HERETIC ASTARTES', u'NURGLE', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachSpearhead_nurgle, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachOutrider_nurgle(DetachOutrider):
    army_name = u'Nurgle (Outrider detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'outrider_nurgle'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'NURGLE', u'IRON WARRIORS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachOutrider_nurgle, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachCommand_nurgle(DetachCommand):
    army_name = u'Nurgle (Supreme command detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'command_nurgle'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachCommand_nurgle, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'transports': True, u'parent': parent, })
        self.lords.add_classes([Mortarion])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavyAux_nurgle(DetachSuperHeavyAux):
    army_name = u'Nurgle (Super-Heavy auxilary detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'super_heavy_aux_nurgle'
    army_factions = [u'HERETIC ASTARTES', u'NURGLE', u'DEATH GUARD', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_nurgle, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Mortarion])
        return None


class DetachAirWing_nurgle(DetachAirWing):
    army_name = u'Nurgle (Air Wing detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'air_wing_nurgle'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'RED CORSAIRS', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_nurgle, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachFort_nurgle(DetachFort):
    army_name = u'Nurgle (Fortification Network)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'fort_nurgle'
    army_factions = [u'DAEMON', u'NURGLE', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachFort_nurgle, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([FeculentGnarlmaws])
        return None


class DetachAuxilary_nurgle(DetachAuxilary):
    army_name = u'Nurgle (Auxilary Support Detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'Auxilary_nurgle'
    army_factions = [u'NURGLE']

    def __init__(self, parent=None):
        super(DetachAuxilary_nurgle, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPatrol__haemunculus_coven_(DetachPatrol):
    army_name = u'<Haemunculus Coven> (Patrol detachment)'
    faction_base = u'<HAEMUNCULUS COVEN>'
    alternate_factions = [u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE']
    army_id = u'patrol__haemunculus_coven_'
    army_factions = [u'<HAEMUNCULUS COVEN>', u'AELDARI', u'DRUKHARI', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE']

    def __init__(self, parent=None):
        super(DetachPatrol__haemunculus_coven_, self).__init__(*[], **{u'heavy': True, u'elite': True, u'hq': True, u'transports': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachBatallion__haemunculus_coven_(DetachBatallion):
    army_name = u'<Haemunculus Coven> (Batallion detachment)'
    faction_base = u'<HAEMUNCULUS COVEN>'
    alternate_factions = [u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE']
    army_id = u'batallion__haemunculus_coven_'
    army_factions = [u'<HAEMUNCULUS COVEN>', u'AELDARI', u'DRUKHARI', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE']

    def __init__(self, parent=None):
        super(DetachBatallion__haemunculus_coven_, self).__init__(*[], **{u'heavy': True, u'elite': True, u'hq': True, u'transports': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachVanguard__haemunculus_coven_(DetachVanguard):
    army_name = u'<Haemunculus Coven> (Vanguard detachment)'
    faction_base = u'<HAEMUNCULUS COVEN>'
    alternate_factions = [u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE']
    army_id = u'vanguard__haemunculus_coven_'
    army_factions = [u'<HAEMUNCULUS COVEN>', u'AELDARI', u'DRUKHARI', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE']

    def __init__(self, parent=None):
        super(DetachVanguard__haemunculus_coven_, self).__init__(*[], **{u'heavy': True, u'elite': True, u'hq': True, u'transports': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachSpearhead__haemunculus_coven_(DetachSpearhead):
    army_name = u'<Haemunculus Coven> (Spearhead detachment)'
    faction_base = u'<HAEMUNCULUS COVEN>'
    alternate_factions = [u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE']
    army_id = u'spearhead__haemunculus_coven_'
    army_factions = [u'<HAEMUNCULUS COVEN>', u'AELDARI', u'DRUKHARI', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE']

    def __init__(self, parent=None):
        super(DetachSpearhead__haemunculus_coven_, self).__init__(*[], **{u'heavy': True, u'elite': True, u'hq': True, u'transports': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachCommand__haemunculus_coven_(DetachCommand):
    army_name = u'<Haemunculus Coven> (Supreme command detachment)'
    faction_base = u'<HAEMUNCULUS COVEN>'
    alternate_factions = [u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE']
    army_id = u'command__haemunculus_coven_'
    army_factions = [u'<HAEMUNCULUS COVEN>', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'DRUKHARI', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachCommand__haemunculus_coven_, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.elite.add_classes([Grotesques])
        return None


class DetachAuxilary__haemunculus_coven_(DetachAuxilary):
    army_name = u'<Haemunculus Coven> (Auxilary Support Detachment)'
    faction_base = u'<HAEMUNCULUS COVEN>'
    alternate_factions = [u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE']
    army_id = u'Auxilary__haemunculus_coven_'
    army_factions = [u'<HAEMUNCULUS COVEN>', u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE']

    def __init__(self, parent=None):
        super(DetachAuxilary__haemunculus_coven_, self).__init__(*[], **{u'heavy': True, u'elite': True, u'hq': True, u'transports': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachSuperHeavy__household_(KnightSuperHeavy):
    army_name = u'<Household> (Super-Heavy detachment)'
    faction_base = u'<HOUSEHOLD>'
    alternate_factions = ['TERRIN', 'GRIFFIN', 'HAWKSHROUD', 'CADMUS', 'MORTAN', 'RAVEN', 'TARANIS', 'KRAST', 'VULKER']
    army_id = u'super_heavy__household_'
    army_factions = [u'<HOUSEHOLD>', u'IMPERIUM', u'IMPERIAL KNIGHTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__household_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, ArmigerHelverins, KnightPreceptor, KnightCastellan, CanisRex, KnightValiant, KnightPorphyron, KnightAcheron, KnightAtropos, KnightCastigator, KnightLancer, KnightMagaera, KnightStyrix])
        return None


class DetachSuperHeavyAux__household_(DetachSuperHeavyAux):
    army_name = u'<Household> (Super-Heavy auxilary detachment)'
    faction_base = u'<HOUSEHOLD>'
    alternate_factions = ['TERRIN', 'GRIFFIN', 'HAWKSHROUD', 'CADMUS', 'MORTAN', 'RAVEN', 'TARANIS', 'KRAST', 'VULKER']
    army_id = u'super_heavy_aux__household_'
    army_factions = [u'<HOUSEHOLD>', u'IMPERIUM', u'IMPERIAL KNIGHTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__household_, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, ArmigerHelverins, KnightPreceptor, KnightCastellan, CanisRex, KnightValiant, KnightPorphyron, KnightAcheron, KnightAtropos, KnightCastigator, KnightLancer, KnightMagaera, KnightStyrix])
        return None


class DetachSuperHeavy_imperial_knights(KnightSuperHeavy):
    army_name = u'Imperial Knights (Super-Heavy detachment)'
    faction_base = u'IMPERIAL KNIGHTS'
    alternate_factions = []
    army_id = u'super_heavy_imperial_knights'
    army_factions = [u'<HOUSEHOLD>', u'IMPERIUM', u'IMPERIAL KNIGHTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_imperial_knights, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, ArmigerHelverins, KnightPreceptor, KnightCastellan, CanisRex, KnightValiant, KnightPorphyron, KnightAcheron, KnightAtropos, KnightCastigator, KnightLancer, KnightMagaera, KnightStyrix])
        return None


class DetachSuperHeavyAux_imperial_knights(DetachSuperHeavyAux):
    army_name = u'Imperial Knights (Super-Heavy auxilary detachment)'
    faction_base = u'IMPERIAL KNIGHTS'
    alternate_factions = []
    army_id = u'super_heavy_aux_imperial_knights'
    army_factions = [u'<HOUSEHOLD>', u'IMPERIUM', u'IMPERIAL KNIGHTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_imperial_knights, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, ArmigerHelverins, KnightPreceptor, KnightCastellan, CanisRex, KnightValiant, KnightPorphyron, KnightAcheron, KnightAtropos, KnightCastigator, KnightLancer, KnightMagaera, KnightStyrix])
        return None


class DetachFort_imperial_knights(DetachFort):
    army_name = u'Imperial Knights (Fortification Network)'
    faction_base = u'IMPERIAL KNIGHTS'
    alternate_factions = []
    army_id = u'fort_imperial_knights'
    army_factions = [u'IMPERIUM', u'IMPERIAL KNIGHTS']

    def __init__(self, parent=None):
        super(DetachFort_imperial_knights, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([SacristanForgeshrine])
        return None


class DetachVanguard_death_company(DetachVanguard):
    army_name = u'Death Company (Vanguard detachment)'
    faction_base = u'DEATH COMPANY'
    alternate_factions = []
    army_id = u'vanguard_death_company'
    army_factions = [u'DEATH COMPANY', u'IMPERIUM', u'BLOOD ANGELS', u'ADEPTUS ASTARTES']

    hq_sec = DCCommand

    def __init__(self, parent=None):
        super(DetachVanguard_death_company, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([DCTycho, Lemartes])
        self.elite.add_classes([DeathCompany, DeathCompanyDreadnought])
        return None


class DetachCommand_death_company(DetachCommand):
    army_name = u'Death Company (Supreme command detachment)'
    faction_base = u'DEATH COMPANY'
    alternate_factions = []
    army_id = u'command_death_company'
    army_factions = [u'DEATH COMPANY', u'IMPERIUM', u'BLOOD ANGELS', u'ADEPTUS ASTARTES']

    hq_sec = DCCommand

    def __init__(self, parent=None):
        super(DetachCommand_death_company, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([DCTycho, Lemartes])
        self.elite.add_classes([DeathCompany, DeathCompanyDreadnought])
        return None


class DetachAuxilary_death_company(DetachAuxilary):
    army_name = u'Death Company (Auxilary Support Detachment)'
    faction_base = u'DEATH COMPANY'
    alternate_factions = []
    army_id = u'Auxilary_death_company'
    army_factions = [u'DEATH COMPANY']

    def __init__(self, parent=None):
        super(DetachAuxilary_death_company, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([DCTycho, Lemartes])
        self.elite.add_classes([DeathCompany, DeathCompanyDreadnought])
        return None


class DetachVanguard_spirit_host(DetachVanguard):
    army_name = u'Spirit Host (Vanguard detachment)'
    faction_base = u'SPIRIT HOST'
    alternate_factions = []
    army_id = u'vanguard_spirit_host'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachVanguard_spirit_host, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'fliers': True, u'parent': parent, })
        self.heavy.add_classes([Wraithlord])
        self.hq.add_classes([Spiritseer])
        self.elite.add_classes([Wraithblades, Wraithguard])
        self.fliers.add_classes([Hemlock])
        return None


class DetachSpearhead_spirit_host(DetachSpearhead):
    army_name = u'Spirit Host (Spearhead detachment)'
    faction_base = u'SPIRIT HOST'
    alternate_factions = []
    army_id = u'spearhead_spirit_host'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSpearhead_spirit_host, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'fliers': True, u'parent': parent, })
        self.heavy.add_classes([Wraithlord])
        self.hq.add_classes([Spiritseer])
        self.elite.add_classes([Wraithblades, Wraithguard])
        self.fliers.add_classes([Hemlock])
        return None


class DetachCommand_spirit_host(DetachCommand):
    army_name = u'Spirit Host (Supreme command detachment)'
    faction_base = u'SPIRIT HOST'
    alternate_factions = []
    army_id = u'command_spirit_host'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachCommand_spirit_host, self).__init__(*[], **{u'lords': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.hq.add_classes([Spiritseer])
        self.elite.add_classes([Wraithblades, Wraithguard])
        return None


class DetachSuperHeavy_spirit_host(DetachSuperHeavy):
    army_name = u'Spirit Host (Super-Heavy detachment)'
    faction_base = u'SPIRIT HOST'
    alternate_factions = []
    army_id = u'super_heavy_spirit_host'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_spirit_host, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachSuperHeavyAux_spirit_host(DetachSuperHeavyAux):
    army_name = u'Spirit Host (Super-Heavy auxilary detachment)'
    faction_base = u'SPIRIT HOST'
    alternate_factions = []
    army_id = u'super_heavy_aux_spirit_host'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_spirit_host, self).__init__(*[], **{u'lords': True, u'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachAirWing_spirit_host(DetachAirWing):
    army_name = u'Spirit Host (Air Wing detachment)'
    faction_base = u'SPIRIT HOST'
    alternate_factions = []
    army_id = u'air_wing_spirit_host'
    army_factions = [u'SAIM-HANN', u'SPIRIT HOST', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachAirWing_spirit_host, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Hemlock])
        return None


class DetachAuxilary_spirit_host(DetachAuxilary):
    army_name = u'Spirit Host (Auxilary Support Detachment)'
    faction_base = u'SPIRIT HOST'
    alternate_factions = []
    army_id = u'Auxilary_spirit_host'
    army_factions = [u'SPIRIT HOST']

    def __init__(self, parent=None):
        super(DetachAuxilary_spirit_host, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'fliers': True, u'parent': parent, })
        self.heavy.add_classes([Wraithlord])
        self.hq.add_classes([Spiritseer])
        self.elite.add_classes([Wraithblades, Wraithguard])
        self.fliers.add_classes([Hemlock])
        return None


class DetachPatrol__order_(DetachPatrol):
    army_name = u'<Order> (Patrol detachment)'
    faction_base = u'<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = u'patrol__order_'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachPatrol__order_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachBatallion__order_(DetachBatallion):
    army_name = u'<Order> (Batallion detachment)'
    faction_base = u'<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = u'batallion__order_'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachBatallion__order_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachBrigade__order_(DetachBrigade):
    army_name = u'<Order> (Brigade detachment)'
    faction_base = u'<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = u'brigade__order_'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachBrigade__order_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachVanguard__order_(DetachVanguard):
    army_name = u'<Order> (Vanguard detachment)'
    faction_base = u'<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = u'vanguard__order_'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachVanguard__order_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachSpearhead__order_(DetachSpearhead):
    army_name = u'<Order> (Spearhead detachment)'
    faction_base = u'<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = u'spearhead__order_'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachSpearhead__order_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachOutrider__order_(DetachOutrider):
    army_name = u'<Order> (Outrider detachment)'
    faction_base = u'<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = u'outrider__order_'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachOutrider__order_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachCommand__order_(DetachCommand):
    army_name = u'<Order> (Supreme command detachment)'
    faction_base = u'<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = u'command__order_'
    army_factions = [u'IMPERIUM', u'ADEPTUS MINISTORUM', u'ADEPTA SORORITAS', u'<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachCommand__order_, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachAuxilary__order_(DetachAuxilary):
    army_name = u'<Order> (Auxilary Support Detachment)'
    faction_base = u'<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = u'Auxilary__order_'
    army_factions = [u'<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachAuxilary__order_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachAuxilary_officio_assassinorum(DetachAuxilary):
    army_name = u'Officio Assassinorum (Auxilary Support Detachment)'
    faction_base = u'OFFICIO ASSASSINORUM'
    alternate_factions = []
    army_id = u'Auxilary_officio_assassinorum'
    army_factions = [u'OFFICIO ASSASSINORUM']

    def __init__(self, parent=None):
        super(DetachAuxilary_officio_assassinorum, self).__init__(*[], **{u'elite': True, u'parent': parent, })
        self.elite.add_classes([VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin])
        return None


class DetachPatrol_adeptus_custodes(DetachPatrol):
    army_name = u'Adeptus Custodes (Patrol detachment)'
    faction_base = u'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = u'patrol_adeptus_custodes'
    army_factions = [u'IMPERIUM', u'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachPatrol_adeptus_custodes, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachBatallion_adeptus_custodes(DetachBatallion):
    army_name = u'Adeptus Custodes (Batallion detachment)'
    faction_base = u'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = u'batallion_adeptus_custodes'
    army_factions = [u'IMPERIUM', u'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachBatallion_adeptus_custodes, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachBrigade_adeptus_custodes(DetachBrigade):
    army_name = u'Adeptus Custodes (Brigade detachment)'
    faction_base = u'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = u'brigade_adeptus_custodes'
    army_factions = [u'IMPERIUM', u'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachBrigade_adeptus_custodes, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachVanguard_adeptus_custodes(DetachVanguard):
    army_name = u'Adeptus Custodes (Vanguard detachment)'
    faction_base = u'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = u'vanguard_adeptus_custodes'
    army_factions = [u'IMPERIUM', u'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachVanguard_adeptus_custodes, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachSpearhead_adeptus_custodes(DetachSpearhead):
    army_name = u'Adeptus Custodes (Spearhead detachment)'
    faction_base = u'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = u'spearhead_adeptus_custodes'
    army_factions = [u'IMPERIUM', u'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachSpearhead_adeptus_custodes, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachOutrider_adeptus_custodes(DetachOutrider):
    army_name = u'Adeptus Custodes (Outrider detachment)'
    faction_base = u'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = u'outrider_adeptus_custodes'
    army_factions = [u'IMPERIUM', u'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachOutrider_adeptus_custodes, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachCommand_adeptus_custodes(DetachCommand):
    army_name = u'Adeptus Custodes (Supreme command detachment)'
    faction_base = u'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = u'command_adeptus_custodes'
    army_factions = [u'IMPERIUM', u'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachCommand_adeptus_custodes, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        return None


class DetachAuxilary_adeptus_custodes(DetachAuxilary):
    army_name = u'Adeptus Custodes (Auxilary Support Detachment)'
    faction_base = u'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = u'Auxilary_adeptus_custodes'
    army_factions = [u'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachAuxilary_adeptus_custodes, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachPatrol_aspect_warrior(DetachPatrol):
    army_name = u'Aspect Warrior (Patrol detachment)'
    faction_base = u'ASPECT WARRIOR'
    alternate_factions = []
    army_id = u'patrol_aspect_warrior'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachPatrol_aspect_warrior, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachBatallion_aspect_warrior(DetachBatallion):
    army_name = u'Aspect Warrior (Batallion detachment)'
    faction_base = u'ASPECT WARRIOR'
    alternate_factions = []
    army_id = u'batallion_aspect_warrior'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachBatallion_aspect_warrior, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachBrigade_aspect_warrior(DetachBrigade):
    army_name = u'Aspect Warrior (Brigade detachment)'
    faction_base = u'ASPECT WARRIOR'
    alternate_factions = []
    army_id = u'brigade_aspect_warrior'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachBrigade_aspect_warrior, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachVanguard_aspect_warrior(DetachVanguard):
    army_name = u'Aspect Warrior (Vanguard detachment)'
    faction_base = u'ASPECT WARRIOR'
    alternate_factions = []
    army_id = u'vanguard_aspect_warrior'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachVanguard_aspect_warrior, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachSpearhead_aspect_warrior(DetachSpearhead):
    army_name = u'Aspect Warrior (Spearhead detachment)'
    faction_base = u'ASPECT WARRIOR'
    alternate_factions = []
    army_id = u'spearhead_aspect_warrior'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSpearhead_aspect_warrior, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachOutrider_aspect_warrior(DetachOutrider):
    army_name = u'Aspect Warrior (Outrider detachment)'
    faction_base = u'ASPECT WARRIOR'
    alternate_factions = []
    army_id = u'outrider_aspect_warrior'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachOutrider_aspect_warrior, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachCommand_aspect_warrior(DetachCommand):
    army_name = u'Aspect Warrior (Supreme command detachment)'
    faction_base = u'ASPECT WARRIOR'
    alternate_factions = []
    army_id = u'command_aspect_warrior'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachCommand_aspect_warrior, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachAirWing_aspect_warrior(DetachAirWing):
    army_name = u'Aspect Warrior (Air Wing detachment)'
    faction_base = u'ASPECT WARRIOR'
    alternate_factions = []
    army_id = u'air_wing_aspect_warrior'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachAirWing_aspect_warrior, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        return None


class DetachAuxilary_aspect_warrior(DetachAuxilary):
    army_name = u'Aspect Warrior (Auxilary Support Detachment)'
    faction_base = u'ASPECT WARRIOR'
    alternate_factions = []
    army_id = u'Auxilary_aspect_warrior'
    army_factions = [u'ASPECT WARRIOR']

    def __init__(self, parent=None):
        super(DetachAuxilary_aspect_warrior, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachPatrol_grey_knights(DetachPatrol):
    army_name = u'Grey Knights (Patrol detachment)'
    faction_base = u'GREY KNIGHTS'
    alternate_factions = []
    army_id = u'patrol_grey_knights'
    army_factions = [u'IMPERIUM', u'GREY KNIGHTS', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPatrol_grey_knights, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachBatallion_grey_knights(DetachBatallion):
    army_name = u'Grey Knights (Batallion detachment)'
    faction_base = u'GREY KNIGHTS'
    alternate_factions = []
    army_id = u'batallion_grey_knights'
    army_factions = [u'IMPERIUM', u'GREY KNIGHTS', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachBatallion_grey_knights, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachBrigade_grey_knights(DetachBrigade):
    army_name = u'Grey Knights (Brigade detachment)'
    faction_base = u'GREY KNIGHTS'
    alternate_factions = []
    army_id = u'brigade_grey_knights'
    army_factions = [u'IMPERIUM', u'GREY KNIGHTS', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachBrigade_grey_knights, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachVanguard_grey_knights(DetachVanguard):
    army_name = u'Grey Knights (Vanguard detachment)'
    faction_base = u'GREY KNIGHTS'
    alternate_factions = []
    army_id = u'vanguard_grey_knights'
    army_factions = [u'IMPERIUM', u'GREY KNIGHTS', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachVanguard_grey_knights, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachSpearhead_grey_knights(DetachSpearhead):
    army_name = u'Grey Knights (Spearhead detachment)'
    faction_base = u'GREY KNIGHTS'
    alternate_factions = []
    army_id = u'spearhead_grey_knights'
    army_factions = [u'IMPERIUM', u'GREY KNIGHTS', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachSpearhead_grey_knights, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachOutrider_grey_knights(DetachOutrider):
    army_name = u'Grey Knights (Outrider detachment)'
    faction_base = u'GREY KNIGHTS'
    alternate_factions = []
    army_id = u'outrider_grey_knights'
    army_factions = [u'IMPERIUM', u'GREY KNIGHTS', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachOutrider_grey_knights, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachCommand_grey_knights(DetachCommand):
    army_name = u'Grey Knights (Supreme command detachment)'
    faction_base = u'GREY KNIGHTS'
    alternate_factions = []
    army_id = u'command_grey_knights'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'GREY KNIGHTS']

    def __init__(self, parent=None):
        super(DetachCommand_grey_knights, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([GKRhino, GKRazorback])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        return None


class DetachAirWing_grey_knights(DetachAirWing):
    army_name = u'Grey Knights (Air Wing detachment)'
    faction_base = u'GREY KNIGHTS'
    alternate_factions = []
    army_id = u'air_wing_grey_knights'
    army_factions = [u'IMPERIUM', u'ADEPTUS ASTARTES', u'GREY KNIGHTS']

    def __init__(self, parent=None):
        super(DetachAirWing_grey_knights, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        return None


class DetachAuxilary_grey_knights(DetachAuxilary):
    army_name = u'Grey Knights (Auxilary Support Detachment)'
    faction_base = u'GREY KNIGHTS'
    alternate_factions = []
    army_id = u'Auxilary_grey_knights'
    army_factions = [u'GREY KNIGHTS']

    def __init__(self, parent=None):
        super(DetachAuxilary_grey_knights, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachPatrol_daemon(DetachPatrol):
    army_name = u'Daemon (Patrol detachment)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'patrol_daemon'
    army_factions = [u'DAEMON', u'TZEENTCH', u'CHAOS', u'KHORNE', u'SLAANESH', u'NURGLE']

    def __init__(self, parent=None):
        super(DetachPatrol_daemon, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachBatallion_daemon(DetachBatallion):
    army_name = u'Daemon (Batallion detachment)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'batallion_daemon'
    army_factions = [u'DAEMON', u'TZEENTCH', u'CHAOS', u'KHORNE', u'SLAANESH', u'NURGLE']

    def __init__(self, parent=None):
        super(DetachBatallion_daemon, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachBrigade_daemon(DetachBrigade):
    army_name = u'Daemon (Brigade detachment)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'brigade_daemon'
    army_factions = [u'DAEMON', u'TZEENTCH', u'CHAOS', u'KHORNE', u'SLAANESH', u'NURGLE']

    def __init__(self, parent=None):
        super(DetachBrigade_daemon, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachVanguard_daemon(DetachVanguard):
    army_name = u'Daemon (Vanguard detachment)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'vanguard_daemon'
    army_factions = [u'DAEMON', u'TZEENTCH', u'CHAOS', u'KHORNE', u'SLAANESH', u'NURGLE']

    def __init__(self, parent=None):
        super(DetachVanguard_daemon, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachSpearhead_daemon(DetachSpearhead):
    army_name = u'Daemon (Spearhead detachment)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'spearhead_daemon'
    army_factions = [u'DAEMON', u'TZEENTCH', u'CHAOS', u'KHORNE', u'SLAANESH', u'NURGLE']

    def __init__(self, parent=None):
        super(DetachSpearhead_daemon, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachOutrider_daemon(DetachOutrider):
    army_name = u'Daemon (Outrider detachment)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'outrider_daemon'
    army_factions = [u'DAEMON', u'TZEENTCH', u'CHAOS', u'KHORNE', u'SLAANESH', u'NURGLE']

    def __init__(self, parent=None):
        super(DetachOutrider_daemon, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachCommand_daemon(DetachCommand):
    army_name = u'Daemon (Supreme command detachment)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'command_daemon'
    army_factions = [u'DAEMON', u'TZEENTCH', u'CHAOS', u'KHORNE', u'SLAANESH', u'NURGLE']

    def __init__(self, parent=None):
        super(DetachCommand_daemon, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        return None


class DetachFort_daemon(DetachFort):
    army_name = u'Daemon (Fortification Network)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'fort_daemon'
    army_factions = [u'DAEMON', u'NURGLE', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachFort_daemon, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([FeculentGnarlmaws])
        return None


class DetachAuxilary_daemon(DetachAuxilary):
    army_name = u'Daemon (Auxilary Support Detachment)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'Auxilary_daemon'
    army_factions = [u'DAEMON']

    def __init__(self, parent=None):
        super(DetachAuxilary_daemon, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachPatrol_adeptus_mechanicus(DetachPatrol):
    army_name = u'Adeptus Mechanicus (Patrol detachment)'
    faction_base = u'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = u'patrol_adeptus_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPatrol_adeptus_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachBatallion_adeptus_mechanicus(DetachBatallion):
    army_name = u'Adeptus Mechanicus (Batallion detachment)'
    faction_base = u'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = u'batallion_adeptus_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachBatallion_adeptus_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachBrigade_adeptus_mechanicus(DetachBrigade):
    army_name = u'Adeptus Mechanicus (Brigade detachment)'
    faction_base = u'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = u'brigade_adeptus_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachBrigade_adeptus_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachVanguard_adeptus_mechanicus(DetachVanguard):
    army_name = u'Adeptus Mechanicus (Vanguard detachment)'
    faction_base = u'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = u'vanguard_adeptus_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'ASTRA MILITARUM', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachVanguard_adeptus_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachSpearhead_adeptus_mechanicus(DetachSpearhead):
    army_name = u'Adeptus Mechanicus (Spearhead detachment)'
    faction_base = u'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = u'spearhead_adeptus_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachSpearhead_adeptus_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachOutrider_adeptus_mechanicus(DetachOutrider):
    army_name = u'Adeptus Mechanicus (Outrider detachment)'
    faction_base = u'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = u'outrider_adeptus_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachOutrider_adeptus_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachCommand_adeptus_mechanicus(DetachCommand):
    army_name = u'Adeptus Mechanicus (Supreme command detachment)'
    faction_base = u'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = u'command_adeptus_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'ASTRA MILITARUM', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachCommand_adeptus_mechanicus, self).__init__(*[], **{u'hq': True, u'elite': True, u'parent': parent, })
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        return None


class DetachAuxilary_adeptus_mechanicus(DetachAuxilary):
    army_name = u'Adeptus Mechanicus (Auxilary Support Detachment)'
    faction_base = u'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = u'Auxilary_adeptus_mechanicus'
    army_factions = [u'ADEPTUS MECHANICUS']

    def __init__(self, parent=None):
        super(DetachAuxilary_adeptus_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachAuxilary_jokaero(DetachAuxilary):
    army_name = u'Jokaero (Auxilary Support Detachment)'
    faction_base = u'JOKAERO'
    alternate_factions = []
    army_id = u'Auxilary_jokaero'
    army_factions = [u'JOKAERO']

    def __init__(self, parent=None):
        super(DetachAuxilary_jokaero, self).__init__(*[], **{u'elite': True, u'parent': parent, })
        self.elite.add_classes([Jokaero])
        return None


class DetachPatrol_warhost(DetachPatrol):
    army_name = u'Warhost (Patrol detachment)'
    faction_base = u'WARHOST'
    alternate_factions = []
    army_id = u'patrol_warhost'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachPatrol_warhost, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachBatallion_warhost(DetachBatallion):
    army_name = u'Warhost (Batallion detachment)'
    faction_base = u'WARHOST'
    alternate_factions = []
    army_id = u'batallion_warhost'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachBatallion_warhost, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachBrigade_warhost(DetachBrigade):
    army_name = u'Warhost (Brigade detachment)'
    faction_base = u'WARHOST'
    alternate_factions = []
    army_id = u'brigade_warhost'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachBrigade_warhost, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachVanguard_warhost(DetachVanguard):
    army_name = u'Warhost (Vanguard detachment)'
    faction_base = u'WARHOST'
    alternate_factions = []
    army_id = u'vanguard_warhost'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachVanguard_warhost, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachSpearhead_warhost(DetachSpearhead):
    army_name = u'Warhost (Spearhead detachment)'
    faction_base = u'WARHOST'
    alternate_factions = []
    army_id = u'spearhead_warhost'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachSpearhead_warhost, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachOutrider_warhost(DetachOutrider):
    army_name = u'Warhost (Outrider detachment)'
    faction_base = u'WARHOST'
    alternate_factions = []
    army_id = u'outrider_warhost'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachOutrider_warhost, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachCommand_warhost(DetachCommand):
    army_name = u'Warhost (Supreme command detachment)'
    faction_base = u'WARHOST'
    alternate_factions = []
    army_id = u'command_warhost'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachCommand_warhost, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.elite.add_classes([Bonesinger])
        return None


class DetachAuxilary_warhost(DetachAuxilary):
    army_name = u'Warhost (Auxilary Support Detachment)'
    faction_base = u'WARHOST'
    alternate_factions = []
    army_id = u'Auxilary_warhost'
    army_factions = [u'WARHOST']

    def __init__(self, parent=None):
        super(DetachAuxilary_warhost, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachPatrol__wych_cult_(DetachPatrol):
    army_name = u'<Wych Cult> (Patrol detachment)'
    faction_base = u'<WYCH CULT>'
    alternate_factions = [u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF']
    army_id = u'patrol__wych_cult_'
    army_factions = [u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'YNNARI', u'DRUKHARI', u'<WYCH CULT>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachPatrol__wych_cult_, self).__init__(*[], **{u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachBatallion__wych_cult_(DetachBatallion):
    army_name = u'<Wych Cult> (Batallion detachment)'
    faction_base = u'<WYCH CULT>'
    alternate_factions = [u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF']
    army_id = u'batallion__wych_cult_'
    army_factions = [u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'YNNARI', u'DRUKHARI', u'<WYCH CULT>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachBatallion__wych_cult_, self).__init__(*[], **{u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachVanguard__wych_cult_(DetachVanguard):
    army_name = u'<Wych Cult> (Vanguard detachment)'
    faction_base = u'<WYCH CULT>'
    alternate_factions = [u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF']
    army_id = u'vanguard__wych_cult_'
    army_factions = [u'DRUKHARI', u'YNNARI', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'<WYCH CULT>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachVanguard__wych_cult_, self).__init__(*[], **{u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachOutrider__wych_cult_(DetachOutrider):
    army_name = u'<Wych Cult> (Outrider detachment)'
    faction_base = u'<WYCH CULT>'
    alternate_factions = [u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF']
    army_id = u'outrider__wych_cult_'
    army_factions = [u'DRUKHARI', u'YNNARI', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'<WYCH CULT>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachOutrider__wych_cult_, self).__init__(*[], **{u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachCommand__wych_cult_(DetachCommand):
    army_name = u'<Wych Cult> (Supreme command detachment)'
    faction_base = u'<WYCH CULT>'
    alternate_factions = [u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF']
    army_id = u'command__wych_cult_'
    army_factions = [u'DRUKHARI', u'YNNARI', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'<WYCH CULT>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachCommand__wych_cult_, self).__init__(*[], **{u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachAirWing__wych_cult_(DetachAirWing):
    army_name = u'<Wych Cult> (Air Wing detachment)'
    faction_base = u'<WYCH CULT>'
    alternate_factions = [u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF']
    army_id = u'air_wing__wych_cult_'
    army_factions = [u'AELDARI', u'DRUKHARI', u'<WYCH CULT>', u'YNNARI', u'<KABAL>', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF']

    def __init__(self, parent=None):
        super(DetachAirWing__wych_cult_, self).__init__(*[], **{u'fliers': True, u'parent': parent, })
        self.fliers.add_classes([Razorwing, Voidraven])
        return None


class DetachAuxilary__wych_cult_(DetachAuxilary):
    army_name = u'<Wych Cult> (Auxilary Support Detachment)'
    faction_base = u'<WYCH CULT>'
    alternate_factions = [u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF']
    army_id = u'Auxilary__wych_cult_'
    army_factions = [u'<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachAuxilary__wych_cult_, self).__init__(*[], **{u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachAuxilary_militarum_auxilla(DetachAuxilary):
    army_name = u'Militarum Auxilla (Auxilary Support Detachment)'
    faction_base = u'MILITARUM AUXILLA'
    alternate_factions = []
    army_id = u'Auxilary_militarum_auxilla'
    army_factions = [u'MILITARUM AUXILLA']

    def __init__(self, parent=None):
        super(DetachAuxilary_militarum_auxilla, self).__init__(*[], **{u'elite': True, u'parent': parent, })
        self.elite.add_classes([Bullgryns, Ogryns, Ratlings, Nork, OgrynBodyguard])
        return None

detachments = [DetachAuxilary_canoptek, DetachPatrol__sept_,
               DetachBatallion__sept_, DetachBrigade__sept_,
               DetachVanguard__sept_, DetachSpearhead__sept_,
               DetachOutrider__sept_, DetachCommand__sept_,
               DetachSuperHeavy__sept_, DetachSuperHeavyAux__sept_,
               DetachAirWing__sept_, DetachFort__sept_,
               DetachAuxilary__sept_, DetachVanguard_ravenwing,
               DetachOutrider_ravenwing, DetachCommand_ravenwing,
               DetachAirWing_ravenwing, DetachAuxilary_ravenwing,
               DetachPatrol_blood_angels,
               DetachBatallion_blood_angels,
               DetachBrigade_blood_angels,
               DetachVanguard_blood_angels,
               DetachSpearhead_blood_angels,
               DetachOutrider_blood_angels,
               DetachCommand_blood_angels,
               DetachSuperHeavy_blood_angels,
               DetachSuperHeavyAux_blood_angels,
               DetachAirWing_blood_angels,
               DetachAuxilary_blood_angels, DetachPatrol_asuryani,
               DetachBatallion_asuryani, DetachBrigade_asuryani,
               DetachVanguard_asuryani, DetachSpearhead_asuryani,
               DetachOutrider_asuryani, DetachCommand_asuryani,
               DetachSuperHeavy_asuryani,
               DetachSuperHeavyAux_asuryani, DetachAirWing_asuryani,
               DetachAuxilary_asuryani, DetachPatrol_slaanesh,
               DetachBatallion_slaanesh, DetachBrigade_slaanesh,
               DetachVanguard_slaanesh, DetachSpearhead_slaanesh,
               DetachOutrider_slaanesh, DetachCommand_slaanesh,
               DetachAirWing_slaanesh, DetachAuxilary_slaanesh,
               DetachAuxilary_skitarii, DetachVanguard_deathwing,
               DetachCommand_deathwing, DetachAuxilary_deathwing,
               DetachSpearhead_deathwing,
               DetachVanguard_astra_telepathica,
               DetachCommand_astra_telepathica,
               DetachAuxilary_astra_telepathica,
               DetachVanguard_incubi, DetachCommand_incubi,
               DetachAuxilary_incubi, DetachPatrol_astra_militarum,
               DetachBatallion_astra_militarum,
               DetachBrigade_astra_militarum,
               DetachVanguard_astra_militarum,
               DetachSpearhead_astra_militarum,
               DetachOutrider_astra_militarum,
               DetachCommand_astra_militarum,
               DetachSuperHeavy_astra_militarum,
               DetachSuperHeavyAux_astra_militarum,
               DetachAirWing_astra_militarum,
               DetachAuxilary_astra_militarum,
               DetachPatrol__regiment_, DetachBatallion__regiment_,
               DetachBrigade__regiment_, DetachVanguard__regiment_,
               DetachSpearhead__regiment_, DetachOutrider__regiment_,
               DetachCommand__regiment_, DetachSuperHeavy__regiment_,
               DetachSuperHeavyAux__regiment_,
               DetachAuxilary__regiment_,
               DetachAuxilary_legion_of_the_damned,
               DetachPatrol__mascue_, DetachBatallion__mascue_,
               DetachBrigade__mascue_, DetachVanguard__mascue_,
               DetachSpearhead__mascue_, DetachOutrider__mascue_,
               DetachCommand__mascue_, DetachAuxilary__mascue_,
               DetachPatrol_deathwatch, DetachBatallion_deathwatch,
               DetachBrigade_deathwatch, DetachVanguard_deathwatch,
               DetachSpearhead_deathwatch, DetachOutrider_deathwatch,
               DetachCommand_deathwatch,
               DetachSuperHeavy_deathwatch, DetachSuperHeavyAux_deathwatch,
               DetachAirWing_deathwatch,
               DetachAuxilary_deathwatch,
               DetachVanguard_officio_prefectus,
               DetachCommand_officio_prefectus,
               DetachAuxilary_officio_prefectus,
               DetachPatrol_space_wolves,
               DetachBatallion_space_wolves,
               DetachBrigade_space_wolves,
               DetachVanguard_space_wolves,
               DetachSpearhead_space_wolves,
               DetachOutrider_space_wolves,
               DetachCommand_space_wolves,
               DetachSuperHeavy_space_wolves, DetachSuperHeavyAux_space_wolves,
               DetachAirWing_space_wolves,
               DetachAuxilary_space_wolves, DetachPatrol_tzeentch,
               DetachBatallion_tzeentch, DetachBrigade_tzeentch,
               DetachVanguard_tzeentch, DetachSpearhead_tzeentch,
               DetachOutrider_tzeentch, DetachCommand_tzeentch,
               DetachSuperHeavyAux_tzeentch, DetachAirWing_tzeentch,
               DetachAuxilary_tzeentch, DetachPatrol_drukhari,
               DetachBatallion_drukhari, DetachBrigade_drukhari,
               DetachVanguard_drukhari, DetachSpearhead_drukhari,
               DetachOutrider_drukhari, DetachCommand_drukhari,
               DetachAirWing_drukhari, DetachAuxilary_drukhari,
               DetachPatrol_heretic_astartes,
               DetachBatallion_heretic_astartes,
               DetachBrigade_heretic_astartes,
               DetachVanguard_heretic_astartes,
               DetachSpearhead_heretic_astartes,
               DetachOutrider_heretic_astartes,
               DetachCommand_heretic_astartes,
               DetachSuperHeavy_heretic_astartes,
               DetachSuperHeavyAux_heretic_astartes,
               DetachAirWing_heretic_astartes,
               DetachAuxilary_heretic_astartes, DetachPatrol__legion_,
               DetachBatallion__legion_, DetachBrigade__legion_,
               DetachVanguard__legion_, DetachSpearhead__legion_,
               DetachOutrider__legion_, DetachCommand__legion_,
               DetachSuperHeavy__legion_,
               DetachSuperHeavyAux__legion_, DetachAirWing__legion_,
               DetachAuxilary__legion_, DetachPatrol_dark_angels,
               DetachBatallion_dark_angels, DetachBrigade_dark_angels,
               DetachVanguard_dark_angels,
               DetachSpearhead_dark_angels,
               DetachOutrider_dark_angels, DetachCommand_dark_angels,
               DetachSuperHeavy_dark_angels, DetachSuperHeavyAux_dark_angels,
               DetachAirWing_dark_angels, DetachAuxilary_dark_angels,
               DetachPatrol_khorne, DetachBatallion_khorne,
               DetachBrigade_khorne, DetachVanguard_khorne,
               DetachSpearhead_khorne, DetachOutrider_khorne,
               DetachCommand_khorne, DetachSuperHeavy_khorne,
               DetachSuperHeavyAux_khorne, DetachAirWing_khorne,
               DetachAuxilary_khorne,
               DetachSuperHeavy_questor_traitoris,
               DetachSuperHeavyAux_questor_traitoris,
               DetachVanguard_scholastica_psykana,
               DetachCommand_scholastica_psykana,
               DetachAuxilary_scholastica_psykana,
               DetachPatrol__forge_world_,
               DetachBatallion__forge_world_,
               DetachBrigade__forge_world_,
               DetachVanguard__forge_world_,
               DetachSpearhead__forge_world_,
               DetachOutrider__forge_world_,
               DetachCommand__forge_world_,
               DetachAuxilary__forge_world_, DetachPatrol_necrons,
               DetachBatallion_necrons, DetachBrigade_necrons,
               DetachVanguard_necrons, DetachSpearhead_necrons,
               DetachOutrider_necrons, DetachCommand_necrons,
               DetachSuperHeavy_necrons, DetachSuperHeavyAux_necrons,
               DetachAirWing_necrons, DetachAuxilary_necrons,
               DetachVanguard_fallen, DetachCommand_fallen,
               DetachAuxilary_fallen, DetachAuxilary_kroot,
               DetachPatrol_adeptus_ministorum,
               DetachBatallion_adeptus_ministorum,
               DetachBrigade_adeptus_ministorum,
               DetachVanguard_adeptus_ministorum,
               DetachSpearhead_adeptus_ministorum,
               DetachOutrider_adeptus_ministorum,
               DetachCommand_adeptus_ministorum,
               DetachAuxilary_adeptus_ministorum,
               DetachPatrol_imperium, DetachBatallion_imperium,
               DetachBrigade_imperium, DetachVanguard_imperium,
               DetachSpearhead_imperium, DetachOutrider_imperium,
               DetachCommand_imperium, DetachSuperHeavy_imperium,
               DetachSuperHeavyAux_imperium, DetachAirWing_imperium,
               DetachAuxilary_imperium,
               DetachSuperHeavy_imperial_knights,
               DetachSuperHeavyAux_imperial_knights,
               DetachFort_imperial_knights,
               DetachSuperHeavy_questor_imperialis,
               DetachSuperHeavyAux_questor_imperialis,
               DetachAirWing_aeronautica_imperialis,
               DetachAuxilary_aeronautica_imperialis,
               DetachPatrol_chaos, DetachBatallion_chaos,
               DetachBrigade_chaos, DetachVanguard_chaos,
               DetachSpearhead_chaos, DetachOutrider_chaos,
               DetachCommand_chaos, DetachSuperHeavy_chaos,
               DetachSuperHeavyAux_chaos, DetachAirWing_chaos,
               DetachFort_chaos, DetachAuxilary_chaos,
               DetachSuperHeavy_c_tan_shards,
               DetachSuperHeavyAux_c_tan_shards,
               DetachAuxilary_c_tan_shards,
               DetachVanguard_inquisition, DetachCommand_inquisition,
               DetachAuxilary_inquisition, DetachPatrol_tyranids,
               DetachBatallion_tyranids, DetachBrigade_tyranids,
               DetachVanguard_tyranids, DetachSpearhead_tyranids,
               DetachOutrider_tyranids, DetachCommand_tyranids,
               DetachAirWing_tyranids, DetachFort_tyranids,
               DetachAuxilary_tyranids, DetachPatrol_t_au_empire,
               DetachBatallion_t_au_empire, DetachBrigade_t_au_empire,
               DetachVanguard_t_au_empire,
               DetachSpearhead_t_au_empire,
               DetachOutrider_t_au_empire, DetachCommand_t_au_empire,
               DetachSuperHeavy_t_au_empire,
               DetachSuperHeavyAux_t_au_empire,
               DetachAirWing_t_au_empire, DetachFort_t_au_empire,
               DetachAuxilary_t_au_empire,
               DetachPatrol_adepta_sororitas,
               DetachBatallion_adepta_sororitas,
               DetachBrigade_adepta_sororitas,
               DetachVanguard_adepta_sororitas,
               DetachSpearhead_adepta_sororitas,
               DetachOutrider_adepta_sororitas,
               DetachCommand_adepta_sororitas,
               DetachAuxilary_adepta_sororitas,
               DetachPatrol__dynasty_, DetachBatallion__dynasty_,
               DetachBrigade__dynasty_, DetachVanguard__dynasty_,
               DetachSpearhead__dynasty_, DetachOutrider__dynasty_,
               DetachCommand__dynasty_, DetachSuperHeavy__dynasty_,
               DetachSuperHeavyAux__dynasty_, DetachAirWing__dynasty_,
               DetachAuxilary__dynasty_, DetachPatrol_aeldari,
               DetachBatallion_aeldari, DetachBrigade_aeldari,
               DetachVanguard_aeldari, DetachSpearhead_aeldari,
               DetachOutrider_aeldari, DetachCommand_aeldari,
               DetachSuperHeavy_aeldari, DetachSuperHeavyAux_aeldari,
               DetachAirWing_aeldari, DetachAuxilary_aeldari,
               DetachFort_aeldari,
               DetachPatrol__hive_fleet_,
               DetachBatallion__hive_fleet_,
               DetachBrigade__hive_fleet_,
               DetachVanguard__hive_fleet_,
               DetachSpearhead__hive_fleet_,
               DetachOutrider__hive_fleet_,
               DetachCommand__hive_fleet_, DetachAirWing__hive_fleet_,
               DetachFort__hive_fleet_, DetachAuxilary__hive_fleet_,
               DetachPatrol__chapter_, DetachBatallion__chapter_,
               DetachBrigade__chapter_, DetachVanguard__chapter_,
               DetachSpearhead__chapter_, DetachOutrider__chapter_,
               DetachCommand__chapter_, DetachSuperHeavy__chapter_,
               DetachSuperHeavyAux__chapter_, DetachAirWing__chapter_,
               DetachAuxilary__chapter_,
               DetachSuperHeavy_questor_mechanicus,
               DetachSuperHeavyAux_questor_mechanicus,
               DetachPatrol_death_guard, DetachBatallion_death_guard,
               DetachBrigade_death_guard, DetachVanguard_death_guard,
               DetachSpearhead_death_guard,
               DetachOutrider_death_guard, DetachCommand_death_guard,
               DetachSuperHeavyAux_death_guard,
               DetachAuxilary_death_guard, DetachAuxilary_vespid,
               DetachPatrol_harlequins, DetachBatallion_harlequins,
               DetachBrigade_harlequins, DetachVanguard_harlequins,
               DetachSpearhead_harlequins, DetachOutrider_harlequins,
               DetachCommand_harlequins, DetachAuxilary_harlequins,
               DetachPatrol_ork, DetachBatallion_ork,
               DetachBrigade_ork, DetachVanguard_ork,
               DetachSpearhead_ork, DetachOutrider_ork,
               DetachCommand_ork, DetachSuperHeavy_ork,
               DetachSuperHeavyAux_ork, DetachAirWing_ork,
               DetachAuxilary_ork, DetachPatrol_militarum_tempestus,
               DetachBatallion_militarum_tempestus,
               DetachCommand_militarum_tempestus,
               DetachAuxilary_militarum_tempestus,
               DetachPatrol_genestealer_cults,
               DetachBatallion_genestealer_cults,
               DetachBrigade_genestealer_cults,
               DetachVanguard_genestealer_cults,
               DetachSpearhead_genestealer_cults,
               DetachOutrider_genestealer_cults,
               DetachCommand_genestealer_cults,
               DetachAuxilary_genestealer_cults,
               DetachPatrol__craftworld_,
               DetachBatallion__craftworld_,
               DetachBrigade__craftworld_,
               DetachVanguard__craftworld_,
               DetachSpearhead__craftworld_,
               DetachOutrider__craftworld_,
               DetachCommand__craftworld_,
               DetachSuperHeavy__craftworld_,
               DetachSuperHeavyAux__craftworld_,
               DetachAirWing__craftworld_,
               DetachAuxilary__craftworld_, DetachPatrol__kabal_,
               DetachBatallion__kabal_, DetachVanguard__kabal_,
               DetachSpearhead__kabal_, DetachCommand__kabal_,
               DetachAirWing__kabal_, DetachAuxilary__kabal_,
               DetachPatrol__clan_, DetachBatallion__clan_,
               DetachBrigade__clan_, DetachVanguard__clan_,
               DetachSpearhead__clan_, DetachOutrider__clan_,
               DetachCommand__clan_, DetachSuperHeavy__clan_,
               DetachSuperHeavyAux__clan_, DetachAirWing__clan_,
               DetachAuxilary__clan_, DetachVanguard__ordo_,
               DetachCommand__ordo_, DetachAuxilary__ordo_,
               DetachPatrol_adeptus_astartes,
               DetachBatallion_adeptus_astartes,
               DetachBrigade_adeptus_astartes,
               DetachVanguard_adeptus_astartes,
               DetachSpearhead_adeptus_astartes,
               DetachOutrider_adeptus_astartes,
               DetachCommand_adeptus_astartes,
               DetachSuperHeavy_adeptus_astartes,
               DetachSuperHeavyAux_adeptus_astartes,
               DetachAirWing_adeptus_astartes,
               DetachAuxilary_adeptus_astartes,
               DetachPatrol_cult_mechanicus,
               DetachBatallion_cult_mechanicus,
               DetachVanguard_cult_mechanicus,
               DetachSpearhead_cult_mechanicus,
               DetachCommand_cult_mechanicus,
               DetachAuxilary_cult_mechanicus,
               DetachAuxilary_sisters_of_silence, DetachPatrol_ynnari,
               DetachBatallion_ynnari, DetachBrigade_ynnari,
               DetachVanguard_ynnari, DetachSpearhead_ynnari,
               DetachOutrider_ynnari, DetachCommand_ynnari,
               DetachSuperHeavy_ynnari, DetachSuperHeavyAux_ynnari,
               DetachAirWing_ynnari, DetachAuxilary_ynnari,
               DetachPatrol_nurgle, DetachBatallion_nurgle,
               DetachBrigade_nurgle, DetachVanguard_nurgle,
               DetachSpearhead_nurgle, DetachOutrider_nurgle,
               DetachCommand_nurgle, DetachSuperHeavyAux_nurgle,
               DetachAirWing_nurgle, DetachAuxilary_nurgle,
               DetachPatrol__haemunculus_coven_,
               DetachBatallion__haemunculus_coven_,
               DetachVanguard__haemunculus_coven_,
               DetachSpearhead__haemunculus_coven_,
               DetachCommand__haemunculus_coven_,
               DetachAuxilary__haemunculus_coven_,
               DetachSuperHeavy__household_,
               DetachSuperHeavyAux__household_,
               DetachVanguard_death_company,
               DetachCommand_death_company,
               DetachAuxilary_death_company,
               DetachVanguard_spirit_host,
               DetachSpearhead_spirit_host, DetachCommand_spirit_host,
               DetachSuperHeavy_spirit_host,
               DetachSuperHeavyAux_spirit_host,
               DetachAirWing_spirit_host, DetachAuxilary_spirit_host,
               DetachPatrol__order_, DetachBatallion__order_,
               DetachBrigade__order_, DetachVanguard__order_,
               DetachSpearhead__order_, DetachOutrider__order_,
               DetachCommand__order_, DetachAuxilary__order_,
               DetachAuxilary_officio_assassinorum,
               DetachPatrol_adeptus_custodes,
               DetachBatallion_adeptus_custodes,
               DetachBrigade_adeptus_custodes,
               DetachVanguard_adeptus_custodes,
               DetachSpearhead_adeptus_custodes,
               DetachOutrider_adeptus_custodes,
               DetachCommand_adeptus_custodes,
               DetachAuxilary_adeptus_custodes,
               DetachPatrol_aspect_warrior,
               DetachBatallion_aspect_warrior,
               DetachBrigade_aspect_warrior,
               DetachVanguard_aspect_warrior,
               DetachSpearhead_aspect_warrior,
               DetachOutrider_aspect_warrior,
               DetachCommand_aspect_warrior,
               DetachAirWing_aspect_warrior,
               DetachAuxilary_aspect_warrior,
               DetachPatrol_grey_knights,
               DetachBatallion_grey_knights,
               DetachBrigade_grey_knights,
               DetachVanguard_grey_knights,
               DetachSpearhead_grey_knights,
               DetachOutrider_grey_knights,
               DetachCommand_grey_knights, DetachAirWing_grey_knights,
               DetachAuxilary_grey_knights, DetachPatrol_daemon,
               DetachBatallion_daemon, DetachBrigade_daemon,
               DetachVanguard_daemon, DetachSpearhead_daemon,
               DetachOutrider_daemon, DetachCommand_daemon,
               DetachFort_daemon, DetachAuxilary_daemon,
               DetachPatrol_adeptus_mechanicus,
               DetachBatallion_adeptus_mechanicus,
               DetachBrigade_adeptus_mechanicus,
               DetachVanguard_adeptus_mechanicus,
               DetachSpearhead_adeptus_mechanicus,
               DetachOutrider_adeptus_mechanicus,
               DetachCommand_adeptus_mechanicus,
               DetachAuxilary_adeptus_mechanicus,
               DetachAuxilary_jokaero, DetachPatrol_warhost,
               DetachBatallion_warhost, DetachBrigade_warhost,
               DetachVanguard_warhost, DetachSpearhead_warhost,
               DetachOutrider_warhost, DetachCommand_warhost,
               DetachAuxilary_warhost, DetachPatrol__wych_cult_,
               DetachBatallion__wych_cult_,
               DetachVanguard__wych_cult_, DetachOutrider__wych_cult_,
               DetachCommand__wych_cult_, DetachAirWing__wych_cult_,
               DetachAuxilary__wych_cult_,
               DetachAuxilary_militarum_auxilla,
               DetachPatrol_thousand_sons,
               DetachBatallion_thousand_sons,
               DetachBrigade_thousand_sons,
               DetachVanguard_thousand_sons,
               DetachSpearhead_thousand_sons,
               DetachOutrider_thousand_sons,
               DetachCommand_thousand_sons,
               DetachSuperHeavy_thousand_sons,
               DetachSuperHeavyAux_thousand_sons,
               DetachAirWing_thousand_sons,
               DetachAuxilary_thousand_sons]

class DetachPlanetstrikeAttacker__sept_(DetachPlanetstrikeAttacker):
    army_name = u'<Sept> (Planetstrike Attacker detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]
    army_id = u'planetstrike attacker__sept_'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__sept_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachPlanetstrikeDefender__sept_(DetachPlanetstrikeDefender):
    army_name = u'<Sept> (Planetstrike Defender detachment)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]
    army_id = u'planetstrike attacker__sept_'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__sept_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachFortPlanetstrike__sept_(DetachFortPlanetstrike):
    army_name = u'<Sept> (Fortification Network)'
    faction_base = u'<SEPT>'
    alternate_factions = [u"T'AU SEPT", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]
    army_id = u'fort__sept_'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike__sept_, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([Shieldline, Droneport, Gunrig])
        return None


class DetachPlanetstrikeAttacker_ravenwing(DetachPlanetstrikeAttacker):
    army_name = u'Ravenwing (Planetstrike Attacker detachment)'
    faction_base = u'RAVENWING'
    alternate_factions = []
    army_id = u'planetstrike attacker_ravenwing'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'DARK ANGELS', u'RAVENWING', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_ravenwing, self).__init__(*[], **{u'hq': True, u'elite': True, u'fast': True, u'fliers': True, u'parent': parent, })
        self.hq.add_classes([Sammael, SpeederSammael, RavenwingTalonmaster])
        self.elite.add_classes([RWAncient, RWApothecary, RWChampion])
        self.fast.add_classes([RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, LandSpeederVengeance])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        return None


class DetachPlanetstrikeAttacker_blood_angels(DetachPlanetstrikeAttacker):
    army_name = u'Blood Angels (Planetstrike Attacker detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'planetstrike attacker_blood_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_blood_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexPredator, BADevastators, BaalPredator])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPlanetstrikeDefender_blood_angels(DetachPlanetstrikeDefender):
    army_name = u'Blood Angels (Planetstrike Defender detachment)'
    faction_base = u'BLOOD ANGELS'
    alternate_factions = [u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>']
    army_id = u'planetstrike attacker_blood_angels'
    army_factions = [u'IMPERIUM', u'FLESH TEARERS', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_blood_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, CodexVindicator, Whirlwind, CodexWhirlwind, CodexPredator, BADevastators, BaalPredator])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPlanetstrikeAttacker_asuryani(DetachPlanetstrikeAttacker):
    army_name = u'Asuryani (Planetstrike Attacker detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'planetstrike attacker_asuryani'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_asuryani, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachPlanetstrikeDefender_asuryani(DetachPlanetstrikeDefender):
    army_name = u'Asuryani (Planetstrike Defender detachment)'
    faction_base = u'ASURYANI'
    alternate_factions = []
    army_id = u'planetstrike attacker_asuryani'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_asuryani, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachPlanetstrikeAttacker_slaanesh(DetachPlanetstrikeAttacker):
    army_name = u'Slaanesh (Planetstrike Attacker detachment)'
    faction_base = u'SLAANESH'
    alternate_factions = []
    army_id = u'planetstrike attacker_slaanesh'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'SLAANESH', u"EMPEROR'S CHILDREN", u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_slaanesh, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_slaanesh(DetachPlanetstrikeDefender):
    army_name = u'Slaanesh (Planetstrike Defender detachment)'
    faction_base = u'SLAANESH'
    alternate_factions = []
    army_id = u'planetstrike attacker_slaanesh'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'HERETIC ASTARTES', u'SLAANESH', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_slaanesh, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker_astra_militarum(DetachPlanetstrikeAttacker):
    army_name = u'Astra Militarum (Planetstrike Attacker detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'planetstrike attacker_astra_militarum'
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_astra_militarum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachPlanetstrikeDefender_astra_militarum(DetachPlanetstrikeDefender):
    army_name = u'Astra Militarum (Planetstrike Defender detachment)'
    faction_base = u'ASTRA MILITARUM'
    alternate_factions = []
    army_id = u'planetstrike attacker_astra_militarum'
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_astra_militarum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachPlanetstrikeAttacker__regiment_(DetachPlanetstrikeAttacker):
    army_name = u'<Regiment> (Planetstrike Attacker detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'planetstrike attacker__regiment_'
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__regiment_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachPlanetstrikeDefender__regiment_(DetachPlanetstrikeDefender):
    army_name = u'<Regiment> (Planetstrike Defender detachment)'
    faction_base = u'<REGIMENT>'
    alternate_factions = [u'CADIAN', u'CATACHAN', u'VALHALLAN', u'VOSTROYAN', u'ARMAGEDDON', u'TALLARN', u'MORDIAN', u'BLOOD BROTHERS']
    army_id = u'planetstrike attacker__regiment_'
    army_factions = [u'IMPERIUM', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__regiment_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachPlanetstrikeAttacker__mascue_(DetachPlanetstrikeAttacker):
    army_name = u'<Mascue> (Planetstrike Attacker detachment)'
    faction_base = u'<MASCUE>'
    alternate_factions = []
    army_id = u'planetstrike attacker__mascue_'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__mascue_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachPlanetstrikeDefender__mascue_(DetachPlanetstrikeDefender):
    army_name = u'<Mascue> (Planetstrike Defender detachment)'
    faction_base = u'<MASCUE>'
    alternate_factions = []
    army_id = u'planetstrike attacker__mascue_'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__mascue_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachPlanetstrikeAttacker_deathwatch(DetachPlanetstrikeAttacker):
    army_name = u'Deathwatch (Planetstrike Attacker detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'planetstrike attacker_deathwatch'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_deathwatch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([LandRaider, LandRaiderCrusader, LandRaiderRedeemer, DWHellblasters])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachPlanetstrikeDefender_deathwatch(DetachPlanetstrikeDefender):
    army_name = u'Deathwatch (Planetstrike Defender detachment)'
    faction_base = u'DEATHWATCH'
    alternate_factions = []
    army_id = u'planetstrike attacker_deathwatch'
    army_factions = [u'DEATHWATCH', u'IMPERIUM', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_deathwatch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([LandRaider, LandRaiderCrusader, LandRaiderRedeemer, DWHellblasters])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachPlanetstrikeAttacker_space_wolves(DetachPlanetstrikeAttacker):
    army_name = u'Space Wolves (Planetstrike Attacker detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'planetstrike attacker_space_wolves'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'SPACE WOLVES', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_space_wolves, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors])
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class DetachPlanetstrikeDefender_space_wolves(DetachPlanetstrikeDefender):
    army_name = u'Space Wolves (Planetstrike Defender detachment)'
    faction_base = u'SPACE WOLVES'
    alternate_factions = []
    army_id = u'planetstrike attacker_space_wolves'
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_space_wolves, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors])
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class DetachPlanetstrikeAttacker_tzeentch(DetachPlanetstrikeAttacker):
    army_name = u'Tzeentch (Planetstrike Attacker detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'planetstrike attacker_tzeentch'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_tzeentch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_tzeentch(DetachPlanetstrikeDefender):
    army_name = u'Tzeentch (Planetstrike Defender detachment)'
    faction_base = u'TZEENTCH'
    alternate_factions = []
    army_id = u'planetstrike attacker_tzeentch'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'HERETIC ASTARTES', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_tzeentch, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker_drukhari(DetachPlanetstrikeAttacker):
    army_name = u'Drukhari (Planetstrike Attacker detachment)'
    faction_base = u'DRUKHARI'
    alternate_factions = []
    army_id = u'planetstrike attacker_drukhari'
    army_factions = [u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'DRUKHARI', u'YNNARI', u'<WYCH CULT>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_drukhari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachPlanetstrikeDefender_drukhari(DetachPlanetstrikeDefender):
    army_name = u'Drukhari (Planetstrike Defender detachment)'
    faction_base = u'DRUKHARI'
    alternate_factions = []
    army_id = u'planetstrike attacker_drukhari'
    army_factions = [u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'AELDARI', u'YNNARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'<HAEMUNCULUS COVEN>', u'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_drukhari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachPlanetstrikeAttacker_heretic_astartes(DetachPlanetstrikeAttacker):
    army_name = u'Heretic Astartes (Planetstrike Attacker detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'planetstrike attacker_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_heretic_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_heretic_astartes(DetachPlanetstrikeDefender):
    army_name = u'Heretic Astartes (Planetstrike Defender detachment)'
    faction_base = u'HERETIC ASTARTES'
    alternate_factions = []
    army_id = u'planetstrike attacker_heretic_astartes'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_heretic_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker__legion_(DetachPlanetstrikeAttacker):
    army_name = u'<Legion> (Planetstrike Attacker detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'planetstrike attacker__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__legion_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionRubrics, Tzaangors, LegionPlagues, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker_thousand_sons(DetachPlanetstrikeAttacker):
    army_name = u'Thousand Sons (Planetstrike Attacker detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'planetstrike attacker_thousand_sons'
    army_factions = [u'CHAOS', u'TZEENTCH', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_thousand_sons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_thousand_sons(DetachPlanetstrikeDefender):
    army_name = u'Thousand Sons (Planetstrike Defender detachment)'
    faction_base = u'THOUSAND SONS'
    alternate_factions = []
    army_id = u'planetstrike attacker_thousand_sons'
    army_factions = [u'CHAOS', u'TZEENTCH', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_thousand_sons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender__legion_(DetachPlanetstrikeDefender):
    army_name = u'<Legion> (Planetstrike Defender detachment)'
    faction_base = u'<LEGION>'
    alternate_factions = [u'BLACK LEGION', u'IRON WARRIORS', u'WORD BEARERS', u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'RED CORSAIRS', u'WORLD EATERS', u"EMPEROR'S CHILDREN", u'THOUSAND SONS']
    army_id = u'planetstrike attacker__legion_'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'SLAANESH', u'NURGLE', u'WORLD EATERS', u'IRON WARRIORS', u'THOUSAND SONS', u'RED CORSAIRS', u'RENEGADES', u"EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__legion_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionRubrics, Tzaangors, LegionPlagues, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker_dark_angels(DetachPlanetstrikeAttacker):
    army_name = u'Dark Angels (Planetstrike Attacker detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'planetstrike attacker_dark_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'RAVENWING', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_dark_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexDevastators, CodexPredator])
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPlanetstrikeDefender_dark_angels(DetachPlanetstrikeDefender):
    army_name = u'Dark Angels (Planetstrike Defender detachment)'
    faction_base = u'DARK ANGELS'
    alternate_factions = [u'<DARK ANGELS SUCCESSORS>']
    army_id = u'planetstrike attacker_dark_angels'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'SALAMANDERS', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_dark_angels, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexDevastators, CodexPredator])
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPlanetstrikeAttacker_khorne(DetachPlanetstrikeAttacker):
    army_name = u'Khorne (Planetstrike Attacker detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'planetstrike attacker_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'KHORNE', u'WORD BEARERS', u'IRON WARRIORS', u'RED CORSAIRS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_khorne, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_khorne(DetachPlanetstrikeDefender):
    army_name = u'Khorne (Planetstrike Defender detachment)'
    faction_base = u'KHORNE'
    alternate_factions = []
    army_id = u'planetstrike attacker_khorne'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'KHORNE', u'HERETIC ASTARTES', u'WORLD EATERS', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_khorne, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker__forge_world_(DetachPlanetstrikeAttacker):
    army_name = u'<Forge World> (Planetstrike Attacker detachment)'
    faction_base = u'<FORGE WORLD>'
    alternate_factions = [u'MARS', u'GRAIA', u'METALICA', u'LUCIUS', u'AGRIPINAA', u'STYGIES VIII', u'RYZA']
    army_id = u'planetstrike attacker__forge_world_'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__forge_world_, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachPlanetstrikeDefender__forge_world_(DetachPlanetstrikeDefender):
    army_name = u'<Forge World> (Planetstrike Defender detachment)'
    faction_base = u'<FORGE WORLD>'
    alternate_factions = [u'MARS', u'GRAIA', u'METALICA', u'LUCIUS', u'AGRIPINAA', u'STYGIES VIII', u'RYZA']
    army_id = u'planetstrike attacker__forge_world_'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__forge_world_, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachPlanetstrikeAttacker_necrons(DetachPlanetstrikeAttacker):
    army_name = u'Necrons (Planetstrike Attacker detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'planetstrike attacker_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_necrons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths])
        self.transports.add_classes([GhostArk])
        return None


class DetachPlanetstrikeDefender_necrons(DetachPlanetstrikeDefender):
    army_name = u'Necrons (Planetstrike Defender detachment)'
    faction_base = u'NECRONS'
    alternate_factions = []
    army_id = u'planetstrike attacker_necrons'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_necrons, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths])
        self.transports.add_classes([GhostArk])
        return None


class DetachPlanetstrikeAttacker_adeptus_ministorum(DetachPlanetstrikeAttacker):
    army_name = u'Adeptus Ministorum (Planetstrike Attacker detachment)'
    faction_base = u'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = u'planetstrike attacker_adeptus_ministorum'
    army_factions = [u'IMPERIUM', u'ADEPTUS MINISTORUM', u'ADEPTA SORORITAS', u'<ORDER>']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_adeptus_ministorum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachPlanetstrikeDefender_adeptus_ministorum(DetachPlanetstrikeDefender):
    army_name = u'Adeptus Ministorum (Planetstrike Defender detachment)'
    faction_base = u'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = u'planetstrike attacker_adeptus_ministorum'
    army_factions = [u'IMPERIUM', u'ADEPTUS MINISTORUM', u'ADEPTA SORORITAS', u'<ORDER>']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_adeptus_ministorum, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachPlanetstrikeAttacker_imperium(DetachPlanetstrikeAttacker):
    army_name = u'Imperium (Planetstrike Attacker detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'planetstrike attacker_imperium'
    army_factions = [u'IMPERIUM', u'<CHAPTER>', u'SPACE WOLVES', u'BLACK TEMPLARS', u'AGRIPINAA', u'SALAMANDERS', u'MORDIAN', u'METALICA', u'CRIMSON FISTS', u'TALLARN', u'ARMAGEDDON', u'GRAIA', u'BLOOD ANGELS', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'STYGIES VIII', u'DARK ANGELS', u'ASTRA MILITARUM', u'LUCIUS', u'<REGIMENT>', u'FLESH TEARERS', u'VOSTROYAN', u'ULTRAMARINES', u'IMPERIAL FISTS', u'ADEPTA SORORITAS', u'RYZA', u'<DARK ANGELS SUCCESSORS>', u'<ORDER>', u'CADIAN', u'RAVEN GUARD', u'ADEPTUS CUSTODES', u'RAVENWING', u'<FORGE WORLD>', u'GREY KNIGHTS', u'ADEPTUS MECHANICUS', u'CATACHAN', u'BLOOD BROTHERS', u'MARS', u'VALHALLAN', u'WHITE SCARS', u'DEATHWATCH', u'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_imperium, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachPlanetstrikeDefender_imperium(DetachPlanetstrikeDefender):
    army_name = u'Imperium (Planetstrike Defender detachment)'
    faction_base = u'IMPERIUM'
    alternate_factions = []
    army_id = u'planetstrike attacker_imperium'
    army_factions = [u'IMPERIUM', u'SPACE WOLVES', u'BLACK TEMPLARS', u'AGRIPINAA', u'SALAMANDERS', u'MORDIAN', u'METALICA', u'CRIMSON FISTS', u'TALLARN', u'ARMAGEDDON', u'GRAIA', u'CULT MECHANICUS', u'BLOOD ANGELS', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'STYGIES VIII', u'DARK ANGELS', u'ASTRA MILITARUM', u'LUCIUS', u'<REGIMENT>', u'FLESH TEARERS', u'VOSTROYAN', u'ULTRAMARINES', u'IMPERIAL FISTS', u'ADEPTA SORORITAS', u'RYZA', u'<DARK ANGELS SUCCESSORS>', u'<ORDER>', u'CADIAN', u'RAVEN GUARD', u'ADEPTUS CUSTODES', u'<CHAPTER>', u'<FORGE WORLD>', u'GREY KNIGHTS', u'ADEPTUS MECHANICUS', u'CATACHAN', u'BLOOD BROTHERS', u'MARS', u'VALHALLAN', u'WHITE SCARS', u'DEATHWATCH', u'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_imperium, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None

class DetachPlanetstrikeAttacker_chaos(DetachPlanetstrikeAttacker):
    army_name = u'Chaos (Planetstrike Attacker detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'planetstrike attacker_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'SLAANESH', u"EMPEROR'S CHILDREN", u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'KHORNE', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_chaos, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_chaos(DetachPlanetstrikeDefender):
    army_name = u'Chaos (Planetstrike Defender detachment)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'planetstrike attacker_chaos'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORLD EATERS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'TZEENTCH', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'SLAANESH', u"EMPEROR'S CHILDREN", u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'KHORNE', u'HERETIC ASTARTES', u'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_chaos, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachFortPlanetstrike_chaos(DetachFortPlanetstrike):
    army_name = u'Chaos (Fortification Network)'
    faction_base = u'CHAOS'
    alternate_factions = []
    army_id = u'fort_chaos'
    army_factions = [u'DAEMON', u'CHAOS', u'NURGLE']

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike_chaos, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([ChaosBastion, FeculentGnarlmaws])
        return None


class DetachPlanetstrikeAttacker_tyranids(DetachPlanetstrikeAttacker):
    army_name = u'Tyranids (Planetstrike Attacker detachment)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'planetstrike attacker_tyranids'
    army_factions = [u'HYDRA', u'JORMUGAND', u'BEHEMOTH', u'GENESTEALER CULTS', u'KRAKEN', u'<HIVE FLEET>', u'TYRANIDS', u'KRONOS', u'GORGON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_tyranids, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachPlanetstrikeDefender_tyranids(DetachPlanetstrikeDefender):
    army_name = u'Tyranids (Planetstrike Defender detachment)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'planetstrike attacker_tyranids'
    army_factions = [u'HYDRA', u'IMPERIUM', u'JORMUGAND', u'BEHEMOTH', u'GENESTEALER CULTS', u'KRAKEN', u'ASTRA MILITARUM', u'<HIVE FLEET>', u'TYRANIDS', u'KRONOS', u'GORGON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_tyranids, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachFortPlanetstrike_tyranids(DetachFortPlanetstrike):
    army_name = u'Tyranids (Fortification Network)'
    faction_base = u'TYRANIDS'
    alternate_factions = []
    army_id = u'fort_tyranids'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike_tyranids, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([Sporocyst])
        return None


class DetachPlanetstrikeAttacker_t_au_empire(DetachPlanetstrikeAttacker):
    army_name = u"T'au Empire (Planetstrike Attacker detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'planetstrike attacker_t_au_empire'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_t_au_empire, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachPlanetstrikeDefender_t_au_empire(DetachPlanetstrikeDefender):
    army_name = u"T'au Empire (Planetstrike Defender detachment)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'planetstrike attacker_t_au_empire'
    army_factions = [u'<SEPT>', u"T'AU SEPT", u"T'AU EMPIRE", u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_t_au_empire, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachFortPlanetstrike_t_au_empire(DetachFortPlanetstrike):
    army_name = u"T'au Empire (Fortification Network)"
    faction_base = u"T'AU EMPIRE"
    alternate_factions = []
    army_id = u'fort_t_au_empire'
    army_factions = [u"T'AU EMPIRE", u"T'AU SEPT", u'<SEPT>', u'FARSIGHT ENCLAVES', u"VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike_t_au_empire, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([Shieldline, Droneport, Gunrig])
        return None


class DetachPlanetstrikeAttacker_adepta_sororitas(DetachPlanetstrikeAttacker):
    army_name = u'Adepta Sororitas (Planetstrike Attacker detachment)'
    faction_base = u'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = u'planetstrike attacker_adepta_sororitas'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_adepta_sororitas, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachPlanetstrikeDefender_adepta_sororitas(DetachPlanetstrikeDefender):
    army_name = u'Adepta Sororitas (Planetstrike Defender detachment)'
    faction_base = u'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = u'planetstrike attacker_adepta_sororitas'
    army_factions = [u'IMPERIUM', u'ADEPTUS MINISTORUM', u'ADEPTA SORORITAS', u'<ORDER>']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_adepta_sororitas, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachPlanetstrikeAttacker__dynasty_(DetachPlanetstrikeAttacker):
    army_name = u'<Dynasty> (Planetstrike Attacker detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH']
    army_id = u'planetstrike attacker__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__dynasty_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths])
        self.transports.add_classes([GhostArk])
        return None


class DetachPlanetstrikeDefender__dynasty_(DetachPlanetstrikeDefender):
    army_name = u'<Dynasty> (Planetstrike Defender detachment)'
    faction_base = u'<DYNASTY>'
    alternate_factions = [u'SAUTEKH', u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH']
    army_id = u'planetstrike attacker__dynasty_'
    army_factions = [u'MEPHRIT', u'NOVOKH', u'NEPREKH', u'NIHILAKH', u'<DYNASTY>', u'SAUTEKH', u'NECRONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__dynasty_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths])
        self.transports.add_classes([GhostArk])
        return None


class DetachPlanetstrikeAttacker_aeldari(DetachPlanetstrikeAttacker):
    army_name = u'Aeldari (Planetstrike Attacker detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'planetstrike attacker_aeldari'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'DRUKHARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'<WYCH CULT>', u'YNNARI', u'HARLEQUINS', u'ASPECT WARRIOR', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'WYCH CULT OF STRIFE']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_aeldari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachPlanetstrikeDefender_aeldari(DetachPlanetstrikeDefender):
    army_name = u'Aeldari (Planetstrike Defender detachment)'
    faction_base = u'AELDARI'
    alternate_factions = []
    army_id = u'planetstrike attacker_aeldari'
    army_factions = [u'THE PROPHETS OF FLESH', u'THE DARK CREED', u'COVEN OF TWELVE', u'SAIM-HANN', u'AELDARI', u'<MASCUE>', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'<HAEMUNCULUS COVEN>', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'ASPECT WARRIOR', u'ALATOIC', u'WARHOST', u'DRUKHARI', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_aeldari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachPlanetstrikeAttacker__hive_fleet_(DetachPlanetstrikeAttacker):
    army_name = u'<Hive Fleet> (Planetstrike Attacker detachment)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'planetstrike attacker__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__hive_fleet_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachPlanetstrikeDefender__hive_fleet_(DetachPlanetstrikeDefender):
    army_name = u'<Hive Fleet> (Planetstrike Defender detachment)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'planetstrike attacker__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__hive_fleet_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachFortPlanetstrike__hive_fleet_(DetachFortPlanetstrike):
    army_name = u'<Hive Fleet> (Fortification Network)'
    faction_base = u'<HIVE FLEET>'
    alternate_factions = [u'BEHEMOTH', u'KRAKEN', u'GORGON', u'JORMUGAND', u'HYDRA', u'KRONOS', u'LEVIATHAN']
    army_id = u'fort__hive_fleet_'
    army_factions = [u'JORMUGAND', u'BEHEMOTH', u'GORGON', u'KRAKEN', u'<HIVE FLEET>', u'HYDRA', u'TYRANIDS', u'LEVIATHAN', u'KRONOS']

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike__hive_fleet_, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([Sporocyst])
        return None


class DetachPlanetstrikeAttacker__chapter_(DetachPlanetstrikeAttacker):
    army_name = u'<Chapter> (Planetstrike Attacker detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'planetstrike attacker__chapter_'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__chapter_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPlanetstrikeDefender__chapter_(DetachPlanetstrikeDefender):
    army_name = u'<Chapter> (Planetstrike Defender detachment)'
    faction_base = u'<CHAPTER>'
    alternate_factions = [u'ULTRAMARINES', u'IMPERIAL FISTS', u'CRIMSON FISTS', u'BLACK TEMPLARS', u'RAVEN GUARD', u'SALAMANDERS', u'WHITE SCARS']
    army_id = u'planetstrike attacker__chapter_'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'ADEPTUS ASTARTES', u'RAVEN GUARD', u'BLACK TEMPLARS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'IMPERIAL FISTS', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__chapter_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPlanetstrikeAttacker_death_guard(DetachPlanetstrikeAttacker):
    army_name = u'Death Guard (Planetstrike Attacker detachment)'
    faction_base = u'DEATH GUARD'
    alternate_factions = []
    army_id = u'planetstrike attacker_death_guard'
    army_factions = [u'NIGHT LORDS', u'ALPHA LEGION', u'RENEGADES', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_death_guard, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachPlanetstrikeDefender_death_guard(DetachPlanetstrikeDefender):
    army_name = u'Death Guard (Planetstrike Defender detachment)'
    faction_base = u'DEATH GUARD'
    alternate_factions = []
    army_id = u'planetstrike attacker_death_guard'
    army_factions = [u'NIGHT LORDS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'NURGLE', u'WORD BEARERS', u'IRON WARRIORS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_death_guard, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachPlanetstrikeAttacker_harlequins(DetachPlanetstrikeAttacker):
    army_name = u'Harlequins (Planetstrike Attacker detachment)'
    faction_base = u'HARLEQUINS'
    alternate_factions = []
    army_id = u'planetstrike attacker_harlequins'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_harlequins, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachPlanetstrikeDefender_harlequins(DetachPlanetstrikeDefender):
    army_name = u'Harlequins (Planetstrike Defender detachment)'
    faction_base = u'HARLEQUINS'
    alternate_factions = []
    army_id = u'planetstrike attacker_harlequins'
    army_factions = [u'HARLEQUINS', u'YNNARI', u'<MASCUE>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_harlequins, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachPlanetstrikeAttacker_ork(DetachPlanetstrikeAttacker):
    army_name = u'Ork (Planetstrike Attacker detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'planetstrike attacker_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_ork, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachPlanetstrikeDefender_ork(DetachPlanetstrikeDefender):
    army_name = u'Ork (Planetstrike Defender detachment)'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'planetstrike attacker_ork'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_ork, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachPlanetstrikeAttacker_genestealer_cults(DetachPlanetstrikeAttacker):
    army_name = u'Genestealer Cults (Planetstrike Attacker detachment)'
    faction_base = u'GENESTEALER CULTS'
    alternate_factions = []
    army_id = u'planetstrike attacker_genestealer_cults'
    army_factions = [u'IMPERIUM', u'GENESTEALER CULTS', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'TYRANIDS', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_genestealer_cults, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([CCommander, LordCommissar, Primaris, TankCommander, TempestorPrime, EnginseerV2, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime, GoliathTruck, CultChimera])
        return None


class DetachPlanetstrikeDefender_genestealer_cults(DetachPlanetstrikeDefender):
    army_name = u'Genestealer Cults (Planetstrike Defender detachment)'
    faction_base = u'GENESTEALER CULTS'
    alternate_factions = []
    army_id = u'planetstrike attacker_genestealer_cults'
    army_factions = [u'IMPERIUM', u'GENESTEALER CULTS', u'CATACHAN', u'CADIAN', u'ASTRA MILITARUM', u'<REGIMENT>', u'TYRANIDS', u'BLOOD BROTHERS', u'MORDIAN', u'VALHALLAN', u'TALLARN', u'VOSTROYAN', u'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_genestealer_cults, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([CCommander, LordCommissar, Primaris, TankCommander, TempestorPrime, EnginseerV2, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime, GoliathTruck, CultChimera])
        return None


class DetachPlanetstrikeAttacker__craftworld_(DetachPlanetstrikeAttacker):
    army_name = u'<Craftworld> (Planetstrike Attacker detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'planetstrike attacker__craftworld_'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__craftworld_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachPlanetstrikeDefender__craftworld_(DetachPlanetstrikeDefender):
    army_name = u'<Craftworld> (Planetstrike Defender detachment)'
    faction_base = u'<CRAFTWORLD>'
    alternate_factions = [u'ULTHWE', u'IYANDEN', u'ALATOIC', u'BIEL-TAN', u'SAIM-HANN']
    army_id = u'planetstrike attacker__craftworld_'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'YNNARI', u'ASPECT WARRIOR', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__craftworld_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachPlanetstrikeDefender__kabal_(DetachPlanetstrikeDefender):
    army_name = u'<Kabal> (Planetstrike Defender detachment)'
    faction_base = u'<KABAL>'
    alternate_factions = []
    army_id = u'planetstrike attacker__kabal_'
    army_factions = [u'YNNARI', u'DRUKHARI', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__kabal_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachPlanetstrikeAttacker__clan_(DetachPlanetstrikeAttacker):
    army_name = u'<Clan> (Planetstrike Attacker detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'planetstrike attacker__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__clan_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachPlanetstrikeDefender__clan_(DetachPlanetstrikeDefender):
    army_name = u'<Clan> (Planetstrike Defender detachment)'
    faction_base = u'<CLAN>'
    alternate_factions = [u'GOFFS', u'BLOOD AXES', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']
    army_id = u'planetstrike attacker__clan_'
    army_factions = [u'<CLAN>', u'GOFFS', u'BLOOD AXES', u'ORK', u'DEATHSKULLS', u'BAD MOONS', u'EVIL SUNZ', u'SNAKEBITES', u'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__clan_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachPlanetstrikeAttacker_adeptus_astartes(DetachPlanetstrikeAttacker):
    army_name = u'Adeptus Astartes (Planetstrike Attacker detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'planetstrike attacker_adeptus_astartes'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'GREY KNIGHTS', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'RAVENWING', u'IMPERIAL FISTS', u'SPACE WOLVES', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_adeptus_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachPlanetstrikeDefender_adeptus_astartes(DetachPlanetstrikeDefender):
    army_name = u'Adeptus Astartes (Planetstrike Defender detachment)'
    faction_base = u'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = u'planetstrike attacker_adeptus_astartes'
    army_factions = [u'IMPERIUM', u'<DARK ANGELS SUCCESSORS>', u'<BLOOD ANGELS SUCCESSORS>', u'BLOOD ANGELS', u'ADEPTUS ASTARTES', u'DEATHWATCH', u'RAVEN GUARD', u'DARK ANGELS', u'BLACK TEMPLARS', u'FLESH TEARERS', u'SALAMANDERS', u'<CHAPTER>', u'CRIMSON FISTS', u'WHITE SCARS', u'GREY KNIGHTS', u'IMPERIAL FISTS', u'SPACE WOLVES', u'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_adeptus_astartes, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachPlanetstrikeDefender_cult_mechanicus(DetachPlanetstrikeDefender):
    army_name = u'Cult Mechanicus (Planetstrike Defender detachment)'
    faction_base = u'CULT MECHANICUS'
    alternate_factions = []
    army_id = u'planetstrike attacker_cult_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_cult_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachPlanetstrikeAttacker_ynnari(DetachPlanetstrikeAttacker):
    army_name = u'Ynnari (Planetstrike Attacker detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'planetstrike attacker_ynnari'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'DRUKHARI', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ALATOIC', u'<WYCH CULT>', u'YNNARI', u'HARLEQUINS', u'ASPECT WARRIOR', u'<MASCUE>', u'BIEL-TAN', u'WARHOST', u'WYCH CULT OF STRIFE']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_ynnari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachPlanetstrikeDefender_ynnari(DetachPlanetstrikeDefender):
    army_name = u'Ynnari (Planetstrike Defender detachment)'
    faction_base = u'YNNARI'
    alternate_factions = []
    army_id = u'planetstrike attacker_ynnari'
    army_factions = [u'SAIM-HANN', u'AELDARI', u'<MASCUE>', u'IYANDEN', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'HARLEQUINS', u'KABAL OF THE BLACK HEART', u'KABAL OF THE FLAYED SKULL', u'KABAL OF THE POISONED TONGUE', u'KABAL OF THE OBSIDIAN ROSE',u'<KABAL>', u'ALATOIC', u'BIEL-TAN', u'WARHOST', u'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_ynnari, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachPlanetstrikeAttacker_nurgle(DetachPlanetstrikeAttacker):
    army_name = u'Nurgle (Planetstrike Attacker detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'planetstrike attacker_nurgle'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'RED CORSAIRS', u'HERETIC ASTARTES', u'NURGLE', u'IRON WARRIORS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_nurgle, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_nurgle(DetachPlanetstrikeDefender):
    army_name = u'Nurgle (Planetstrike Defender detachment)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'planetstrike attacker_nurgle'
    army_factions = [u'<LEGION>', u'NIGHT LORDS', u'DAEMON', u'WORD BEARERS', u'ALPHA LEGION', u'BLACK LEGION', u'CHAOS', u'DEATH GUARD', u'HERETIC ASTARTES', u'NURGLE', u'IRON WARRIORS', u'RED CORSAIRS', u'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_nurgle, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachFortPlanetstrike_nurgle(DetachFortPlanetstrike):
    army_name = u'Nurgle (Fortification Network)'
    faction_base = u'NURGLE'
    alternate_factions = []
    army_id = u'fort_nurgle'
    army_factions = [u'DAEMON', u'NURGLE', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike_nurgle, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([FeculentGnarlmaws])
        return None


class DetachPlanetstrikeDefender__haemunculus_coven_(DetachPlanetstrikeDefender):
    army_name = u'<Haemunculus Coven> (Planetstrike Defender detachment)'
    faction_base = u'<HAEMUNCULUS COVEN>'
    alternate_factions = [u'PROPHETS OF FLESH']
    army_id = u'planetstrike attacker__haemunculus_coven_'
    army_factions = [u'<HAEMUNCULUS COVEN>', u'AELDARI', u'DRUKHARI', u'PROPHETS OF FLESH']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__haemunculus_coven_, self).__init__(*[], **{u'heavy': True, u'elite': True, u'hq': True, u'transports': True, u'troops': True, u'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachPlanetstrikeAttacker__order_(DetachPlanetstrikeAttacker):
    army_name = u'<Order> (Planetstrike Attacker detachment)'
    faction_base = u'<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = u'planetstrike attacker__order_'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__order_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachPlanetstrikeDefender__order_(DetachPlanetstrikeDefender):
    army_name = u'<Order> (Planetstrike Defender detachment)'
    faction_base = u'<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = u'planetstrike attacker__order_'
    army_factions = [u'IMPERIUM', u'ADEPTA SORORITAS', u'<ORDER>', u'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__order_, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachPlanetstrikeAttacker_adeptus_custodes(DetachPlanetstrikeAttacker):
    army_name = u'Adeptus Custodes (Planetstrike Attacker detachment)'
    faction_base = u'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = u'planetstrike attacker_adeptus_custodes'
    army_factions = [u'IMPERIUM', u'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_adeptus_custodes, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachPlanetstrikeDefender_adeptus_custodes(DetachPlanetstrikeDefender):
    army_name = u'Adeptus Custodes (Planetstrike Defender detachment)'
    faction_base = u'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = u'planetstrike attacker_adeptus_custodes'
    army_factions = [u'IMPERIUM', u'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_adeptus_custodes, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachPlanetstrikeAttacker_aspect_warrior(DetachPlanetstrikeAttacker):
    army_name = u'Aspect Warrior (Planetstrike Attacker detachment)'
    faction_base = u'ASPECT WARRIOR'
    alternate_factions = []
    army_id = u'planetstrike attacker_aspect_warrior'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_aspect_warrior, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachPlanetstrikeDefender_aspect_warrior(DetachPlanetstrikeDefender):
    army_name = u'Aspect Warrior (Planetstrike Defender detachment)'
    faction_base = u'ASPECT WARRIOR'
    alternate_factions = []
    army_id = u'planetstrike attacker_aspect_warrior'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'ASPECT WARRIOR', u'YNNARI', u'ALATOIC', u'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_aspect_warrior, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachPlanetstrikeAttacker_grey_knights(DetachPlanetstrikeAttacker):
    army_name = u'Grey Knights (Planetstrike Attacker detachment)'
    faction_base = u'GREY KNIGHTS'
    alternate_factions = []
    army_id = u'planetstrike attacker_grey_knights'
    army_factions = [u'IMPERIUM', u'GREY KNIGHTS', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_grey_knights, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachPlanetstrikeDefender_grey_knights(DetachPlanetstrikeDefender):
    army_name = u'Grey Knights (Planetstrike Defender detachment)'
    faction_base = u'GREY KNIGHTS'
    alternate_factions = []
    army_id = u'planetstrike attacker_grey_knights'
    army_factions = [u'IMPERIUM', u'GREY KNIGHTS', u'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_grey_knights, self).__init__(*[], **{u'heavy': True, u'troops': True, u'fliers': True, u'elite': True, u'hq': True, u'fast': True, u'transports': True, u'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachPlanetstrikeAttacker_daemon(DetachPlanetstrikeAttacker):
    army_name = u'Daemon (Planetstrike Attacker detachment)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'planetstrike attacker_daemon'
    army_factions = [u'DAEMON', u'TZEENTCH', u'CHAOS', u'KHORNE', u'SLAANESH', u'NURGLE']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_daemon, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachPlanetstrikeDefender_daemon(DetachPlanetstrikeDefender):
    army_name = u'Daemon (Planetstrike Defender detachment)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'planetstrike attacker_daemon'
    army_factions = [u'DAEMON', u'TZEENTCH', u'CHAOS', u'KHORNE', u'SLAANESH', u'NURGLE']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_daemon, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachFortPlanetstrike_daemon(DetachFortPlanetstrike):
    army_name = u'Daemon (Fortification Network)'
    faction_base = u'DAEMON'
    alternate_factions = []
    army_id = u'fort_daemon'
    army_factions = [u'DAEMON', u'NURGLE', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike_daemon, self).__init__(*[], **{u'fort': True, u'parent': parent, })
        self.fort.add_classes([FeculentGnarlmaws])
        return None


class DetachPlanetstrikeAttacker_adeptus_mechanicus(DetachPlanetstrikeAttacker):
    army_name = u'Adeptus Mechanicus (Planetstrike Attacker detachment)'
    faction_base = u'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = u'planetstrike attacker_adeptus_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_adeptus_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachPlanetstrikeDefender_adeptus_mechanicus(DetachPlanetstrikeDefender):
    army_name = u'Adeptus Mechanicus (Planetstrike Defender detachment)'
    faction_base = u'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = u'planetstrike attacker_adeptus_mechanicus'
    army_factions = [u'RYZA', u'IMPERIUM', u'ADEPTUS MECHANICUS', u'STYGIES VIII', u'LUCIUS', u'AGRIPINAA', u'METALICA', u'MARS', u'GRAIA', u'<FORGE WORLD>', u'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_adeptus_mechanicus, self).__init__(*[], **{u'heavy': True, u'hq': True, u'elite': True, u'troops': True, u'fast': True, u'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachPlanetstrikeAttacker_warhost(DetachPlanetstrikeAttacker):
    army_name = u'Warhost (Planetstrike Attacker detachment)'
    faction_base = u'WARHOST'
    alternate_factions = []
    army_id = u'planetstrike attacker_warhost'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_warhost, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachPlanetstrikeDefender_warhost(DetachPlanetstrikeDefender):
    army_name = u'Warhost (Planetstrike Defender detachment)'
    faction_base = u'WARHOST'
    alternate_factions = []
    army_id = u'planetstrike attacker_warhost'
    army_factions = [u'SAIM-HANN', u'IYANDEN', u'AELDARI', u'ASURYANI', u'ULTHWE', u'<CRAFTWORLD>', u'YNNARI', u'ALATOIC', u'BIEL-TAN', u'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_warhost, self).__init__(*[], **{u'heavy': True, u'troops': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachPlanetstrikeAttacker__wych_cult_(DetachPlanetstrikeAttacker):
    army_name = u'<Wych Cult> (Planetstrike Attacker detachment)'
    faction_base = u'<WYCH CULT>'
    alternate_factions = [u'WYCH CULT OF STRIFE']
    army_id = u'planetstrike attacker__wych_cult_'
    army_factions = [u'DRUKHARI', u'YNNARI', u'CULT OF STRIFE', u'CULT OF THE CURSED BLADE', u'CULT OF THE RED GRIEF', u'<WYCH CULT>', u'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__wych_cult_, self).__init__(*[], **{u'troops': True, u'fliers': True, u'transports': True, u'hq': True, u'fast': True, u'elite': True, u'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None
mission_detachments = {u'Planetstrike (Attacker)':
                       [DetachPlanetstrikeAttacker__sept_,
                        DetachPlanetstrikeAttacker_ravenwing,
                        DetachPlanetstrikeAttacker_blood_angels,
                        DetachPlanetstrikeAttacker_asuryani,
                        DetachPlanetstrikeAttacker_slaanesh,
                        DetachPlanetstrikeAttacker_astra_militarum,
                        DetachPlanetstrikeAttacker__regiment_,
                        DetachPlanetstrikeAttacker__mascue_,
                        DetachPlanetstrikeAttacker_deathwatch,
                        DetachPlanetstrikeAttacker_space_wolves,
                        DetachPlanetstrikeAttacker_tzeentch,
                        DetachPlanetstrikeAttacker_drukhari,
                        DetachPlanetstrikeAttacker_heretic_astartes,
                        DetachPlanetstrikeAttacker__legion_,
                        DetachPlanetstrikeAttacker_dark_angels,
                        DetachPlanetstrikeAttacker_khorne,
                        DetachPlanetstrikeAttacker__forge_world_,
                        DetachPlanetstrikeAttacker_necrons,
                        DetachPlanetstrikeAttacker_adeptus_ministorum,
                        DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_chaos,
                        DetachPlanetstrikeAttacker_tyranids,
                        DetachPlanetstrikeAttacker_t_au_empire,
                        DetachPlanetstrikeAttacker_adepta_sororitas,
                        DetachPlanetstrikeAttacker__dynasty_,
                        DetachPlanetstrikeAttacker_aeldari,
                        DetachPlanetstrikeAttacker__hive_fleet_,
                        DetachPlanetstrikeAttacker__chapter_,
                        DetachPlanetstrikeAttacker_death_guard,
                        DetachPlanetstrikeAttacker_harlequins, DetachPlanetstrikeAttacker_ork,
                        DetachPlanetstrikeAttacker_genestealer_cults,
                        DetachPlanetstrikeAttacker__craftworld_,
                        DetachPlanetstrikeAttacker__clan_,
                        DetachPlanetstrikeAttacker_adeptus_astartes,
                        DetachPlanetstrikeAttacker_ynnari, DetachPlanetstrikeAttacker_nurgle,
                        DetachPlanetstrikeAttacker__order_,
                        DetachPlanetstrikeAttacker_adeptus_custodes,
                        DetachPlanetstrikeAttacker_aspect_warrior,
                        DetachPlanetstrikeAttacker_grey_knights,
                        DetachPlanetstrikeAttacker_daemon,
                        DetachPlanetstrikeAttacker_adeptus_mechanicus,
                        DetachPlanetstrikeAttacker_warhost,
                        DetachPlanetstrikeAttacker__wych_cult_,
                        DetachPlanetstrikeAttacker_thousand_sons],
                       u'Planetstrike (Defender)':
                       [DetachPlanetstrikeDefender__sept_, DetachFortPlanetstrike__sept_,
                        DetachPlanetstrikeDefender_blood_angels,
                        DetachPlanetstrikeDefender_asuryani,
                        DetachPlanetstrikeDefender_slaanesh,
                        DetachPlanetstrikeDefender_astra_militarum,
                        DetachPlanetstrikeDefender__regiment_,
                        DetachPlanetstrikeDefender__mascue_,
                        DetachPlanetstrikeDefender_deathwatch,
                        DetachPlanetstrikeDefender_space_wolves,
                        DetachPlanetstrikeDefender_tzeentch,
                        DetachPlanetstrikeDefender_drukhari,
                        DetachPlanetstrikeDefender_heretic_astartes,
                        DetachPlanetstrikeDefender__legion_,
                        DetachPlanetstrikeDefender_dark_angels,
                        DetachPlanetstrikeDefender_khorne,
                        DetachPlanetstrikeDefender__forge_world_,
                        DetachPlanetstrikeDefender_necrons,
                        DetachPlanetstrikeDefender_adeptus_ministorum,
                        DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_chaos,
                        DetachFortPlanetstrike_chaos, DetachPlanetstrikeDefender_tyranids,
                        DetachFortPlanetstrike_tyranids,
                        DetachPlanetstrikeDefender_t_au_empire,
                        DetachFortPlanetstrike_t_au_empire,
                        DetachPlanetstrikeDefender_adepta_sororitas,
                        DetachPlanetstrikeDefender__dynasty_,
                        DetachPlanetstrikeDefender_aeldari,
                        DetachPlanetstrikeDefender__hive_fleet_,
                        DetachFortPlanetstrike__hive_fleet_,
                        DetachPlanetstrikeDefender__chapter_,
                        DetachPlanetstrikeDefender_death_guard,
                        DetachPlanetstrikeDefender_harlequins, DetachPlanetstrikeDefender_ork,
                        DetachPlanetstrikeDefender_genestealer_cults,
                        DetachPlanetstrikeDefender__craftworld_,
                        DetachPlanetstrikeDefender__kabal_, DetachPlanetstrikeDefender__clan_,
                        DetachPlanetstrikeDefender_adeptus_astartes,
                        DetachPlanetstrikeDefender_cult_mechanicus,
                        DetachPlanetstrikeDefender_ynnari, DetachPlanetstrikeDefender_nurgle,
                        DetachFortPlanetstrike_nurgle,
                        DetachPlanetstrikeDefender__haemunculus_coven_,
                        DetachPlanetstrikeDefender__order_,
                        DetachPlanetstrikeDefender_adeptus_custodes,
                        DetachPlanetstrikeDefender_aspect_warrior,
                        DetachPlanetstrikeDefender_grey_knights,
                        DetachPlanetstrikeDefender_daemon, DetachFortPlanetstrike_daemon,
                        DetachPlanetstrikeDefender_adeptus_mechanicus,
                        DetachPlanetstrikeDefender_warhost,
                        DetachPlanetstrikeDefender_thousand_sons], }

detachments += [DamnedVanguard, SilenceVanguard, AssassinVanguard]
