from builder.games.wh40k8ed.options import OneOf, Gear
from builder.games.wh40k8ed.unit import Unit
import melee, ranged, wargear


class SergeantWeapon(OneOf):
    def add_single(self):
        self.single = [
            self.variant(*ranged.Boltgun),
            self.variant(*ranged.CombiFlamer),
            self.variant(*ranged.CombiGrav),
            self.variant(*ranged.CombiMelta),
            self.variant(*ranged.CombiPlasma),
            self.variant(*ranged.StormBolter)
        ]

    def add_double(self, sword=True):
        self.double = [
            self.variant(*ranged.BoltPistol),
            self.variant(*ranged.GravPistol),
            self.variant(*ranged.PlasmaPistol)] +\
            ([self.variant(*melee.Chainsword)] if sword else []) +\
            [self.variant(*melee.PowerSword),
             self.variant(*melee.PowerAxe),
             self.variant(*melee.PowerMaul)]
        self.claw = self.variant(*melee.LightningClaw)
        self.double += [self.claw,
                        self.variant(*melee.PowerFist)]

    def __init__(self, parent, sword=True, double=False, stern=False):
        super(SergeantWeapon, self).__init__(parent, 'Sergeant Equipment')
        if double:
            self.add_double(sword)
            if not stern:
                self.double.append(self.variant(*melee.ThunderHammer))
        self.add_single()
        if not double:
            self.add_double(sword)
            if not stern:
                self.double.append(self.variant(*melee.ThunderHammer))

    @staticmethod
    def ensure_pairs(first, second):
        for opt in second.single:
            opt.active = not first.cur in first.single
        for opt in first.single:
            opt.active = not second.cur in second.single


class ClawUser(Unit):
    def build_points(self):
        res = super(ClawUser, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 2 * get_cost(melee.LightningClaw) - 12
        return res


def add_pistol_options(obj):
    obj.variant(*ranged.BoltPistol)
    obj.variant(*ranged.PlasmaPistol)
    obj.variant(*ranged.GravPistol)


def add_combi_weapons(obj):
    obj.combi = [
        obj.variant(*ranged.CombiFlamer),
        obj.variant(*ranged.CombiGrav),
        obj.variant(*ranged.CombiMelta),
        obj.variant(*ranged.CombiPlasma),
        obj.variant(*ranged.StormBolter)]


def add_melee_weapons(obj, axe=True):
    obj.variant(*melee.Chainsword)
    obj.variant(*melee.PowerSword)
    if axe:
        obj.variant(*melee.PowerAxe)
    obj.variant(*melee.PowerMaul)
    obj.variant(*melee.PowerLance)
    obj.claw = obj.variant(*melee.LightningClaw)
    obj.variant(*melee.PowerFist)
    if 'CHARACTER' in obj.parent.keywords:
        obj.variant(*melee.ThunderHammerChar)
    else:
        obj.variant(*melee.ThunderHammer)


def add_special_weapons(obj):
    obj.spec = [
        obj.variant(*ranged.Flamer),
        obj.variant(*ranged.Plasmagun),
        obj.variant(*ranged.Meltagun),
        obj.variant(*ranged.Gravgun)]


def add_heavy_weapons(obj):
    obj.heavy = [
        obj.variant(*ranged.MissileLauncher),
        obj.variant(*ranged.HeavyBolter),
        obj.variant(*ranged.MultiMelta),
        obj.variant(*ranged.Lascannon),
        obj.variant(*ranged.GravCannonAmp),
        obj.variant(*ranged.PlasmaCannon)]


def add_term_melee_weapons(obj):
    obj.claw = obj.variant(*melee.LightningClaw)
    obj.fist = obj.variant(*melee.PowerFist)
    if 'CHARACTER' in obj.parent.keywords:
        obj.variant(*wargear.StormShieldChar)
        obj.variant(*melee.ThunderHammerChar)
    else:
        obj.variant(*wargear.StormShield)
        obj.variant(*melee.ThunderHammer)


def add_term_combi_weapons(obj):
    obj.variant(*ranged.StormBolter)
    obj.variant(*ranged.CombiPlasma)
    obj.variant(*ranged.CombiFlamer)
    obj.variant(*ranged.CombiMelta)


def add_term_heavy_weapons(obj):
    obj.heavy = [
        obj.variant(*ranged.HeavyFlamer),
        obj.variant(*ranged.AssaultCannon),
        obj.variant('Cyclone missile launcher and storm bolter',
                    get_costs(ranged.CycloneMissileLauncher, ranged.StormBolter),
                    gear=create_gears(ranged.CycloneMissileLauncher, ranged.StormBolter))]


def add_dred_heavy_weapons(obj):
    obj.variant(*ranged.AssaultCannon)
    obj.variant(*ranged.HeavyPlasmaCannon)
    obj.variant(*ranged.MultiMelta)
    obj.variant(*ranged.TwinLascannon)


class AstartesUnit(Unit):
    faction = ['Imperium', 'Adeptus Astartes']
    wiki_faction = 'Space Marines'


class SMUnit(AstartesUnit):
    faction = ['<Chapter>']

    @classmethod
    def calc_faction(cls):
        return ['BLACK TEMPLARS', 'ULTRAMARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'RAVEN GUARD', 'SALAMANDERS',
                'WHITE SCARS']


class CodexBAUnit(SMUnit):
    @classmethod
    def calc_faction(cls):
        return super(CodexBAUnit, cls).calc_faction() + ['BLOOD ANGELS',
                                                         '<BLOOD ANGELS SUCCESSORS>',
                                                         'FLESH TEARERS']


class CodexDAUnit(SMUnit):
    @classmethod
    def calc_faction(cls):
        return super(CodexDAUnit, cls).calc_faction() + ['DARK ANGELS',
                                                         '<DARK ANGELS SUCCESSORS>']


class CommonSMUnit(SMUnit):
    @classmethod
    def calc_faction(cls):
        return super(CommonSMUnit, cls).calc_faction() + ['BLOOD ANGELS',
                                                         '<BLOOD ANGELS SUCCESSORS>',
                                                          'FLESH TEARERS',
                                                          'DARK ANGELS',
                                                          '<DARK ANGELS SUCCESSORS>',
                                                          'SPACE WOLVES']


def get_name(item):
    return item[0]


def get_cost(item):
    return item[1]

def get_costs(*items):
    return sum(i[1] for i in items)

def join_and(*items):
    items = [get_name(el) for el in items]
    if len(items) < 3:
        return u' and '.join(items)
    return u'{} and {}'.format(u', '.join(items[:-1]), items[-1])


def points_price(price, *gears):
    for el in gears:
        price += el[1]
    return price


def create_gears(*gears):
    result = []
    for el in gears:
        result.append(Gear(el[0]))
    return result
