from hq import Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince,\
    DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord,\
    PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer,\
    Warpsmith, Kharn, Haarken,\
    Lucius, Cypher, ExaltedChampion
from elites import Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute,\
    Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines,\
    PlagueMarinesV2
from troops import ChaosCultists, ChaosSpaceMarines, LegionBerzerkers,\
    LegionPlagues, LegionNoises
from fast import ChaosBikers, Raptors, WarpTalons, ChaosSpawn
from heavy import ChaosLandRaider, ChaosPredator, ChaosVindicator,\
    Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators
from transport import ChaosRhino
from fliers import Heldrake
from lords import LordOfSkulls

unit_types = [Abaddon, BikeLord, BikeSorcerer, ChaosLord,
              DaemonPrince, DarkApostle, DiscLord, DiscSorcerer,
              Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer,
              SteedLord, SteedSorcerer, TermoLord, TermoSorcerer,
              Warpsmith, Berzerkers, ChaosTerminators, Chosen, Fallen,
              Helbrute, Mutilators, NoiseMarines, PlagueMarines,
              Possessed, RubricMarines, ChaosCultists,
              ChaosSpaceMarines, ChaosBikers, Raptors, WarpTalons,
              ChaosLandRaider, ChaosPredator, ChaosVindicator,
              Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators,
              Heldrake, ChaosRhino, LordOfSkulls, Kharn,
              LegionBerzerkers, LegionPlagues,
              Lucius, LegionNoises, Cypher, ChaosSpawn,
              ExaltedChampion, PlagueMarinesV2, Haarken]
