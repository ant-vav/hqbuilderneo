from builder.games.wh40k8ed.unit import FlierUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList, OptionalSubUnit
import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class Stormfang(FlierUnit, armory.SWUnit):
    type_name = u'Stormfang Gunship'
    type_id = 'storfang_v1'
    power = 15
    keywords = ['Vehicle', 'Transport', 'Fly']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Stormfang.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Two Twin heavy bolters', get_cost(ranged.TwinHeavyBolter) * 2,
                         gear=[Gear('Twin heavy bolter', count=2)])
            self.variant(*ranged.SkyhammerMissileLauncher)
            self.variant('Two Twin Multi-meltas', get_cost(ranged.TwinMultiMelta) * 2,
                         gear=[Gear('Twin multi-melta', count=2)])

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Stormfang.Weapon2, self).__init__(parent, '')
            self.variant("Two stormstrike missile launchers", get_cost(ranged.StormstrikeMissileLauncher) * 2,
                         gear=[Gear("Stormstrike missile launcher", count=2)])
            self.variant("Two lascannons", 2 * get_cost(ranged.Lascannon), gear=[Gear('Lascannon', count=2)])

    def __init__(self, parent):
        super(Stormfang, self).__init__(parent=parent, gear=[
            Gear('Helfrost destructor')
        ], points=points_price(get_cost(units.StormfangGunship), ranged.HelfrostDestructor))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)


class SWStormhawk(FlierUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.StormhawkInterceptor)
    type_id = 'stormhawk_v1'

    keywords = ['Vehicle', 'Fly']
    power = 10

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(SWStormhawk.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.IcarusStormcannon)
            self.variant(*ranged.LasTalon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(SWStormhawk.Weapon2, self).__init__(parent, '')
            self.variant('Two heavy bolters', get_cost(ranged.HeavyBolter) * 2,
                         gear=create_gears(ranged.HeavyBolter, ranged.HeavyBolter))
            self.variant(*ranged.SkyhammerMissileLauncher)
            self.variant(*ranged.TyphoonMissileLauncher)

    def __init__(self, parent):
        gear = [ranged.AssaultCannon, ranged.AssaultCannon]
        cost = points_price(get_cost(units.StormhawkInterceptor), *gear)
        super(SWStormhawk, self).__init__(parent, name=get_name(units.StormhawkInterceptor),
                                        gear=create_gears(*gear),
                                        points=cost)
        self.Weapon1(self)
        self.Weapon2(self)


class Stormwolf(FlierUnit, armory.SWUnit):
    type_name = u'Stormwolf'
    type_id = 'stormwolf_v1'
    power = 16
    keywords = ['Vehicle', 'Transport', 'Fly']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Stormwolf.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Two Twin heavy bolters', 2 * get_cost(ranged.TwinHeavyBolter),
                         gear=[Gear('Twin heavy bolter', count=2)])
            self.variant(*ranged.SkyhammerMissileLauncher)
            self.variant('Two Twin Multi-meltas', 2 * get_cost(ranged.TwinMultiMelta),
                         gear=[Gear('Twin multi-melta', count=2)])

    def __init__(self, parent):
        gear = [ranged.TwinHelfrostCannon, ranged.Lascannon, ranged.Lascannon]
        cost = points_price(get_cost(units.Stormwolf), *gear)
        super(Stormwolf, self).__init__(parent=parent, gear=create_gears(*gear),
                                        points=cost)
        self.wep1 = self.Weapon1(self)
