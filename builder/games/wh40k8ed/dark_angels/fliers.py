from builder.games.wh40k8ed.unit import FlierUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList
import armory, units, ranged
from builder.games.wh40k8ed.utils import get_name, create_gears, points_price


class NephilimJetfighter(FlierUnit, armory.DAUnit):
    type_name = get_name(units.NephilimJetfighter)
    type_id = 'nephilim_jetfighter_v1'
    faction = ['Ravenwing']
    keywords = ['Vehicle', 'Fly']
    power = 9

    class Weapon(OneOf):
        def __init__(self, parent):
            super(NephilimJetfighter.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant(*ranged.AvengerMegaBolter)
            self.variant(*ranged.TwinLascannon)

    def __init__(self, parent):
        gear = [ranged.BlackswordMissileLauncher] * 2 + [ranged.TwinHeavyBolter]
        super(NephilimJetfighter, self).__init__(parent=parent, gear=create_gears(*gear),
                                                 points=points_price(armory.get_cost(units.NephilimJetfighter), *gear))
        self.wep = self.Weapon(self)


class RavenwingDarkTalon(FlierUnit, armory.DAUnit):
    type_name = get_name(units.RavenwingDarkTalon)
    type_id = 'ravenwing_dark_talon_v1'
    faction = ['Ravenwing']
    keywords = ['Vehicle', 'Fly']
    power = 9

    def __init__(self, parent):
        gear = [ranged.HurricaneBolter] * 2 + [ranged.RiftCannon]
        super(RavenwingDarkTalon, self).__init__(parent=parent, gear=create_gears(*gear),
                                                 points=points_price(armory.get_cost(units.RavenwingDarkTalon), *gear),
                                                 static=True)


class DAStormravenGunship(FlierUnit, armory.DAUnit):
    type_name = get_name(units.StormravenGunship)
    type_id = 'da_stormraven_gunship_v1'

    keywords = ['Vehicle', 'Fly', 'Transport']
    power = 15

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DAStormravenGunship.Weapon1, self).__init__(parent, 'Weapon')
            self.twinlinkedheavybolter = self.variant(*ranged.TwinHeavyBolter)
            self.twinlinkedmultimelta = self.variant(*ranged.TwinMultiMelta)
            self.typhoonmissilelauncher = self.variant(*ranged.TyphoonMissileLauncher)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DAStormravenGunship.Weapon2, self).__init__(parent, '')
            self.twinlinkedassaultcannon = self.variant(*ranged.TwinAssaultCannon)
            self.twinlinkedlascannon = self.variant(*ranged.TwinLascannon)
            self.twinlinkedplasmacannon = self.variant(*ranged.TwinHeavyPlasmaCannon)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DAStormravenGunship.Options, self).__init__(parent, 'Options')
            self.hurricanebolters = self.variant('Side sponsons with hurricane bolters',
                                                 armory.get_cost(ranged.HurricaneBolter) * 2,
                                                 gear=create_gears(
                                                     ranged.HurricaneBolter,
                                                     ranged.HurricaneBolter
                                                 ))

    def __init__(self, parent):
        gear = [ranged.StormstrikeMissileLauncher] * 2
        super(DAStormravenGunship, self).__init__(
            parent=parent,
            gear=create_gears(*gear),
            points=points_price(armory.get_cost(units.StormravenGunship), *gear))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.opt = self.Options(self)
