__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OneOf, OptionsList
import wargear, melee, ranged


class LordMelee(OneOf):
    def __init__(self, parent, void=False):
        super(LordMelee, self).__init__(parent, 'Melee weapons')
        self.variant(*ranged.StaffOfLight)
        if void:
            self.variant(*melee.Voidscythe)
        self.variant(*melee.HyperphaseSword)
        self.variant(*melee.Voidblade)
        self.variant(*melee.Warscythe)


class Orb(OptionsList):
    def __init__(self, parent):
        super(Orb, self).__init__(parent, 'Options')
        self.variant(*wargear.ResurrectionOrb)


class NecronUnit(Unit):
    faction = ['Necrons']
    wiki_faction = 'Necrons'


class DynastyUnit(NecronUnit):
    faction = ['<Dynasty>']

    @classmethod
    def calc_faction(cls):
        return ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NIHILAKH', 'NEPREKH', 'MAYNARKH']
