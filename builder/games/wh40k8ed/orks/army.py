from builder.games.wh40k8ed.rosters import Wh40k8edBase
from detachments import Vanguard


class Orks8Ed(Wh40k8edBase):
    army_name = u'Army of Orks'
    faction_base = u'ORK'
    alternate_factions = []
    army_id = u'orks'

    def __init__(self):
        super(Orks8Ed, self).__init__()
        for det in [Vanguard]:
            self.det.build_detach(det, u'ORK', group=det.faction_base)
