import armory
from builder.games.wh40k8ed.unit import Unit, FortUnit
from builder.games.wh40k8ed.utils import *
import units


class MekboyWorkshop(FortUnit, armory.OrkClanUnit):
    type_name = get_name(units.MekboyWorkshop)
    type_id = 'mek_workshop_v1'
    power = 4

    def __init__(self, parent):
        super(MekboyWorkshop, self).__init__(parent, points=get_cost(units.MekboyWorkshop),
                                             static=True)
