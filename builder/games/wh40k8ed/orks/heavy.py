from builder.games.wh40k8ed.unit import Unit, HeavyUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, \
    Gear, UnitDescription, Count, ListSubUnit, SubUnit, UnitList,\
    OptionalSubUnit
import melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *
import armory


class BigGunz(HeavyUnit, armory.OrkClanUnit):
    type_name = get_name(units.BigGunz) + ' (Index)'
    type_id = 'big_gunz_v1'
    keywords = ['Infantry', 'Artillery', 'Vehicle', 'Gretchin', 'Grot Gunners']
    power = 2

    class Gun(OneOf):
        def __init__(self, parent):
            super(BigGunz.Gun, self).__init__(parent, 'Big Gun Equipped')
            self.variant(*ranged.Kannon)
            self.variant(*ranged.Lobba)
            self.variant(*ranged.ZzapGun)

    class BigGun(ListSubUnit):
        def __init__(self, parent):
            super(BigGunz.BigGun, self).__init__(parent, 'Big Gun', points=get_costs(units.BigGunz) + get_costs(units.GrotGunners) * 2,
                                                 gear=UnitDescription('Grot Gunner', count=2))
            self.gun = BigGunz.Gun(self)

    def __init__(self, parent):
        super(BigGunz, self).__init__(parent)
        self.models = UnitList(self, self.BigGun, min_limit=1, max_limit=6)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class MekGunz(HeavyUnit, armory.OrkClanUnit):
    type_name = get_name(units.MekGunz)
    type_id = 'mek_gunz_v1'
    keywords = ['Vehicle', 'Artillery', 'Gretchin']
    power = 2

    class MekGun(ListSubUnit):
        def __init__(self, parent):
            super(MekGunz.MekGun, self).__init__(parent, 'Mek Gun', points=get_costs(units.MekGunz),
                                                 gear=UnitDescription('Grot Gunner', count=6))
            self.gun = MekGunz.Gun(self)

    class Gun(OneOf):
        def __init__(self, parent):
            super(MekGunz.Gun, self).__init__(parent, 'Mek Gun Equipped')
            self.variant(*ranged.Bubblechukka)
            self.variant(*ranged.KustomMegaKannon)
            self.variant(*ranged.SmashaGun)
            self.variant(*ranged.TraktorKannon)

    def __init__(self, parent):
        super(MekGunz, self).__init__(parent)
        self.models = UnitList(self, self.MekGun, min_limit=1, max_limit=6)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class Battlewagon(HeavyUnit, armory.OrkClanUnit):
    type_name = get_name(units.Battlewagon)
    type_id = 'battlewagon_v1'
    power = 8

    class Options(OptionsList):
        def __init__(self, parent):
            super(Battlewagon.Options, self).__init__(parent, 'Options')
            self.variant(*melee.DeffRolla)
            self.variant(*wargear.HardCase)

    class Kannons(OptionsList):
        def __init__(self, parent):
            super(Battlewagon.Kannons, self).__init__(parent, 'Kannon', limit=1)
            self.variant(*ranged.Kannon)
            self.variant(*ranged.Killkannon)
            self.variant(*ranged.ZzapGun)

    def __init__(self, parent):
        super(Battlewagon, self).__init__(parent, points=get_costs(units.Battlewagon))
        self.k = self.Kannons(self)
        self.shootas = Count(self, 'Shootas', 0, 4, get_cost(ranged.Shoota), gear=create_gears(ranged.Shoota)[0])
        armory.BattlewagonGubbins(self)
        self.ops = self.Options(self)


class Gunwagon(HeavyUnit, armory.OrkClanUnit):
    type_name = get_name(units.Gunwagon)
    type_id = 'gunwagon_v1'
    power = 9

    class Kannons(OneOf):
        def __init__(self, parent):
            super(Gunwagon.Kannons, self).__init__(parent, 'Kannon')
            self.variant(*ranged.Kannon)
            self.variant(*ranged.Killkannon)
            self.variant(*ranged.ZzapGun)

    def __init__(self, parent):
        super(Gunwagon, self).__init__(parent, points=get_costs(units.Gunwagon), gear=[Gear('Periscope')])
        self.k = self.Kannons(self)
        self.shootas = Count(self, 'Shootas', 0, 4, get_cost(ranged.Shoota), gear=create_gears(ranged.Shoota)[0])
        armory.BattlewagonGubbins(self)


class Bonebreaka(HeavyUnit, armory.OrkClanUnit):
    type_name = get_name(units.Bonebreaka)
    type_id = 'bonebreaka_v1'
    power = 9

    def __init__(self, parent):
        super(Bonebreaka, self).__init__(parent, points=points_price(get_costs(units.Gunwagon), melee.DeffRolla),
                                         gear=create_gears(melee.DeffRolla))
        self.k = Battlewagon.Kannons(self)
        self.shootas = Count(self, 'Shootas', 0, 4, get_cost(ranged.Shoota), gear=create_gears(ranged.Shoota)[0])
        armory.BattlewagonGubbins(self)


class KillaKans(HeavyUnit, armory.OrkClanUnit):
    type_name = get_name(units.KillaKans)
    type_id = 'killa_kans_v1'
    keywords = ['Vehicle', 'Gretchin']
    power = 2

    class KillaKan(ListSubUnit):
        class Ranged(OneOf):
            def __init__(self, parent):
                super(KillaKans.KillaKan.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.BigShoota)
                self.variant(*ranged.RokkitLauncha)
                # self.variant(*ranged.KustomMegaBlasta)
                self.variant(*ranged.Grotzooka)
                self.variant(*ranged.Skorcha)

        class Melee(OneOf):
            def __init__(self, parent):
                super(KillaKans.KillaKan.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.BuzzSaw)
                self.variant(*melee.KanKlaw)
                self.variant(*melee.Drilla)
        
        def __init__(self, parent):
            super(KillaKans.KillaKan, self).__init__(parent, 'Killa kan', points=get_costs(units.KillaKans))
            self.Ranged(self)
            self.Melee(self)

    def __init__(self, parent):
        super(KillaKans, self).__init__(parent)
        self.models = UnitList(self, self.KillaKan, min_limit=1, max_limit=5)

    def build_power(self):
        power = self.power
        cnt = self.get_count()
        if cnt > 3:
            power += 12
        elif cnt > 1:
            power += 5
        return power

    def get_count(self):
        return self.models.count


class DeffDreads(HeavyUnit, armory.OrkClanUnit):
    type_name = get_name(units.DeffDreads)
    type_id = 'deff_dreads_v1'
    keywords = ['Vehicle']
    power = 5

    class DeffDread(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent, is_ranged=True):
                super(DeffDreads.DeffDread.Weapon, self).__init__(parent, 'Weapon')
                if is_ranged:
                    self.variant(*ranged.BigShoota)
                else:
                    self.variant(*melee.DredKlaw)
                self.variant(*ranged.RokkitLauncha)
                self.variant(*ranged.KustomMegaBlasta)
                self.variant(*ranged.Skorcha)
                self.variant(*melee.DreadSaw)

        def __init__(self, parent):
            cost = points_price(get_cost(units.DeffDreads))
            super(DeffDreads.DeffDread, self).__init__(parent, 'Deff Dread', cost)
            self.Weapon(self)
            self.Weapon(self)
            self.Weapon(self, False)
            self.Weapon(self, False)

    def __init__(self, parent):
        super(DeffDreads, self).__init__(parent)
        self.models = UnitList(self, self.DeffDread, min_limit=1, max_limit=3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.get_count() + (self.get_count() == 3)


class Morkanaut(HeavyUnit, armory.OrkClanUnit):
    type_name = get_name(units.Morkanaut)
    type_id = 'morkanaut_v1'
    keywords = ['Vehicle', 'Transport']
    power = 15

    class Options(OptionsList):
        def __init__(self, parent):
            super(Morkanaut.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.KustomForceField)

    def __init__(self, parent):
        gear = [
            ranged.KustomMegaBlasta,
            ranged.TwinBigShoota,
            ranged.TwinBigShoota,
            ranged.RokkitLauncha,
            ranged.RokkitLauncha,
            ranged.KustomMegaZappa,
            melee.KlawOfGork
        ]
        costs = points_price(get_costs(units.Morkanaut), *gear)
        gear = create_gears(*gear)
        super(Morkanaut, self).__init__(parent, points=costs, gear=gear)
        self.Options(self)


class Gorkanaut(HeavyUnit, armory.OrkClanUnit):
    type_name = get_name(units.Gorkanaut)
    type_id = 'gorkanaut_v1'
    keywords = ['Vehicle', 'Transport']
    power = 15

    def __init__(self, parent):
        gear = [
            ranged.DeffstormMegaShoota,
            ranged.TwinBigShoota,
            ranged.TwinBigShoota,
            ranged.RokkitLauncha,
            ranged.RokkitLauncha,
            ranged.Skorcha,
            melee.KlawOfGork
        ]
        cost = points_price(get_costs(units.Gorkanaut), *gear)
        gear = create_gears(*gear)
        super(Gorkanaut, self).__init__(parent, points=cost, gear=gear, static=True)


class Lootas(HeavyUnit, armory.OrkClanUnit):
    type_name = get_name(units.Lootas)
    type_id = 'lootas_v1'
    keywords = ['Infantry', get_name(units.Lootas)]
    power = 4
    model_gear = [ranged.Deffgun, ranged.Stikkbombs]
    model = UnitDescription('Loota', options=create_gears(*model_gear))

    class SpannersOptions(OptionalSubUnit):
        def __init__(self, parent):
            super(Lootas.SpannersOptions, self).__init__(parent, 'Options')
            self.spanner = UnitList(self, Lootas.Spanner, min_limit=1, max_limit=3)

    class Spanner(ListSubUnit):
        class Ranged(OneOf):
            def __init__(self, parent):
                super(Lootas.Spanner.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.KustomMegaBlasta)
                self.variant(*ranged.BigShoota)
                self.variant(*ranged.RokkitLauncha)

        def __init__(self, parent):
            gear = [ranged.Stikkbombs]
            cost = points_price(get_costs(units.Lootas), *gear)
            super(Lootas.Spanner, self).__init__(parent, 'Spanner', points=cost, gear=create_gears(*gear))
            self.Ranged(self)

    def __init__(self, parent):
        super(Lootas, self).__init__(parent)
        self.lootas = Count(self, 'Lootas', min_limit=5, max_limit=15,
                            points=points_price(get_costs(units.Lootas), *self.model_gear),
                            gear=self.model.clone())
        self.spanner_opt = self.SpannersOptions(self)

    def get_count(self):
        return self.lootas.cur + self.spanner_opt.count

    def check_rules(self):
        super(Lootas, self).check_rules()
        self.lootas.min, self.lootas.max = (5 - self.spanner_opt.count,
                                            15 - self.spanner_opt.count)
        if self.spanner_opt.count > (self.get_count() / 5):
            self.error("One Loota per 5 models in unit may be replaced by Spanner")

    def build_power(self):
        return self.power + (9 if self.get_count() > 10 else (4 if self.get_count() > 5 else 0))


class FlashGitz(HeavyUnit, armory.OrkUnit):
    faction = ['Freebooterz']
    keywords = ['Infantry']
    type_id = 'flashgitz_v1'
    type_name = get_name(units.FlashGitz)
    power = 7
    model_gear = [ranged.Snazzgun, ranged.Stikkbombs]
    model = UnitDescription('Flash Git', options=create_gears(*model_gear))


    # gunz for hire hack
    @classmethod
    def check_faction(cls, fac):
        return True

    class FlashGit(Count):
        @property
        def description(self):
            return [FlashGitz.model.clone().set_count(self.cur)]

    class Kaptin(Unit):
        class Weapon(OptionsList):
            def __init__(self, parent):
                super(FlashGitz.Kaptin.Weapon, self).__init__(parent, 'Weapon', limit=1)
                self.variant(*ranged.Slugga)
                self.variant(*melee.Choppa)

        class Options(OptionsList):
            def __init__(self, parent):
                super(FlashGitz.Kaptin.Options, self).__init__(parent, 'Options')
                self.variant(*wargear.GitfindaSquig)

        def __init__(self, parent):
            cost = points_price(get_costs(units.FlashGitz), *FlashGitz.model_gear)
            gear = create_gears(*FlashGitz.model_gear)
            super(FlashGitz.Kaptin, self).__init__(parent, name='Kaptin', points=cost, gear=gear)
            self.Weapon(self)
            self.opts = self.Options(self)
            # self.ammo_runt = FlashGitz.AmmoRunt(self)

    def __init__(self, parent):
        super(FlashGitz, self).__init__(parent)
        self.kaptin = SubUnit(self, self.Kaptin(parent=self))
        self.models = self.FlashGit(self, 'Flash Gitz', min_limit=4, max_limit=9,
                                    points=points_price(get_costs(units.FlashGitz), *FlashGitz.model_gear))

        self.runtz = Count(self, 'Ammo runtz', 0, 2, get_costs(units.AmmoRunt),
                           gear=UnitDescription('Ammo Runt'))

    def check_rules(self):
        super(FlashGitz, self).check_rules()
        self.runtz.max = self.get_count() / 5

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return self.power * (1 + (self.get_count() > 5))
