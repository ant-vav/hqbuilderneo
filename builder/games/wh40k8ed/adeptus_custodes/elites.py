__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import Gear, UnitDescription, OneOf,\
    ListSubUnit, UnitList, SubUnit, OptionsList, Count
import armory
import units
import wargear
import ranged
import melee
from builder.games.wh40k8ed.utils import *


class CustodianWardens(ElitesUnit, armory.CustodesUnit):
    type_name = get_name(units.CustodianWardens)
    type_id = 'custodian_warders_v1'
    keywords = ['Infantry']

    power = 10

    class Warder(Count):
        @property
        def description(self):
            return UnitDescription('Custodian Warden', count=self.cur - self.parent.spear.cur, options=create_gears(ranged.CastellanAxe, melee.Misericordia))

    def __init__(self, parent):
        super(CustodianWardens, self).__init__(parent)
        self.models = self.Warder(self, 'Custodian Warders', 3, 10, points_price(get_cost(units.CustodianWardens), melee.Misericordia), per_model=True)
        self.spear = Count(self, 'Guardian spears', 0, 3, get_cost(ranged.GuardianSpear),
                           gear=UnitDescription('Custodian Warden', options=create_gears(ranged.GuardianSpear, melee.Misericordia)))

    def get_count(self):
        return self.models.cur

    def build_points(self):
        return super(CustodianWardens, self).build_points() + get_cost(ranged.CastellanAxe) * (self.models.cur - self.spear.cur)

    def check_rules(self):
        super(CustodianWardens, self).check_rules()
        self.spear.max = self.models.cur

    def build_power(self):
        return self.power + 4 * (self.models.count - 3)


class AllarusVexilus(ElitesUnit, armory.CustodesUnit):
    type_name = get_name(units.AllarusVexilusPraetor)
    type_id = 'allarus_vexilus_v1'
    kwname = 'Vexilus Praetor'
    keywords = ['Infantry', 'Character', 'Terminator']

    power = 7

    class Vexilla(OneOf):
        def __init__(self, parent):
            super(AllarusVexilus.Vexilla, self).__init__(parent, 'Custodes Vexilla')
            self.variant(*wargear.VexillaImperius)
            self.variant(*wargear.VexillaDefensor)
            self.variant(*wargear.VexillaMagnifica)

    def __init__(self, parent):
        super(AllarusVexilus, self).__init__(parent, points=points_price(get_cost(units.AllarusVexilusPraetor), ranged.BalistusGrenadeLauncher),
                                             gear=create_gears(ranged.BalistusGrenadeLauncher))
        self.Vexilla(self)
        armory.Misericordia(self)


class Vexilus(ElitesUnit, armory.CustodesUnit):
    type_name = get_name(units.VexilusPraetor)
    type_id = 'vexilus_v1'
    keywords = ['Infantry', 'Character']

    power = 6

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Vexilus.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.GuardianSpear)
            self.variant(*ranged.CastellanAxe)
            self.variant(*wargear.StormShieldChar)
            self.mis = self.variant(*melee.Misericordia)

    def __init__(self, parent):
        super(Vexilus, self).__init__(parent, points=get_cost(units.VexilusPraetor))
        AllarusVexilus.Vexilla(self)
        self.wep = self.Weapon(self)
        self.mis = armory.Misericordia(self)

    def check_rules(self):
        super(Vexilus, self).check_rules()
        self.mis.used = self.mis.visible = self.wep.cur != self.wep.mis


class AllarusCustodians(ElitesUnit, armory.CustodesUnit):
    type_name = get_name(units.AllarusCustodians)
    type_id = 'allarus_v1'
    keywords = ['Infantry', 'Terminator']

    power = 13

    class Custorian(ListSubUnit):
        type_name = 'Allarus Custodian'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(AllarusCustodians.Custorian.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ranged.CastellanAxe)
                self.variant(*ranged.GuardianSpear)

        def __init__(self, parent):
            super(AllarusCustodians.Custorian, self).__init__(parent, points=points_price(get_cost(units.AllarusCustodians), ranged.BalistusGrenadeLauncher))
            self.Weapon(self)
            armory.Misericordia(self)

    def __init__(self, parent):
        super(AllarusCustodians, self).__init__(parent)
        self.models = UnitList(self, self.Custorian, 3, 10)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power + 5 * (self.models.count - 3)


class VenerableContemptor(ElitesUnit, armory.CustodesUnit):
    type_name = get_name(units.VenerableContemptorDreadnought)
    type_id = 'cust_dread_v'
    keywords = ['Vehicle', 'Dreadnought']
    power = 10

    class Weapon(OneOf):
        def __init__(self, parent):
            super(VenerableContemptor.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.MultiMelta)
            self.variant(*ranged.KheresPatternAssaultCannon)

    def __init__(self, parent):
        gear = [melee.DreadnoughtCombatWeapon, ranged.CombiBolter]
        super(VenerableContemptor, self).__init__(parent, points=points_price(get_cost(units.VenerableContemptorDreadnought), *gear),
                                                  gear=create_gears(*gear))
        self.Weapon(self)
