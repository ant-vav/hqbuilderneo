__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.options import Gear, UnitDescription, OneOf
import armory


class Greyfax(HqUnit, armory.InquisitionUnit):
    type_name = u'Inquisitor Greyfax'
    type_id = 'greyfax_v1'
    faction = ['Ordo Hereticus']
    keywords = ['Character', 'Infantry', 'Inquisitor']
    power = 5

    def __init__(self, parent):
        super(Greyfax, self).__init__(
            parent, static=True, unique=True, points=85,
            gear=[Gear('Master-crafted condemnor boltgun'), Gear('Master-crafted power sword'),
                  Gear('Frag grenades'), Gear('Krak grenades'), Gear('Psyk-out grenades')])


class Eisenhorn(HqUnit, armory.InquisitionUnit):
    type_name = u'Inquisitor Eisenhorn'
    type_id = 'eisenhorn_v1'
    faction = ['Ordo Xenos']
    kwname = 'Eisenhorn'
    keywords = ['Character', 'Infantry', 'Inquisitor', 'Psyker']
    power = 5

    def __init__(self, parent):
        super(Eisenhorn, self).__init__(
            parent, static=True, unique=True, points=100,
            gear=[Gear('Artificer bolt pistol'), Gear('Barbarisater'),
                  Gear('Runestaff'), Gear('Electrobane grenades')])


class Karamazov(HqUnit, armory.InquisitionUnit):
    type_name = u'Inquisitor Karamazov'
    type_id = 'inquisitorkaramazov_v1'
    faction = ['Ordo Hereticus']
    keywords = ['Character', 'Vehicle', 'Inquisitor']
    power = 8

    def __init__(self, parent):
        super(Karamazov, self).__init__(
            parent=parent, static=True, unique=True, points=150,
            gear=[Gear('Master-crafted power sword'),
                  UnitDescription('Throne of Judgement', options=[
                      Gear('Master-crafted multi melta'), Gear('Stomping feet')])])


class Coteaz(HqUnit, armory.InquisitionUnit):
    type_name = u'Inquisitor Coteaz'
    type_id = 'inquisitorcoteaz_v1'
    faction = ['Ordo Malleus']
    keywords = ['Character', 'Vehicle', 'Inquisitor', 'Psyker']
    power = 4

    def __init__(self, parent):
        super(Coteaz, self).__init__(
            parent=parent, static=True, unique=True, points=100,
            gear=[Gear('Bolt pistol'),
                  Gear('Master-crafted Nemesis Daemon hammer'),
                  Gear('Psyber-eagle')])


class Inquisitor(HqUnit, armory.OrdoUnit):
    type_name = u'Inquisitor'
    type_id = 'inquisitor_v1'

    keywords = ['Character', 'Infantry']
    power = 5

    def __init__(self, parent):
        super(Inquisitor, self).__init__(parent=parent, points=55,
                                         gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.Weapon1(self)
        self.Weapon2(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Inquisitor.Weapon1, self).__init__(parent, 'Pistol')
            armory.add_inq_pistol_weapons(self, True)
            armory.add_inq_ranged_weapons(self, True)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Inquisitor.Weapon2, self).__init__(parent, 'Weapon')
            self.variant('Chainsword')
            armory.add_inq_melee_weapons(self, True)
            # armory.add_inq_ranged_weapons(self, True)
            armory.add_inq_force_weapons(self)


class TermoMalleus(HqUnit, armory.InquisitionUnit):
    type_name = u'Ordo Malleus Inquisitor in Terminator armour'
    type_id = 'malleus_termo_inq_v1_'
    keywords = ['Character', 'Infantry', 'Terminator']
    power = 7

    def __init__(self, parent):
        super(TermoMalleus, self).__init__(parent, points=91,
                                           gear=[Gear('Psyk-out grenades')])
        self.Weapon1(self)
        self.Weapon2(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TermoMalleus.Weapon1, self).__init__(parent, 'Ranged weapon')
            self.variant('Storm bolter', 2)
            self.variant('Combi-flamer', 11)
            self.variant('Combi-melta', 19)
            self.variant('Combi-plasma', 15)
            self.variant('Psycannon', 20)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TermoMalleus.Weapon2, self).__init__(parent, 'Melee weapon')
            self.variant('Nemesis daemon hammer', 25)
            armory.add_inq_force_weapons(self)
