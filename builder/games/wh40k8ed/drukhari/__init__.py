from hq import Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus
from elites import Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn,\
    Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul
from troops import KabaliteWarriors, Wracks, Wyches
from fast import ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges
from heavy import Cronos, Ravager, Talos
from transports import Raider, Venom
from fliers import Razorwing, Voidraven

from builder.games.wh40k8ed.sections import HQSection, ElitesSection, FastSection, UnitType


class ArchonHQ(HQSection):
    def __init__(self, *args, **kwargs):
        super(ArchonHQ, self).__init__(*args, **kwargs)
        self.archon_types = []

    def unit_type_add(self, ut):
        if ut == Archon:
            self.archon_types += [super(ArchonHQ, self).unit_type_add(ut)]
        else:
            super(ArchonHQ, self).unit_type_add(ut)

    def count_archons(self):
        res = 0
        for ut in self.archon_types:
            res += ut.count
        return res


class DEElites(ElitesSection):
    def __init__(self, *args, **kwargs):
        super(DEElites, self).__init__(*args, **kwargs)
        self.master_types = []
        self.court_types = []

    def unit_type_add(self, ut):
        if ut == Beastmaster:
            self.master_types += [super(DEElites, self).unit_type_add(ut)]
        elif ut in [Lhamaean, UrGhul, Sslyth, Medusae]: 
            self.court_types += [super(DEElites, self).unit_type_add(ut, slot=0)]
        else:
            super(DEElites, self).unit_type_add(ut)

    def count_masters(self):
        res = 0
        for ut in self.master_types:
            res += ut.count
        return res

    def check_rules(self):
        super(DEElites, self).check_rules()
        court_count = sum(float(t.count) for t in self.court_types)
        if court_count > 4:
            self.error('No more then 4 COURT OF ARCHON models may be taken in the same detachment')
        if court_count > 0 and self.parent.hq.count_archons() == 0:
            self.error("You can only include COURT OF ARCHON models if detachment also contains at least one ARCHON")
        master_count = self.count_masters()
        if master_count > 0:
            if self.parent.fast is None:
                self.error("You must include at least one DRUKHARI BEAST unit for every BEASTMASTER in the detachment")
            else:
                if self.parent.fast.count_beasts() < master_count:
                    self.error("You must include at least one DRUKHARI BEAST unit for every BEASTMASTER in the detachment")


class BeastFast(FastSection):
    def __init__(self, *args, **kwargs):
        super(BeastFast, self).__init__(*args, **kwargs)
        self.beast_types = []

    def unit_type_add(self, ut):
        if ut in [RazorwingFlocks, Khymerae, ClawedFiends]:
            self.beast_types += [super(BeastFast, self).unit_type_add(ut)]
        else:
            super(BeastFast, self).unit_type_add(ut)

    def count_beasts(self):
        return sum(float(t.count) * float(t.slot) for t in self.beast_types)
    
    def count_slots(self):
        res = sum(float(t.count) * float(t.slot) for t in self.types if t not in self.beast_types)
        beast_count = 0 if self.parent.elite.count_masters() > 0 else\
                      self.count_beasts()
        return res + beast_count

    def check_rules(self):
        super(BeastFast, self).check_rules()
        if self.count_beasts() > 0 and self.parent.elite.count_masters() == 0:
            self.error("You can only include DRUKHARI BEASTS if there are BEASTMASTAR units in the same detachment")
        if self.count_beasts() > 3:
            self.error("You cannot include more then 3 DRUKHARI BEAST units in the same detachment")
    

def get_drukhari_patrol_cp(detach_list):
    """Count DRUKHARI patrol detachments and add corrsponding number of CP
    """
    from builder.games.wh40k8ed.rosters import DetachPatrol
    det_count = sum(isinstance(det, DetachPatrol) and u'DRUKHARI' in det.army_factions for det in detach_list)
    return 8 if det_count >= 6 else (4 if det_count >= 3 else 0)


unit_types = [Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus,
              Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn,
              Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul,
              KabaliteWarriors, Wracks, Wyches,
              ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges,
              Cronos, Ravager, Talos, Raider, Venom, Razorwing, Voidraven]
