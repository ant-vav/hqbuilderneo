__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, OptionsList,\
    Gear, SubUnit, UnitDescription, Count, ListSubUnit, UnitList
import armory, melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class Talos(HeavyUnit, armory.CovenUnit):
    type_name = get_name(units.Talos)
    type_id = 'talos_v1'

    keywords = ['Monster']
    power = 6

    class SingleTalos(ListSubUnit):
        type_name = u'Talos'

        class Melee1(OneOf):
            def __init__(self, parent):
                super(Talos.SingleTalos.Melee1, self).__init__(parent, 'Melee')
                self.variant(*melee.MacroScalpel)
                self.variant(*melee.IchorInjector)
                self.variant(*ranged.TwinLiquifierGun)

        class Melee2(OneOf):
            def __init__(self, parent):
                super(Talos.SingleTalos.Melee2, self).__init__(parent, 'Melee')
                self.variant(*melee.MacroScalpel)
                self.variant(*melee.ChainFlails)
                self.variant(*melee.TalosGauntlet)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(Talos.SingleTalos.Ranged, self).__init__(parent, 'Ranged')
                self.variant('Two splinter cannons', points=get_cost(ranged.SplinterCannon) * 2,
                             gear=create_gears(ranged.SplinterCannon) * 2)
                self.variant(*ranged.StingerPod)
                self.variant('Two heat lances', points=get_cost(ranged.HeatLance) * 2,
                             gear=create_gears(ranged.HeatLance) * 2)
                self.variant('Two haywire blaster', points=get_cost(ranged.HaywireBlaster) * 2,
                             gear=create_gears(ranged.HaywireBlaster) * 2)

        def __init__(self, parent):
            super(Talos.SingleTalos, self).__init__(parent, 'Talos', points=get_cost(units.Talos))
            self.Melee1(self)
            self.Melee2(self)
            self.Ranged(self)

    def __init__(self, parent):
        super(Talos, self).__init__(parent, 'Talos')
        self.models = UnitList(self, self.SingleTalos, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class Cronos(HeavyUnit, armory.CovenUnit):
    type_name = get_name(units.Cronos)
    type_id = 'chronos_v1'

    keywords = ['Monster']
    power = 4

    class SingleCronos(ListSubUnit):
        type_name = u'Cronos'

        class OptWeapon(OptionsList):
            def __init__(self, parent):
                super(Cronos.SingleCronos.OptWeapon, self).__init__(parent, 'Options')
                self.variant(*ranged.SpiritVortex)
                self.variant(*wargear.SpiritProbe)

        def __init__(self, parent):
            super(Cronos.SingleCronos, self).__init__(parent, 'Cronos', points=get_cost(units.Cronos),
                                                        gear=[Gear('Spirit leech tentacles'), Gear('Spirit syphon')])
            self.OptWeapon(self)

    def __init__(self, parent):
        super(Cronos, self).__init__(parent, 'Cronos')
        self.models = UnitList(self, self.SingleCronos, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class Ravager(HeavyUnit, armory.KabalUnit):
    type_name = u'Ravager'
    type_id = 'ravager_v1'

    keywords = ['Vehicle', 'Fly']
    power = 7

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Ravager.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.DarkLance)
            self.variant(*ranged.DisintegratorCannon)

    def __init__(self, parent):
        super(Ravager, self).__init__(parent, 'Ravager', points=get_cost(units.Ravager),
                                      gear=[Gear('Bladevanes')])
        [self.Weapon(self) for i in range(0, 3)]
        armory.Equipment(self)
