__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, OptionsList,\
    Gear, SubUnit, UnitDescription, Count
import armory, melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class Lhamaean(ElitesUnit, armory.KabalUnit):
    type_name = get_name(units.Lhamaean)
    type_id = 'lhamaean_v1'

    keywords = ['Infantry', 'Court of the Archon']
    power = 1

    def __init__(self, parent):
        super(Lhamaean, self).__init__(parent, points=get_cost(units.Lhamaean), gear=[
            Gear('Splinter pistol'), Gear('Shaimeshi blade')
        ], static=True)


class Medusae(ElitesUnit, armory.KabalUnit):
    type_name = get_name(units.Medusae)
    type_id = 'medusae_v1'

    keywords = ['Infantry', 'Court of the Archon']
    power = 2

    def __init__(self, parent):
        super(Medusae, self).__init__(parent, points=get_cost(units.Medusae), gear=[
            Gear('Eyeburst')
        ], static=True)


class Sslyth(ElitesUnit, armory.KabalUnit):
    type_name = get_name(units.Sslyth)
    type_id = 'sslyth_v1'

    keywords = ['Infantry', 'Court of the Archon']
    power = 2

    def __init__(self, parent):
        super(Sslyth, self).__init__(parent, points=get_cost(units.Sslyth), gear=[
            Gear('Splinter pistol'), Gear('Shardcarbine'),
            Gear('Sslyth battle-blade')
        ], static=True)


class UrGhul(ElitesUnit, armory.KabalUnit):
    type_name = get_name(units.UrGhul)
    type_id = 'urghul_v1'

    keywords = ['Court of the Archon', 'Infantry']
    power = 1

    def __init__(self, parent):
        super(UrGhul, self).__init__(parent, points=get_cost(units.UrGhul), gear=[
            Gear('Claws and talons')
        ], static=True)


class KabaliteTrueborn(ElitesUnit, armory.KabalUnit):
    type_name = get_name(units.KabaliteTrueborn) + ' (Index)'
    type_id = 'kabaltrue_v1'

    keywords = ['Character', 'Infantry']
    power = 4

    member = UnitDescription('Kabalite Trueborn')

    class Dracon(Unit):
        type_name = u'Dracon'

        class Options(OptionsList):
            def __init__(self, parent):
                super(KabaliteTrueborn.Dracon.Options, self).\
                    __init__(parent, name='Options')
                self.variant(*ranged.PhantasmGrenadeLauncher)

        class Melee(OptionsList):
            def __init__(self, parent):
                super(KabaliteTrueborn.Dracon.Melee, self).__init__(parent, 'Melee weapon',
                                                                    limit=1)
                self.variant(*melee.PowerSword)
                self.variant(*melee.Agoniser)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(KabaliteTrueborn.Dracon.Ranged, self).\
                    __init__(parent, name='Ranged weapon')
                self.variant(*ranged.SplinterRifle)
                self.variant(*ranged.SplinterPistol)
                self.variant(*ranged.BlastPistol)

        def __init__(self, parent):
            super(KabaliteTrueborn.Dracon, self).\
                __init__(parent, points=get_cost(units.KabaliteTrueborn))
            self.rng = self.Ranged(self)
            self.mle = self.Melee(self)
            self.opt = self.Options(self)

    class Special(OneOf):
        def __init__(self, parent):
            super(KabaliteTrueborn.Special, self).__init__(parent, 'Special weapon')
            self.rifle = self.variant(*ranged.SplinterRifle)
            self.variant(*ranged.Shredder)
            self.variant(*ranged.Blaster)

        @property
        def description(self):
            return [KabaliteTrueborn.member.clone().add(self.cur.gear)]

    class Heavy(OneOf):
        def __init__(self, parent):
            super(KabaliteTrueborn.Heavy, self).__init__(parent, 'Heavy weapon')
            self.rifle = self.variant(*ranged.SplinterRifle)
            self.variant(*ranged.SplinterCannon)
            self.variant(*ranged.DarkLance)

        @property
        def description(self):
            return [KabaliteTrueborn.member.clone().add(self.cur.gear)]

    def init_weapon_list(self):
        self.sp1 = self.Special(self)
        self.sp2 = self.Special(self)
        self.sp3 = self.Special(self)
        self.sp4 = self.Special(self)
        self.hv1 = self.Heavy(self)
        self.hv2 = self.Heavy(self)

    def __init__(self, parent):
        super(KabaliteTrueborn, self).__init__(parent, name=get_name(units.KabaliteTrueborn))
        self.wars = Count(self, 'Kabalite Trueborn', 4, 19, get_cost(units.KabaliteTrueborn),
                          per_model=True)
        self.ldr = SubUnit(self, self.Dracon(parent=self))
        self.init_weapon_list()

    def check_rules(self):
        super(KabaliteTrueborn, self).check_rules()
        min_used = sum(o.cur != o.rifle for o in self.get_all_spec())
        self.wars.min = max(4, min_used)

    def get_all_spec(self):
        return [self.hv1, self.hv2, self.sp1, self.sp2, self.sp3, self.sp4]

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        spec_count = sum([o.cur != o.rifle for o in self.get_all_spec()])
        desc.add(self.member.clone().add(Gear('Splinter rifle')).set_count(self.wars.cur - spec_count))
        for g in self.get_all_spec():
            if g.used and g.cur != g.rifle:
                desc.add_dup(g.description)
        return desc

    def get_count(self):
        return self.wars.cur + 1

    def build_power(self):
        return 2 + 3 * ((self.get_count() + 4) / 5)


class Bloodbrides(ElitesUnit, armory.CultUnit):
    type_name = get_name(units.HekatrixBloodbrides) + ' (Index)'
    type_id = 'bloodbrides_v1'

    keywords = ['Infantry']

    member = UnitDescription('Hekatrix Bloodbride', options=[Gear('Darklight grenades')])

    class Syren(Unit):
        type_name = u'Syren'

        class Options(OptionsList):
            def __init__(self, parent):
                super(Bloodbrides.Syren.Options, self).\
                    __init__(parent, name='Options')
                self.variant(*ranged.PhantasmGrenadeLauncher)

        class Pistol(OneOf):
            def __init__(self, parent):
                super(Bloodbrides.Syren.Pistol, self).\
                    __init__(parent, name='Ranged weapon')
                self.variant(*ranged.SplinterPistol)
                self.variant(*ranged.BlastPistol)

        class Melee(OneOf):
            def __init__(self, parent):
                super(Bloodbrides.Syren.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.HekatariiBlade)
                self.variant(*melee.PowerSword)
                self.variant(*melee.Agoniser)

        def __init__(self, parent):
            super(Bloodbrides.Syren, self).\
                __init__(parent, points=get_cost(units.HekatrixBloodbrides),
                         gear=[Gear('Darklight grenades')])
            self.mle = self.Melee(self)
            self.rng = self.Pistol(self)
            self.opt = self.Options(self)

    class WychWeapon(OneOf):
        def __init__(self, parent):
            super(Bloodbrides.WychWeapon, self).__init__(parent, 'Wych weapons')
            self.variant('Splinter pistol and Hekatrii blade', gear=[
                Gear('Splinter pistol'), Gear('Hekatrii blade')
            ])
            self.variant(*melee.Razorflails)
            self.variant(*melee.HydraGauntlets)
            self.variant(*melee.ShardnetAndImpaler, gear=[
                Gear('Shardnet'), Gear('Impaler')
            ])

        @property
        def description(self):
            return [Bloodbrides.member.clone().add(self.cur.gear)]

    def __init__(self, parent):
        super(Bloodbrides, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Syren(parent=self))
        self.wars = Count(self, 'Bloodbrides', 4, 19, points=get_cost(units.HekatrixBloodbrides),
                          per_model=True)
        self.ww1 = self.WychWeapon(self)
        self.ww2 = self.WychWeapon(self)
        self.ww3 = self.WychWeapon(self)

    def get_count(self):
        return self.wars.cur + 1

    def check_rules(self):
        super(Bloodbrides, self).check_rules()
        self.ww2.used = self.ww2.visible = self.get_count() >= 10
        self.ww3.used = self.ww3.visible = self.get_count() >= 10

    def get_all_spec(self):
        return [self.ww1, self.ww2, self.ww3]

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        spec_count = sum([(o.used) for o in self.get_all_spec()])
        desc.add(self.member.clone().add([Gear('Splinter pistol'), Gear('Hekatrii blade')]).set_count(self.wars.cur - spec_count))
        for g in self.get_all_spec():
            if g.used:
                desc.add_dup(g.description)
        return desc

    def build_power(self):
        return 1 + 3 * (self.get_count() / 5)


class Incubi(ElitesUnit, armory.DEUnit):
    type_name = get_name(units.Incubi)
    type_id = 'incubi_v1'

    faction = ['Incubi', 'Ynnari']
    keywords = ['Infantry']
    power = 4

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Incubi.Weapon, self).__init__(parent, 'Klaivex Weapon', visible=False)
            self.variant(*melee.Klaive)
            self.variant(*melee.Demiklaives)

        @property
        def description(self):
            return UnitDescription('Klaivex').add(self.cur.gear)

    def __init__(self, parent):
        super(Incubi, self).__init__(parent)
        self.Weapon(self)
        self.models = Count(self, 'Incubi', 4, 9, points=get_cost(units.Incubi), per_model=True,
                            gear=UnitDescription('Incubi', options=[Gear('Klaive')]))

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return self.power * (1 + (self.models.cur > 4))


class Mandrakes(ElitesUnit, armory.DEUnit):
    type_name = get_name(units.Mandrakes)
    type_id = 'mandrakes_v1'

    keywords = ['Infantry']
    power = 4

    def __init__(self, parent):
        super(Mandrakes, self).__init__(parent, points=get_cost(units.Mandrakes),
                                        gear=[UnitDescription('Nightfiend', options=[
                                            Gear('Baleblast'), Gear('Glimmersteel blade')])])
        self.models = Count(self, 'Mandrakes', 4, 9, points=get_cost(units.Mandrakes), per_model=True,
                            gear=UnitDescription('Mandrake', options=[
                                Gear('Baleblast'), Gear('Glimmersteel blade')]))

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return self.power * (1 + (self.models.cur > 4))


class Beastmaster(ElitesUnit, armory.CultUnit):
    type_name = get_name(units.Beastmaster)
    type_id = 'beastmaster_v1'

    keywords = ['Infantry', 'Character', 'Fly', 'Skyboard']
    power = 2

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Beastmaster.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*melee.Agoniser)
            self.variant(*melee.PowerSword)

    def __init__(self, parent):
        super(Beastmaster, self).__init__(parent, points=get_cost(units.Beastmaster),
                                          gear=[Gear('Splinter pods')])
        self.Weapon(self)


class Grotesques(ElitesUnit, armory.CovenUnit):
    type_name = get_name(units.Grotesques)
    type_id = 'grotesk_v1'

    keywords = ['Infantry']
    member = UnitDescription('Grotesque', options=[Gear('Flesh gauntlet')])
    power = 6

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Grotesques.Weapon, self).__init__(parent, name='Weapon')
            self.used = self.visible = False
            self.variant(*melee.MonstrousCleaver)
            self.gun = self.variant(*ranged.LiquifierGun)
            armory.add_torture_weapons(self)

        @property
        def description(self):
            return Grotesques.member.clone().add(self.cur.gear)

    class Grotesques(Count):
        @property
        def description(self):
            return Grotesques.member.clone().add(Gear('Monstrous cleaver'))\
                                            .set_count(self.cur - self.parent.gun.cur)

    def __init__(self, parent):
        super(Grotesques, self).__init__(parent, 'Grotesques')
        self.warriors = self.Grotesques(self, 'Grotesque', 3, 10, points=points_price(get_cost(units.Grotesques), melee.FleshGauntlet),
                                        per_model=True)
        self.wep = self.Weapon(self)
        self.gun = Count(self, 'Liquifier gun', 0, 10, points=get_cost(ranged.LiquifierGun),
                         gear=Grotesques.member.clone().add(Gear('Liquifier gun')))

    def get_count(self):
        return self.warriors.cur

    def check_rules(self):
        super(Grotesques, self).check_rules()
        self.gun.max = self.warriors.cur

    def build_points(self):
        return super(Grotesques, self).build_points() + (self.warriors.cur - self.gun.cur) * get_cost(melee.MonstrousCleaver)

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.warriors.description)
        # if self.wep.cur != self.wep.gun:
        #     desc.add_dup(self.wep.description)
        #     desc.add_dup(self.gun.description)
        # else:
        desc.add_dup(self.gun.description[0].set_count(self.gun.cur))
        return desc

    def build_power(self):
        return 2 * self.warriors.cur
