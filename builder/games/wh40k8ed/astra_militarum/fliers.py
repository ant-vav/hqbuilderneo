__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FlierUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    ListSubUnit, UnitList, OptionalSubUnit, SubUnit, OptionsList
import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *
from fast import Squadron


class Valkyrie(ListSubUnit):
    type_name = u'Valkyrie'
    power = 8

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant(*ranged.MultiLaser)
            self.variant(*ranged.Lascannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon2, self).__init__(parent=parent, name='')
            self.variant(*ranged.HellstrikeMissiles)
            self.variant('Multiple rocket pods', 2 * get_cost(ranged.MultipleRocketPod),
                         gear=[Gear('Multiple rocket pod', count=2)])

    class Weapon3(OptionsList):
        def __init__(self, parent):
            super(Valkyrie.Weapon3, self).__init__(parent=parent, name='Sponsons')
            self.variant('Heavy bolters', 2 * get_cost(ranged.HeavyBolter),
                         gear=[Gear('Heavy bolter', count=2)])

    def __init__(self, parent):
        super(Valkyrie, self).__init__(parent=parent, points=get_cost(units.Valkyries))
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)


class Valkyries(Squadron, FlierUnit, armory.BloodBrothersUnit):
    type_name = get_name(units.Valkyries)
    type_id = 'valkyrie_squad_v1'
    unit_class = Valkyrie

    faction = ['Aeronautica Imperialis']
    keywords = ['Vehicle', 'Transport', 'Fly']
