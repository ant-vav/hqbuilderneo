__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList
import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class Chimera(TransportUnit, armory.RegimentUnit):
    type_name = get_name(units.Chimera)
    type_id = 'chimera_v1'

    keywords = ['Vehicle', 'Transport']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant(*ranged.HeavyBolter)
            self.variant(*ranged.HeavyFlamer)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon2, self).__init__(parent=parent, name='')
            self.variant(*ranged.MultiLaser)
            self.variant(*ranged.HeavyBolter)
            self.variant(*ranged.HeavyFlamer)

    def __init__(self, parent):
        super(Chimera, self) .__init__(parent=parent, points=get_cost(units.Chimera))
        self.Weapon1(self)
        self.Weapon2(self)
        armory.VehicleEquipment(self)


class Taurox(TransportUnit, armory.RegimentUnit):
    type_name = get_name(units.Taurox)
    type_id = 'taurox_v1'

    keywords = ['Vehicle', 'Transport']
    power = 4

    class Vehicle(OptionsList):
        def __init__(self, parent, sentinel=False, open_tank=False, lights=True, smoke=True):
            super(Taurox.Vehicle, self).__init__(parent, name='Options', limit=1)
            self.sb = self.variant(*ranged.StormBolter)
            self.hs = self.variant(*ranged.HeavyStubber)

    def __init__(self, parent):
        gear = [ranged.Autocannon] * 2
        cost = points_price(get_cost(units.Taurox), *gear)
        super(Taurox, self) .__init__(parent=parent, points=cost,
                                      gear=create_gears(*gear))
        self.Vehicle(self)


class TauroxPrime(TransportUnit, armory.BloodBrothersUnit):
    type_name = get_name(units.TauroxPrime)
    type_id = 'taurox_prime_v1'

    keywords = ['Vehicle', 'Transport']
    faction = ['Tempestus']
    power = 6

    class MainGun(OneOf):
        def __init__(self, parent):
            super(TauroxPrime.MainGun, self).__init__(parent, 'Main weapon')
            self.variant(*ranged.TauroxBattleCannon)
            self.variant(*ranged.TauroxGatlingCannon)
            self.variant(*ranged.TauroxMissileLauncher)

    class SideGuns(OneOf):
        def __init__(self, parent):
            super(TauroxPrime.SideGuns, self).__init__(parent, 'Side weapons')
            self.variant('Hot-shot volley guns', 2 * get_cost(ranged.HotShotVolleyGun),
                         gear=[Gear('Hot-shot volley gun', count=2)])
            self.variant('Autocannons', 2 * get_cost(ranged.Autocannon),
                         gear=[Gear('Autocannon', count=2)])

    def __init__(self, parent):
        super(TauroxPrime, self).__init__(parent, points=get_cost(units.TauroxPrime))
        self.MainGun(self)
        self.SideGuns(self)
        Taurox.Vehicle(self)
