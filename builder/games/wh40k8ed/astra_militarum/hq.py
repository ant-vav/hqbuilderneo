__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HqUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, SubUnit,\
    OptionsList
from heavy import LemanRuss
import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class CCommander(HqUnit, armory.RegimentUnit):
    type_name = get_name(units.CompanyCommander)
    type_id = 'comp_commander_v1'

    keywords = ['Character', 'Officer', 'Infantry']
    power = 2

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CCommander.Weapon1, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Chainsword)
            armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CCommander.Weapon2, self).__init__(parent, 'Pistol')
            self.variant(*ranged.Laspistol)
            # self.variant('Shotgun', 0)
            armory.add_ranged_weapons(self)

    def __init__(self, parent):
        super(CCommander, self).__init__(parent, gear=[
            Gear('Frag grenades')
        ], points=get_cost(units.CompanyCommander))
        self.Weapon1(self)
        self.Weapon2(self)


class TankCommander(HqUnit, armory.RegimentUnit):
    type_name = get_name(units.TankCommander)
    type_id = 'tank_commander_v1'

    keywords = ['Character', 'Vehicle', 'Leman Russ']
    power = 13

    def __init__(self, parent):
        super(TankCommander, self).__init__(
            parent, points=get_cost(units.TankCommander))
        LemanRuss.MainGun(self)
        LemanRuss.Weapon(self)
        LemanRuss.Sponsons(self)
        armory.VehicleEquipment(self)


class Creed(HqUnit, armory.IGUnit):
    type_name = get_name(units.LordCastellanCreed)
    type_id = 'creed_v1'

    faction = ['Cadian']
    keywords = ['Character', 'Officer', 'Infantry']
    power = 4

    def __init__(self, parent):
        super(Creed, self).__init__(parent, gear=[
            Gear('Hot-shot laspistol', count=2),
            Gear('Power sword')
        ], unique=True, static=True,
        points=get_cost(units.LordCastellanCreed))

    def build_statistics(self):
        res = super(Creed, self).build_statistics()
        if self.star == 2:
            res['Command points'] = 2
        return res


class Pask(HqUnit, armory.IGUnit):
    type_name = get_name(units.KnightCommanderPask)
    type_id = 'pask_v1'
    faction = ['Cadian']
    keywords = ['Character', 'Vehicle', 'Leman Russ', 'Tank Commander']
    power = 13

    class HandOfSteel(Unit):
        type_name = u'Hand of Steel'

        # class MainGun(OneOf):
        #     def __init__(self, parent):
        #         super(Pask.HandOfSteel.MainGun, self).__init__(parent, 'Turret weapon')
        #         self.variant('Battle cannon', 22)
        #         self.variant('Exterminator autocannon', 25)
        #         self.variant('Vanquisher battle cannon', 25)
        #         self.variant('Eradicator nova cannon', 25)
        #         self.variant('Demolisher cannon', 40)
        #         self.variant('Punisher gatling cannon', 20)
        #         self.variant('Executioner plasma cannon', 20)

        def __init__(self, parent):
            super(Pask.HandOfSteel, self).__init__(parent)
            LemanRuss.MainGun(self)
            LemanRuss.Weapon(self)
            LemanRuss.Sponsons(self)
            armory.VehicleEquipment(self)

    def __init__(self, parent):
        super(Pask, self).__init__(parent, unique=True, gear=[
            UnitDescription('Knight Commander Pask')
        ], points=get_cost(units.KnightCommanderPask))
        SubUnit(self, self.HandOfSteel(parent=self))


class Straken(HqUnit, armory.IGUnit):
    type_name = get_name(units.ColonelIronHandStraken)
    type_id = 'straken_v1'

    faction = ['Catachan']
    keywords = ['Character', 'Officer', 'Infantry']
    power = 4

    def __init__(self, parent):
        super(Straken, self).__init__(parent, gear=[
            Gear('Plasma pistol'), Gear('Shotgun'),
            Gear('Krak grenades'), Gear("Bionic arm with devil's claw")
        ], unique=True, static=True, points=get_cost(units.ColonelIronHandStraken))


class LordCommissar(HqUnit, armory.PrefectusUnit):
    type_name = get_name(units.LordCommissar)
    type_id = 'lord_commissar_v1'

    keywords = ['Character', 'Commissar', 'Infantry']
    power = 4

    class MWeapon1(OneOf):
        def __init__(self, parent):
            super(LordCommissar.MWeapon1, self).__init__(parent, 'Melee weapon')
            armory.add_melee_weapons(self)

    class MWeapon2(OptionsList):
        def __init__(self, parent):
            super(LordCommissar.MWeapon2, self).__init__(parent, '', limit=1)
            armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(LordCommissar.Weapon2, self).__init__(parent, 'Pistol')
            self.variant(*ranged.BoltPistol)
            armory.add_ranged_weapons(self)

    def __init__(self, parent):
        super(LordCommissar, self).__init__(parent, points=get_cost(units.LordCommissar))
        self.MWeapon1(self)
        self.MWeapon2(self)
        self.Weapon2(self)


class Yarrik(HqUnit, armory.PrefectusUnit):
    type_name = get_name(units.CommissarYarrick)
    type_id = 'yarrik_v1'

    keywords = ['Character', 'Commissar', 'Infantry']
    power = 7

    def __init__(self, parent):
        super(Yarrik, self).__init__(parent, gear=[
            Gear('Bolt pistol'), Gear('Storm bolter'),
            Gear('Power klaw'), Gear('Bale Eye')
        ], unique=True, static=True, points=get_cost(units.CommissarYarrick))

    @classmethod
    def check_faction(cls, fac):
        return super(armory.PrefectusUnit, cls).check_faction(fac)


class TempestorPrime(HqUnit, armory.BloodBrothersUnit):
    type_name = get_name(units.TempestorPrime)
    type_id = 'tempestor_prime_v1'

    faction = ['Militarum Tempestus']
    keywords = ['Character', 'Officer', 'Infantry']
    power = 3

    class Weapon1(OptionsList):
        def __init__(self, parent):
            super(TempestorPrime.Weapon1, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Chainsword)
            armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TempestorPrime.Weapon2, self).__init__(parent, 'Pistol')
            self.variant(*ranged.HotShotLaspistol)
            self.variant(*wargear.TempestusCommandRod)
            self.variant(*ranged.BoltPistol)
            self.variant(*ranged.PlasmaPistol)

    def __init__(self, parent):
        super(TempestorPrime, self).__init__(parent, gear=[
            Gear('Frag grenades'), Gear('Krak grenades')
        ], points=get_cost(units.TempestorPrime))
        self.Weapon1(self)
        self.Weapon2(self)


class Primaris(HqUnit, armory.PsykanaUnit):
    type_name = get_name(units.PrimarisPsyker)
    type_id = 'ig_psyker_v1'

    keywords = ['Character', 'Infantry', 'Psyker']
    power = 2

    def __init__(self, parent):
        gear = [ranged.Laspistol, melee.ForceStave]
        cost = points_price(get_cost(units.PrimarisPsyker), *gear)
        super(Primaris, self).__init__(parent, gear=create_gears(*gear),
                                       static=True, points=cost)


class SlyMarbo(HqUnit, armory.IGUnit):
    type_name = get_name(units.SlyMarbo)
    type_id = 'marbo_v1'

    faction = ['Catachan']
    keywords = ['Character', 'Infantry']
    power = 4

    def __init__(self, parent):
        super(SlyMarbo, self).__init__(parent, gear=[
            Gear('Ripper pistol'), Gear('Envenomed blade'),
            Gear('Frag grenades')
        ], unique=True, static=True,points=get_cost(units.SlyMarbo))
