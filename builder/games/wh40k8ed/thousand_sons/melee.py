BetentacledMaw = ('Betentacled maw', 0)
BrutalAssaultWeapon = ('Brutal assault weapon', 0)
Chainsword = ('Chainsword', 0)
DaemonJaws = ('Daemon jaws', 8)
DaemonicAxe = ('Daemonic axe', 10)
DefilerClaws = ('Defiler claws', 0)
DefilerScourge = ('Defiler scourge', 12)
DiviningSpear = ('Divining spear', 1)
EnormousClaws = ('Enormous claws', 0)
ForceStave = ('Force stave', 8)
ForceSword = ('Force sword', 8)
HelbruteFist = ('Helbrute fist', 30)
HelbruteFistPair = ('Helbrute fist', 40)
HelbruteHammer = ('Helbrute hammer', 30)
HeldrakeClaws = ('Heldrake claws', 17)
HellforgedSword = ('Hellforged sword', 10)
HideousMutations = ('Hideous mutations', 0)
LasherTendrils = ('Lasher tendrils', 12)
MaleficTalons = ('Malefic talons', 0)
MaleficTalonsPair = ('Malefic Talons', 10)
MaulerfiendFists = ('Maulerfiend fists', 0)
PowerScourge = ('Power scourge', 35)
PowerSword = ('Power sword', 4)
TzaangorBlades = ('Tzaangor blades', 0)
