__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
import armory
import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class LegionRubrics(TroopsUnit, armory.SonsUnit):
    type_name = get_name(units.RubricMarines)
    type_id = 'rubr_v1'
    keywords = ['Infantry']
    power = 7
    sorc_points = get_cost(units.RubricMarines)

    class Champion(Unit):
        type_name = u'Aspiring Sorcerer'

        class Ranged(OneOf):
            def __init__(self, parent):
                super(LegionRubrics.Champion.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.InfernoBoltPistol)
                self.variant(*ranged.PlasmaPistol)
                self.variant(*ranged.WarpflamePistol)

        def __init__(self, parent):
            cost = points_price(parent.sorc_points, melee.ForceStave)
            super(LegionRubrics.Champion, self).__init__(parent, points=cost,
                                                         gear=create_gears(melee.ForceStave))
            self.wep2 = self.Ranged(self)

    class RubricMarine(ListSubUnit):
        type_name = u'Rubric Marine'

        class Ranged(OneOf):
            def __init__(self, parent):
                super(LegionRubrics.RubricMarine.Ranged, self).__init__(parent, 'Weapon')
                self.variant(*ranged.InfernoBoltgun)
                self.variant(*ranged.Warpflamer)
                self.can = self.variant(*ranged.SoulreaperCannon)

        class Icon(OptionsList):
            def __init__(self, parent):
                super(LegionRubrics.RubricMarine.Icon, self).__init__(parent, 'Icon of Chaos')
                self.variant(*wargear.IconOfFlame)

        def __init__(self, parent):
            super(LegionRubrics.RubricMarine, self).__init__(parent, points=get_cost(units.RubricMarines))
            self.rng = self.Ranged(self)
            self.icon = self.Icon(self)

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.rng.cur == self.rng.can

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.any

    def __init__(self, parent):
        super(LegionRubrics, self).__init__(parent)
        self.ldr = SubUnit(self, self.Champion(parent=self))
        self.models = UnitList(self, self.RubricMarine, 4, 19)

    def check_rules(self):
        super(LegionRubrics, self).check_rules()
        icnt = sum(u.has_icon() for u in self.models.units)
        if icnt > 1:
            self.error('Only one Rubric Marine may carry Icon of Flame')
        canmax = self.get_count() / 10
        cancnt = sum(u.has_heavy() for u in self.models.units)
        if cancnt > canmax:
            self.error('No more then 1 Rubric Marine per 10 models may take soulreaper cannon')

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + (20 if self.get_count() > 15 else
                             (14 if self.get_count() > 10 else
                              (7 if self.get_count() > 5 else 0)))


class Tzaangors(TroopsUnit, armory.SonsUnit):
    type_name = get_name(units.Tzaangors)
    type_id = 'tzaangors_v1'
    keywords = ['Infantry']
    power = 4

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Tzaangors.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Tzaangor blades')
            self.variant('Autopistol and chainsword', gear=[
                Gear('Autopistol'), Gear('Chainsword')
            ])

    class Twistbray(Unit):
        type_name = u'Twistbray'

        def __init__(self, parent):
            super(Tzaangors.Twistbray, self).__init__(parent, points=7)
            Tzaangors.Weapon(self)

    class Tzaangor(ListSubUnit):
        type_name = u'Tzaangor'

        class Options(OptionsList):
            def __init__(self, parent):
                super(Tzaangors.Tzaangor.Options, self).__init__(parent, 'Options', limit=1)
                self.horn = self.variant(*wargear.Brayhorn)
                self.icon = self.variant(*wargear.IconOfFlame)

        def __init__(self, parent):
            super(Tzaangors.Tzaangor, self).__init__(parent, points=7)
            Tzaangors.Weapon(self)
            self.opt = self.Options(self)

        @ListSubUnit.count_gear
        def has_horn(self):
            return self.opt.horn.value

    def __init__(self, parent):
        super(Tzaangors, self).__init__(parent)
        self.ldr = SubUnit(self, self.Twistbray(parent=self))
        self.members = UnitList(self, self.Tzaangor, 9, 29)

    def get_count(self):
        return self.members.count + 1

    def build_power(self):
        return self.power + 3 * ((self.get_count() - 1) / 10)

    def check_rules(self):
        super(Tzaangors, self).check_rules()
        if sum(u.has_horn() for u in self.members.units) > 1:
            self.error('Only one model may carry a brayhorn')
