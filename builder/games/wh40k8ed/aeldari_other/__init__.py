from hq import TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne
from elites import DeathJester, Solitaire
from troops import Troupe
from fast import Skyweavers
from transport import Starweaver
from heavy import Voidweavers
from fort import WebwayGate

unit_types = [TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne,
              DeathJester, Solitaire, Troupe, Skyweavers, Starweaver,
              Voidweavers, WebwayGate]
