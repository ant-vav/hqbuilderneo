__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, FastUnit
from builder.games.wh40k8ed.options import OneOf, UnitList,\
    Gear, UnitDescription, ListSubUnit
import armory, melee, ranged, units
from builder.games.wh40k8ed.utils import *


class Skyweavers(FastUnit, armory.HarlequinUnit):
    type_name = get_name(units.Skyweavers)
    type_id = 'skyweaver_v1'

    keywords = ['Biker', 'Fly']

    class Skyweaver(ListSubUnit):
        class Hand(OneOf):
            def __init__(self, parent):
                super(Skyweavers.Skyweaver.Hand, self).__init__(parent,
                                                                'Hand weapon')
                self.variant(*ranged.StarBolas)
                self.variant(*melee.Zephyrglaive)

        class Bike(OneOf):
            def __init__(self, parent):
                super(Skyweavers.Skyweaver.Bike, self).__init__(parent,
                                                                'Bike weapon')
                self.variant(*ranged.ShurikenCannon)
                self.variant(*ranged.HaywireCannon)

        def __init__(self, parent):
            super(Skyweavers.Skyweaver, self).__init__(parent, 'Skyweaver', points=get_cost(units.Skyweavers))
            self.Hand(self)
            self.Bike(self)

    def __init__(self, parent):
        super(Skyweavers, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Skyweaver, 2, 6)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return 5 * ((1 + self.get_count()) / 2)
