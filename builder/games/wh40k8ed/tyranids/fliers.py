__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FlierUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, SubUnit,\
    OptionsList, ListSubUnit, UnitList
import ranged, melee, armory, units, biomorphs
from builder.games.wh40k8ed.utils import *


class Harpy(FlierUnit, armory.FleetUnit):
    type_name = get_name(units.Harpy)
    type_id = 'harpy_v1'
    keywords = ['Monster', 'Fly']
    power = 9

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Harpy.Weapon1, self).__init__(parent, name='Weapon')
            self.variant('Two stranglethorn cannons', 2 * get_cost(ranged.StranglethornCannon), gear=[
                Gear('Stranglethorn cannon', count=2)
            ])
            self.variant('Two heavy venom cannon', 2 * get_cost(ranged.HeavyVenomCannon), gear=[
                Gear('Heavy venom cannon', count=2)
            ])

    def __init__(self, parent):
        gear = [melee.ScythingWings, ranged.StingerSalvo]
        cost = points_price(get_cost(units.Harpy), *gear)
        super(Harpy, self).__init__(parent, points=cost,
                                    gear=create_gears(*gear))
        self.Weapon1(self)


class HiveCrone(FlierUnit, armory.FleetUnit):
    type_name = get_name(units.HiveCrone)
    type_id = 'crone_v1'
    keywords = ['Monster', 'Fly']
    power = 8

    def __init__(self, parent):
        gear = [ranged.DroolCannon, ranged.Tentaclids, melee.ScythingWings,
                melee.WickedSpur]
        cost = points_price(get_cost(units.HiveCrone), *gear)
        super(HiveCrone, self).__init__(parent, points=cost,
                                        gear=create_gears(*gear),
                                        static=True)
