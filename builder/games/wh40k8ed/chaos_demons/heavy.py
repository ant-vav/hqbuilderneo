__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit
from builder.games.wh40k8ed.options import OneOf, UnitDescription, OptionsList
import armory, units, wargear
from builder.games.wh40k8ed.utils import *


class SoulGrinder(HeavyUnit, armory.DevotedUnit):
    type_name = get_name(units.SoulGrinder)
    type_id = 'grinder_v1'

    keywords = ['Vehicle']
    power = 12

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SoulGrinder.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Warpsword')
            self.variant('Warpclaw')

    def __init__(self, parent):
        super(SoulGrinder, self).__init__(parent, points=get_cost(units.SoulGrinder), gear=[
            Gear('Harvester cannon'), Gear('Phlegm bombardment'),
            Gear('Iron claw')
        ])
        self.Weapon(self)


class SkullCannon(HeavyUnit, armory.KhorneUnit):
    type_name = get_name(units.SkullCannon)
    type_id = 'skull_cannon_v1'
    keywords = ['Chariot', 'Bloodletter']
    power = 5

    def __init__(self, parent):
        super(SkullCannon, self).__init__(parent, gear=[Gear('Skull cannon'),
                                                        UnitDescription('Bloodletter', count=2, options=[Gear('Hellblade')])],
                                          static=True, points=get_cost(units.SkullCannon))


class BurningChariot(HeavyUnit, armory.TzeentchUnit):
    type_name = get_name(units.BurningChariot)
    type_id = 'burnbing_chariot_v1'
    keywords = ['Chariot', 'Exalted flamer', 'Fly', 'Flamer']
    power = 6

    class Options(OptionsList):
        def __init__(self, parent):
            super(BurningChariot.Options, self).__init__(parent, 'Options')
            self.variant('Horrors', get_cost(wargear.ChantingHorrors)*3, gear=[UnitDescription(get_name(wargear.ChantingHorrors), count=3)])

    def __init__(self, parent):
        super(BurningChariot, self).__init__(parent, gear=[UnitDescription('Exalted Flamer',
                                                                           options=[Gear('Fire of Tzeentch'), Gear('Tongues of flame')]),
                                                           UnitDescription('Screamer', count=2,
                                                                           options=[Gear('Slashing talons'), Gear('Lamprey bite')])],
                                             points=get_cost(units.BurningChariot))
        self.Options(self)


class SeekerChariot(HeavyUnit, armory.SlaaneshUnit):
    type_name = get_name(units.SeekerChariot)
    type_id = 'seeker_char_v1'
    keywords = ['Chariot']
    power = 4

    def __init__(self, parent):
        super(SeekerChariot, self).__init__(
            parent, gear=[UnitDescription('Exalted Alluress', options=[Gear('Lashes of torment')]),
                          UnitDescription('Daemonette Charioteer', options=[Gear('Piercing claws')]),
                          UnitDescription('Steed of Slanesh', count=2, options=[Gear('Lashing tongue')])],
            static=True, points=get_cost(units.SeekerChariot))


class ExSeekerChariot(HeavyUnit, armory.SlaaneshUnit):
    type_name = get_name(units.ExaltedSeekerChariot)
    type_id = 'ex_seeker_char_v1'
    keywords = ['Chariot']
    power = 5

    def __init__(self, parent):
        super(ExSeekerChariot, self).__init__(
            parent, gear=[UnitDescription('Exalted Alluress', options=[Gear('Lashes of torment')]),
                          UnitDescription('Daemonette Charioteer', count=3, options=[Gear('Piercing claws')]),
                          UnitDescription('Steed of Slanesh', count=4, options=[Gear('Lashing tongue')])],
            static=True, points=get_cost(units.ExaltedSeekerChariot))
