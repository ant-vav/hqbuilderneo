from builder.games.wh40k8ed.space_marines.troops import TacticalSquad, ScoutSquad, Intercessors
from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList
import armory, melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class BATacticalSquad(armory.BAUnit, TacticalSquad):
    type_name = u'Blood Angels Tactical Squad'
    type_id = 'ba_tactical_squad_v1'
    obsolete = True
    armory = armory


class BAScoutSquad(armory.BAUnit, ScoutSquad):
    type_name = u'Blood Angels Scout Squad'
    type_id = 'ba_scout_squad_v1'
    obsolete = True
    armory = armory


class BAIntercessors(armory.BAUnit, Intercessors):
    type_name = u'Blood Angels Intercessor Squad'
    type_id = 'ba_intercessors_v1'
    swords = [melee.Chainsword, melee.PowerSword]


class BATacticalSquadV2(TroopsUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + armory.get_name(units.TacticalSquad)
    type_id = 'ba_tactical_squad_v2'

    keywords = ['Infantry']

    common_gear = [ranged.FragGrenades, ranged.KrakGrenades]

    model = UnitDescription('Space Marine', options=armory.create_gears(*(common_gear + [ranged.BoltPistol])))

    class Sergeant(armory.ClawUser):
        type_name = 'Space Marine Sergeant'

        class MBombs(OptionsList):
            def __init__(self, parent):
                super(BATacticalSquadV2.Sergeant.MBombs, self).__init__(parent, 'Options')
                self.variant(*ranged.MeltaBombs)

        def __init__(self, parent):
            super(BATacticalSquadV2.Sergeant, self).__init__(parent, points=armory.get_cost(units.TacticalSquad),
                                                             gear=armory.create_gears(*BATacticalSquadV2.common_gear))
            self.wep1 = armory.SergeantWeapon(self, double=True)
            self.wep2 = armory.SergeantWeapon(self, double=False)
            self.MBombs(self)

        def check_rules(self):
            super(BATacticalSquadV2.Sergeant, self).check_rules()
            armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    class Marine(Count):
        @property
        def description(self):
            return BATacticalSquadV2.model.clone().add(armory.create_gears(ranged.Boltgun)).\
                set_count(self.cur - sum(c.used for c in [self.parent.special, self.parent.heavy]))

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(BATacticalSquadV2.SpecialWeapon, self).__init__(parent, 'Special weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_special_weapons(self)

        @property
        def description(self):
            return []

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(BATacticalSquadV2.HeavyWeapon, self).__init__(parent, 'Heavy weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_heavy_weapons(self)

        @property
        def description(self):
            return []

    def __init__(self, parent):
        super(BATacticalSquadV2, self).__init__(parent, armory.get_name(units.TacticalSquad))
        self.sup = SubUnit(self, self.Sergeant(parent=self))
        self.squad = self.Marine(self, 'Space Marines', 4, 9, per_model=True,
                                 points=armory.get_cost(units.TacticalSquad))
        self.special = self.SpecialWeapon(self)
        self.heavy = self.HeavyWeapon(self)

    def build_description(self):
        res = super(BATacticalSquadV2, self).build_description()
        if self.special.used:
            res.add_dup(BATacticalSquadV2.model.clone().add(self.special.cur.gear))
        if self.heavy.used:
            res.add_dup(BATacticalSquadV2.model.clone().add(self.heavy.cur.gear))
        return res

    def get_count(self):
        return self.squad.cur + 1

    def build_power(self):
        return 5 + 4 * (self.get_count() > 5)

    def check_rules(self):
        super(BATacticalSquadV2, self).check_rules()

        self.special.visible = self.special.used = (not self.heavy.used or
                                                    self.heavy.cur == self.heavy.bolt or
                                                    self.get_count() == 10)
        self.heavy.visible = self.heavy.used = (not self.special.used or
                                                self.special.cur == self.special.bolt or
                                                self.get_count() == 10)


class BAScoutSquadV2(TroopsUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.ScoutSquad)
    type_id = 'ba_scout_squad_v2'
    power = 4
    keywords = ['Infantry']
    model = UnitDescription('Scout', options=armory.create_gears(ranged.FragGrenades,
                                                                 ranged.KrakGrenades,
                                                                 ranged.BoltPistol))

    class Sergeant(Unit):
        type_name = u'Scout Sergeant'

        class Options(OptionsList):
            def __init__(self, parent):
                super(BAScoutSquadV2.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.variant(*wargear.CamoCloak)

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(BAScoutSquadV2.Sergeant.Weapon1, self).__init__(parent, 'Weapon')
                self.variant(*ranged.Boltgun)
                self.variant(*ranged.SniperRifle)
                self.variant(*ranged.AstartesShotgun)
                self.variant(*melee.CombatKnife)
                armory.add_pistol_options(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(BAScoutSquadV2.Sergeant.Weapon2, self).__init__(parent, '')
                armory.add_pistol_options(self)
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(BAScoutSquadV2.Sergeant, self).__init__(parent, points=armory.get_cost(units.ScoutSquad),
                                                           gear=armory.create_gears(ranged.FragGrenades,
                                                                                    ranged.KrakGrenades))

            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)

    class Scout(ListSubUnit):
        type_name = u'Scout'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(BAScoutSquadV2.Scout.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ranged.Boltgun)
                self.variant(*ranged.SniperRifle)
                self.variant(*ranged.AstartesShotgun)
                self.variant(*melee.CombatKnife)
                self.heavy = [self.variant(*ranged.HeavyBolter),
                              self.variant(*ranged.MissileLauncher)]

        def __init__(self, parent):
            super(BAScoutSquadV2.Scout, self).__init__(parent, points=armory.get_cost(units.ScoutSquad),
                                                        gear=armory.create_gears(ranged.FragGrenades,
                                                                                 ranged.KrakGrenades,
                                                                                 ranged.BoltPistol))
            self.wep = self.Weapon(self)
            BAScoutSquadV2.Sergeant.Options(self)

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.wep.cur in self.wep.heavy

    def __init__(self, parent):
        super(BAScoutSquadV2, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))
        self.squad = UnitList(self, self.Scout, 4, 9)

    def get_count(self):
        return self.squad.count + 1

    def check_rules(self):
        super(BAScoutSquadV2, self).check_rules()
        hcnt = sum(u.has_heavy() for u in self.squad.units)
        if hcnt > 1:
            self.error('Only one Scout may carry heavy weapon; taken: {}'.format(hcnt))

    def build_power(self):
        return self.power * (1 + (self.squad.count > 4))
