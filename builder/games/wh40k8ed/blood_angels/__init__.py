from hq import BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1,\
    BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain,\
    Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants,\
    Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath,\
    SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth
from troops import BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2,\
    BAScoutSquadV2
from elites import BACompanyAncient, BACompanyChampion, BACompanyVeterans,\
    SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad,\
    SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany,\
    DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad,\
    BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans,\
    SanguinaryNovitiateIndex, BADreadnought
from fast import BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes
from heavy import BADevastators, BaalPredator

from builder.games.wh40k8ed.sections import HQSection


class DCCommand(HQSection):
    from builder.games.wh40k8ed.space_marines.hq import GravisCaptain, TerminatorChaplain
    death_visions = [GravisCaptain, TerminatorChaplain, BABikeCaptain,
                     BACaptain, BATerminatorCaptain, BAChaplainV1,
                     BACataphractiiCaptain, BAChaplainV2,
                     BALieutenants]
    
    def __init__(self, *args, **kwargs):
        super(DCCommand, self).__init__(*args, **kwargs)
        self.dc_types = []
        for cls in self.death_visions:
            class DCCommander(cls):
                keywords = ['Death Company']

                def build_statistics(self):
                    res = super(self.__class__, self).build_statistics()
                    res['Command points'] = -1
                    res['Death Visions of Sanguinius'] = 1
                    return res

            self.dc_types.append(DCCommander)

    def add_classes(self, class_list):
        super(DCCommand, self).add_classes(class_list + self.dc_types)


unit_types = [BABikeCaptain, BACaptain, BATerminatorCaptain,
              BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine,
              BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain,
              BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants,
              Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor,
              Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo,
              Lemartes, Seth, BATacticalSquad, BAScoutSquad, BAIntercessors,
              BATacticalSquadV2, BAScoutSquadV2, BACompanyAncient,
              BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate,
              BASternguardVeteranSquad, BAVanguardVeteranSquad,
              SanguinaryAncient, SanguinaryGuard, TerminatorAncient,
              DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought,
              BATerminatorAssaultSquad, BACompanyChampionIndex,
              BACompanyAncientIndex, BAJumpCompanyVeterans,
              SanguinaryNovitiateIndex, BADreadnought,
              BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes,
              BADevastators, BaalPredator]
