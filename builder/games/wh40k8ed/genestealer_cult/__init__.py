from hq import Patriarch, Magus, Primus, Iconward, Abominant
from troops import AcolyteHybrids, NeophyteHybrids
from elites import HybridMetamorphs, Aberrants, PurestrainGenestealers
from transport import GoliathTruck, CultChimera
from fast import CultScoutSentinelSquad, CultArmouredSentinelSquad
from heavy import CultLemanRuss, GoliathRockgrinder

unit_types = [Patriarch, Magus, Primus, Iconward, AcolyteHybrids,
              NeophyteHybrids, HybridMetamorphs, Aberrants,
              PurestrainGenestealers, GoliathTruck, CultChimera,
              CultScoutSentinelSquad, CultArmouredSentinelSquad,
              CultLemanRuss, GoliathRockgrinder, Abominant]
