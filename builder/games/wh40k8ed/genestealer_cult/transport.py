__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TransportUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, UnitList,\
    UnitDescription, OptionsList, Count, SubUnit, ListSubUnit
import armory, units, melee, ranged
from builder.games.wh40k8ed.utils import *


class GoliathTruck(TransportUnit, armory.CultUnit):
    type_name = get_name(units.GoliathTruck)
    type_id = 'gs_goliath_truck_v1'
    power = 5
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Chimera')

    class Charges(OptionsList):
        def __init__(self, parent):
            super(GoliathTruck.Charges, self).__init__(parent, 'Options')
            self.variant(*ranged.CacheOfDemolitionCharges)

    def __init__(self, parent):
        gear = [ranged.HeavyStubber, ranged.TwinAutocannon]
        cost = points_price(get_cost(units.GoliathTruck), *gear)
        super(GoliathTruck, self) .__init__(parent=parent, points=cost,
                                            gear=create_gears(*gear))
        self.Charges(self)


class CultChimera(TransportUnit, armory.CultUnit):
    type_name = get_name(units.CultChimera)
    type_id = 'gs_chimera_v1'
    power = 5
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Chimera')

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CultChimera.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant(*ranged.HeavyBolter)
            self.variant(*ranged.HeavyFlamer)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CultChimera.Weapon2, self).__init__(parent=parent, name='')
            self.variant(*ranged.MultiLaser)
            self.variant(*ranged.HeavyBolter)
            self.variant(*ranged.HeavyFlamer)

    class Weapon3(OptionsList):
        def __init__(self, parent):
            super(CultChimera.Weapon3, self).__init__(parent, '')
            self.variant(*ranged.HunterKillerMissile)
            self.sb = self.variant(*ranged.StormBolter)
            self.hs = self.variant(*ranged.HeavyStubber)

        def check_rules(self):
            super(CultChimera.Weapon3, self).check_rules()
            OptionsList.process_limit([self.sb, self.hs], 1)

    def __init__(self, parent):
        gear = [ranged.LasgunArray, ranged.LasgunArray]
        cost = points_price(get_cost(units.CultChimera), *gear)
        super(CultChimera, self) .__init__(parent=parent, points=cost,
                                           gear=create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self
)
