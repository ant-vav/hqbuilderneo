__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OptionsList, Count, Gear
import melee, ranged


def add_special_weapons(instance):
    instance.special = [
        instance.variant(*ranged.Flamer),
        instance.variant(*ranged.GrenadeLauncher),
        instance.variant(*ranged.Webber)
    ]


def add_pistols(instance):
    instance.variant(*ranged.BoltPistol)
    instance.variant(*ranged.Laspistol)
    instance.variant(*ranged.WebPistol)


def add_melee_weapons(instance):
    instance.variant(*melee.Chainsword)
    instance.variant(*melee.PowerMaul)
    instance.variant(*melee.PowerPick)
    instance.variant(*melee.CultistKnife)


def add_mining_weapons(instance):
    instance.mining = [
        instance.variant(*ranged.HeavyStubber),
        instance.variant(*ranged.MiningLaser),
        instance.variant(*ranged.SeismicCannon)
    ]


def add_heavy_weapons(instance):
    instance.variant(*ranged.Autocannon)
    instance.variant(*ranged.HeavyBolter)
    instance.variant(*ranged.Lascannon)
    instance.variant(*ranged.Mortar)
    instance.variant(*ranged.MissileLauncher)


class CultUnit(Unit):
    faction = ['Tyranids', 'Genestealer Cults']
    wiki_faction = "Genestealer Cults"
