__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, UnitList,\
    UnitDescription, OptionsList, Count, SubUnit, ListSubUnit
import armory, units, melee, ranged
from builder.games.wh40k8ed.utils import *


class Squadron(FastUnit, armory.CultUnit):
    unit_class = None
    unit_min = 1
    unit_max = 3

    def __init__(self, parent):
        super(Squadron, self).__init__(parent)
        self.tanks = UnitList(self, self.unit_class, self.unit_min, self.unit_max)

    def get_count(self):
        return self.tanks.count

    def build_power(self):
        return self.power * self.get_count()


class ScoutSentinel(ListSubUnit):
    type_name = u'Cult Scout Sentinel'
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Scout-Sentinel-Squadron')

    def __init__(self, parent):
        super(ScoutSentinel, self).__init__(parent=parent, points=get_cost(units.CultScoutSentinels))
        self.Weapon(self)
        self.Weapon2(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ScoutSentinel.Weapon, self).__init__(parent=parent, name='Weapon')
            self.multilaser = self.variant(*ranged.MultiLaser)
            self.heavyflamer = self.variant(*ranged.HeavyFlamer)
            self.autocannon = self.variant(*ranged.Autocannon)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)
            self.lascannon = self.variant(*ranged.Lascannon)

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(ScoutSentinel.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*melee.SentinelChainsaw)
    

class ArmouredSentinel(ListSubUnit):
    type_name = u'Cult Armoured Sentinel'
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Armoured-Sentinel-Squadron')

    def __init__(self, parent):
        super(ArmouredSentinel, self).__init__(parent=parent, points=get_cost(units.CultArmouredSentinels))
        self.Weapon(self)
        ScoutSentinel.Weapon2(self)

    class Weapon(ScoutSentinel.Weapon):
        def __init__(self, parent):
            super(ArmouredSentinel.Weapon, self).__init__(parent=parent)
            self.plasmacannon = self.variant(*ranged.PlasmaCannon)


class CultScoutSentinelSquad(Squadron):
    type_name = get_name(units.CultScoutSentinels)
    type_id = 'gs_scout_sentinel_squad_v1'
    unit_class = ScoutSentinel
    power = 2


class CultArmouredSentinelSquad(Squadron):
    type_name = get_name(units.CultArmouredSentinels)
    type_id = 'gs_armoured_sentinel_squad_v1'
    unit_class = ArmouredSentinel
    power = 3
