__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OptionsList
import melee, ranged, wargear

def add_pistol_options(obj):
    obj.variant(*ranged.BoltPistol)
    obj.variant(*ranged.PlasmaPistol)
    obj.variant(*ranged.InfernoPistol)

def add_melee_weapons(obj):
    obj.variant(*melee.Chainsword)
    obj.variant(*melee.PowerAxe)
    obj.variant(*melee.PowerMaul)
    obj.variant(*melee.PowerSword)

def add_heavy_weapons(obj):
    obj.variant(*ranged.HeavyBolter)
    obj.variant(*ranged.HeavyFlamer)
    obj.variant(*ranged.MultiMelta)

def add_special_weapons(obj):
    obj.variant(*ranged.StormBolter)
    obj.variant(*ranged.Flamer)
    obj.variant(*ranged.Meltagun)

def add_ranged_weapons(obj):
    obj.variant(*ranged.Boltgun)
    obj.variant(*ranged.CombiFlamer)
    obj.variant(*ranged.CombiMelta)
    obj.variant(*ranged.CombiPlasma)
    obj.variant(*ranged.CondemnorBoltgun)
    obj.variant(*ranged.StormBolter)


class Options(OptionsList):
    def __init__(self, parent):
        super(Options, self).__init__(parent, 'Options', limit=1)
        self.variant(*wargear.SimulacrumImperialis)

class MinistorumUnit(Unit):
    faction = ['Imperium', 'Adeptus Ministorum']


class SororitasUnit(MinistorumUnit):
    faction = ['Adepta Sororitas', '<Order>']

    @classmethod
    def calc_faction(cls):
        return ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
