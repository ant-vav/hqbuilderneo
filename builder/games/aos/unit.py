from builder.core2 import Unit as CoreUnit
from options import ModelCount

__author__ = 'Ivan Truskov'


class Unit(CoreUnit):
    min_size = 1
    max_size = 1
    default_points = 0
    allegiance = None
    model_name = ''
    roles = []

    def __init__(self, parent, *args, **kwargs):
        super(Unit, self).__init__(parent, *args, **kwargs)
        self.models = ModelCount(self, self.name, self.min_size,
                                 self.max_size, self.default_points)

    def get_count(self):
        return self.models.cur

    def get_roles(self):
        return self.roles
