from roster import Roster, AosVanguard, AosBattlehost, AosWarhost,\
    AosPointsOnly
from sections import BattlefieldRole

__author__ = 'Ivan Truskov'


class AllianceOrderBase(Roster):
    def __init__(self):
        super(AllianceOrderBase, self).__init__()
        from stormcast_eternals import unit_types as stormcast_units
        self.add_units(stormcast_units)
        from shadowblades import unit_types as shadowblade_units, DarkRiders
        self.add_units(shadowblade_units)
        BattlefieldRole.build_unit_type(self.battleline, DarkRiders, visible=False)

        self.collect_allegiances()


class AllianceOrderWarhost(AllianceOrderBase, AosWarhost):
    army_name = 'Grand Alliance: Order'
    army_id = 'order_wh_v1'


class AllianceOrderBattlehost(AllianceOrderBase, AosBattlehost):
    army_name = 'Grand Alliance: Order'
    army_id = 'order_bh_v1'


class AllianceOrderVanguard(AllianceOrderBase, AosVanguard):
    army_name = 'Grand Alliance: Order'
    army_id = 'order_vg_v1'


class AllianceOrderPoints(AllianceOrderBase, AosPointsOnly):
    army_name = 'Grand Alliance: Order'
    army_id = 'order_pts_v1'
