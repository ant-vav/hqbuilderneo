__author__ = 'Denis Romanov'
from builder.core.unit import Unit, OptionsList
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.beastmen.armory import *
from builder.core.options import norm_point_limits


unit_standards = filter_items(beast_standard, 50)
unit_gifts = filter_items(gifts, 25)


class Harpies(FCGUnit):
    name = 'Harpies'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Harpies', model_price=11, min_models=5, max_model=10,
            base_gear=['Claws'],
            options=[
                OptionsList('arm', 'Options', [
                    ['Scouts', 3, 'sc'],
                ])
            ]
        )


class RazorgorHerd(FCGUnit):
    name = 'Razorgor Herd'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Razorgor', model_price=55, min_models=1, base_gear=['Tusks'])


class Bestigor(FCGUnit):
    name = 'Bestigor Herd'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Bestigor', model_price=12, min_models=10,
            musician_price=6, standard_price=12,
            champion_name='Gouge-horn', champion_price=12,
            champion_options=[OptionsList('gifts', 'Gifts of Chaos', unit_gifts, points_limit=25)],
            base_gear=['Great weapon', 'Heavy armour'],
            magic_banners=unit_standards
        )


class RazorgorChariot(Unit):
    name = 'Razorgor Chariot'
    base_points = 145
    static = True

    def __init__(self, boss=False):
        Unit.__init__(self)
        self.boss = boss

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        if not self.boss:
            self.description.add(ModelDescriptor(name='Bestigor', count=1, gear=['Great weapon']).build())
        self.description.add(ModelDescriptor(name='Gor', count=1, gear=['Spear']).build())
        self.description.add(ModelDescriptor(name='Razorgor', count=1).build())


class Centigors(Unit):
    name = 'Centigors'

    base_gear = ['Light armour']
    model_price = 25

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Centigor', 5, 100, self.model_price)
        fcg = [
            ['Gorehoof', 14, 'chmp'],
            ['Standard bearer', 7, 'sb'],
            ['Musician', 14, 'mus']
        ]
        self.command_group = self.opt_options_list('Command group', fcg)
        self.boss = self.opt_options_list('', [['Ghorros Warhoof', 155, 'boss']])
        self.opt = self.opt_options_list('Weapon', [['Throwing axe', 2, 'axe']])
        self.wep = self.opt_one_of('', [
            ['Spear and Shield', 0, 'ss'],
            ['Great Weapon', 2, 'gw'],
        ])

    def get_unique(self):
        return self.boss.get_selected()

    def check_rules(self):
        self.command_group.set_active_options(['chmp'], not self.boss.get('boss'))
        self.boss.set_active_options(['boss'], not self.command_group.get('chmp'))
        self.points.set(self.build_points(count=1, exclude=[self.opt.id, self.wep.id])
                        + self.count.get() * (self.opt.points() + self.wep.points()))
        self.build_description(options=[])
        d = ModelDescriptor('Centigor', gear=self.base_gear,
                            points=self.model_price).add_gear_opt(self.opt).add_gear_opt(self.wep)
        if self.command_group.get('chmp'):
            self.description.add(d.clone().set_name('Gorehoof').add_points(14).build(1))
        if self.boss.get('boss'):
            self.description.add(ModelDescriptor('Ghorros Warhoof', points=155,
                                                 gear=['Mansmasher', 'Skull of the Unicorn Lord',
                                                       'Light armour']).build(1))
        if self.command_group.get('sb'):
            self.description.add(d.clone().add_gear('Standard bearer', 14).build(1))
        if self.command_group.get('mus'):
            self.description.add(d.clone().add_gear('Musician', 7).build(1))
        command_count = len(self.command_group.get_all())
        self.description.add(d.build(self.count.get() - command_count))

    def get_count(self):
        return self.count.get() + (1 if self.boss.get('boss') else 0)

    def has_ghorros(self):
        return self.boss.get('boss')


class Minotaurs(Unit):
    name = 'Minotaurs'
    base_gear = ['Hand weapon', 'Light armour']

    model_name = 'Minotaur'
    model_price = 55
    musician_price = 10
    standard_price = 20
    champion_price = 20
    champion_name = 'Bloodkine'
    champion_gear_limit = 25

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count(self.model_name, 3, 100, self.model_price)
        fcg = [
            [self.champion_name, self.champion_price, 'chmp'],
            ['Standard bearer', self.standard_price, 'sb'],
            ['Musician', self.musician_price, 'mus']
        ]
        self.command_group = self.opt_options_list('Command group', fcg)
        self.magic_wep = self.opt_options_list('Bloodkine\'s magic weapon',
                                               filter_items(beast_weapon, self.champion_gear_limit), 1)
        self.magic_arm = self.opt_options_list('Bloodkine\'s magic armour',
                                               filter_items(beast_armour, self.champion_gear_limit), 1)
        self.magic_enc = self.opt_options_list('Bloodkine\'s enchanted items',
                                               filter_items(beast_enchanted, self.champion_gear_limit), 1)
        self.magic_tal = self.opt_options_list('Bloodkine\'s talismans',
                                               filter_items(beast_talisman, self.champion_gear_limit), 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_enc, self.magic_tal]
        self.wep = self.opt_options_list('Options', [
            ['Shield', 4, 'sh'],
            ['Hand weapon', 4, 'hw'],
            ['Great weapon', 8, 'gw'],
        ], limit=1)

    def check_rules(self):
        for opt in self.magic:
            opt.set_visible(self.command_group.get('chmp'))
        norm_point_limits(self.champion_gear_limit, self.magic)
        self.points.set(self.build_points(count=1, exclude=[self.wep.id]) + self.wep.points() * self.count.get())
        self.build_description(options=[])
        d = ModelDescriptor(self.model_name, gear=self.base_gear, points=self.model_price)
        if self.command_group.get('chmp'):
            rg = ModelDescriptor(self.champion_name, points=self.model_price + self.champion_price,
                                 gear=['Hand weapon'])
            for opt in self.magic:
                rg.add_gear_opt(opt)
            if self.wep.get('sh'):
                if not self.magic_arm.is_selected(beast_shields_ids):
                    rg.add_gear('Shield')
            else:
                rg.add_gear_opt(self.wep)
            if not self.magic_arm.is_selected(beast_armour_suits_ids):
                rg.add_gear('Light armour')
            self.description.add(rg.build(1))
        d.add_gear_opt(self.wep)
        if self.command_group.get('sb'):
            self.description.add(d.clone().add_gear('Standard bearer',
                                                    self.standard_price).build(1))
        if self.command_group.get('mus'):
            self.description.add(d.clone().add_gear('Musician', self.musician_price).build(1))
        command_count = len(self.command_group.get_all())
        self.description.add(d.build(self.count.get() - command_count))

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.get_selected()), [])
