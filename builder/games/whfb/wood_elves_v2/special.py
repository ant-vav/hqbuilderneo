from builder.core2 import *
from builder.games.whfb.roster import FCGUnit
from armory import Standards, Arrows
from core import GladeGuard

__author__ = 'Denis Romanov'


class WildwoodRangers(FCGUnit):
    type_name = 'Wildwood Rangers'
    type_id = 'wildwoodrangers_v1'

    def __init__(self, parent):
        super(WildwoodRangers, self).__init__(
            parent, model_name='Wildwood Ranger', model_price=11, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Wildwood Warden', champion_price=10,
            gear=[Gear('Great weapon'), Gear('Light armour')]
        )
        self.magic_banners = [Standards(self, limit=25)]
        # self.model_options = [self.Opt(self)]


class Treekin(FCGUnit):
    type_name = 'Tree Kin'
    type_id = 'treekins_v1'

    def __init__(self, parent):
        super(Treekin, self).__init__(
            parent, model_name='Tree Kin', model_price=45, min_models=3,
            champion_name='Tree Kin Elder', champion_price=10,
            gear=[Gear('Hand weapon')]
        )


class DeepWoodScouts(FCGUnit):
    type_name = 'Deepwood Scouts'
    type_id = 'deepwoodscouts_v1'

    def __init__(self, parent):
        super(DeepWoodScouts, self).__init__(
            parent, model_name='Deepwood Scout', model_price=13, min_models=5,
            musician_price=10,
            standard_price=10,
            champion_name='Master Scout', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Asrai longbow')]
        )
        self.model_options = [Arrows(self)]


class Sisters(FCGUnit):
    type_name = 'Sisters of the Thorn'
    type_id = 'sistersthorn_v1'

    def __init__(self, parent):
        super(Sisters, self).__init__(
            parent, model_name='Sisters of the Thorn', model_price=26, min_models=5,
            musician_price=10,
            standard_price=10,
            champion_name='Handmaiden of the Thorn', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Blackbriar javelin')]
        )


class WildRiders(FCGUnit):
    type_name = 'Wild Riders'
    type_id = 'wildriders_v1'

    class Opt(OptionsList):
        def __init__(self, parent):
            super(WildRiders.Opt, self).__init__(parent, 'Options')
            self.variant('Shield', points=2)

    def __init__(self, parent):
        super(WildRiders, self).__init__(
            parent, model_name='Wild Rider', model_price=26, min_models=5,
            musician_price=10,
            standard_price=10,
            champion_name='Wild Hunter', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Asrai spear'), Gear('Light armour')]
        )
        self.magic_banners = [Standards(self, limit=50)]
        self.model_options = [self.Opt(self)]


class Wardancers(FCGUnit):
    type_name = 'Wardancers'
    type_id = 'wardancers_v1'

    def __init__(self, parent):
        super(Wardancers, self).__init__(
            parent, model_name='Wardancer', model_price=15, min_models=5,
            musician_price=10,
            champion_name='Bladesinger', champion_price=10,
            gear=[Gear('Hand weapon', count=2)]
        )
        self.spears = Count(self, 'Asrai spear', 0, 5, 1)
        self.unit_options = [self.spears]

    def check_rules(self):
        super(Wardancers, self).check_rules()
        self.spears.max = self.get_count()


class WarhawkRiders(FCGUnit):
    type_name = 'Warhawk Riders'
    type_id = 'warhawkriders_v1'

    def __init__(self, parent):
        super(WarhawkRiders, self).__init__(
            parent, model_name='Warhawk Rider', model_price=45, min_models=3,
            champion_name='Wind Rider', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Asrai longbow'), Gear('Asrai spear')]
        )

