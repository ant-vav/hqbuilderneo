from builder.core2 import *
from builder.games.whfb.roster import FCGUnit
from armory import Standards, Arrows

__author__ = 'Denis Romanov'


class GladeGuard(FCGUnit):
    type_name = 'Glade Guard'
    type_id = 'gladeguard_v1'

    def __init__(self, parent):
        super(GladeGuard, self).__init__(
            parent, model_name='Glade Guard', model_price=12, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Lord\'s Bowman', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Asrai longbow')]
        )
        self.magic_banners = [Standards(self, limit=25)]
        self.model_options = [Arrows(self)]


class GladeRiders(FCGUnit):
    type_name = 'Glade Riders'
    type_id = 'gladeriders_v1'

    def __init__(self, parent):
        super(GladeRiders, self).__init__(
            parent, model_name='Glade Rider', model_price=19, min_models=5,
            musician_price=10,
            standard_price=10,
            champion_name='Glade Knight', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Asrai longbow'), Gear('Asrai spear')]
        )
        self.magic_banners = [Standards(self, limit=25)]
        self.model_options = [Arrows(self)]


class EternalGuard(FCGUnit):
    type_name = 'Eternal Guard'
    type_id = 'eternalguard_v1'

    class Opt(OptionsList):
        def __init__(self, parent):
            super(EternalGuard.Opt, self).__init__(parent, 'Options')
            self.variant('Shield', points=1)

    def __init__(self, parent):
        super(EternalGuard, self).__init__(
            parent, model_name='Eternal Guard', model_price=11, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Eternal Warden', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Asrai spear'), Gear('Light armour')]
        )
        self.magic_banners = [Standards(self, limit=25)]
        self.model_options = [self.Opt(self)]


class Dryads(FCGUnit):
    type_name = 'Dryads'
    type_id = 'dryads_v1'

    def __init__(self, parent):
        super(Dryads, self).__init__(
            parent, model_name='Dryad', model_price=11, min_models=10,
            champion_name='Branch Nymph', champion_price=10,
            gear=[Gear('Hand weapon')]
        )
