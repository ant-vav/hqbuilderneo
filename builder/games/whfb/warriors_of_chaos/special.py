__author__ = 'Denis Romanov'
from builder.core.unit import Unit, OptionsList, OneOf
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.warriors_of_chaos.armory import woc_standard, filter_items

unit_standards = filter_items(woc_standard, 50)


class Hellstriders(FCGUnit):
    name = 'Hellstriders of Slaanesh'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Hellstrider', model_price=19, min_models=5,
            musician_price=10, standard_price=10, champion_name='Hellreaver', champion_price=10,
            magic_banners=unit_standards,
            base_gear=['Shield'],
            options=[
                OneOf('wep', 'Weapon', [
                    ['Spear', 0, 'sp'],
                    ['Hellscourge', 1, 'hs'],
                ]),
            ]
        )

    def check_rules(self):
        self.ban.set_visible_options(['woc_blas', 'woc_rage'], False)
        FCGUnit.check_rules(self)


class Chosen(FCGUnit):
    name = 'Chosen'

    def __init__(self):
        self.marks = OptionsList('opt', 'Options', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 2, 'tz'],
            ['Mark of Nurgle', 2, 'ng'],
            ['Mark of Slaanesh', 1, 'sl'],
        ], limit=1)
        FCGUnit.__init__(
            self, model_name='Chosen', model_price=18, min_models=5,
            musician_price=10, standard_price=10, champion_name='Chosen Champion', champion_price=10,
            magic_banners=unit_standards,
            base_gear=['Hand weapon', 'Chaos armour'],
            options=[
                OptionsList('arm', 'Armour', [
                    ['Shield', 1, 'sh'],
                ]),
                OptionsList('wep', 'Weapon', [
                    ['Hand weapon', 2, 'hw'],
                    ['Great weapon', 3, 'gw'],
                    ['Halberd', 3, 'hb'],
                ], limit=1),
                self.marks
            ]
        )

    def check_rules(self):
        self.ban.set_visible_options(['woc_blas'], self.marks.get('tz'))
        self.ban.set_visible_options(['woc_rage'], self.marks.get('kh'))
        FCGUnit.check_rules(self)


class Knights(FCGUnit):
    name = 'Chaos Knights'

    def __init__(self):
        self.marks = OptionsList('opt', 'Options', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 2, 'tz'],
            ['Mark of Nurgle', 2, 'ng'],
            ['Mark of Slaanesh', 1, 'sl'],
        ], limit=1)
        self.soc = OptionsList('soc', 'Archaon\'s upgrade', [['Swords of Chaos', 5, 'soc']])
        FCGUnit.__init__(
            self, model_name='Chaos Knight', model_price=40, min_models=5,
            musician_price=10, standard_price=10, champion_name='Doom Knight', champion_price=10,
            magic_banners=unit_standards,
            base_gear=['Hand weapon', 'Chaos armour', 'Shield', 'Barding'],
            options=[
                OptionsList('wep', 'Weapon', [
                    ['Lance', 2, 'ln'],
                    ['Ensorcelled weapon', 3, 'gw'],
                ], limit=1),
                self.marks, self.soc
            ]
        )

    def check_rules(self):
        self.ban.set_visible_options(['woc_blas'], self.marks.get('tz'))
        self.ban.set_visible_options(['woc_rage'], self.marks.get('kh'))
        self.soc.set_active_options(['soc'], self.get_roster().has_archaon() and not self.marks.is_any_selected())
        FCGUnit.check_rules(self)

    @property
    def is_soc(self):
        return self.soc.get('soc')


class Ogres(FCGUnit):
    name = 'Chaos Ogres'

    def __init__(self):
        self.marks = OptionsList('opt', 'Options', [
            ['Mark of Khorne', 4, 'kh'],
            ['Mark of Tzeentch', 4, 'tz'],
            ['Mark of Nurgle', 4, 'ng'],
            ['Mark of Slaanesh', 2, 'sl'],
        ], limit=1)
        FCGUnit.__init__(
            self, model_name='Chaos Ogre', model_price=33, min_models=3,
            musician_price=10, standard_price=10, champion_name='Chaos Mutant', champion_price=10,
            base_gear=['Hand weapon', 'Heavy armour'],
            options=[
                OptionsList('wep', 'Weapon', [
                    ['Hand weapon', 3, 'hw'],
                    ['Great weapon', 8, 'gw'],
                ], limit=1),
                self.marks
            ]
        )


class DragonOgres(FCGUnit):
    name = 'Dragon Ogres'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Dragon Ogre', model_price=60, min_models=3,
            champion_name='Dragon Ogre Shartak', champion_price=10,
            base_gear=['Hand weapon', 'Light armour'],
            options=[
                OptionsList('wep', 'Weapon', [
                    ['Hand weapon', 3, 'hw'],
                    ['Halberd', 8, 'hl'],
                    ['Great weapon', 8, 'gw'],
                ], limit=1),
            ]
        )


class Trolls(FCGUnit):
    name = 'Chaos Trolls'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Chaos Troll', model_price=35, min_models=3,
            base_gear=['Hand weapon'],
            options=[
                OptionsList('wep', 'Weapon', [
                    ['Hand weapon', 3, 'hw'],
                ], limit=1),
            ]
        )


class GorebeastChariot(Unit):
    name = 'Gorebeast Chariot'
    base_points = 130
    gear = ['Scythes']

    def __init__(self, boss=None):
        Unit.__init__(self)
        self.boss = boss
        self.marks = self.opt_options_list('Options', [
            ['Mark of Khorne', 10, 'kh'],
            ['Mark of Tzeentch', 10, 'tz'],
            ['Mark of Nurgle', 10, 'ng'],
            ['Mark of Slaanesh', 5, 'sl'],
        ], limit=1)

    def check_rules(self):
        if self.boss:
            boss_mark = self.boss.mark
            for mark_id in self.marks.get_all_ids():
                self.marks.set_active_options([mark_id], mark_id == boss_mark)
        Unit.check_rules(self)
        self.description.add(ModelDescriptor(name='Chaos Charioteer', count=1 if self.boss else 2,
                                             gear=['Halberd']).build())
        self.description.add(ModelDescriptor(name='Gorebeast', count=1).build())


class Warshine(Unit):
    name = 'Chaos Warshine'
    base_points = 125

    def __init__(self, boss=None):
        Unit.__init__(self)
        self.boss = boss
        self.marks = self.opt_options_list('Options', [
            ['Mark of Khorne', 10, 'kh'],
            ['Mark of Tzeentch', 10, 'tz'],
            ['Mark of Nurgle', 10, 'ng'],
            ['Mark of Slaanesh', 5, 'sl'],
        ], limit=1)

    def check_rules(self):
        if self.boss:
            boss_mark = self.boss.mark
            for mark_id in self.marks.get_all_ids():
                self.marks.set_active_options([mark_id], mark_id == boss_mark)
        Unit.check_rules(self)
        if not self.boss:
            self.description.add(ModelDescriptor(name='Chaos Shinemaster', count=1, gear=['Hand weapon']).build())
        self.description.add(ModelDescriptor(name='Chaos Shine Bearers', count=1, gear=['Hand weapon']).build())


class Chimera(Unit):
    name = 'Chimera'
    base_points = 230
    gear = ['Scythes']

    def __init__(self):
        Unit.__init__(self)
        self.marks = self.opt_options_list('Options', [
            ['Flaming Breath', 30, 'fb'],
            ['Regenerating Flash', 15, 'rf'],
            ['Venomous Ooze', 15, 'vo'],
        ])
