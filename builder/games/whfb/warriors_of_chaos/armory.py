__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *


woc_weapon = [
    ['Hellfire Sword', 65, 'woc_hf'],
    ['Sword of Change', 30, 'woc_ch'],
    ['Filth Mace', 30, 'woc_fm'],  # Nurgle only
] + magic_weapons

woc_armour = [
    ['Helm of Many Eyes', 25, 'woc_many']
] + magic_armours

woc_armour_suits_ids = magic_armour_suits_ids
woc_shields_ids = magic_shields_ids

woc_talisman = talismans

woc_enchanted = [
    ['Chalice of Chaos', 10, 'woc_coc'],
    ['Pendant of Slaanesh', 50, 'woc_sl'],  # Slaanesh only
] + enchanted_items

woc_arcane = [
    ['Skull of Katam', 15, 'woc_sok'],
] + arcane_items

woc_standard = magic_standards + [
    ['Blasted Standard', 25, 'woc_blas'],  # Tzeentch only
    ['Banner of Rage', 25, 'woc_rage'],  # Khorne only
]

rewards = [
    ['Daemonblade', 50, 'blade'],  # weapon
    ['Collar of Khorne', 45, 'collar'],  # talisman, Khorne only
    ['Unholy Strike', 35, 'us'],
    ['Flaming Breath', 30, 'fb'],
    ['Chaos Familiar', 25, 'cf'],  # arcane
    ['Scaled Skin', 20, 'skin'],
    ['Allure of Slaanesh', 15, 'all'],  # Slaanesh only
    ['Poisonous Slime', 15, 'slime'],
    ['Acid Ichor', 10, 'ichor'],
    ['Burning Body', 10, 'bb'],
    ['Soul Feeder', 10, 'feeder'],
    ['Third Eye of Tzeentch', 10, 'eye'],  # Tzeench only
    ['Nurgle\'s Rot', 10, 'rot'],  # Nurgle only
    ['Hideous Visage', 5, 'viz'],
]