__author__ = 'Denis Romanov'


from builder.games.whfb.armory import *


class Weapon(BaseWeapon):
    def __init__(self, parent, name='Magic weapon', limit=None):
        super(Weapon, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('Hydra Blade', 100)
        self.variant('Chillblade', 50)
        self.add_base()


class Standards(BaseStandards):
    def __init__(self, parent, name='Magic standards', limit=None):
        super(Standards, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('Banner of Nagarythe', 100)
        self.add_base()


class Armour(BaseArmour):
    def __init__(self, parent, name='Magic armour', suits=True, shields=True, limit=None):
        super(Armour, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.add_base(suits=suits, shields=shields)


class Arcane(BaseArcane):
    def __init__(self, parent, name='Arcane items', limit=None):
        super(Arcane, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('The Gem of Spite', 35)
        self.variant('The Sacrificial Dagger', 25)
        self.variant('Tome of Furion', 25)
        self.add_base()


class Enchanted(BaseEnchanted):
    def __init__(self, parent, name='Enchanted items', limit=None):
        super(Enchanted, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('Black Dragon Egg', 50)
        self.variant('Cloak of Twilight', 50)
        self.add_base()


class Talismans(BaseTalismans):
    def __init__(self, parent, name='Talismans', limit=None):
        super(Talismans, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('The Black Amulet', 60)
        self.variant('Ring of Hotek', 50)
        self.add_base()
