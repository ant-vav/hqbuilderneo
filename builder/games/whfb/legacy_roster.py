__author__ = 'Ivan Truskov'
from itertools import groupby

from builder.core.percent_section import PercentSection
from builder.core2 import Roster


class LegacyFantasy(Roster):
    game_name = 'Warhammer Fantasy Battle'
    base_tags = ['Warhammer Fantasy']

    def __init__(self, lords, heroes, core, special, rare):
        self.lords = PercentSection('Lords', lords, max=25, roster=self)
        self.heroes = PercentSection('Heroes', heroes, max=25, roster=self)
        self.core = PercentSection('Core units', core, min=25, roster=self)
        self.special = PercentSection('Special units', special, max=50, roster=self)
        self.rare = PercentSection('Rare units', rare, max=25, roster=self)
        super(LegacyFantasy, self).__init__([self.lords, self.heroes, self.core, self.special, self.rare], limit=2500)

    def check_limit(self, name, group, limit, mul):
        limit = limit * mul
        if sum((1 for _ in group)) > limit:
            self.error("Army cannot include more then {1} of {0}.".format(name, limit))

    def check_special_limit(self, name, group, limit, mul):
        return self.check_limit(name, group, limit, mul)

    def check_rare_limit(self, name, group, limit, mul):
        return self.check_limit(name, group, limit, mul)

    def check_rules(self):
        if len(self.lords.get_units()) + len(self.heroes.get_units()) < 1:
            self.error("Army must include at least one lord or hero as commander.")

        elif sum((len(lst.get_units()) for lst in [self.lords, self.heroes, self.core, self.special, self.rare])) < 4:
            self.error("Army must include at least 3 units besides commander.")

        mul = 1 if self._limit < 3000 else 2
        for key, group in groupby(self.special.get_units(), lambda unit: unit.name):
            self.check_special_limit(key, group, 3, mul)
        for key, group in groupby(self.rare.get_units(), lambda unit: unit.name):
            self.check_rare_limit(key, group, 2, mul)
