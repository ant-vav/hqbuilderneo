__author__ = 'Denis Romanov'
from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor


class Warhorse(Unit):
    name = 'Warhorse'
    base_points = 18

    def __init__(self, points=18, barding=6):
        self.base_points = points
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Barding', barding, 'brd']
        ])


class ImperialGriffon(Unit):
    name = 'Imperial Griffon'
    base_points = 170

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Bloodroar', 25, 'br'],
            ['Two Heads', 30, 'th'],
        ])


class Pegasus(Unit):
    name = 'Imperial Pegasus'
    base_points = 45

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Iron-hard Hooves', 5, 'ih'],
            ['Swift as the Wind', 10, 'sw'],
        ])


class Altar(Unit):
    name = 'War Altar of Sigmar'
    base_points = 150
    gear = ['The Golden Griffon']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [['Horn of Sigismund', 10, 'horn']])

    def check_rules(self):
        Unit.check_rules(self)
        self.description.add(ModelDescriptor(name='Warhorse', count=2).build())
