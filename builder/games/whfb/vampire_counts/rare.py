__author__ = 'Ivan Truskov'
from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.vampire_counts.armory import vampire_magic_standards, vampire_magic_weapons, filter_items


class Terrorgheist(Unit):
    name = 'Terrorgheist'
    base_points = 225

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Rancid Maw', 15, 'rm'],
            ['Infested', 10, 'inf']
        ])


class Varghulf(Unit):
    static = True
    name = 'Varghulf'
    base_points = 175


class BloodKnights(FCGUnit):
    name = 'Blood Knights'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Blood Knight', model_price=50, min_models=4,
            musician_price=10, standard_price=10, champion_name='Kastellan', champion_price=15,
            champion_gear=filter_items(vampire_magic_weapons, 25),
            magic_banners=[['Flag of Blood Keep', 75, 'bloodflag']] + filter_items(vampire_magic_standards, 50),
            base_gear=['Nightmare', 'Barding', 'Heavy armour', 'Hand weapon', 'Lance', 'Shield'],
        )


class CairnWraiths(Unit):
    name = 'Cair Wraiths'

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Cairn Wraiths', 3, 10, 50)
        self.opt = self.opt_options_list('Options', [
            ['Tomb Banshee', 25, 'tb']
        ])

    def check_rules(self):
        self.points.set(self.build_points(count=1))
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Cair Wraith', count=self.count.get()-len(self.opt.get_all()),
                                             points=50, gear=['Great weapon']).build())
        if self.opt.get('tb'):
            self.description.add(ModelDescriptor(name='Tomb Banshee', points=75, count=1).build())


class BlackCoach(Unit):
    name = 'Black Coach'
    base_points = 195
    static = True

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description()
        self.description.add(ModelDescriptor(name='Nightmare', count=2).build())
        self.description.add(ModelDescriptor(name='Cairn Wraith', count=1, gear=['Great Weapon']).build())


class MortisEngine(Unit):
    name = 'Mortis Engine'
    base_points = 220

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [['Blasphemous Tome', 20, 'bt']])

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description()
        self.description.add(ModelDescriptor(name='Corpsemaster', gear=['Hand weapon'], count=1).build())
        self.description.add(ModelDescriptor(name='Banshee Swarm', gear=['Hand weapon'], count=1).build())
        self.description.add(ModelDescriptor(name='Spirit Horde', gear=['Hand weapon'], count=1).build())
