__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit, Unit
from builder.core.unit import OptionsList, OptionalSubUnit
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor
from armory import skaven_pile, skaven_standard, filter_items, moulder_weapon

unit_standards = filter_items(skaven_standard, 50)


class Clanrats(FCGUnit):
    name = 'Clanrats'

    class WeaponTeam(Unit):
        name = 'Weapon Team'
        gear = ['Hand weapon', 'Heavy armour']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Poisoned Wind Mortar', 65],
                ['Warpfire Thrower', 70],
                ['Ratling Gun', 55],
                ['Doom-flayer', 55],
            ])

    def __init__(self):
        self.team = OptionalSubUnit('team', '', [self.WeaponTeam()])
        FCGUnit.__init__(
            self, model_name='Clanrat', model_price=4, min_models=20,
            musician_price=4, standard_price=8, champion_name='Clawleader', champion_price=8,
            base_gear=['Hand weapon', 'Light armour'], unit_options=[self.team],
            options=[
                OptionsList('arm', '', [
                    ['Spear', .5, 'sp'],
                    ['Shield', .5, 'sh'],
                ])
            ]
        )

    def get_count(self):
        return FCGUnit.get_count(self) + self.team.get_count()


class NightRunners(FCGUnit):
    name = 'Night Runners'

    class WeaponTeam(Unit):
        name = 'Weapon Team'
        gear = ['Hand weapon', 'Heavy armour', 'Warp-grinder']
        base_points = 60
        static = True

    def __init__(self):
        self.team = OptionalSubUnit('team', '', [self.WeaponTeam()])
        FCGUnit.__init__(
            self, model_name='Night Runner', model_price=7, min_models=10,
            champion_name='Nightleader', champion_price=8,
            base_gear=['Hand weapon', 'Hand weapon', 'Throwing stars'], unit_options=[self.team],
            options=[
                OptionsList('arm', '', [
                    ['Slings', 1, 'sl'],
                ])
            ]
        )

    def get_count(self):
        return FCGUnit.get_count(self) + self.team.get_count()

    def has_weapon_team(self):
        return self.team.get_count() > 0


class Stormvermin(FCGUnit):
    name = 'Stormvermin'

    def __init__(self):
        self.team = OptionalSubUnit('team', '', [Clanrats.WeaponTeam()])
        self.bodyguards = OptionsList('bg', 'Queek\'s upgrade', [
            ['Bodyguard', 4, 'bg']
        ])
        FCGUnit.__init__(
            self, model_name='Stormvermin', model_price=7, min_models=10,
            musician_price=5, standard_price=10, champion_name='Fangleader', champion_price=10,
            magic_banners=unit_standards, unit_options=[self.team],
            base_gear=['Hand weapon', 'Halberd', 'Heavy armour'],
            options=[
                OptionsList('arm', '', [
                    ['Shield', 1, 'sh'],
                ]), self.bodyguards
            ],
            champion_options=[OptionsList('fo', 'Fangleader\'s options', filter_items(skaven_pile, 15), limit=1)]
        )

    def get_count(self):
        return FCGUnit.get_count(self) + self.team.get_count()

    def check_rules(self):
        self.bodyguards.set_active_options(['bg'], self.get_roster().has_queek())
        FCGUnit.check_rules(self)

    def is_bodyguards(self):
        return self.bodyguards.get('bg')


class Slaves(FCGUnit):
    name = 'Skavenslaves'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Slave', model_price=2, min_models=20,
            musician_price=2, champion_name='Pawleader', champion_price=4,
            base_gear=['Hand weapon'],
            options=[
                OptionsList('arm', '', [
                    ['Spear', .5, 'sp'],
                    ['Sling', .5, 'sl'],
                    ['Shield', .5, 'sh'],
                ])
            ]
        )


class Swarm(FCGUnit):
    name = 'Rat Swarm'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Rat Swarm', model_price=25, min_models=2, max_model=10)


class GiantRats(Unit):
    name = 'Giant Rats'

    class MasterMoulder(Unit):
        name = 'Master Moulder'
        gear = ['Light armour', 'Hand weapon', 'Whip']
        base_points = 25 + 8

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_options_list('Weapon', [
                ['Great weapon', 4, 'gw'],
                ['Thing-catcher', 20, 'tc'],
            ])
            self.magic_wep = self.opt_options_list('Magic weapon', moulder_weapon, 1)
            self.pile = self.opt_options_list('Scavenge-pile', skaven_pile)

        def check_rules(self):
            norm_point_limits(30, [self.magic_wep, self.pile])
            Unit.check_rules(self)

    class Skweel(Unit):
        static = True
        name = 'Packmaster Skweel Gnawtooth'
        gear = ['Light armour', 'Hand weapon', 'Warp-lash']
        base_points = 100 + 8

    def __init__(self, beast_name='Giant Rat', beast_points=3, beast_gear='Sharp teeth and claws', beast_limit=5):
        Unit.__init__(self)
        self.beast_name = beast_name
        self.beast_points = beast_points
        self.beast_gear = beast_gear
        self.beast_limit = beast_limit

        self.rats = self.opt_count(self.beast_name, self.beast_limit, 100, self.beast_points)
        self.master = self.opt_count('Packmaster', 1, 20, 8)
        self.moulder = self.opt_optional_sub_unit('', [self.MasterMoulder()])
        self.skweel = self.opt_optional_sub_unit('', [self.Skweel()])

    def check_rules(self, opt=None):
        master_limit = int(self.rats.get() / self.beast_limit)
        self.moulder.set_active_options([self.MasterMoulder.name],
                                        (master_limit == 1 and not self.skweel.get_count()) or master_limit > 1)
        self.skweel.set_active_options([self.Skweel.name],
                                       (master_limit == 1 and not self.moulder.get_count()) or master_limit > 1)
        bosses = self.moulder.get_count() + self.skweel.get_count()
        self.master.update_range(max(1 - bosses, 0), master_limit - bosses)
        self.points.set(self.build_points(count=1))
        self.build_description(options=[self.moulder, self.skweel])
        self.description.add(ModelDescriptor('Packmaster', gear=['Light armour', 'Hand weapon', 'Whip'],
                                             points=8).build(self.master.get()))
        d = ModelDescriptor(self.beast_name, gear=[self.beast_gear], points=self.beast_points)
        if opt and opt.is_any_selected():
            self.description.add(d.clone().build(self.rats.get() - 1))
            self.description.add(d.add_gear_opt(opt).build(1))
        else:
            self.description.add(d.clone().build(self.rats.get()))

    def get_unique(self):
        if self.skweel.get_count() > 0:
            return self.Skweel.name
