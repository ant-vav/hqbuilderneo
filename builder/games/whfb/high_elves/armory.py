__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *

he_weapon = [
    ['Blade of Leaping Gold', 75, 'he_gold'],
    ['Star Lance', 30, 'he_star'],  # mounted only
    ['The Reaver Bow', 25, 'he_reaver'],
] + magic_weapons

he_armour = [
    ['Armour of Caledor', 50, 'he_caledor'],
    ['Shadow Armour', 25, 'he_shadow'],
    ['Shield of the Merwyrm', 15, 'he_merwyrm'],
] + magic_armours

he_armour_suits_ids = magic_armour_suits_ids + ['he_caledor', 'he_shadow']
he_shields_ids = magic_shields_ids + ['he_merwyrm']

he_talisman = [
    ['Golden Crown of Atrazar', 10, 'he_atrazar']
] + talismans

he_enchanted = [
    ['Moranions\'s Wayshard', 50, 'he_mor'],  # foot only
    ['Khaine\'s Ring of Fury', 25, 'he_fury'],
    ['Gem on Sunfire', 20, 'he_sunfire'],
    ['Cloak of Beards', 10, 'he_beards'],
] + enchanted_items

he_arcane = [
    ['Book of Hoeth', 55, 'he_book']
] + arcane_items

he_standard = [
    ['Banner of the World Dragon', 50, 'he_dragon']
] + magic_standards
