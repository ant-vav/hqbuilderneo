__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from units import Alpha, Ranger, Fresh, Specialist


class SkLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(SkLeader, self).__init__(*args, **kwargs)
        UnitType(self, Alpha)


class SkTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(SkTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Ranger)


class SkNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(SkNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Fresh)


class SkSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(SkSpecialists, self).__init__(*args, max_limit=3, **kwargs)
        UnitType(self, Specialist)


class SkitariiKillTeam(SWAGRoster):
    army_name = 'Skitarii Ranger Kill Team'
    army_id = 'skitarii_swag_v1'

    def __init__(self):
        super(SkitariiKillTeam, self).__init__(
            SkLeader, SkTroopers, SkSpecialists, SkNewRecruits)
