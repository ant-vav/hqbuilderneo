__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Basic(OptionsList):
    def __init__(self, parent, warrior=False):
        super(Basic, self).__init__(parent, 'Basic weapons')
        if warrior:
            self.variants("Gauss flayer", 50, var_dicts=[
                {'name': 'Weapon reload', 'points': 25}
            ])
        else:
            self.variants("Gauss blaster", 60, var_dicts=[
                {'name': 'Weapon reload', 'points': 30}
            ])
            self.variants("Tesla carbine", 65, var_dicts=[
                {'name': 'Weapon reload', 'points': 33}
            ])


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Phase shifter', 15)
        self.variant('Photo-visor', 15)
        self.variant('Mindshackle scarabs', 25)
        self.variant('Shadowloom', 30)


class Special(OptionsList):
    def __init__(self, parent):
        super(Special, self).__init__(parent, 'Special Weapons')
        self.variants("Synaptic disintegrator", 40, var_dicts=[
            {'name': 'Weapon reload', 'points': 20}
        ])
