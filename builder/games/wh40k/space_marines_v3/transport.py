__author__ = 'dromanow'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import Gear, SubUnit, OneOf, OptionsList
from armory import Vehicle
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle
from builder.games.wh40k.imperial_armour.volume2.transport import InfernumRazorback, LuciusDropPod, IATransport


class DiscountedTransport(SpaceMarinesBaseVehicle):
    model_points = 0

    def build_points(self):
        res = super(DiscountedTransport, self).build_points()
        if hasattr(self.roster, 'discount') and self.roster.discount is True:
            res -= self.model_points
        return res

    def check_rules(self):
        super(DiscountedTransport, self).check_rules()
        # import pdb; pdb.set_trace()
        self.points = self.build_points()


class Rhino(DiscountedTransport):
    type_id = 'rhino_v3'
    type_name = "Rhino"
    model_points = 35

    def __init__(self, parent, points=model_points):
        super(Rhino, self).__init__(parent=parent, points=points, gear=[
            Gear('Storm bolter'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.opt = Vehicle(self)


class Razorback(DiscountedTransport):
    type_name = u'Razorback'
    type_id = 'razorback_v3'
    model_points = 55

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Razorback.Weapon, self).__init__(parent=parent, name='Weapon')

            self.variant('Twin-linked Heavy Bolter', 0)
            self.variant('Twin-linked Heavy Flamer', 0)
            self.variant('Twin-linked Assault Cannon', 20)
            self.variant('Twin-linked Lascannon', 20)
            self.variant('Lascannon and Twin-linked Plasma Gun', 20)

    def __init__(self, parent, points=model_points):
        super(Razorback, self).__init__(parent=parent, points=points,
                                        gear=[Gear('Smoke Launchers'), Gear('Searchlight')],
                                        tank=True, transport=True)
        self.Weapon(self)
        Vehicle(self)


class DropPod(DiscountedTransport):
    type_id = 'drop_pod_v3'
    type_name = u'Drop Pod'
    model_points = 35

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DropPod.Weapon, self).__init__(parent, 'Weapon')
            self.sbgun = self.variant('Storm bolter', 0)
            self.dwind = self.variant('Deathwind missile launcher', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DropPod.Options, self).__init__(parent, 'Options')
            self.locator = self.variant('Locator beacon', 10)

    def __init__(self, parent, points=model_points):
        super(DropPod, self).__init__(parent=parent, points=points, deep_strike=True, transport=True)
        self.weapon = DropPod.Weapon(self)
        self.opt = DropPod.Options(self)


class Transport(IATransport):
    def __init__(self, parent, crusader=False, dreadnought=False, drop_only=False, raider=False):
        from heavy import LandRaiderCrusader, LandRaider
        super(Transport, self).__init__(parent=parent, name='Transport')
        squad = crusader or (not dreadnought and not drop_only)
        self.rhino = squad and SubUnit(self, Rhino(parent=parent))
        self.razor = squad and SubUnit(self, Razorback(parent=parent))
        self.drop = SubUnit(self, DropPod(parent=parent))
        self.lrc = crusader and SubUnit(self, LandRaiderCrusader(parent=None))
        self.infernum = squad and SubUnit(self, InfernumRazorback(parent=None))
        self.lucius = dreadnought and SubUnit(self, LuciusDropPod(parent=None))
        self.landraider = raider and SubUnit(self, LandRaider(parent=None))
        self.models += [u for u in [self.rhino, self.razor, self.drop,
                                    self.lrc, self.landraider] if u]
        self.ia_models += [u for u in [self.infernum, self.lucius] if u]

    def is_razor(self):
        return self.razor and self.razor.visible


class TerminatorTransport(IATransport):
    def __init__(self, parent):
        super(TerminatorTransport, self).__init__(parent=parent, name='Transport')
        from heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer
        self.lr = SubUnit(self, LandRaider(parent=None))
        self.lrc = SubUnit(self, LandRaiderCrusader(parent=None))
        self.lrr = SubUnit(self, LandRaiderRedeemer(parent=None))
        self.models += [self.lrc, self.lr, self.lrr]
