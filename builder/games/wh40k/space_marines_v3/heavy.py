__author__ = 'Ivan Truskov'

from armory import *
from transport import Transport, TerminatorTransport
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle,\
    SpaceMarinesBaseSquadron
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit
from builder.core2 import *
from builder.games.wh40k.unit import Unit


class Devastators(IATransportedUnit):
    type_name = u'Devastator Squad'
    type_id = 'devastators_v3'

    model_points = 14
    model_gear = Armour.power_armour_set

    class Sergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(Devastators.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        class Weapon1(Ranged, Chainsword, Boltgun):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        def __init__(self, parent):
            super(Devastators.Sergeant, self).__init__(
                name='Space Marine Sergeant',
                parent=parent, points=70 - 4 * Devastators.model_points,
                gear=Devastators.model_gear + [Gear('Signum')]
            )
            self.wep1 = self.Weapon1(self, 'Weapon')
            self.wep2 = self.Weapon2(self, '')
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(Devastators.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Sergeant'
            return desc

    class HeavyWeapon(Heavy, Boltgun):
        pass

    class Options(OptionsList):
        def __init__(self, parent):
            super(Devastators.Options, self).__init__(parent, 'Options')
            self.variant('Armorium Cherub', 5)
            self.purge = self.variant('Cybernetic augmentation (Purge the Weak!)', 50)

        def check_rules(self):
            super(Devastators.Options, self).check_rules()
            # no proper check for type of roster, sorry
            try:
                self.purge.used = self.purge.visible = self.parent.roster.hq.vayland.count > 0
            except Exception:
                self.purge.used = self.purge.visible = False

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(Devastators, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.get_leader()(None))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.heavy = [self.HeavyWeapon(self, 'Heavy Weapon') for i in range(0, 4)]
        self.opt = self.Options(self)
        self.transport = Transport(parent=self)

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points,
                               count=self.get_count(), options=self.opt.description)
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=Devastators.model_gear + [Gear('Bolt pistol')],
            points=Devastators.model_points
        )
        count = self.marines.cur
        for o in self.heavy:
            if o.cur != o.boltgun:
                spec_marine = marine.clone()
                spec_marine.points += o.points + o.flakk.points
                spec_marine.add(o.description)
                desc.add_dup(spec_marine)
                count -= 1

        marine.add(Gear('Boltgun'))
        marine.count = count
        desc.add(marine)
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1


class CenturionDevastators(IATransportedUnit):
    type_name = u'Centurion Devastator Squad'
    type_id = 'centurion_devastator_squad_v3'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CenturionDevastators.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.hurricanebolter = self.variant('Hurricane bolter', 0)
            self.missilelauncher = self.variant('Missile launcher', 10)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CenturionDevastators.Weapon2, self).__init__(parent=parent, name='')
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 15)
            self.gravcannonandgravamp = self.variant('Grav-cannon and grav-amp', 25,
                                                     gear=[Gear('Grav-cannon'), Gear('Grav-amp')])

    class Centurion(ListSubUnit):

        def __init__(self, parent):
            super(CenturionDevastators.Centurion, self).__init__(parent=parent, name='Centurion', points=55)
            self.wep1 = CenturionDevastators.Weapon1(self)
            self.wep2 = CenturionDevastators.Weapon2(self)

    class Sergeant(Unit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(CenturionDevastators.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.omniscope = self.variant('Omniscope', 10)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(CenturionDevastators.Sergeant, self).__init__(parent=parent, name='Centurion Sergeant', points=55)
            self.wep1 = CenturionDevastators.Weapon1(self)
            self.wep2 = CenturionDevastators.Weapon2(self)
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(CenturionDevastators.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Centurion Veteran Sergeant'
            return desc

    def __init__(self, parent):
        super(CenturionDevastators, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(parent=None))
        self.cent = UnitList(self, self.Centurion, min_limit=2, max_limit=5)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.cent.count + 1


class Thunderfire(Unit):
    type_name = "Thunderfire cannons"
    gunner = UnitDescription(name='Techmarine gunner', options=[
        Gear('Artificer armour'),
        Gear('Bolt pistol'),
        Gear('Frag grenades'),
        Gear('Krak grenades'),
        Gear('Servo-harness'),
    ])

    def __init__(self, parent):
        super(Thunderfire, self).__init__(parent=parent)
        self.models = Count(self, 'Thunderfire cannon', 1, 3, 100, True)
        self.transport = Transport(self, drop_only=True)

    def check_rules(self):
        super(Thunderfire, self).check_rules()
        self.transport.used = self.transport.visible = self.count == 1

    def get_count(self):
        return self.models.cur

    def build_description(self):
        desc = super(Thunderfire, self).build_description()
        desc.add(self.gunner.clone().set_count(self.count))
        return desc

    def build_statistics(self):
        return self.note_transport(super(Thunderfire, self).build_statistics())


class Predator(SpaceMarinesBaseVehicle):
    type_name = "Predator"
    type_id = "predator_v3"

    class Turret(OneOf):
        def __init__(self, parent):
            super(Predator.Turret, self).__init__(parent=parent, name='Turret')
            self.autocannon = self.variant('Autocannon', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 25)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(Predator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.sponsonswithheavybolters = self.variant('Sponsons with heavy bolters', 20,
                                                         gear=Gear('Heavy bolter', count=2))
            self.sponsonswithlascannons = self.variant('Sponsons with lascannons', 40,
                                                       gear=Gear('Lascannon', count=2))

    def __init__(self, parent):
        super(Predator, self).__init__(parent=parent, points=75, gear=[
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.turret = self.Turret(self)
        self.side = self.Sponsons(self)
        self.opt = Vehicle(self)


class Predators(SpaceMarinesBaseSquadron):
    type_name = "Predators"
    type_id = "predators_v3"
    unit_class = Predator


class Whirlwind(SpaceMarinesBaseVehicle):
    type_name = "Whirlwind"
    type_id = "Whirlwind_v3"

    def __init__(self, parent):
        super(Whirlwind, self).__init__(parent=parent, points=65, gear=[
            Gear('Whirlwind multiple missile launcher'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = Vehicle(self)


class Whirlwinds(SpaceMarinesBaseSquadron):
    type_name = "Whirlwinds"
    type_id = "Whirlwinds_v3"
    unit_class = Whirlwind


class Vindicator(SpaceMarinesBaseVehicle):
    type_name = "Vindicator"
    type_id = "vindicator_v3"

    def __init__(self, parent):
        super(Vindicator, self).__init__(parent=parent, points=120, gear=[
            Gear('Demolisher cannon'),
            Gear('Storm bolter'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = Vehicle(self, shield=True)


class Vindicators(SpaceMarinesBaseSquadron):
    type_name = "Vindicators"
    type_id = "vindicators_v3"
    unit_class = Vindicator


class Hunter(SpaceMarinesBaseVehicle):
    type_name = "Hunter"
    type_id = "hunter_v3"

    def __init__(self, parent):
        super(Hunter, self).__init__(parent=parent, points=70, gear=[
            Gear('Skyspear missile launcher'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = Vehicle(self)


class Hunters(SpaceMarinesBaseSquadron):
    type_name = "Hunters"
    type_id = "hunters_v3"
    unit_class = Hunter


class Stalker(SpaceMarinesBaseVehicle):
    type_name = "Stalkers"
    type_id = "stalkers_v1"

    def __init__(self, parent):
        super(Stalker, self).__init__(parent=parent, points=75, gear=[
            Gear('Icarus stormcannon array'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = Vehicle(self)


class Stalkers(SpaceMarinesBaseSquadron):
    type_name = "Stalkers"
    type_id = "stalkers_v3"
    unit_class = Stalker


class StormravenGunship(SpaceMarinesBaseVehicle):
    type_name = u'Stormraven Gunship'
    type_id = 'stormraven_gunship_v3'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon1, self).__init__(parent, 'Weapon')
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.twinlinkedmultimelta = self.variant('Twin-linked Multi-melta', 0)
            self.typhoonmissilelauncher = self.variant('Typhoon missile launcher', 25)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon2, self).__init__(parent, '')
            self.twinlinkedassaultcannon = self.variant('Twin-linked Assault cannon', 0)
            self.twinlinkedplasmacannon = self.variant('Twin-linked Plasma cannon', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked Lascannon', 0)

    class Options(OptionsList):
        def __init__(self, parent):
            super(StormravenGunship.Options, self).__init__(parent, 'Options')
            self.hurricanebolters = self.variant('Side sponsons with hurricane bolters', 30, gear=[
                Gear('Hurricane bolter', count=2)
            ])
            self.searchlight = self.variant('Searchlight', 1)
            self.extraarmour = self.variant('Extra armour', 5)
            self.locatorbeacon = self.variant('Locator beacon', 10)

    def __init__(self, parent):
        super(StormravenGunship, self).__init__(parent=parent, points=200, gear=[
            Gear('Ceramite plating'),
            Gear('Stormstrike missile', count=4),
        ], transport=True)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.opt = self.Options(self)


class Stormravens(SpaceMarinesBaseSquadron):
    type_name = u'Stormraven gunships'
    type_id = 'stormraven_wing_v3'
    unit_class = StormravenGunship
    unit_max = 4


class LandRaider(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_v3'
    type_name = "Land Raider"

    def __init__(self, parent):
        super(LandRaider, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked heavy bolter'),
            Gear('Twin-linked lascannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.opt = Vehicle(self, blade=False, melta=True)


class LandRaiderCrusader(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_crusader_v3'
    type_name = "Land Raider Crusader"

    def __init__(self, parent):
        super(LandRaiderCrusader, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Hurricane bolter', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag Assault Launcher')
        ], tank=True, transport=True)
        self.opt = Vehicle(self, blade=False, melta=True)


class LandRaiderRedeemer(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_redeemer_v3'
    type_name = "Land Raider Redeemer"

    def __init__(self, parent):
        super(LandRaiderRedeemer, self).__init__(parent=parent, points=240, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Flamestorm cannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag Assault Launcher'),
        ], tank=True, transport=True)
        self.opt = Vehicle(self, blade=False, melta=True)


from builder.games.wh40k.imperial_armour.dataslates.space_marines import BaseDeimos


class DeimosVindicator(BaseDeimos, SpaceMarinesBaseVehicle):
    def get_options(self):
        class Options(Vehicle):
            def __init__(self, parent):
                super(Options, self).__init__(parent, shield=True)
        return Options
