__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf, OptionsList,\
    ListSubUnit, UnitList
from builder.games.wh40k.unit import Unit
from armory import Vehicle


class Bikers(Unit):
    type_name = u'Bikers'
    type_id = 'bikers_v1'

    class Biker(ListSubUnit):
        type_name = u'Biker'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Bikers.Biker.Weapon, self).__init__(parent, 'Melee weapon')
                self.variant('Close combat weapon')
                self.variant('Power weapon', 5)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Bikers.Biker.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)
                self.variant('Deathwatch teleport homer', 10)

        def __init__(self, parent):
            super(Bikers.Biker, self).__init__(parent, points=30, gear=[
                Gear('Twin-linked boltgun'), Gear('Special issue ammunition'),
                Gear('Frag grenades'), Gear('Krak grenades')
            ])
            self.Weapon(self)
            self.Options(self)

    def __init__(self, parent):
        super(Bikers, self).__init__(parent)
        self.models = UnitList(self, self.Biker, 1, 5)

    def get_count(self):
        return self.models.count


class Rhino(Unit):
    type_id = 'rhino_v1'
    type_name = "Rhino"

    def __init__(self, parent):
        super(Rhino, self).__init__(parent=parent, points=35, gear=[
            Gear('Storm bolter'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ])
        self.opt = Vehicle(self)


class Razorback(Unit):
    type_name = u'Razorback'
    type_id = 'razorback_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Razorback.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Twin-linked Heavy Bolter', 0)
            self.variant('Twin-linked Lascannon', 20)
            self.variant('Twin-linked Assault Cannon', 20)

    def __init__(self, parent):
        super(Razorback, self).__init__(parent=parent, points=55,
                                        gear=[Gear('Smoke Launchers'), Gear('Searchlight')])
        self.Weapon(self)
        Vehicle(self)


class DropPod(Unit):
    type_id = 'drop_pod_v1'
    type_name = u'Drop Pod'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DropPod.Weapon, self).__init__(parent, 'Weapon')
            self.sbgun = self.variant('Storm bolter', 0)
            self.dwind = self.variant('Deathwind missile launcher', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DropPod.Options, self).__init__(parent, 'Options')
            self.locator = self.variant('Locator beacon', 10)

    def __init__(self, parent):
        super(DropPod, self).__init__(parent=parent, points=35)
        self.weapon = DropPod.Weapon(self)
        self.opt = DropPod.Options(self)


class Corvus(Unit):
    type_name = u'Corvus Blackstar'
    type_id = 'corvus_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Corvus.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Twin-linked assault cannon')
            self.variant('Twin-linked lascannon')

    class Payload(OneOf):
        def __init__(self, parent):
            super(Corvus.Payload, self).__init__(parent, 'Payload')
            self.variant('Four stormstrike missiles', gear=[Gear('Stormstrike missiles', count=4)])
            self.variant('Twin-linked Blackstar rocket launcher', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Corvus.Options, self).__init__(parent, 'Options')
            self.variant('Searchlight', 1)
            self.variant('Extra armour', 5)
            self.variant('Locator beacon', 10)
            self.variant('Hurricane bolter', 15)
            self.alt = [self.variant('Infernum halo-launcher', 5),
                        self.variant('Auspex array', 10)]

        def check_rules(self):
            super(Corvus.Options, self).check_rules()
            OptionsList.process_limit(self.alt, 1)

    def __init__(self, parent):
        super(Corvus, self).__init__(parent, points=180, gear=[
            Gear('Blackstar cluster launcher'),
            Gear('Ceramite plating')
        ])
        self.Weapon(self)
        self.Payload(self)
        self.Options(self)
