__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf,\
    OptionsList
from builder.games.wh40k.roster import Unit
from armory import BaseWeapon,\
    SpecialWargear, Melee, Ranged, PlainMelee, Boltgun,\
    BoltPistol, HolyRelicVigilant, HolyWeapon, ArcanaWeapon


class WatchMaster(Unit):
    type_name = u'Watch Master'
    type_id = 'watch_master_v1'

    class Spear(BaseWeapon):
        def __init__(self, *args, **kwargs):
            super(WatchMaster.Spear, self).__init__(*args, **kwargs)
            self.variant('Guardian Spear')

    class Weapon(HolyWeapon, Spear):
        pass

    def __init__(self, parent):
        super(WatchMaster, self).__init__(parent, points=175, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Iron halo')
        ])
        self.wep = self.Weapon(self, 'Weapon')
        SpecialWargear(self)
        self.relic = HolyRelicVigilant(self, watchmaster=True)

    def check_rules(self):
        super(WatchMaster, self).check_rules()
        self.relic.used = self.relic.visible = self.wep.cur not in self.wep.relic_options

    def get_unique_gear(self):
        return self.relic.get_unique() + self.wep.get_unique()

    def build_description(self):
        result = super(WatchMaster, self).build_description()
        if not self.relic.used and self.relic.has_key():  # noqa
            result.add(Gear('Clavis'))
        return result


class WatchCaptain(Unit):
    type_name = u'Watch Captain'
    type_id = 'watch_captain_v1'

    class Armour(OneOf):
        def __init__(self, parent):
            super(WatchCaptain.Armour, self).__init__(parent, 'Armour')
            common = [Gear('Frag grenades'), Gear('Krak grenades'), Gear('Special issue ammunition')]
            self.variant('Power armour', gear=common)
            self.variant('Artificer armour', 20,
                         gear=common + [Gear('Artificer armour')])
            self.tda = self.variant('Terminator armour', 25,
                                    gear=[Gear('Terminator armour'),
                                          Gear('Storm bolter')])

        def is_tda(self):
            return self.cur == self.tda

    class ExtraOptions(OptionsList):
        def __init__(self, parent):
            super(WatchCaptain.ExtraOptions, self).__init__(parent, '')
            self.variant('Jump pack', 15)
            self.variant('Storm shield', 15)

    class RangedWeapon(Melee, ArcanaWeapon, Ranged, Boltgun, BoltPistol):
        pass

    class CaptainMelee(BaseWeapon):
        def __init__(self, *args, **kwargs):
            super(WatchCaptain.CaptainMelee, self).__init__(*args, **kwargs)
            xeno = self.variant('Xenophase blade', 25)
            self.pwr_weapon += [xeno]
            power = self.variant('Power sword')
            relic = self.variant('Relic blade', 10)
            self.tda_weapon += [power, relic]

    class MeleeWeapon(HolyWeapon, Ranged, Boltgun, CaptainMelee, Melee, PlainMelee):
        pass

    def __init__(self, parent):
        super(WatchCaptain, self).__init__(parent, points=95,
                                           gear=[Gear('Iron halo')])
        self.armour = self.Armour(self)
        self.ranged = self.RangedWeapon(self, 'Ranged weapon')
        self.melee = self.MeleeWeapon(self, 'Melee weapon', armour=self.armour)
        SpecialWargear(self)
        self.extra = self.ExtraOptions(self)
        self.relic = HolyRelicVigilant(self)

    def check_rules(self):
        super(WatchCaptain, self).check_rules()
        self.ranged.used = self.ranged.visible =\
                           self.extra.used = self.extra.visible =\
                                             not self.armour.is_tda()
        self.relic.used = self.relic.visible = self.melee.cur not in self.melee.relic_options

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.relic.get_unique() + self.melee.get_unique() + self.ranged.get_unique()


class Chaplain(Unit):
    type_id = 'chaplain_v1'
    type_name = u'Chaplain'

    class Crozius(BaseWeapon):
        def __init__(self, *args, **kwargs):
            super(Chaplain.Crozius, self).__init__(*args, **kwargs)
            self.variant('Crozius Arcanum')

    class MeleeWeapon(HolyWeapon, Crozius):
        pass

    class RangedWeapon(ArcanaWeapon, Ranged, Boltgun, BoltPistol):
        pass

    def __init__(self, parent):
        super(Chaplain, self).__init__(parent, points=95, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Special issue ammunition'), Gear('Rozarius')
        ])
        self.mle = self.MeleeWeapon(self, 'Melee weapon')
        self.rng = self.RangedWeapon(self, 'Ranged weapon')
        SpecialWargear(self)
        self.relic = HolyRelicVigilant(self)

    def check_rules(self):
        super(Chaplain, self).check_rules()
        self.relic.used = self.relic.visible = self.mle.cur not in self.mle.relic_options

    def get_unique_gear(self):
        return self.relic.get_unique() + self.mle.get_unique() + self.rng.get_unique()


class Librarian(Unit):
    type_id = 'librarian_v1'
    type_name = u'Librarian'

    class Force(BaseWeapon):
        def __init__(self, *args, **kwargs):
            super(Librarian.Force, self).__init__(*args, **kwargs)
            self.variant('Force sword')

    class MeleeWeapon(HolyWeapon, Force):
        pass

    class RangedWeapon(ArcanaWeapon, Ranged, Boltgun, BoltPistol):
        pass

    class TerminatorRanged(OptionsList):
        def __init__(self, parent):
            super(Librarian.TerminatorRanged, self).__init__(parent, 'Ranged weapon', limit=1)
            self.variant('Storm bolter', 5)
            self.variant('Combi-flamer', 10)
            self.variant('Combi-melta', 10)
            self.variant('Combi-plasma', 10)

    class Armour(OneOf):
        def __init__(self, parent):
            super(Librarian.Armour, self).__init__(parent, 'Armour')
            self.variant('Power armour', gear=[Gear('Frag grenades'),
                                               Gear('Krak grenades'),
                                               Gear('Special issue ammunition')])
            self.tda = self.variant('Terminator armour')

        def is_tda(self):
            return self.cur == self.tda

    class Level(OneOf):
        def __init__(self, parent):
            super(Librarian.Level, self).__init__(parent, 'Mastery level')
            self.variant('Mastery level 1', gear=[])
            self.lvl2 = self.variant('mastery level 2', 25)

    def __init__(self, parent):
        super(Librarian, self).__init__(parent, points=70,
                                        gear=[Gear('Psychic hood')])
        self.lvl = self.Level(self)
        self.armour = self.Armour(self)
        self.rng1 = self.RangedWeapon(self, 'Ranged weapon')
        self.rng2 = self.TerminatorRanged(self)
        self.mle = self.MeleeWeapon(self, 'Melee weapon')
        self.opt = SpecialWargear(self)
        self.relic = HolyRelicVigilant(self)

    def check_rules(self):
        super(Librarian, self).check_rules()
        self.rng1.used = self.rng1.visible = not self.armour.is_tda()
        self.rng2.used = self.rng2.visible = self.armour.is_tda()
        self.relic.used = self.relic.visible = self.mle.cur not in self.mle.relic_options

    def get_unique_gear(self):
        return self.relic.get_unique() + self.mle.get_unique() + self.rng1.get_unique()

    def count_charges(self):
        return 1 + (self.lvl.cur == self.lvl.lvl2)

    def build_statistics(self):
        return self.note_charges(super(Librarian, self).build_statistics())
