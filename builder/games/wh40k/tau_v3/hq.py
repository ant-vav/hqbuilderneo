__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import OneOf, OptionsList, Gear, Count,\
    UnitDescription
from armory import *
from builder.games.wh40k.roster import Unit


class Commander(Unit):
    type_name = u'Commander'
    type_id = 'commander_v3'
    system_slots = 4

    class Suit(OneOf):
        def __init__(self, parent):
            super(Commander.Suit, self).__init__(parent, 'Battlesuit')
            self.variant('Crisis battlesuit')
            self.coldstar = self.variant('XV86 Coldstar battlesuit', 60,
                                         gear=[Gear('XV86 Coldstar battlesuit'),
                                               Gear('High output burst cannon'),
                                               Gear('Missile pod')])

    def __init__(self, parent):
        super(Commander, self).__init__(parent=parent, points=85)
        self.suit = self.Suit(self)
        self.weapon = Weapon(self, slots=self.system_slots)
        self.support = SupportSystem(self, slots=self.system_slots)
        self.signature = SignatureSystem(self, slots=None, commander=True)
        self.drones = Drones(self)

    def check_rules(self):
        self.system_slots = 2 if self.suit.cur == self.suit.coldstar else 4
        self.signature.used = self.signature.visible = self.suit.cur != self.suit.coldstar
        self.drones.check_rules()
        free_slots = self.system_slots - self.weapon.count_slots() - self.support.count_slots()
        for s in [self.weapon, self.support]:
            s.set_free_slots(free_slots)

    def get_unique_gear(self):
        return self.signature.get_unique_gear()

    def build_statistics(self):
        res = super(Commander, self).build_statistics()
        res['Models'] += self.drones.count
        return res


class Ethereal(Unit):
    type_name = u'Ethereal'
    type_id = 'ethereal_v3'

    def __init__(self, parent):
        super(Ethereal, self).__init__(parent=parent, points=50)
        self.Weapon(self)
        self.opt = self.Options(self)
        self.drones = Drones(self)

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Ethereal.Weapon, self).__init__(parent=parent, name='Weapon', limit=1)
            self.honourblade = self.variant('Honour blade', 5)
            self.twoequalisers = self.variant('Two equalisers', 10, gear=Gear('Equaliser', count=2))

    class Options(OptionsList):
        def __init__(self, parent):
            super(Ethereal.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.blacksunfilter = self.variant('Blacksun filter', 5)
            self.homingbeacon = self.variant('Homing beacon', 5)
            self.armour = self.variant('Recon armour', 5)
            self.hover = self.variant('Hover drone', 5)

    def check_rules(self):
        self.drones.check_rules()

    def build_statistics(self):
        res = super(Ethereal, self).build_statistics()
        res['Models'] += self.drones.count
        return res


class Farsight(Unit):
    type_name = u'Commander Farsight, fallen hero of Vior\'La'
    type_id = 'commanderfarsight_v3'

    def __init__(self, parent):
        super(Farsight, self).__init__(
            parent=parent, name='Commander Farsight', points=165, unique=True, gear=[
                Gear('Crisis battlesuit'), Gear('Shield generator'),
                Gear('Plasma rifle'), Gear('Dawn blade')
            ]
        )


class Shadowsun(Unit):
    type_name = u'Commander Shadowsun, heroine of the third sphere expansion'
    type_id = 'commandershadowsun_v3'

    def __init__(self, parent):
        super(Shadowsun, self).__init__(
            parent=parent, points=135, name='Commander Shadowsun',
            unique=True, gear=[
                Gear('XV22 Stealth battlesuit'), Gear('Advanced targeting system'), Gear('Fusion blaster', count=2)
            ]
        )
        self.commandlinkdrone = Count(self, 'Command link drone', min_limit=0, max_limit=1, points=20)
        self.mv52shielddrone = Count(self, 'MV52 Shield drone', min_limit=0, max_limit=2, points=20)

    def build_statistics(self):
        res = super(Shadowsun, self).build_statistics()
        res['Models'] += self.commandlinkdrone.cur + self.mv52shielddrone.cur
        return res


class Aunshi(Unit):
    type_name = u'Aun\'shi, hero of Fio\'Vash'
    type_id = 'aunshi_v3'

    def __init__(self, parent):
        super(Aunshi, self).__init__(
            parent=parent, name='Aun\'shi', points=110, unique=True,
            gear=[Gear('Honour blade'), Gear('EMP Grenades'), Gear('Photon grenades'), Gear('Shield generator')]
        )


class Aunva(Unit):
    type_name = u'Aun\'va, master of the undying spirit'
    type_id = 'aunva_v3'

    def __init__(self, parent):
        super(Aunva, self).__init__(parent=parent, points=100, unique=True, gear=[
            UnitDescription('Aun\'va', options=[Gear('The Paradox of Duality'), Gear('Recon armour')]),
            UnitDescription(name='Honour Guard', options=[Gear('Honour blade'), Gear('Recon armour'),
                                                          Gear('Photon grenades')], count=2)])

    def build_statistics(self):
        res = super(Aunva, self).build_statistics()
        res['Models'] += 2
        return res


class Aundo(Unit):
    type_name = "Aun'do, Master of the Gifted Stars"
    type_id = 'aundo_v3'

    def __init__(self, parent):
        super(Aundo, self).__init__(parent, points=75, unique=True, static=True, gear=[
            Gear('Honour blade'), Gear('Homing beacon'), Gear('Hover drone')
        ])


class Cadre(Unit):
    type_name = u'Cadre Fireblade'
    type_id = 'cadrefireblade_v3'

    def __init__(self, parent):
        super(Cadre, self).__init__(
            parent=parent, points=60,
            gear=[Gear('Pulse rifle'), Gear('Photon grenades'), Gear('Markerlight')])
        self.drones = Drones(self)

    def check_rules(self):
        self.drones.check_rules()

    def build_statistics(self):
        res = super(Cadre, self).build_statistics()
        res['Models'] += self.drones.count
        return res


class Darkstider(Unit):
    type_name = u'Darkstrider'
    type_id = 'darkstider_v3'

    def __init__(self, parent):
        super(Darkstider, self).__init__(
            parent=parent, points=100, unique=True,
            gear=[Gear('Recon armour'), Gear('Pulse carabine'),
                  Gear('Photon greandes'), Gear('Blacksun filter'),
                  Gear('Markerlight'), Gear('Structural analyzer')]
        )
