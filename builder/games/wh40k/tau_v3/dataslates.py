__author__ = 'dvarh'

from builder.core2 import Gear, OneOf, Count
from armory import SupportSystem
from builder.games.wh40k.roster import HeavySection,\
    LordsOfWarSection, FastSection, Unit, UnitType


class Rvarna(Unit):
    type_name = u'XV107 R\'varna battlesuit'
    type_id = 'xv107rvarna_v3'
    imperial_armour = True

    def __init__(self, parent):
        super(Rvarna, self).__init__(
            name='R\'varna Shas\'vre',
            parent=parent, points=260,
            gear=[Gear('R\'varna battlesuit'), Gear('Pulse submunitions cannon', count=2),
                  Gear('Riptide shield generator')]
        )
        SupportSystem(self, slots=2, rvarna=True)
        self.shieldedmissiledrone = Count(self, 'Shielded Missile Drone', min_limit=0, max_limit=2, points=25)


class Yvahra(Unit):
    type_name = u'XV109 Y\'vahra battlesuit'
    type_id = 'xv109yvahra_v3'
    imperial_armour = True

    def __init__(self, parent):
        super(Yvahra, self).__init__(
            name='Y\'vahra Shas\'vre',
            parent=parent, points=230,
            gear=[Gear('Y\'vahra battlesuit'),
                  Gear('Ionic discharge cannon'),
                  Gear('Phased plasma-flamer'),
                  Gear('Ravelin shield generator'),
                  Gear('Vectored thruster array'),
                  Gear('Flechette disperdal pods')]
        )
        SupportSystem(self, slots=2, riptide=True)
        drone1 = Count(self, 'Shielded Missile Drone', 0, 2, points=25)
        drone2 = Count(self, 'Shield Drone', 0, 2, points=12)
        self.drones = [drone1, drone2]

    def check_rules(self):
        super(Yvahra, self).check_rules()
        Count.norm_counts(0, 2, self.drones)


class Taunar(Unit):
    type_name = u'KX139 Ta\'unar Supremacy Armour'
    type_id = 'kx139taunar_v3'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Taunar.Weapon, self).__init__(parent, 'Arm-mounted weapon')
            self.variant(name='Tri-axis ion cannon', points=0),
            self.variant(name='Fusion eradicator', points=0),

    def __init__(self, parent):
        super(Taunar, self).__init__(
            name='KX139 Ta\'unar Supremacy Armour',
            parent=parent, points=600,
            gear=[Gear('Vigilance defence system'),
                  Gear('Barrier shield generator')]
        )
        wep1 = self.Weapon(self)
        wep2 = self.Weapon(self)
        self.weapons = [wep1, wep2]


class TauHeavySupport(HeavySection):
    def __init__(self, parent):
        super(TauHeavySupport, self).__init__(parent)
        UnitType(self, Rvarna)


class TauFastAttack(FastSection):
    def __init__(self, parent):
        super(TauFastAttack, self).__init__(parent)
        UnitType(self, Yvahra)


class TauLordsSection(LordsOfWarSection):
    def __init__(self, parent):
        super(TauLordsSection, self).__init__(parent)
        UnitType(self, Taunar)
