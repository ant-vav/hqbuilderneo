__author__ = 'Ivan Truskov'

from builder.core2.section import Section
from builder.core2 import Unit, Gear, Count, UnitDescription,\
    OneOf, SubUnit, UnitType, ListSubUnit, UnitList


class Shieldline(Unit):
    type_name = u'Tidewall Shieldline'
    type_id = 'tidewall_shieldline_v3'

    def __init__(self, parent):
        super(Shieldline, self).__init__(parent=parent, points=60, static=True)


class Droneport(Unit):
    type_name = u'Tidewall Droneport'
    type_id = 'tidewall_droneport_v3'

    def __init__(self, parent):
        super(Droneport, self).__init__(parent=parent, points=60)
        self.drones = [
            Count(self, 'Marker drones', 0, 4, 0, gear=Gear('Marker drone')),
            Count(self, 'Shield drones', 0, 4, 0, gear=Gear('Shield drone'))
        ]

    def check_rules(self):
        Count.norm_counts(0, 4, self.drones)

    def build_description(self):
        res = super(Droneport, self).build_description()
        cnt = 4 - sum(c.cur for c in self.drones)
        if cnt:
            res.add(Gear('Gun drone', count=cnt))
        return res


class Gunrig(Unit):
    type_name = u'Tidewall Gunrig'
    type_id = 'tidewall_gunrig_v3'

    def __init__(self, parent):
        super(Gunrig, self).__init__(parent=parent, points=85,
                                     gear=[Gear('Twin-linked railgun')], static=True)


class Gunfort(Unit):
    type_name = u'Tidewall Gunfort'
    type_id = 'tidewall_gunfort_v3'

    def __init__(self, parent):
        super(Gunfort, self).__init__(parent=parent, points=255,
                                      gear=[UnitDescription('Tidewall Gunrig', count=3, options=[
                                          Gear('Twin-linked railgun')
                                      ])], static=True)


class DefenseNetwork(Unit):
    type_name = u'Tidewall Defense Network'
    type_id = 'tidewall_network_v3'

    class Choice(OneOf):
        def __init__(self, parent):
            super(DefenseNetwork.Choice, self).__init__(parent, 'Alternatives')
            gunrig = Gunrig(None)
            port = Droneport(None)
            self.gunport = SubUnit(parent, gunrig)
            self.droneport = SubUnit(parent, port)
            self.gun_option = self.variant(gunrig.name, gunrig.base_points, gear=[])
            self.variant(port.name, port.base_points, gear=[])

        def check_rules(self):
            self.gunport.visible = self.gunport.used = self.cur == self.gun_option
            self.droneport.visible = self.droneport.used = self.cur != self.gun_option

        @property
        def points(self):
            return 0

    def __init__(self, parent):
        super(DefenseNetwork, self).__init__(parent, points=60 * 4,
                                             gear=[UnitDescription('Tidewall Shieldline', count=4, points=60)])
        self.Choice(self)


class Rampart(Unit):
    type_name = u'Tidewall Rampart'
    type_id = 'tidewall_rampart_v3'

    class SubDroneport(Droneport, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Rampart, self).__init__(parent)
        Count(self, 'Tidewall Shieldline', 2, 99, 60, True,
              gear=UnitDescription('Tidewall Shieldline', 60))
        Count(self, 'Tidewall Gunrig', 1, 2, 85, True,
              gear=UnitDescription('Tidewall Gunrig', 85, options=[Gear('Twin-linked railgun')]))
        UnitList(self, self.SubDroneport, 1, 2)


class TidewallFortifications(Section):
    def __init__(self, *args, **kwargs):
        super(TidewallFortifications, self).__init__(*args, **kwargs)
        UnitType(self, Shieldline)
        UnitType(self, Droneport)
        UnitType(self, Gunrig)
        UnitType(self, Gunfort)
        UnitType(self, DefenseNetwork)
        UnitType(self, Rampart)
