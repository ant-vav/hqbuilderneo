__author__ = 'Ivan Truskov'

from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as EscLords
from builder.games.wh40k.escalation.space_marines import GKLordsOfWar
from hq import *
from elites import *
from troops import *
from fast import *
from heavy import *
from lords import *
from builder.games.wh40k.imperial_armour.dataslates.space_marines import Xiphon
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusSection
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Wh40kKillTeam, PrimaryDetachment,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, Formation, FlyerSection, LordsOfWarSection
from builder.games.wh40k.fortifications import Fort


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        UnitType(self, BrotherCaptain)
        UnitType(self, Stern)
        UnitType(self, Crowe)
        UnitType(self, Voldus)
        UnitType(self, Champion)
        UnitType(self, Librarian)
        self.techmarine = UnitType(self, Techmarine, active=False, slot=0)


class HQ(BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        UnitType(self, PurifierSquad)
        UnitType(self, PaladinSquad)
        UnitType(self, Dreadnought)


class Elites(BaseElites):
    pass


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, StrikeSquad)
        UnitType(self, TerminatorSquad)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, Rhino)
        UnitType(self, Razorback)
        UnitType(self, InterceptorSquad)
        raven = UnitType(self, StormravenGunship)
        raven.visible = False
        UnitType(self, Stormravens)


class FastAttack(BaseFastAttack):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        UnitType(self, Xiphon)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, PurgationSquad)
        UnitType(self, Dreadknight)
        UnitType(self, LandRaider)
        UnitType(self, LandRaiderCrusader)
        UnitType(self, LandRaiderRedeemer)


class HeavySupport(BaseHeavySupport):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        UnitType(self, DeimosVindicator)


class BaseLordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(BaseLordsOfWar, self).__init__(parent)
        UnitType(self, Draigo)


class LordsOfWar(CerastusSection, EscLords, GKLordsOfWar, BaseLordsOfWar):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman)


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [Stormravens])


class GreyKnightsV3Base(Wh40kBase):

    def __init__(self):
        super(GreyKnightsV3Base, self).__init__(
            hq=HQ(parent=self), elites=Elites(parent=self), troops=Troops(parent=self),
            fast=FastAttack(parent=self), heavy=HeavySupport(parent=self),
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )

    def check_rules(self):
        techmarines = len(self.hq.units) - self.hq.techmarine.count
        self.hq.techmarine.active = techmarines > 0
        if techmarines < self.hq.techmarine.count:
            self.error("For each HQ choice you may include one Techmarine.")


class GreyKnightsV3CAD(GreyKnightsV3Base, CombinedArmsDetachment):
    army_id = 'gk_v3_cad'
    army_name = 'Grey Knights (Combined arms detachment)'


class GreyKnightsV3NSF(GreyKnightsV3CAD):
    army_id = 'gk_v3_nsf'
    army_name = 'Grey Knights (Nemesis Strike Force detachment)'

    def __init__(self):
        super(GreyKnightsV3NSF, self).__init__()
        self.elites.min, self.elites.max = (0, 4)
        self.troops.min, self.troops.max = (1, 4)
        self.fast.min, self.fast.max = (0, 2)
        self.heavy.min, self.heavy.max = (0, 2)


class GreyKnightsV3DSF(GreyKnightsV3CAD):
    army_id = 'gk_v3_dsf'
    army_name = 'Grey Knights (DaemonHunter Force detachment)'

    def __init__(self):
        super(GreyKnightsV3Base, self).__init__(
            hq=None, elites=None, troops=Troops(parent=self),
            fast=FastAttack(parent=self), heavy=HeavySupport(parent=self),
            fort=None,
            lords=None
        )
        self.troops.min, self.troops.max = (0, 1)
        self.fast.min, self.fast.max = (0, 1)
        self.heavy.min, self.heavy.max = (0, 1)

    def check_rules(self):
        if len(self.troops.units) + len(self.fast.units) != 1:
            self.error("Compulsory 1 Troops or 1 Fast Attack.")


class GreyKnightsV3AD(GreyKnightsV3Base, AlliedDetachment):
    army_id = 'gk_v3_ally'
    army_name = 'Grey Knights (Allied detachment)'


class GreyKnightsV3PA(GreyKnightsV3Base, PlanetstrikeAttacker):
    army_id = 'gk_v3_pa'
    army_name = 'Grey Knights (Planetstrike attacker detachment)'


class GreyKnightsV3PD(GreyKnightsV3Base, PlanetstrikeDefender):
    army_id = 'gk_v3_pd'
    army_name = 'Grey Knights (Planetstrike defender detachment)'


class GreyKnightsV3SA(GreyKnightsV3Base, SiegeAttacker):
    army_id = 'gk_v3_sa'
    army_name = 'Grey Knights (Siege War attacker detachment)'


class GreyKnightsV3SD(GreyKnightsV3Base, SiegeDefender):
    army_id = 'gk_v3_sd'
    army_name = 'Grey Knights (Siege War defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'gk_v3_asd'
    army_name = 'Grey Knights (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class GreyKnightsV3KillTeam(Wh40kKillTeam):
    army_id = 'gk_v3_kt'
    army_name = 'Grey Knights'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(GreyKnightsV3KillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [StrikeSquad])

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(GreyKnightsV3KillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [PurifierSquad])

    class KTFastAttack(FastSection):
        def __init__(self, parent):
            super(GreyKnightsV3KillTeam.KTFastAttack, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [Rhino, Razorback])
            UnitType(self, InterceptorSquad)

    def __init__(self):
        super(GreyKnightsV3KillTeam, self).__init__(
            self.KTTroops(self), self.KTElites(self),
            self.KTFastAttack(self)
        )


class GreyKnightsBrotherhood(Formation):
    army_id = 'gk_v3_brotherhood'
    army_name = 'Grey Knights Brotherhood'

    def __init__(self):
        super(GreyKnightsBrotherhood, self).__init__()
        UnitType(self, GrandMaster, min_limit=1, max_limit=1)
        UnitType(self, BrotherCaptain, min_limit=1, max_limit=1)
        UnitType(self, Champion, min_limit=1, max_limit=1)
        UnitType(self, StrikeSquad, min_limit=3, max_limit=3)
        UnitType(self, TerminatorSquad, min_limit=3, max_limit=3)
        UnitType(self, InterceptorSquad, min_limit=2, max_limit=2)
        UnitType(self, Dreadnought, min_limit=1, max_limit=1)
        UnitType(self, PurgationSquad, min_limit=2, max_limit=2)
        UnitType(self, Dreadknight, min_limit=1, max_limit=1)


class BulwarkOfPurity(Formation):
    army_id = 'gk_v3_bulwark'
    army_name = 'Bulwark of Purity'

    def __init__(self):
        super(BulwarkOfPurity, self).__init__()
        self.libr = UnitType(self, Librarian, min_limit=1, max_limit=1)
        self.walkers = [
            UnitType(self, PaladinSquad, min_limit=2, max_limit=2),
            UnitType(self, TerminatorSquad, min_limit=2, max_limit=2)
        ]

    def check_rules(self):
        super(BulwarkOfPurity, self).check_rules()
        for ut in self.walkers:
            for un in ut.units:
                if un.transport.any:
                    self.error('No unit in this formation can take a dedicated transport')
                    break


faction = 'Grey Knights'


class GreyKnightsV3(Wh40k7ed, Wh40kImperial):
    army_id = 'gk_v3'
    army_name = 'Grey Knights'
    # development = True
    faction = faction

    def __init__(self):
        super(GreyKnightsV3, self).__init__([GreyKnightsV3NSF, GreyKnightsV3CAD, GreyKnightsV3DSF,
                                             FlierDetachment, GreyKnightsBrotherhood, BulwarkOfPurity])


class GreyKnightsV3Missions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'gk_v3_mis'
    army_name = 'Grey Knights'
    # development = True
    faction = faction

    def __init__(self):
        super(GreyKnightsV3Missions, self).__init__([
            GreyKnightsV3NSF, GreyKnightsV3CAD, GreyKnightsV3DSF,
            GreyKnightsV3PA, GreyKnightsV3PD, GreyKnightsV3SA, GreyKnightsV3SD,
            FlierDetachment, GreyKnightsBrotherhood, BulwarkOfPurity])


detachments = [GreyKnightsV3NSF, GreyKnightsV3CAD, GreyKnightsV3DSF,
               GreyKnightsV3PA, GreyKnightsV3PD, GreyKnightsV3SA, GreyKnightsV3SD,
               FlierDetachment, GreyKnightsV3AD, GreyKnightsBrotherhood, BulwarkOfPurity]


unit_types = [
    BrotherCaptain, Stern, Crowe, Champion, Librarian,
    Techmarine, PurifierSquad, PaladinSquad, Dreadnought, StrikeSquad,
    TerminatorSquad, Rhino, Razorback, InterceptorSquad, Stormravens,
    PurgationSquad, Dreadknight, LandRaider, LandRaiderCrusader,
    LandRaiderRedeemer, Draigo, Voldus
]
