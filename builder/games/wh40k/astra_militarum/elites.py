__author__ = 'Denis Romanow'

from builder.core2 import *
from armory import Transport, Melee, TempestusTransport
from builder.games.wh40k.roster import Unit


class OgrynSquad(Unit):
    type_name = u'Ogryns'
    type_id = 'ogrynsquad_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Ogryns')

    model_gear = [Gear('Flak armour'), Gear('Ripper gun'), Gear('Frag grenades')]

    def __init__(self, parent):
        super(OgrynSquad, self).__init__(parent=parent, points=130 - 40 * 2, gear=UnitDescription(
            'Ogryn Bone \'ead', points=130 - 40 * 2, options=self.model_gear
        ))
        self.ogryns = Count(self, 'Ogryns', min_limit=2, max_limit=9, points=40,
                            gear=UnitDescription('Ogryn', points=40, options=self.model_gear))
        self.transport = Transport(self)

    def get_count(self):
        return self.ogryns.cur + 1

    def build_statistics(self):
        return self.note_transport(super(OgrynSquad, self).build_statistics())


class BullgrynSquad(Unit):
    type_name = u'Bullgryns'
    type_id = 'bullgrynsquad_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Bullgryns')

    model_gear = [Gear('Flak armour'), Gear('Frag grenades')]
    model = UnitDescription('Bullgryn', points=45, options=model_gear)

    class Leader(Unit):
        class Options(OneOf):
            def __init__(self, parent):
                super(BullgrynSquad.Leader.Options, self).__init__(parent, name='Weapon')
                self.variant('Grenadier gauntlet and slabshield', 0, gear=[Gear('Grenadier gauntlet'),
                                                                           Gear('Slabshield')])
                self.variant('Power maul and brute shield', 15, gear=[Gear('Power maul'), Gear('Brute shield')])

        def __init__(self):
            super(BullgrynSquad.Leader, self).__init__(None, name='Bullgryn Bone \'ead', points=145 - 45 * 2,
                                                       gear=BullgrynSquad.model_gear)
            self.Options(self)

    class Bull(Count):
        @property
        def description(self):
            return BullgrynSquad.model.clone().add([Gear('Grenadier gauntlet'), Gear('Slabshield')]).\
                set_count(self.cur - self.parent.spec.cur)

    def __init__(self, parent):
        super(BullgrynSquad, self).__init__(parent=parent)
        SubUnit(self, self.Leader())
        self.ogryns = self.Bull(self, 'Bullgryn', min_limit=2, max_limit=9, points=45)
        self.spec = Count(
            self, 'Power maul and brute shield', 0, 2, 15,
            gear=self.model.clone().add([Gear('Power maul'), Gear('Brute shield')]).add_points(15)
        )
        self.transport = Transport(self)

    def get_count(self):
        return self.ogryns.cur + 1

    def check_rules(self):
        super(BullgrynSquad, self).check_rules()
        self.spec.max = self.ogryns.cur

    def build_statistics(self):
        return self.note_transport(super(BullgrynSquad, self).build_statistics())


class PsykerSquad(Unit):
    type_name = u'Wyrdvane Psykers'
    type_id = 'psykerbattlesquad_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Wyrdvane-Psykers')

    def __init__(self, parent):
        super(PsykerSquad, self).__init__(parent=parent)
        self.psykers = Count(
            self, 'Wyrdvane Psyker', min_limit=5, max_limit=10, points=12,
            gear=UnitDescription('Wyrdvane Psyker', points=12, options=[
                Gear('Flak armour'), Gear('Laspistol'), Gear('Close combat weapon')
            ]))
        self.transport = Transport(self)

    def get_count(self):
        return self.psykers.cur

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(self.note_transport(super(PsykerSquad, self).build_statistics()))


class RatlingSquad(Unit):
    type_name = u'Ratlings'
    type_id = 'ratlingsquad_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Ratlings')

    def __init__(self, parent):
        super(RatlingSquad, self).__init__(parent=parent)
        self.ratlings = Count(
            self, 'Ratlings', min_limit=3, max_limit=10, points=10,
            gear=UnitDescription('Ratling', points=10, options=[Gear('Flak armour'), Gear('Laspistol'),
                                                                Gear('Sniper rifle')]))

    def get_count(self):
        return self.ratlings.cur


class TempestusCommandSquad(Unit):
    type_name = u'Militarum Tempestus Command Squad'
    type_id = 'command_squad_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Militarum-Tempestus-Command-Squad')

    carapace = [Gear('Carapace armour'), Gear('Frag grenades'), Gear('Krak grenades')]

    scion = UnitDescription('Tempestus Scion', options=carapace)

    class TempestorPrime(Unit):

        class Pistol(OneOf):
            def __init__(self, parent, name):
                super(TempestusCommandSquad.TempestorPrime.Pistol, self).__init__(parent, name=name)
                self.variant('Hot-shot laspistol', 0)
                self.variant('Bolt pistol', 0)
                self.variant('Plasma pistol', 15)

        def __init__(self, parent):
            super(TempestusCommandSquad.TempestorPrime, self).__init__(parent, name='Tempestor Prime',
                                                                       gear=TempestusCommandSquad.carapace)
            Melee(self, 'Weapon')
            self.Pistol(self, '')

    class CasterWeapon(OneOf):
        def __init__(self, parent):
            super(TempestusCommandSquad.CasterWeapon, self).__init__(parent, 'Vox-caster operator\'s weapon')
            self.variant('Hot-shot lasgun',
                         gear=[TempestusCommandSquad.scion.clone().add(Gear('Hot-shot lasgun')).add(Gear('Vox-caster'))])
            self.variant('Hot-shot laspistol',
                         gear=[TempestusCommandSquad.scion.clone().add(Gear('Hot-shot laspistol')).add(Gear('Vox-caster'))])

    class Options(OptionsList):
        def __init__(self, parent):
            super(TempestusCommandSquad.Options, self).__init__(parent, name='Options')
            spec = TempestusCommandSquad.scion.clone().add(Gear('Hot-shot lasgun'))
            self.caster = self.variant('Vox-caster', 5, gear=[])
            self.variant('Platoon standard', 10, gear=[spec.clone().add(Gear('Platoon standard'))])
            self.variant('Medi-pack', 15, gear=[spec.clone().add(Gear('Medi-pack'))])
            self.casterweapon = TempestusCommandSquad.CasterWeapon(parent)

        def check_rules(self):
            super(TempestusCommandSquad.Options, self).check_rules()
            self.casterweapon.used = self.casterweapon.visible = self.caster.value

    def __init__(self, parent):
        super(TempestusCommandSquad, self).__init__(parent, points=85)
        SubUnit(self, self.TempestorPrime(None))
        self.opt = self.Options(self)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=1, points=g['points'],
                  gear=TempestusCommandSquad.scion.clone().add(Gear(g['name'])))
            for g in [
                dict(name='Flamer', points=5),
                dict(name='Greanade launcher', points=5),
                dict(name='Hot-shot volley gun', points=10),
                dict(name='Meltagun', points=10),
                dict(name='Plasma gun', points=15),
            ]
        ]
        self.transport = TempestusTransport(self)

    def check_rules(self):
        super(TempestusCommandSquad, self).check_rules()
        Count.norm_counts(0, 4 - self.opt.count, self.spec)

    def build_description(self):
        desc = super(TempestusCommandSquad, self).build_description()
        desc.add(self.scion.clone().add(Gear('Hot-shot lasgun')).set_count(4 - self.opt.count -
                                                                           sum(o.cur for o in self.spec)))
        return desc

    def count_members(self):
        return 5


class Tempestor(Unit):

    class Pistol(OneOf):
        def __init__(self, parent, name):
            super(Tempestor.Pistol, self).__init__(parent, name=name)
            self.variant('Hot-shot laspistol', 0)
            self.variant('Bolt pistol', 0)
            self.variant('Plasma pistol', 15)

    def __init__(self, parent):
        super(Tempestor, self).__init__(parent, name='Tempestor', gear=TempestusCommandSquad.carapace, points=70 - 4 * 12)
        Melee(self, 'Weapon')
        self.Pistol(self, '')


class Scions(ListSubUnit):
    type_name = u'Militarum Tempestus Scions'
    type_id = 'scions_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Militarum-Tempestus-Scions')

    scion = UnitDescription('Tempestus Scion', options=TempestusCommandSquad.carapace, points=12)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Scions.Options, self).__init__(parent, name='Options')
            spec = Scions.scion.clone().add(Gear('Hot-shot lasgun'))
            self.variant('Vox-caster', 5, gear=[spec.clone().add(Gear('Vox-caster'))])

    class Scion(Count):
        @property
        def description(self):
            return Scions.scion.clone().add(Gear('Hot-shot lasgun')).set_count(self.cur - self.parent.opt.count -
                                                                               sum(o.cur for o in self.parent.spec))

    def __init__(self, parent):
        super(Scions, self).__init__(parent)
        SubUnit(self, Tempestor(None))
        self.scions = self.Scion(self, 'Tempestus Scion', 4, 9, points=12)
        self.opt = self.Options(self)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=2, points=g['points'],
                  gear=Scions.scion.clone().add(Gear(g['name'])).add_points(g['points']))
            for g in [
                dict(name='Flamer', points=5),
                dict(name='Greanade launcher', points=5),
                dict(name='Hot-shot volley gun', points=10),
                dict(name='Meltagun', points=10),
                dict(name='Plasma gun', points=15),
            ]
        ]
        self.transport = TempestusTransport(self)

    def check_rules(self):
        super(Scions, self).check_rules()
        Count.norm_counts(0, 2, self.spec)

    def count_members(self):
        return 1 + self.scions.cur

    @ListSubUnit.count_gear
    def has_transport(self):
        return self.transport.any


class MilitarumTempestusPlatoon(Unit):
    type_name = u'Militarum Tempestus Platoon'
    type_id = 'militarum_tempestus_platoon_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Militarum-Tempestus-Platoon')

    class Command(OptionalSubUnit):
        def __init__(self, parent):
            super(MilitarumTempestusPlatoon.Command, self).__init__(parent, name='Command Squad')
            self.squad = SubUnit(self, TempestusCommandSquad(None))

    def __init__(self, parent):
        super(MilitarumTempestusPlatoon, self).__init__(parent)
        self.command = self.Command(self)
        self.scions = UnitList(self, Scions, 1, 3)

    def transport_count(self):
        return sum(u.has_transport() for u in self.scions.units) +\
            (self.command.squad.unit.transport.any if self.command.any else 0)

    def note_transport(self, statistic):
        count = self.transport_count()
        if count:
            statistic['Models'] += count
            statistic['Units'] += count
        return statistic

    def build_statistics(self):
        statistics = {}
        statistics['Units'] = self.scions.count + self.command.count
        statistics['Models'] = (self.command.squad.unit.count_members() if self.command.any else 0) +\
                               sum((u.count_members() * u.count for u in self.scions.units))
        self.note_transport(statistics)
        return statistics
