__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf
from builder.games.wh40k.imperial_knight_v2.lords import MechanicusOption
from builder.games.wh40k.adepta_sororitas_v4.armory import CadianRelicsHoly, CadianWeaponArcana,\
    CadianWeaponHoly


class Ranged(OneOf, MechanicusOption):
    def __init__(self, parent, name='Ranged weapon'):
        super(Ranged, self).__init__(parent, name)
        self.variant('Phosphor blast pistol', 5 * self.freeflag)
        self.variant('Radium pistol', 5 * self.freeflag)
        self.variant('Arc pistol', 10 * self.freeflag)


class Carbine(OneOf):
    def __init__(self, parent, *args):
        super(Carbine, self).__init__(parent, *args)
        self.carbine = self.variant('Radium carbine', 0)


class Rifle(OneOf):
    def __init__(self, parent, *args):
        super(Rifle, self).__init__(parent, *args)
        self.rifle = self.variant('Galvanic rifle', 0)


class Melee(OptionsList, MechanicusOption):
    def __init__(self, parent):
        super(Melee, self).__init__(parent, 'Melee weapon', limit=1)
        self.variant('Taser goad', 10 * self.freeflag)
        self.variant('Power sword', 15 * self.freeflag)
        self.variant('Arc maul', 20 * self.freeflag)


class Special(OneOf, MechanicusOption):
    def __init__(self, parent, name='Special weapon'):
        super(Special, self).__init__(parent, name)
        self.variant('Arc rifle', 15 * self.freeflag)
        self.variant('Transuranic arquebus', 25 * self.freeflag)
        self.variant('Plasma caliver', 30 * self.freeflag)


class SpecialIssue(OptionsList, MechanicusOption):
    def __init__(self, parent):
        super(SpecialIssue, self).__init__(parent, 'Special Issue Wargear')
        self.refr = self.variant('Refractor field', 5 * self.freeflag)
        self.conv = self.variant('Conversion field', 10 * self.freeflag)
        self.variant('Digital weapons', 10 * self.freeflag)

    def check_rules(self):
        self.refr.used = self.refr.visible = not self.conv.value
        self.conv.used = self.conv.visible = not self.refr.value


class Relics(OptionsList, MechanicusOption):
    def __init__(self, parent):
        super(Relics, self).__init__(parent, 'Relics of Mars', limit=1)
        self.variant('Arkhan\'s Divinator', 5 * self.freeflag)
        self.variant('The Omniscient Mask', 20 * self.freeflag)
        self.variant('Pater Radium', 20 * self.freeflag)
        self.variant('The Skull of Elder Nikola', 25 * self.freeflag)


class CadiaRelics(CadianRelicsHoly, Relics):
    pass


class WeaponRelic(MechanicusOption):
    def __init__(self, parent, melee=True):
        super(WeaponRelic, self).__init__(parent)
        if melee:
            self.relic_options = [self.variant('The Phase Taser', 15 * self.freeflag)]
        else:
            self.relic_options = [self.variant('Phosphoenix', 25 * self.freeflag)]

    def get_unique(self):
        if isinstance(self, OneOf):
            if self.cur in self.relic_options:
                return self.description
        if isinstance(self, OptionsList):
            if any(opt.value for opt in self.relic_options):
                return self.description
        return []


class HolyWeaponRelic(CadianWeaponHoly, WeaponRelic):
    def __init__(self, *args, **kwargs):
        super(HolyWeaponRelic, self).__init__(*args, melee=True, **kwargs)
        self.relic_options += [self.worthy_blade]


class ArcanaWeaponRelic(CadianWeaponArcana, WeaponRelic):
    def __init__(self, *args, **kwargs):
        super(ArcanaWeaponRelic, self).__init__(*args, melee=False, **kwargs)
        self.relic_options += [self.qann]


class VehicleEquipment(OptionsList, MechanicusOption):
    def __init__(self, parent):
        super(VehicleEquipment, self).__init__(parent, 'Vehicle equipment')
        self.variant('Cognis heavy stubber', 5 * self.freeflag)
        self.launchers = self.variant('Smoke launchers', 5 * self.freeflag)
        self.variant('Mindscanner probe', 10 * self.freeflag)
        self.variant('Cognis manipulator', 25 * self.freeflag)
