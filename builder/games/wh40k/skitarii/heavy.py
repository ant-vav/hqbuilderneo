__author__ = 'Ivan Truskov'
from builder.core2 import Gear, UnitList, ListSubUnit, OneOf
from armory import VehicleEquipment, MechanicusOption
from builder.games.wh40k.roster import Unit


class Ironstriders(Unit):
    type_name = u'Ironstrider Ballistarii'
    type_id = 'striders_v1'

    class Ironstrider(ListSubUnit):
        class MainWeapon(OneOf, MechanicusOption):
            def __init__(self, parent):
                super(Ironstriders.Ironstrider.MainWeapon, self).__init__(
                    parent, 'Main weapon')
                self.variant('Twin-linked cognis autocannon', 0)
                self.variant('Twin-linked cognis lascannon', 20 * self.freeflag)

        def __init__(self, parent):
            super(Ironstriders.Ironstrider, self).__init__(parent, 'Ironstrider Ballistarius',
                                                   55, gear=[
                                                       Gear('Broad spectrum data-tether'),
                                                       Gear('Searchlight')
                                                   ])
            self.MainWeapon(self)

    def __init__(self, parent):
        super(Ironstriders, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Ironstrider, 1, 6)

    def get_count(self):
        return self.models.count


class Dunecrawlers(Unit):
    type_name = u'Onager Dunecrawlers'
    type_id = 'crawler_v1'

    class Dunecrawler(ListSubUnit):
        class MainWeapon(OneOf, MechanicusOption):
            def __init__(self, parent):
                super(Dunecrawlers.Dunecrawler.MainWeapon, self).__init__(
                    parent, 'Main weapon')
                self.variant('Eradication beamer', 0)
                self.variant('Twin-linked heavy phosphor blaster', 15 * self.freeflag)
                self.variant('Neutron laser and cognis heavy stubber', 25 * self.freeflag,
                             gear=[Gear('Neutron laser'), Gear('Cognis heavy stubber')])
                self.array = self.variant('Icarus array', 35 * self.freeflag)

        def __init__(self, parent):
            super(Dunecrawlers.Dunecrawler, self).__init__(parent, 'Onager Dunecrawler',
                                                   90, gear=[
                                                       Gear('Broad spectrum data-tether'),
                                                       Gear('Emanatus force field'),
                                                       Gear('Searchlight')
                                                   ])
            self.wep = self.MainWeapon(self)
            self.opt = VehicleEquipment(self)

        def check_rules(self):
            super(Dunecrawlers.Dunecrawler, self).check_rules()
            # the scan is blurred here, so this is a wild guess
            self.opt.launchers.visible = self.opt.launchers.used = not self.wep.cur == self.wep.array

    def __init__(self, parent):
        super(Dunecrawlers, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Dunecrawler, 1, 3)

    def get_count(self):
        return self.models.count
