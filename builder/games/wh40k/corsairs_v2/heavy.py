__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf, OptionsList,\
    UnitList, ListSubUnit, UnitDescription, OptionalSubUnit, SubUnit
from hq import CharacterTraits
from transport import Falcon, Venom
from builder.games.wh40k.unit import Unit


class BalestrikeBand(Unit):
    type_name = u'Corsair Balestrike band'
    type_id = 'balestrike_v2'
    imperial_armour = True

    class Corsair(ListSubUnit):
        class Upgrade(OptionsList):
            def __init__(self, parent):
                super(BalestrikeBand.Corsair.Upgrade, self).__init__(parent, 'Upgrade')
                self.variant('Upgrade to Corsair Felarch', 5, gear=[])

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(BalestrikeBand.Corsair.Weapon1, self).__init__(parent, 'Weapon')
                self.variant('Lasblaster')
                self.variant('Dissonance cannon', 10)
                self.variant('Shuriken cannon', 10)
                self.variant('Eldar missile launcher', 25)
                self.variant('Dark lance', 15)
                self.variant('Splinter cannon', 15)

        class Weapon2(OptionsList):
            def __init__(self, parent):
                super(BalestrikeBand.Corsair.Weapon2, self).__init__(parent, 'Felarch weapons', limit=2)
                self.variant('Close combat weapon')
                self.variant('Power weapon', 15)
                self.variant('Venom blade', 10)
                self.variant('Blast pistol', 20)
                self.variant('Dissonance pistol', 15)

        def __init__(self, parent):
            super(BalestrikeBand.Corsair, self).__init__(parent, 'Corsair', 10, [
                Gear('Shadowwave grenades'), Gear('Plasma grenades')
            ])
            self.wep1 = self.Weapon1(self)
            self.up = self.Upgrade(self)
            self.wep2 = self.Weapon2(self)
            self.traits = CharacterTraits(self)

        def check_rules(self):
            super(BalestrikeBand.Corsair, self).check_rules()
            self.wep2.used = self.wep2.visible = self.up.any
            self.traits.used = self.traits.visible = self.up.any
            self.wep1.used = self.wep1.visible = not self.wep2.any

        def build_description(self):
            res = super(BalestrikeBand.Corsair, self).build_description()
            res.add(self.root_unit.options1.description)\
               .add_points(self.root_unit.options1.points)
            return res

        @ListSubUnit.count_gear
        def is_felarch(self):
            return self.up.any

    class Options1(OptionsList):
        def __init__(self, parent):
            super(BalestrikeBand.Options1, self).__init__(parent, 'Options')
            self.packs = self.variant('Corsair jet pack', 5, per_model=True)
            self.armour = self.variant('Heavy mesh armour', 5, per_model=True)

    class Options2(OptionsList):
        def __init__(self, parent, name=''):
            super(BalestrikeBand.Options2, self).__init__(parent, name)
            self.variant('Haywire grenades', 25)
            self.variant('Tanglefield grenades', 10)
            self.variant('Void hardened armour', 10)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(BalestrikeBand.Transport, self).__init__(parent, 'Transport', limit=1)
            self.venom = SubUnit(self, Venom(parent=None))
            self.falcon = SubUnit(self, Falcon(parent=None))

        def check_rules(self):
            super(BalestrikeBand.Transport, self).check_rules()
            self.used = self.visible = not self.parent.options1.packs.value
            if self.venom.visible and self.parent.count > 5:
                self.parent.error("Corsair Balestrike Band must be no more then 5 in number to take a Venom")
            if self.falcon.visible and self.parent.count <= 5:
                self.parent.error("Corsair Balestrike Band must be more then 5 in number to take a Falcon")

    def __init__(self, parent):
        super(BalestrikeBand, self).__init__(parent)
        self.options1 = self.Options1(self)
        self.options2 = self.Options2(self)
        self.models = UnitList(self, self.Corsair, 3, 10)
        self.transport = self.Transport(self)

    def build_points(self):
        return super(BalestrikeBand, self).build_points()\
            + self.options1.points * (self.count - 1)

    def get_count(self):
        return self.models.count

    def check_rules(self):
        super(BalestrikeBand, self).check_rules()
        felarch_count = sum(u.is_felarch() for u in self.models.units)
        if felarch_count > 1:
            self.error('No more than 3 Corsair may be upgraded to Felarch')

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        res.add(self.options2.description)
        res.add(self.models.description)
        res.add(self.transport.description)
        return res

    def build_statistics(self):
        return self.note_transport(super(BalestrikeBand, self).build_statistics())


class FireStorms(Unit):
    type_name = "Corsair Fire Storms"
    type_id = "firestorms_v2"

    class FireStorm(ListSubUnit):
        type_name = "Fire Storm"
        type_id = "firestorm_v2"

        class Weapon(OneOf):
            def __init__(self, parent):
                super(FireStorms.FireStorm.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Twin-linked shuriken catapult', 0)
                self.variant('Shuriken cannon', 10)
                self.variant('Splinter cannon', 10)

        def __init__(self, parent):
            super(FireStorms.FireStorm, self).__init__(parent=parent, points=150, gear=[
                Gear('Fire Storm scatter laser'),
            ], )
            self.wep = self.Weapon(self)

        def build_description(self):
            desc = super(FireStorms.FireStorm, self).build_description()
            desc.add(self.root_unit.opt.description).add_points(self.root_unit.opt.points)
            return desc

    class Options(OptionsList):
        def __init__(self, parent):
            super(FireStorms.Options, self).__init__(parent, 'Options')
            self.variant('Corsair kinetic shroud', 15, per_model=True)
            self.variant('Corsair void burners', 5, per_model=True)
            self.variant('Star engines', 15, per_model=True)

    def __init__(self, parent):
        super(FireStorms, self).__init__(parent)
        self.models = UnitList(self, self.FireStorm, 1, 3)
        self.opt = self.Options(self)

    def build_points(self):
        return super(FireStorms, self).build_points()\
            + self.opt.points * (self.models.count - 1)

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        res.add(self.models.description)
        return res

    def get_count(self):
        return self.models.count


class FirePrisms(Unit):
    type_name = "Corsair Fire Prisms"
    type_id = "fireprisms_v2"

    class FirePrism(ListSubUnit):
        type_name = "Fire Prism"
        type_id = "fireprism_v2"

        def __init__(self, parent):
            super(FirePrisms.FirePrism, self).__init__(parent=parent, points=125, gear=[
                Gear('Prism cannon')
            ], )
            self.wep = FireStorms.FireStorm.Weapon(self)

        def build_description(self):
            desc = super(FirePrisms.FirePrism, self).build_description()
            desc.add(self.root_unit.opt.description).add_points(self.root_unit.opt.points)
            return desc

    def __init__(self, parent):
        super(FirePrisms, self).__init__(parent)
        self.models = UnitList(self, self.FirePrism, 1, 3)
        self.opt = FireStorms.Options(self)

    def build_points(self):
        return super(FirePrisms, self).build_points()\
            + self.opt.points * (self.models.count - 1)

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        res.add(self.models.description)
        return res

    def get_count(self):
        return self.models.count


class WarpHunters(Unit):
    type_name = "Corsair Warp Hunter Squadron"
    type_id = "warphunters_v2"

    class WarpHunter(ListSubUnit):
        type_name = "Warp Hunter"
        type_id = "warphunter_v2"

        def __init__(self, parent):
            super(WarpHunters.WarpHunter, self).__init__(parent=parent, points=185, gear=[
                Gear('D-Flail')
            ], )
            self.wep = FireStorms.FireStorm.Weapon(self)

        def build_description(self):
            desc = super(WarpHunters.WarpHunter, self).build_description()
            desc.add(self.root_unit.opt.description).add_points(self.root_unit.opt.points)
            return desc

    def __init__(self, parent):
        super(WarpHunters, self).__init__(parent)
        self.models = UnitList(self, self.WarpHunter, 1, 3)
        self.opt = FireStorms.Options(self)

    def build_points(self):
        return super(WarpHunters, self).build_points()\
            + self.opt.points * (self.models.count - 1)

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        res.add(self.models.description)
        return res

    def get_count(self):
        return self.models.count


class NightSpinners(Unit):
    type_name = "Corsair Night Spinners"
    type_id = "nightspinners_v2"

    class NightSpinner(ListSubUnit):
        type_name = "Night Spinner"
        type_id = "nightspinner_v2"

        def __init__(self, parent):
            super(NightSpinners.NightSpinner, self).__init__(parent=parent, points=100, gear=[
                Gear('Doomweaver')
            ], )
            self.wep = FireStorms.FireStorm.Weapon(self)

        def build_description(self):
            desc = super(NightSpinners.NightSpinner, self).build_description()
            desc.add(self.root_unit.opt.description).add_points(self.root_unit.opt.points)
            return desc

    def __init__(self, parent):
        super(NightSpinners, self).__init__(parent)
        self.models = UnitList(self, self.NightSpinner, 1, 3)
        self.opt = FireStorms.Options(self)

    def build_points(self):
        return super(NightSpinners, self).build_points()\
            + self.opt.points * (self.models.count - 1)

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        res.add(self.models.description)
        return res

    def get_count(self):
        return self.models.count


class Lynx(Unit):
    type_name = u'Corsair Lynx'
    type_id = 'lynx_v2'
    imperial_armour = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Lynx.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Lynx pulsar')
            self.variant('Sonic lance')

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Lynx.Weapon2, self).__init__(
                parent, '')
            self.variant('Shuriken cannon')
            self.variant('Splinter cannon')
            self.variant('Scatter laser')
            self.variant('Starcannon', 5)
            self.variant('Bright lance', 5)
            self.variant('Dark lance', 5)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Lynx.Options, self).__init__(parent, 'Options')
            self.variant('Corsair kinetic shroud', 15)
            self.variant('Star engines', 15)

    def __init__(self, parent):
        super(Lynx, self).__init__(parent, 'Lynx', 255)
        self.Weapon1(self)
        self.Weapon2(self)
        self.Options(self)
