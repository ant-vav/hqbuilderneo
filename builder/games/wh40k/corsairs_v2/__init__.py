__author__ = 'Ivan Truskov'
__summary__ = 'Imperial Armour volume 11 Doom of Mymeara 2nd edition'


class FalsePrince(object):
    def is_druglord(self):
        return False

    def is_keybearer(self):
        return False

    def is_junk_collector(self):
        return False

    def is_ancient(self):
        return False


from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Roster,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, Detachment, LordsOfWarSection
from builder.games.wh40k.fortifications import Fort

from hq import Prince, Dreamer, Baron
from troops import CloudDancerBand, GhostwalkerBand, ReaverBand
from elites import MalevolentBand, VoidstormBand, WaspSquadron
from fast import VyperSquadron, Nightwing, Phoenix, HornetSquadron
from heavy import BalestrikeBand, FireStorms, FirePrisms, WarpHunters,\
    NightSpinners, Lynx
from lords import Manpire


class Hq(HQSection):
    def __init__(self, parent):
        super(Hq, self).__init__(parent)
        self.prince = UnitType(self, Prince)
        self.dreamer = UnitType(self, Dreamer)
        UnitType(self, Baron)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, MalevolentBand)
        UnitType(self, VoidstormBand)
        UnitType(self, WaspSquadron)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, CloudDancerBand)
        UnitType(self, GhostwalkerBand)
        UnitType(self, ReaverBand)


class FastAttack(FastSection):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        UnitType(self, VyperSquadron)
        UnitType(self, Nightwing)
        UnitType(self, Phoenix)
        UnitType(self, HornetSquadron)


class HeavySupport(HeavySection):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        UnitType(self, BalestrikeBand)
        UnitType(self, FireStorms)
        UnitType(self, FirePrisms)
        self.wh = UnitType(self, WarpHunters)
        UnitType(self, NightSpinners)
        self.lynx = UnitType(self, Lynx)


class LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        UnitType(self, Manpire)


class CorsairRoster(Roster):
    def check_rules_chain(self):
        self.find_prince()
        super(CorsairRoster, self).check_rules_chain()

    def find_prince(self):
        self.prince = FalsePrince()


class CorsairV2Base(CorsairRoster, Wh40kBase):
    imperial_armour = True

    def __init__(self):
        self.hq = Hq(self)
        self.heavy = HeavySupport(self)
        super(CorsairV2Base, self).__init__(
            hq=self.hq, elites=Elites(self), troops=Troops(self),
            fast=FastAttack(self), heavy=self.heavy, fort=Fort(self),
            lords=LordsOfWar(self)
        )

    def is_coven(self):
        return False

    def find_prince(self):
        if self.hq.prince.count != 1:
            self.prince = FalsePrince()
        else:
            self.prince = self.hq.prince.units[0]

    def check_rules(self):
        super(CorsairV2Base, self).check_rules()
        if self.hq.prince.count != 1:
            self.error('There must be one and only one Prince in the detachment')
        if self.hq.dreamer.count > 1:
            self.error('There is a maximum of one Void Dreamer in detachment')
        if self.heavy.wh.count > 1:
            self.error('There is a maximum of one Warp hunter squadron in detachment')
        if self.heavy.lynx.count > 1:
            self.error('There is a maximum of one Lynx in detachment')


class CorsairCoterie(CorsairRoster, Wh40kBase):
    army_name = 'Corsair Coterie'
    army_id = 'corsair_v2_coterie'

    def is_coven(self):
        return False

    def __init__(self):
        self.hq = Hq(self)
        self.elites = Elites(self)
        self.troops = Troops(self)
        self.fast = FastAttack(self)
        self.heavy = HeavySupport(self)
        super(CorsairCoterie, self).__init__(
            sections=[self.hq, self.elites, self.troops,
                      self.fast, self.heavy])
        self.hq.min, self.hq.max = (1, 1)
        self.troops.min, self.troops.max = (1, 3)
        self.fast.max = 2
        self.elites.max = 2
        self.heavy.max = 1

    def find_prince(self):
        self.prince = self.parent.roster.prince


class CorsairRaidingCompany(CorsairRoster, Wh40k7ed):
    army_id = 'corsair_v2_raiding_company'
    army_name = 'Corsair Fleet Raiding Company'
    imperial_armour = True

    def is_coven(self):
        return False

    class Coteries(Detachment):
        def __init__(self, parent):
            super(CorsairRaidingCompany.Coteries, self).__init__(
                parent, 'cot', 'Coteries', [CorsairCoterie], 1, 4)

    def __init__(self):
        self.hq = Hq(self)
        self.cot = self.Coteries(self)
        elites = Elites(self)
        elites.max = 1

        super(Wh40k7ed, self).__init__(
            [self.hq, elites, LordsOfWar(self), self.cot])
        self.extra_checks = dict()

    def find_prince(self):
        all_princes = []
        all_princes.extend(self.hq.prince.units)
        for coterie in self.cot.units:
            all_princes.extend(coterie.sub_roster.roster.hq.prince.units)
        if len(all_princes) > 0:
            self.prince = all_princes[0]
        else:
            self.prince = FalsePrince()

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        all_princes = []
        all_princes.extend(self.hq.prince.units)
        all_dreamers = []
        all_dreamers.extend(self.hq.dreamer.units)
        all_hunters = []
        all_lynx = []
        for coterie in self.cot.units:
            all_princes.extend(coterie.sub_roster.roster.hq.prince.units)
            all_dreamers.extend(coterie.sub_roster.roster.hq.dreamer.units)
            all_hunters.extend(coterie.sub_roster.roster.heavy.wh.units)
            all_lynx.extend(coterie.sub_roster.roster.heavy.lynx.units)
        if len(all_princes) != 1:
            self.error('There must be one and only one Prince in the detachment')
        if len(all_dreamers) > 1:
            self.error('There is a maximum of one Void Dreamer in detachment')
        if len(all_hunters) > 1:
            self.error('There is a maximum of one Warp hunter squadron in detachment')
        if len(all_lynx) > 1:
            self.error('There is a maximum of one Lynx in detachment')


class CorsairV2CAD(CorsairV2Base, CombinedArmsDetachment):
    army_id = 'corsair_v2_cad'
    army_name = 'Corsairs (Combined arms detachment)'


class CorsairV2AD(CorsairV2Base, AlliedDetachment):
    army_id = 'corsair_v2_ad'
    army_name = 'Corsairs (Allied detachment)'


class CorsairV2PA(CorsairV2Base, PlanetstrikeAttacker):
    army_id = 'corsair_v2_pa'
    army_name = 'Corsairs (Planetstrike attacker detachment)'


class CorsairV2PD(CorsairV2Base, PlanetstrikeDefender):
    army_id = 'corsair_v2_pd'
    army_name = 'Corsairs (Planetstrike defender detachment)'


class CorsairV2SA(CorsairV2Base, SiegeAttacker):
    army_id = 'corsair_v2_sa'
    army_name = 'Corsairs (Siege War attacker detachment)'


class CorsairV2SD(CorsairV2Base, SiegeDefender):
    army_id = 'corsair_v2_sd'
    army_name = 'Corsairs (Siege War defender detachment)'


faction = 'Corsairs'


class CorsairV2(Wh40k7ed):
    army_id = 'corsair_v2'
    army_name = 'Corsairs'
    faction = faction
    imperial_armour = True

    @property
    def imperial_armour_on(self):
        return self.imperial_armour

    def __init__(self):
        super(CorsairV2, self).__init__([CorsairV2CAD, CorsairRaidingCompany], [])


class CorsairV2Missions(Wh40k7edMissions):
    army_id = 'corsair_v2_mis'
    army_name = 'Corsairs'
    faction = faction
    imperial_armour = True

    @property
    def imperial_armour_on(self):
        return self.imperial_armour

    def __init__(self):
        super(CorsairV2Missions, self).__init__([CorsairV2CAD, CorsairV2PA, CorsairV2PD, CorsairV2SA, CorsairV2SD,
                                                 CorsairRaidingCompany], [])


detachments = [
    CorsairV2CAD,
    CorsairV2AD,
    CorsairV2PA,
    CorsairV2PD,
    CorsairV2SA,
    CorsairV2SD,
    CorsairRaidingCompany
]

from builder.games.wh40k.necrons_v3 import faction as fac_necr
from builder.games.wh40k.tyranids_v2 import faction as fac_tyr
from builder.games.wh40k.chaos_daemons_v3 import faction as fac_daem
allies_exclusion = [fac_necr, fac_tyr, fac_daem]
