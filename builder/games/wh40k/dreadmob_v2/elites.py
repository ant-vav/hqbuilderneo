__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear,\
    Count, SubUnit, OptionalSubUnit, ListSubUnit, UnitList,\
    UnitDescription
from builder.games.wh40k.orks_v3.elites import BurnaBoyz as CodexBurnas
from troops import ScrapTrukk
from builder.games.wh40k.roster import Unit


class SlashaMob(Unit):
    type_name = u'Cybork Slasha Mob'
    type_id = 'slasha_v2'

    class Painboy(Unit):
        class Options(OneOf):
            def __init__(self, parent):
                super(SlashaMob.Painboy.Options, self).__init__(parent, 'Options')
                self.variant('Bosspole', 5)

        def __init__(self, parent, mob_options):
            super(SlashaMob.Painboy, self).__init__(parent, 'Cybork painboy',
                                                    50, gear=[
                                                        Gear('\'Urty Siringe'),
                                                        Gear('Doc\'s Tools'),
                                                        Gear('Cybork body')])
            self.mob_options = mob_options
            self.opt = self.Options(self)
            self.help = Count(self, 'Grot Orderly', 0, 2, 5)

        def build_description(self):
            desc = super(SlashaMob.Painboy, self).build_description()
            if self.mob_options.bombs:
                desc.add(Gear('Stikkbombz')).add_points(1)
            if self.mob_options.armour:
                desc.add(Gear('\'Eavy armour')).add_points(5)
            return desc

    class Cybork(ListSubUnit):
        class Melee(OneOf):
            def __init__(self, parent):
                super(SlashaMob.Cybork.Melee, self).__init__(parent, 'Melee weapon')
                self.variant('Choppa', 0)
                self.variant('Big choppa', 5)
                self.variant('Power Klaw', 25)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(SlashaMob.Cybork.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant('Slugga', 0)
                self.variant('Twin-linked Shoota', 5)
                self.variant('Shoota/rokkit', 10)
                self.variant('Shoota/scorcha', 10)

        def __init__(self, parent):
            super(SlashaMob.Cybork, self).__init__(parent, 'Cybork Slasha', 30,
                                                   gear=[Gear('Cybork body')])
            self.Melee(self)
            self.Ranged(self)

        def build_description(self):
            desc = super(SlashaMob.Cybork, self).build_description()
            if self.root_unit.mob_options.bombs:
                desc.add(Gear('Stikkbombz')).add_points(1)
            if self.root_unit.mob_options.armour:
                desc.add(Gear('\'Eavy armour')).add_points(5)
            return desc

    class MobOptions(OptionsList):
        def __init__(self, parent):
            super(SlashaMob.MobOptions, self).__init__(parent, 'Mob options')
            self.bombs = self.variant('Stikkbombz', 1, per_model=True)
            self.armour = self.variant('\'Eavy armour', 5, per_model=True)

        @property
        def points(self):
            return super(SlashaMob.MobOptions, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(SlashaMob, self).__init__(parent)
        self.mob_options = self.MobOptions(self)
        self.units = UnitList(self, self.Cybork, 4, 9)
        self.boss = SubUnit(self, self.Painboy(parent=None, mob_options=self.mob_options))

    def get_count(self):
        return 1 + self.units.count


class Junka(Unit):
    type_name = "Mekboy Junka"
    type_id = 'junka_v2'
    imperial_armour = True

    class FreeOptions(OneOf):
        def __init__(self, parent):
            super(Junka.FreeOptions, self).__init__(parent, 'Options')
            self.variant("Reinforced Ram", 0)
            self.variant("Deff Rolla", 0)
            self.variant("Wreckin\' Ball", 0)
            self.variant('Grabbin\' Klaw', 0)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Junka.Options, self).__init__(parent, '')
            self.variant("'ard case", 15)
            self.variant("'Eavy plates", 20)

    class Weapon(OneOf):
        def __init__(self, parent, number):
            super(Junka.Weapon, self).__init__(parent, 'Weapon {}'.format(number))
            self.variant('Big shoota', 0)
            self.variant('Scorcha', 0)
            self.variant('Rokkit launcha', 5)
            self.variant('Twin-linked big shoota', 10)
            self.variant('Twin-linked rokkit launcha', 15)
            self.variant('Kustom mega blasta', 15)

    class Upgrades(OptionsList):
        def __init__(self, parent):
            super(Junka.Upgrades, self).__init__(parent, 'Upgrades', limit=1)
            self.variant('Supa-scorcha', 20)
            self.variant('Big zzappa', 30)
            self.variant('2 Grot bomms', 30, gear=[Gear('Grot bomm', count=2)])
            self.variant('Junka force field generator', 75)
            self.variant('Junka shokk attack gun', 100)
            self.variant('Boomgun', 70)

    def __init__(self, parent):
        super(Junka, self).__init__(parent, points=65,
                                    gear=[Gear('Grot Riggers'), Gear('Turbo-charga')])

        self.dev = self.FreeOptions(self)
        self.opt = self.Options(self)
        self.wep = [self.Weapon(self, i + 1) for i in range(0, 3)]
        self.spec = self.Upgrades(self)


class BurnaBoyz(CodexBurnas):
    model_points = 15
    model_description = UnitDescription(name='Burna Boyz', points=model_points, options=[Gear('Burna'), Gear('Stikkbombs')])

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(BurnaBoyz.Transport, self).__init__(parent, 'Transport')
            self.trukk = SubUnit(self, ScrapTrukk(parent=None))

    def get_transport_class(self):
        return BurnaBoyz.Transport

    def check_rules(self):
        super(BurnaBoyz, self).check_rules()
        if self.get_count() > 12 and self.transport.any:
            self.error("Cannot take trukk for mobs larger then 12")
