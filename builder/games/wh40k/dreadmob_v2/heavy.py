__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear,\
    Count, SubUnit, OptionalSubUnit
from builder.games.wh40k.orks_v3.fast import BurnaBommer as CodexBurna,\
    BlitzaBommer as CodexBlitza
from builder.games.wh40k.orks_v3.heavy import LootedWagon as CodexWagon,\
    Lootas as CodexLootas
from builder.games.wh40k.roster import Unit


class Lifta(Unit):
    type_name = "Lifta Wagon"
    type_id = 'lifta_v2'
    base_points = 225

    class Options(OptionsList):
        def __init__(self, parent):
            super(Lifta.Options, self).__init__(parent, 'Options')
            self.rolla = self.variant('Deff rolla', 20)
            self.variant("Red paint job", 5)
            self.variant("Grot riggers", 5)
            self.variant("Stikkbomb chukka", 5)
            self.variant('Armour plates', 10)
            self.variant("Boarding plank", 5)
            self.variant("Wreckin' ball", 10)
            self.variant("Grabbin' klaw", 5)
            self.ram = self.variant("Reinforces ram", 5)

        def check_rules(self):
            self.process_limit([self.rolla, self.ram], 1)

    def __init__(self, parent):
        super(Lifta, self).__init__(parent, points=Lifta.base_points,
                                    gear=[Gear('Lifta-Droppa')])

        self.bs = Count(self, "Big shoota", 0, 2, 5)
        self.rl = Count(self, "Rokkit launcha", 0, 2, 10)
        self.Options(self)

    def check_rules(self):
        Count.norm_counts(0, 2, [self.bs, self.rl])


class BigTrakk(Unit):
    type_name = "Big Trakk"
    type_id = 'big_trakk_v2'
    base_points = 50

    class PintleWeapon(OneOf):
        def __init__(self, parent, number):
            super(BigTrakk.PintleWeapon, self).__init__(
                parent, 'Pintle weapon {}'.format(number))
            self.variant('Big shoota', 0)
            self.variant('Skorcha', 5)
            self.variant('Rokkit launcha', 10)

    class PintleWeapon2(OptionsList):
        def __init__(self, parent, number):
            super(BigTrakk.PintleWeapon2, self).__init__(
                parent, 'Pintle weapon {}'.format(number))
            self.variant('Big shoota', 5)
            self.variant('Skorcha', 10)
            self.variant('Rokkit launcha', 15)

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(BigTrakk.Weapon, self).__init__(
                parent, 'Main weapon', limit=1)
            self.variant('Kannon', 10)
            self.variant('Lobba', 10)
            self.variant("Zzap gun", 15)
            self.variant('Supa-scorcha', 10)
            self.variant('Big lobba', 20)
            self.variant('Killkannon', 45)
            self.variant('Big-zzappa', 30)
            self.variant('Flakka-gunz', 40)
            self.variant('Supa-kannon', 70)

    class Options1(OptionsList):
        def __init__(self, parent):
            super(BigTrakk.Options1, self).__init__(parent, 'Options')
            self.variant("Boarding plank", 5)
            self.variant('\'Ard case', 10)
            self.variant("Stikkbomb chukka", 5)
            self.variant("Red paint job", 5)
            self.variant("Grot riggers", 5)

    class Options2(OptionsList):
        def __init__(self, parent):
            super(BigTrakk.Options2, self).__init__(parent, '', limit=1)
            self.variant("Deff rolla", 10)
            self.variant("Reinforced ram", 10)
            self.variant("Wreckin' ball", 10)
            self.variant("Grabbin' klaw", 10)

    def __init__(self, parent):
        super(BigTrakk, self).__init__(parent, points=50,
                                       gear=[Gear('Armour plates')])
        self.PintleWeapon(self, 1)
        self.PintleWeapon(self, 2)
        self.PintleWeapon2(self, 3)
        self.PintleWeapon2(self, 4)
        self.Weapon(self)
        self.Options1(self)
        self.Options2(self)
        Count(self, 'Grot sponsons', 0, 2, 5)


class MegaDread(Unit):
    type_name = u'Mega-dread'
    type_id = 'mega_dread_v2'
    base_points = 175

    class WeaponKannon(OneOf):
        def __init__(self, parent):
            super(MegaDread.WeaponKannon, self).__init__(parent, 'Ranged weapon')
            self.variant('Killkannon', 0)
            self.variant('Supa-scorcha', 0)
            self.variant('Rippa klaw', 0)

    class WeaponKlaw(OneOf):
        def __init__(self, parent):
            super(MegaDread.WeaponKlaw, self).__init__(parent, 'Close combat weapon')
            self.variant('Rippa klaw', 0)
            self.variant('Kill saw', 0)
            self.variant('Supa-skorcha', 0)
            self.variant('Killkannon', 35)

    class Options(OptionsList):
        def __init__(self, parent):
            super(MegaDread.Options, self).__init__(parent, 'Options')
            self.variant('Big shoota', 10)
            self.variant("Grot riggers", 5)
            self.variant("Mega-charga", 15)

    class SecondaryWeapon(OneOf):
        def __init__(self, parent, name='Secondary weapon'):
            super(MegaDread.SecondaryWeapon, self).__init__(parent, name=name)
            self.variant("Big Shoota", 0)
            self.variant("Scorcha", 0)
            self.variant("Rokkit Launcha", 5)
            self.variant("Kustom mega-blasta", 10)

    def __init__(self, parent):
        super(MegaDread, self).__init__(parent, points=MegaDread.base_points,
                                        gear=[Gear('Armour plates')])
        self.WeaponKannon(self)
        self.WeaponKlaw(self)
        self.SecondaryWeapon(self)
        self.SecondaryWeapon(self, '')
        self.Options(self)


class BurnaBommer(CodexBurna):
    base_points = 125


class BlitzaBommer(CodexBlitza):
    base_points = 135


class LootedWagon(CodexWagon):
    base_points = 35


class Lootas(CodexLootas):
    base_points = 5

    class LootedTransport(OptionalSubUnit):
        def __init__(self, parent):
            super(Lootas.LootedTransport, self).__init__(parent, 'Transport')
            SubUnit(self, LootedWagon(parent=self))

    def get_transport(self):
        return Lootas.LootedTransport
