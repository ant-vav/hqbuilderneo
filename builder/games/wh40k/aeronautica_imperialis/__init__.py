from hq import *
from fast import *
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    PlanetstrikeAttacker, PlanetstrikeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, FastSection, FlyerSection, UnitType


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        UnitType(self, OfficerFleet)


class FastAttack(FastSection):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        UnitType(self, Valkyries)


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [Valkyries])


class NavyV1Base(Wh40kBase):
    def __init__(self):
        super(NavyV1Base, self).__init__(
            elites=None, troops=None, heavy=None,
            fort=None,
            lords=None,
            hq=HQ(parent=self),
            fast=FastAttack(parent=self)
        )


class NavyV1Section(NavyV1Base, CombinedArmsDetachment):
    army_id = 'navy_v1_section'
    army_name = 'Imperial Navy Section'

    def __init__(self):
        super(NavyV1Section, self).__init__()
        self.hq.min, self.hq.max = (0, 1)
        self.fast.min, self.fast.max = (1, 1)


class NavyV1PA(NavyV1Base, PlanetstrikeAttacker):
    army_id = 'navy_v1_pa'
    army_name = 'Imperial Navy (Planetstrike attacker detachment)'


class NavyV1PD(NavyV1Base, PlanetstrikeDefender):
    army_id = 'navy_v1_pd'
    army_name = 'Imperial Navy (Planetstrike defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'navy_v1_asd'
    army_name = 'Imperial Navy (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


faction = 'Imperial Navy'


class NavyV1(Wh40k7ed):
    army_id = 'navy_v1'
    army_name = 'Imperial Navy'
    faction = faction

    def __init__(self):
        super(NavyV1, self).__init__([NavyV1Section, FlierDetachment])


class NavyV1Missions(Wh40k7edMissions):
    army_id = 'navy_v1_mis'
    army_name = 'Imperial Navy'
    # development = True
    faction = faction

    def __init__(self):
        super(NavyV1Missions, self).__init__([NavyV1Section, FlierDetachment,
                                              NavyV1PA, NavyV1PD])


detachments = [NavyV1Section, FlierDetachment, NavyV1PA, NavyV1PD]

unit_types = [OfficerFleet, Valkyries]
