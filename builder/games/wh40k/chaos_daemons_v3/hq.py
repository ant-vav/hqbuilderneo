__author__ = 'Ivan Truskov'

from builder.core2 import Count, Gear, OneOf, OptionsList,\
    UnitDescription
from builder.games.wh40k.roster import Unit
from options import KhorneArtifacts, TzeentchArtifacts,\
    NurgleArtifacts, SlaaneshArtifacts


class GiftBearer(Unit):
    gift_limit = 50

    def __init__(self, parent, *args, **kwargs):
        super(GiftBearer, self).__init__(parent, *args, **kwargs)
        self.les = Count(self, "Lesser Rewards", 0, 5, 10, order=50)
        self.grt = Count(self, "Greater Rewards", 0, 2, 20, order=51)
        self.exlt = Count(self, "Exalted Rewards", 0, 1, 30, order=52)

    def check_rules(self):
        super(GiftBearer, self).check_rules()
        Count.norm_points(self.gift_limit, [self.les, self.grt, self.exlt])


class FuryThirster(GiftBearer):
    type_name = u'Bloodthirster of Unfettered Fury'
    type_id = 'bloodthirster_fury_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Bloodthirster-of-Unfettered-Fury')

    def __init__(self, parent):
        super(FuryThirster, self).__init__(parent, self.type_name, 250, gear=[
            Gear('Warp-forged armour'),
            Gear('Lash of Khorne')
        ])
        self.art = KhorneArtifacts(self, thirster=True)

    def build_description(self):
        res = super(FuryThirster, self).build_description()
        if not self.art.replaces_weapon():
            res.add(Gear('Axe of Khorne'))
        return res

    def get_unique_gear(self):
        return self.art.description if self.art.any else []


class RageThirster(GiftBearer):
    type_name = u'Bloodthirster of Insensate Rage'
    type_id = 'bloodthirster_rage_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Bloodthirster-of-Insensate-Rage')

    def __init__(self, parent):
        super(RageThirster, self).__init__(parent, self.type_name, 275, gear=[
            Gear('Warp-forged armour')
        ])
        self.art = KhorneArtifacts(self, thirster=True)

    def build_description(self):
        res = super(RageThirster, self).build_description()
        if not self.art.replaces_weapon():
            res.add(Gear('Great axe of Khorne'))
        return res

    def get_unique_gear(self):
        return self.art.description if self.art.any else []


class WrathThirster(GiftBearer):
    type_name = u'Wrath of Khorne Bloodthirster'
    type_id = 'bloodthirster_wrath_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Wrath-of-Khorne-Bloodthirster')

    def __init__(self, parent):
        super(WrathThirster, self).__init__(parent, self.type_name, 300, gear=[
            Gear('Warp-forged armour'),
            Gear('Hellfire'),
            Gear('Bloodlflail')
        ])
        self.art = KhorneArtifacts(self, thirster=True)

    def build_description(self):
        res = super(WrathThirster, self).build_description()
        if not self.art.replaces_weapon():
            res.add(Gear('Axe of Khorne'))
        return res

    def get_unique_gear(self):
        return self.art.description if self.art.any else []


class Scarbrand(Unit):
    type_name = u'Skarbrand, The Exiled One'
    type_id = 'skarbrand_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Skarbrand')

    def __init__(self, parent):
        super(Scarbrand, self).__init__(parent, 'Skarbrand', 225, gear=[
            Gear('Warp-forged Armour'), Gear('Slaughter and Carnage')
        ], unique=True, static=True)


class Fateweaver(Unit):
    type_name = "Kairos Fateweaver"
    type_id = 'kairos_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Kairos-Fateweaver')

    def __init__(self, parent):
        super(Fateweaver, self).__init__(parent, points=300, gear=[
            Gear('Staff of Tomorrow')
        ], unique=True, static=True)

    def count_charges(self):
        return 4

    def build_statistics(self):
        return self.note_charges(super(Fateweaver, self).build_statistics())


class Kugath(Unit):
    type_name = u'Ku\'gath Plaguefather'
    type_id = 'kugath_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Ku-gath-Plaguefather')

    def __init__(self, parent):
        super(Kugath, self).__init__(parent, points=260, gear=[
            Gear('Necrotic Missiles')
        ], unique=True, static=True)

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(Kugath, self).build_statistics())


class LordOfChange(GiftBearer):
    type_name = u'Lord of Change'
    type_id = 'loc_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Lord-of-Change')

    class Mastery(OneOf):
        def __init__(self, parent):
            super(LordOfChange.Mastery, self).__init__(parent, 'Mastery')
            self.lvl2 = self.variant('Mastery level 2')
            self.lvl3 = self.variant('Mastery level 3', 25)

        def count_charges(self):
            if self.cur == self.lvl2:
                return 2
            else:
                return 3

    def __init__(self, parent):
        super(LordOfChange, self).__init__(parent, points=230)
        self.mastery = self.Mastery(self)
        self.art = TzeentchArtifacts(self, loc=True)

    def get_unique_gear(self):
        return self.art.description if self.art.any else []

    def count_charges(self):
        return self.mastery.count_charges()

    def build_statistics(self):
        return self.note_charges(super(LordOfChange, self).build_statistics())


class Unclean(GiftBearer):
    type_name = u'Great Unclean One'
    type_id = 'guo_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Great-Unclean-One')

    class Mastery(OneOf):
        def __init__(self, parent):
            super(Unclean.Mastery, self).__init__(parent, 'Mastery')
            self.lvl1 = self.variant('Mastery level 1')
            self.lvl2 = self.variant('Mastery level 2', 25)
            self.lvl3 = self.variant('Mastery level 3', 50)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            elif self.cur == self.lvl2:
                return 2
            else:
                return 3

    def __init__(self, parent):
        super(Unclean, self).__init__(parent, points=190)
        self.mastery = self.Mastery(self)
        self.art = NurgleArtifacts(self, guo=True)

    def get_unique_gear(self):
        return self.art.description if self.art.any else []

    def count_charges(self):
        return self.mastery.count_charges()

    def build_statistics(self):
        return self.note_charges(super(Unclean, self).build_statistics())


class Keeper(GiftBearer):
    type_name = u'Keeper of Secrets'
    type_id = 'kos_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Keeper-of-Secrets')

    class Mastery(OneOf):
        def __init__(self, parent):
            super(Keeper.Mastery, self).__init__(parent, 'Mastery')
            self.lvl1 = self.variant('Mastery level 1')
            self.lvl2 = self.variant('Mastery level 2', 25)
            self.lvl3 = self.variant('Mastery level 3', 50)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            elif self.cur == self.lvl2:
                return 2
            else:
                return 3

    def __init__(self, parent):
        super(Keeper, self).__init__(parent, points=170)
        self.mastery = self.Mastery(self)
        self.art = SlaaneshArtifacts(self, keeper=True)

    def get_unique_gear(self):
        return self.art.description if self.art.any else []

    def count_charges(self):
        return self.mastery.count_charges()

    def build_statistics(self):
        return self.note_charges(super(Keeper, self).build_statistics())


class DaemonPrince(GiftBearer):
    type_name = "Daemon Prince"
    type_id = 'dp_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Daemon-Prince')

    class Dedication(OneOf):
        def __init__(self, parent):
            super(DaemonPrince.Dedication, self).__init__(parent, 'Dedication')
            self.kh = self.variant('Daemon of Khorne', 15)
            self.tz = self.variant('Daemon of Tzeench', 25)
            self.ng = self.variant('Daemon of Nurgle', 15)
            self.sl = self.variant('Daemon of Slaanesh', 10)

    def get_dedication(self):
        if self.god.cur == self.god.kh:
            return 'kh'
        if self.god.cur == self.god.tz:
            return 'tz'
        if self.god.cur == self.god.ng:
            return 'ng'
        return 'sl'

    class Options(OptionsList):
        def __init__(self, parent):
            super(DaemonPrince.Options, self).__init__(parent, 'Options')
            self.variant('Daemonic flight', 40)
            self.variant('Warp-forged armour', 20)

    class Mastery(OneOf):
        def __init__(self, parent):
            super(DaemonPrince.Mastery, self).__init__(parent, 'Mastery')
            self.variant('(none)', gear=[])
            self.lvl1 = self.variant('Mastery level 1', 25)
            self.lvl2 = self.variant('Mastery level 2', 50)
            self.lvl3 = self.variant('Mastery level 3', 75)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            elif self.cur == self.lvl2:
                return 2
            elif self.cur == self.lvl3:
                return 3
            else:
                return 0

    def __init__(self, parent):
        super(DaemonPrince, self).__init__(parent, points=145)
        self.god = self.Dedication(self)
        self.opt = self.Options(self)
        self.mastery = self.Mastery(self)
        self.art = {
            'kh': KhorneArtifacts(self, prince=True),
            'tz': TzeentchArtifacts(self, prince=True),
            'ng': NurgleArtifacts(self, prince=True),
            'sl': SlaaneshArtifacts(self, prince=True)
        }

    def check_rules(self):
        super(DaemonPrince, self).check_rules()
        self.mastery.used = self.mastery.visible = not self.god.cur == self.god.kh
        for god in self.art.keys():
            self.art[god].used = self.art[god].visible = god == self.get_dedication()

    def get_unique_gear(self):
        for god in self.art.keys():
            if god == self.get_dedication():
                return self.art[god].description if self.art[god].any else []

    def count_charges(self):
        return self.mastery.count_charges()

    def build_statistics(self):
        return self.note_charges(super(DaemonPrince, self).build_statistics())


class Skulltaker(Unit):
    type_name = "Skulltaker"
    type_id = 'skulltaker_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Skulltaker')

    class Options(OptionsList):
        def __init__(self, parent):
            super(Skulltaker.Options, self).__init__(parent, 'Ride')
            self.variant("Juggernaut of Khorne", 45)

    def __init__(self, parent):
        super(Skulltaker, self).__init__(parent, points=100, gear=[
            Gear('Lesser Locus of Abjuration'),
            Gear('Cloak of Sculls'), Gear('The Slayer Sword')
        ], unique=True)
        self.ride = self.Options(self)


class Karanak(Unit):
    type_name = "Karanak"
    type_id = 'karanak_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Karanak')

    def __init__(self, parent):
        super(Karanak, self).__init__(parent, points=120, gear=[
            Gear('Greater Locus of Fury'),
            Gear('Brass Collar of Bloody Vengeance')
        ], unique=True, static=True)


class HKhorne(GiftBearer):
    type_name = "Herald of Khorne"
    type_id = 'hok_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Herald-of-Khorne')

    gift_limit = 30
    base_points = 55
    gear = ['Hellblade']

    class MovementOptions(OptionsList):
        def __init__(self, parent):
            super(HKhorne.MovementOptions, self).__init__(parent, 'Ride', limit=1)
            self.variant("Juggernaut of Khorne", 45)
            self.throne = self.variant('Blood Throne of Khorne', 75)

    class Locus(OptionsList):
        def __init__(self, parent):
            super(HKhorne.Locus, self).__init__(parent, 'Locus', limit=1)
            self.variant('Lesser Locus of Abjuration', 10)
            self.variant('Greater Locus of Fury', 20)
            self.variant('Exalted Locus of Wrath', 25)

    def __init__(self, parent):
        super(HKhorne, self).__init__(parent, points=55)
        self.ride = self.MovementOptions(self)
        self.opt = self.Locus(self)
        self.art = KhorneArtifacts(self, herald=True)

    def build_description(self):
        res = super(HKhorne, self).build_description()
        if not self.art.replaces_weapon():
            res.add(Gear('Hellblade'))
        return res

    def get_unique_gear(self):
        return self.art.description if self.art.any else []


class Changeling(Unit):
    type_name = "The Changeling"
    type_id = 'changeling_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='The-Changeling')

    def __init__(self, parent):
        super(Changeling, self).__init__(parent, points=76,
                                         gear=[Gear("Lesser Locus of Transmogrification")],
                                         static=True, unique=True)

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(Changeling, self).build_statistics())


class BlueScribes(Unit):
    type_name = "The Blue Scribes"
    type_id = 'bscribes_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='The-Blue-Scribes')

    def __init__(self, parent):
        super(BlueScribes, self).__init__(parent, points=81, gear=[
            Gear('Scrolls of Sorcery')
        ], static=True, unique=True)


class HTzeentch(GiftBearer):
    type_name = "Herald of Tzeentch"
    type_id = 'hot_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Herald-of-Tzeentch')

    gift_limit = 30

    class Ride(OptionsList):
        def __init__(self, parent):
            super(HTzeentch.Ride, self).__init__(parent, 'Movement Optins')
            self.disc = self.variant('Disc of Tzeentch', 25)
            self.chariot = self.variants('Burning Chariot of Tzeentch', 50, var_dicts=[
                {'name': 'Blue Horrors crew', 'points':10}
            ])

        def check_rules(self):
            super(HTzeentch.Ride, self).check_rules()
            OptionsList.process_limit([self.disc, self.chariot], 1)

    class Loci(OptionsList):
        def __init__(self, parent):
            super(HTzeentch.Loci, self).__init__(parent, 'Locus', limit=1)
            self.variant('Lesser Locus of Transmogrification', 10)
            self.variant('Lesser Locus of Metamorphosis', 15)
            self.variant('Greater Locus of Change', 20)
            self.variant('Greater Locus of Trickery', 20)
            self.variant('Exalted Locus of Conjuration', 25)
            self.variant('Exalted Locus of Creation', 35)

    def __init__(self, parent):
        super(HTzeentch, self).__init__(parent, points=45)
        self.ride = self.Ride(self)
        self.opt = self.Loci(self)
        self.mastery = Unclean.Mastery(self)
        self.art = TzeentchArtifacts(self, herald=True)

    def check_rules(self):
        super(HTzeentch, self).check_rules()
        self.ride.used = self.ride.visible = not self.art.dais.value

    def get_unique_gear(self):
        return self.art.description if self.art.any else []

    def count_charges(self):
        return self.mastery.count_charges()

    def build_statistics(self):
        return self.note_charges(super(HTzeentch, self).build_statistics())


class ExaltedFlamer(Unit):
    type_name = u'Exalted Flamer of Tzeentch'
    type_id = 'eflamer_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Exalted-Flamer-of-Tzeentch')

    def __init__(self, parent):
        super(ExaltedFlamer, self).__init__(parent, points=50, static=True)


class Epidemius(Unit):
    type_name = "Epidemius"
    type_id = 'epidemius_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Epidemius')

    def __init__(self, parent):
        super(Epidemius, self).__init__(parent, points=110, gear=[
            Gear('Lesser Locus of Virulence'), Gear('Plaguesword')
        ], static=True, unique=True)


class HNurgle(GiftBearer):
    type_name = "Herald of Nurgle"
    type_id = 'hon_v3'
    gift_limit = 30
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Herald-of-Nurgle')

    class Options(OptionsList):
        def __init__(self, parent):
            super(HNurgle.Options, self).__init__(parent, 'Ride')
            self.variant("Palanquin of Nurgle", 40)

    class Loci(OptionsList):
        def __init__(self, parent):
            super(HNurgle.Loci, self).__init__(parent, 'Locus', limit=1)
            self.variant('Lesser Locus of Virulence', 10)
            self.variant('Greater Locus of Fecundity', 25)
            self.variant('Exalted Locus of Contagion', 25)

    class Mastery(OneOf):
        def __init__(self, parent):
            super(HNurgle.Mastery, self).__init__(parent, 'Mastery')
            self.variant('(none)', gear=[])
            self.lvl1 = self.variant('Mastery level 1', 25)
            self.lvl2 = self.variant('Mastery level 2', 50)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            elif self.cur == self.lvl2:
                return 2
            else:
                return 0

    def __init__(self, parent):
        super(HNurgle, self).__init__(parent, points=45, gear=[Gear('Plaguesword')])

        self.ride = self.Options(self)
        self.opt = self.Loci(self)
        self.mastery = self.Mastery(self)
        self.art = NurgleArtifacts(self, herald=True)

    def get_unique_gear(self):
        return self.art.description if self.art.any else []

    def count_charges(self):
        return self.mastery.count_charges()

    def build_statistics(self):
        return self.note_charges(super(HNurgle, self).build_statistics())


class Masque(Unit):
    type_name = "The Masque of Slaanesh"
    type_id = 'masque_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='The-Masque-of-Slaanesh')

    def __init__(self, parent):
        super(Masque, self).__init__(parent, points=75,
                                     static=True, unique=True)


class HSlaanesh(GiftBearer):
    type_name = "Herald of Slaanesh"
    type_id = 'hos_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Herald-of-Slaanesh')

    gift_limit = 30

    class MovementOptions(OptionsList):
        def __init__(self, parent):
            super(HSlaanesh.MovementOptions, self).__init__(parent, 'Ride', limit=1)
            self.variant('Steed of Slaanesh', 15)
            self.variant('Seeker Chariot', 30)
            self.variant('Exalted Seeker Chariot', 80)

    class Loci(OptionsList):
        def __init__(self, parent):
            super(HSlaanesh.Loci, self).__init__(parent, 'Locus', limit=1)
            self.variant('Lesser Locus of Grace', 10)
            self.variant('Greater Locus of Swiftness', 20)
            self.variant('Exalted Locus of Beguilement', 30)

    def __init__(self, parent):
        super(HSlaanesh, self).__init__(parent, points=45)
        self.ride = self.MovementOptions(self)
        self.opt = self.Loci(self)
        self.mastery = HNurgle.Mastery(self)
        self.art = SlaaneshArtifacts(self, herald=True)

    def get_unique_gear(self):
        return self.art.description if self.art.any else []

    def count_charges(self):
        return self.mastery.count_charges()

    def build_statistics(self):
        return self.note_charges(super(HSlaanesh, self).build_statistics())


class Belakor(Unit):
    type_name = u'Be\'lakor'
    type_id = 'belakor_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Be-lakor')

    def __init__(self, parent):
        super(Belakor, self).__init__(parent, name='Be\'lakor', points=350, unique=True,
                                      gear=[Gear('The Blade of Shadow')])

    def count_charges(self):
        return 3

    def build_statistics(self):
        return self.note_charges(super(Belakor, self).build_statistics())
