__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList


class KhorneArtifacts(OptionsList):
    def __init__(self, parent, prince=False, thirster=False, herald=False):
        super(KhorneArtifacts, self).__init__(parent, 'Hellforged artefact of Khorne', limit=1)
        self.king = (prince or herald) and self.variant("A'rgath, the King of Blades", 15)
        self.dealer = (prince or thirster) and self.variant('Deathdealer', 15)
        self.crown = (prince or thirster or herald) and self.variant('The Crimson Crown', 40)
        self.skullreaver = (prince or thirster) and self.variant('Skullreaver', 30)
        self.hunger = (prince or herald) and self.variant('Khartoth the Bloodhunger', 25)
        self.armour = (prince or thirster) and self.variant('Armour of Scorn', 30)

    def replaces_weapon(self):
        for var in [self.king, self.dealer, self.skullreaver, self.hunger]:
            if var and var.value:
                return True

        return False


class TzeentchArtifacts(OptionsList):
    def __init__(self, parent, prince=False, loc=False, herald=False):
        super(TzeentchArtifacts, self).__init__(parent, 'Hellforged artefact of Tzeentch', limit=1)
        self.paradox = (loc or herald) and self.variant('Paradox', 25)
        self.grimoire = herald and self.variant('The Endless Grimoire', 35)
        self.bane = (prince or herald) and self.variant('Soul Bane', 15)
        # i suppose this rules away other movement options
        self.dais = herald and self.variant('The Oracular Dais', 35)
        self.robe = (loc or herald or prince) and self.variant('The Impossible Robe', 25)
        self.stave = (loc or herald) and self.variant('The Everstave', 20)


class NurgleArtifacts(OptionsList):
    def __init__(self, parent, prince=False, guo=False, herald=False):
        super(NurgleArtifacts, self).__init__(parent, 'Hellforged artefact of Nurgle', limit=1)
        self.grotti = (prince or guo or herald) and self.variant('Grotti the Nurgling', 40)
        self.corruption = (prince or guo or herald) and self.variant('Corruption', 25)
        self.horn = (prince or herald) and self.variant('Horn of Nurgle\'s Rot', 35)
        self.epidemia = guo and self.variant('Epidemia', 20)
        self.bell = herald and self.variant('The Doomsday Bell', 30)
        self.head = herald and self.variant("Death's Head of Duke Olaks", 15)


class SlaaneshArtifacts(OptionsList):
    def __init__(self, parent, prince=False, keeper=False, herald=False, alluress=False):
        super(SlaaneshArtifacts, self).__init__(parent, 'Hellforged artefact of Slaanesh', limit=1)
        self.stealer = (keeper or prince) and self.variant('Soulstealer', 20)
        self.shard = (keeper or prince) and self.variant('Silvershard', 30)
        self.claw = (keeper or herald) and self.variant('The Slothful Claw', 10)
        self.gem = herald and self.variant('The Forbidden Gem', 15)
        self.whips = alluress and self.variant('Whips of Agony', 15)
        self.mark = (keeper or prince or herald) and self.variant('The Mark of Excess', 15)
