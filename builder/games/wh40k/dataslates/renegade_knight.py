__author__ = 'Ivan Truskov'
__summary__ = 'Imperial Knight: Renegade'

from builder.core2 import Gear, OneOf, OptionsList
from builder.games.wh40k.roster import Unit, LordsOfWarSection,\
    Wh40kBase, UnitType


class Renegade(Unit):
    type_name = u'Renegade Knight'
    type_id = 'renegade_knight_v1'

    class Caparace(OptionsList):
        def __init__(self, parent):
            super(Renegade.Caparace, self).__init__(parent, 'Caparace weapon', limit=1)
            self.variant('Ironstorm missile pod', 30)
            self.icarus = self.variant('Twin Icarus autocannon', 35)
            self.variant('Stormspear missile pod', 40)

    class Subcaliber(OneOf):
        def __init__(self, parent):
            super(Renegade.Subcaliber, self).__init__(parent, 'Subcaliber weapon')
            self.variant('Heavy stubber', 0)
            self.variant('Meltagun', 5)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Renegade.Weapon1, self).__init__(parent, 'Left weapon')
            self.variant('Thunderstrike gauntlet')
            self.variant('Thermal cannon', 45)
            self.variant('Rapid-fire battle cannon and heavy stubber', 50, gear=[
                Gear('Rapid-fire battle cannon'), Gear('Heavy stubber')
            ])
            self.variant('Avenger gatling cannon and heavy flamer', 50, gear=[
                Gear('Avenger gatling cannon'), Gear('Heavy flamer')
            ])

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Renegade.Weapon2, self).__init__(parent, 'Right weapon')
            self.variant('Reaper chainsword')
            self.variant('Thermal cannon', 55)
            self.variant('Rapid-fire battle cannon and heavy stubber', 60, gear=[
                Gear('Rapid-fire battle cannon'), Gear('Heavy stubber')
            ])
            self.variant('Avenger gatling cannon and heavy flamer', 60, gear=[
                Gear('Avenger gatling cannon'), Gear('Heavy flamer')
            ])

    def __init__(self, parent):
        super(Renegade, self).__init__(parent, points=325,
                                       gear=[Gear('Ion shield')])
        self.Weapon1(self)
        self.Weapon2(self)
        self.Subcaliber(self)
        self.Caparace(self)


class Lords(LordsOfWarSection):
    def __init__(self, parent):
        super(Lords, self).__init__(parent)
        UnitType(self, Renegade)


class Forsworn(Wh40kBase):
    army_id = 'renegade_knights_forsworn'
    army_name = 'Forsworn Knight detachment'

    def __init__(self):
        self.knights = Lords(parent=self)
        self.knights.min, self.knights.max = (1, 3)
        super(Forsworn, self).__init__(
            lords=[self.knights]
        )


faction = 'Renegade Knights'
detachments = [Forsworn]
unit_types = [Renegade]
