__author__ = 'Ivan Truskov'

from builder.core2 import Count, OneOf, SubUnit, OptionalSubUnit, Gear, UnitDescription
from troops import KabaliteWarriors, Wyches
from fast import Raider, Venom
from armory import WrackTool, Torment, Torture
from builder.games.wh40k.roster import Unit


class KabaliteTrueborn(KabaliteWarriors):
    type_name = u'Kabalite Trueborn'
    type_id = 'kabaltrue'

    member_name = 'Kabalite Trueborn'
    member_points = 11

    class Dracon(KabaliteWarriors.Sybarite):
        subtype_name = u'Dracon'
        subtype_points = 21

    def get_leader_type(self):
        return self.Dracon

    def init_weapon_list(self):
        self.sp1 = self.Special(self)
        self.sp2 = self.Special(self)
        self.sp3 = self.Special(self)
        self.sp4 = self.Special(self)
        self.hv1 = self.Heavy(self)
        self.hv2 = self.Heavy(self)

    def __init__(self, parent):
        super(KabaliteTrueborn, self).__init__(parent)

    def get_all_spec(self):
        return [self.hv1, self.hv2, self.sp1, self.sp2, self.sp3, self.sp4]

    def check_rules(self):
        ldr = self.ldr.count
        spec_count = sum([(o.used and not o.cur == o.rifle) for o in self.get_all_spec()])
        Count.norm_counts(max(5 - ldr, spec_count), 20 - ldr, [self.wars])


class Bloodbrides(Wyches):
    type_name = u'Bloodbrides'
    type_id = 'bride'

    member_name = 'Bloodbride'
    member_points = 13

    class Syren(Wyches.Hekatrix):
        subtype_name = u'Syren'
        subtype_points = 13 + 10


class Incubi(Unit):
    type_name = u'Incubi'
    type_id = 'incub'

    class Klaivex(Unit):
        base_points = 20 + 10
        type_name = u'Klaivex'
        gear = ['Incubus warsuit']

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Incubi.Klaivex.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Klaive', 0)
                self.variant('Demiklaives', 15)

        def __init__(self, parent):
            super(Incubi.Klaivex, self).__init__(parent, self.type_name, self.base_points, gear=[Gear('Incubus warsuit')])
            self.Weapon(self)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Incubi.Transport, self).__init__(parent, 'Transport', order=100)
            SubUnit(self, Raider(parent=None))
            SubUnit(self, Venom(parent=None))

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Incubi.Boss, self).__init__(parent=parent, name='Leader', limit=1)
            SubUnit(self, Incubi.Klaivex(parent=None))

    def __init__(self, parent):
        super(Incubi, self).__init__(parent, self.type_name)
        self.warriors = Count(self, 'Incubi', 3, 10, 20, per_model=True,
                              gear=UnitDescription('Incubus', 20, options=[Gear('Klaive'), Gear('Incubus warsuit')]), order=10)
        self.leader = self.Boss(self)
        self.transport = self.Transport(self)

    def check_rules(self):
        ldr = self.leader.count
        Count.norm_counts(3 - ldr, 10 - ldr, [self.warriors])

    def get_count(self):
        return self.warriors.cur + self.leader.count

    def build_statistics(self):
        return self.note_transport(super(Incubi, self).build_statistics())


class Mandrakes(Unit):
    type_name = u'Mandrakes'
    type_id = 'mandrake'
    common_gear = [Gear('Baleblast'), Gear('Close combat weapon')]

    class Nightfiend(Unit):
        def __init__(self, parent):
            super(Mandrakes.Nightfiend, self).__init__(parent, 'Nightfiend', 22, Mandrakes.common_gear, static=True)

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Mandrakes.Boss, self).__init__(parent=parent, name='Leader', limit=1)
            SubUnit(self, Mandrakes.Nightfiend(parent=None))

    def __init__(self, parent):
        super(Mandrakes, self).__init__(parent, self.type_name)
        self.warriors = Count(self, 'Mandrake', 3, 10, 12, True,
                              gear=UnitDescription('Mandrake', 12, options=Mandrakes.common_gear), order=10)
        self.leader = self.Boss(self)

    def check_rules(self):
        ldr = self.leader.count
        Count.norm_counts(3 - ldr, 10 - ldr, [self.warriors])

    def get_count(self):
        return self.warriors.cur + self.leader.count


class Wracks(Unit):
    type_name = u'Wracks'
    type_id = 'wrack'

    take_own_transport = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Wracks.Weapon, self).__init__(parent, 'Weapon', order=11)
            self.tool = self.variant('Wrack tool', 0)
            self.variant('Liquefier gun', 15)
            self.variant('Ossefactor', 15)

    class Acothyst(Unit):
        class Weapon(Torment, WrackTool):
            pass

        def __init__(self, parent):
            super(Wracks.Acothyst, self).\
                __init__(parent, 'Acothyst', 20, [Gear('Gnarlskin')])
            self.Weapon(self)
            Torture(self, is_acothyst=True)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Wracks.Transport, self).__init__(parent, 'Transport', order=100)
            SubUnit(self, Raider(parent=None))
            SubUnit(self, Venom(parent=None))

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Wracks.Boss, self).__init__(parent=parent, name='Leader', limit=1)
            SubUnit(self, Wracks.Acothyst(parent=None))

    def __init__(self, parent):
        super(Wracks, self).__init__(parent, self.type_name)

        self.warriors = Count(self, 'Wrack', 5, 10, 10, True, order=10)
        self.rng1 = self.Weapon(self)
        self.rng2 = self.Weapon(self)
        if self.take_own_transport:
            self.transport = self.Transport(self)
        self.leader = self.Boss(self)

    def get_count(self):
        return self.warriors.cur + self.leader.count

    def check_rules(self):
        ldr = self.leader.count
        Count.norm_counts(5 - ldr, 10 - ldr, [self.warriors])
        self.rng2.used = self.rng2.visible = self.get_count() == 10

    def get_all_spec(self):
        return [self.rng1, self.rng2]

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.leader.description)
        model = UnitDescription(name='Wrack', points=10, options=[Gear('Gnarlskin')])
        spec_count = sum([(o.used and o.cur != o.tool) for o in self.get_all_spec()])
        desc.add(model.clone().add(Gear('Wrack tool', count=2)).set_count(self.warriors.cur - spec_count))
        for g in self.get_all_spec():
            if g.used and g.tool != g.cur:
                desc.add_dup(model.clone().add(g.description).add(Gear('Wrack tool')).add_points(g.points))
        if self.take_own_transport:
            desc.add(self.transport.description)
        return desc

    def build_statistics(self):
        if self.take_own_transport:
            return self.note_transport(super(Wracks, self).build_statistics())
        else:
            return super(Wracks, self).build_statistics()


class Grotesques(Unit):
    type_name = u'Grotesques'
    type_id = 'grotesk'

    class Aberration(Unit):
        class Weapon(Torture):
            def __init__(self, parent):
                super(Grotesques.Aberration.Weapon, self).__init__(parent, name='Weapon')
                self.variant('Liquifier gun', 15)

        def __init__(self, parent):
            super(Grotesques.Aberration, self).__init__(parent, 'Aberration', 35 + 10, [Gear('Gnarlskin'), Gear('Flesh gauntlet')])
            self.Weapon(self)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Grotesques.Transport, self).__init__(parent, 'Transport', order=100)
            SubUnit(self, Raider(parent=None))

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Grotesques.Boss, self).__init__(parent=parent, name='Leader', limit=1)
            SubUnit(self, Grotesques.Aberration(parent=None))

    def __init__(self, parent):
        super(Grotesques, self).__init__(parent, 'Grotesques')
        self.warriors = Count(self, 'Grotesque', 3, 10, 35, True, order=10)
        self.guns = Count(self, 'Liquifier gun', 0, 10, 15, order=11)
        self.leader = self.Boss(self)
        self.transport = self.Transport(self)

    def get_count(self):
        return self.warriors.cur + self.leader.count

    def check_rules(self):
        ldr = self.leader.count
        Count.norm_counts(3 - ldr, 10 - ldr, [self.warriors])
        Count.norm_counts(0, self.warriors.cur, [self.guns])

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.leader.description)
        model = UnitDescription(name='Grotesque', points=35, options=[Gear('Gnarlskin'), Gear('Flesh gauntlet')])
        base_cnt = self.warriors.cur - self.guns.cur
        if base_cnt > 0:
            desc.add(model.clone().add([Gear('Close combat weapon')]).set_count(base_cnt))
        if self.guns.cur > 0:
            desc.add(model.clone().add([Gear('Liquifier gun')]).add_points(15).set_count(self.guns.cur))
        desc.add(self.transport.description)
        return desc

    def build_statistics(self):
        return self.note_transport(super(Grotesques, self).build_statistics())
