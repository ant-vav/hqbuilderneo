__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, Count,\
    OptionalSubUnit, SubUnit, UnitDescription, Gear
from armory import Melee, Special, Heavy, Pistol, Rifle, WychWeapon
from fast import Raider, Venom
from builder.games.wh40k.roster import Unit


class KabaliteWarriors(Unit):
    type_name = u'Kabalite Warriors'
    type_id = 'kabalwar'

    member_name = 'Kabalite Warrior'
    member_points = 8

    class Sybarite(Unit):
        subtype_name = u'Sybarite'
        subtype_points = 18

        class Options(OptionsList):
            def __init__(self, parent):
                super(KabaliteWarriors.Sybarite.Options, self).\
                    __init__(parent, name='Options')
                self.variant('Haywire grenades', 5)
                self.variant('Phantasm grenade launcher', 10)

        class Pistol(Pistol, Rifle):
            def __init__(self, parent):
                super(KabaliteWarriors.Sybarite.Pistol, self).\
                    __init__(parent, name='Ranged weapon')
                self.variant('Blast pistol', 15)

        def __init__(self, parent):
            super(KabaliteWarriors.Sybarite, self).\
                __init__(parent, self.subtype_name, self.subtype_points, gear=[Gear('Kabalite armour')])
            self.mle = Melee(self)
            self.rng = self.Pistol(self)
            self.opt = self.Options(self)

    class Special(Special, Rifle):
        pass

    class Heavy(Heavy, Rifle):
        pass

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(KabaliteWarriors.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, Raider(parent=None))
            SubUnit(self, Venom(parent=None))

    def get_leader_type(self):
        return self.Sybarite

    def init_weapon_list(self):
        self.sp1 = self.Special(self)
        self.sp2 = self.Special(self)
        self.hv1 = self.Heavy(self)
        self.hv2 = self.Heavy(self)

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(KabaliteWarriors.Boss, self).__init__(parent=parent, name='Leader', limit=1)
            SubUnit(self, parent.get_leader_type()(parent=None))

    def __init__(self, parent):
        super(KabaliteWarriors, self).__init__(parent, name=self.type_name)
        self.wars = Count(self, self.member_name, 5, 20, self.member_points, True)
        self.init_weapon_list()
        self.ldr = self.Boss(self)
        self.transport = self.Transport(self)

    def check_rules(self):
        ldr = self.ldr.count
        Count.norm_counts(5 - ldr, 20 - ldr, [self.wars])
        self.sp2.used = self.sp2.visible = self.get_count() == 20
        self.hv2.used = self.hv2.visible = self.get_count() >= 10

    def get_all_spec(self):
        return [self.hv1, self.hv2, self.sp1, self.sp2]

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        model = UnitDescription(name=self.member_name, points=self.member_points, options=[Gear('Kabalite armour'), Gear('Close combat weapon')])
        spec_count = sum([(o.used and not o.cur == o.rifle) for o in self.get_all_spec()])
        desc.add(model.clone().add(Gear('Splinter rifle')).set_count(self.wars.cur - spec_count))
        for g in self.get_all_spec():
            if g.used and not g.cur == g.rifle:
                desc.add_dup(model.clone().add(g.description).add_points(g.points))
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.wars.cur + self.ldr.count

    def build_statistics(self):
        return self.note_transport(super(KabaliteWarriors, self).build_statistics())


class Wyches(Unit):
    type_name = u'Wyches'
    type_id = 'wych'

    member_name = 'Wych'
    member_points = 10

    class Hekatrix(Unit):
        subtype_name = u'Hekatrix'
        subtype_points = 10 + 10

        class Options(OptionsList):
            def __init__(self, parent):
                super(Wyches.Hekatrix.Options, self).\
                    __init__(parent, name='Options')
                self.variant('Haywire grenades', 5)
                self.variant('Phantasm grenade launcher', 10)

        class Pistol(Pistol):
            def __init__(self, parent):
                super(Wyches.Hekatrix.Pistol, self).\
                    __init__(parent, name='Ranged weapon')
                self.variant('Blast pistol', 15)

        def __init__(self, parent):
            super(Wyches.Hekatrix, self).\
                __init__(parent, self.subtype_name, self.subtype_points, gear=[Gear('Wychsuit'), Gear('Plasma grenades')])
            self.mle = Melee(self)
            self.rng = self.Pistol(self)
            self.opt = self.Options(self)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Wyches.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, Raider(parent=None))
            SubUnit(self, Venom(parent=None))

    def get_leader_type(self):
        return self.Hekatrix

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Wyches.Boss, self).__init__(parent=parent, name='Leader', limit=1)
            SubUnit(self, parent.get_leader_type()(parent=None))

    def __init__(self, parent):
        super(Wyches, self).__init__(parent, self.type_name)
        self.wars = Count(self, self.member_name, 5, 10, self.member_points, True)
        self.ww1 = WychWeapon(self, has_default=True)
        self.ww2 = WychWeapon(self, has_default=True)
        self.ww3 = WychWeapon(self, has_default=True)

        self.ldr = self.Boss(self)
        self.transport = self.Transport(self)

    def get_count(self):
        return self.wars.cur + self.ldr.count

    def check_rules(self):
        ldr = self.ldr.count
        Count.norm_counts(5 - ldr, 15 - ldr, [self.wars])
        self.ww2.used = self.ww2.visible = self.get_count() >= 10
        self.ww3.used = self.ww3.visible = self.get_count() >= 10

    def get_all_spec(self):
        return [self.ww1, self.ww2, self.ww3]

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        model = UnitDescription(name=self.member_name, points=self.member_points, options=[Gear('Wychsuit'), Gear('Plasma grenades')])
        spec_count = sum([(o.used and not o.cur == o.default) for o in self.get_all_spec()])
        desc.add(model.clone().add([Gear('Splinter pistol'), Gear('Close combat weapon')]).set_count(self.wars.cur - spec_count))
        for g in self.get_all_spec():
            if g.used and not g.cur == g.default:
                desc.add_dup(model.clone().add(g.description).add_points(g.points))
        desc.add(self.transport.description)
        return desc

    def build_statistics(self):
        return self.note_transport(super(Wyches, self).build_statistics())
