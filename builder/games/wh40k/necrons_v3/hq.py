__author__ = 'dante'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import Gear, SubUnit, OneOf
from builder.games.wh40k.roster import Unit
from armory import Ranged, Melee, Technoarcana, Artefacts, Relics


class Overlord(Unit):
    type_name = u'Overlord'
    type_id = 'necronoverlord_v3'

    def __init__(self, parent):
        super(Overlord, self).__init__(parent=parent, points=80)
        Ranged(self)
        self.melee = Melee(self, overlord=True)
        Technoarcana(self)
        self.art = Artefacts(self, self.melee)
        if parent.roster.supplement == 'mephrit':
            self.rel = Relics(self)
        else:
            self.rel = None

    def get_unique_gear(self):
        result = self.art.get_unique() + self.melee.get_unique()
        if self.rel:
            result += self.rel.description
        return result


class Lord(Unit):
    type_name = u'Necron Lord'
    type_id = 'necronlord_v3'

    def __init__(self, parent):
        super(Lord, self).__init__(parent=parent, points=50)
        Ranged(self)
        melee = Melee(self)
        Technoarcana(self)
        self.art = Artefacts(self, melee)

    def get_unique_gear(self):
        return self.art.get_unique()


class Cryptek(Unit):
    type_name = u'Cryptek'
    type_id = 'cryptek_v3'

    def __init__(self, parent):
        super(Cryptek, self).__init__(parent=parent, points=65)
        self.opt = self.Options(self)
        self.art = Artefacts(self, cryptek=True)
        if parent.roster.supplement == 'mephrit':
            self.rel = Relics(self, cryptek=True)
        else:
            self.rel = None

    class Options(Technoarcana):
        def __init__(self, parent):
            super(Cryptek.Options, self).__init__(parent=parent, cryptek=True)
            self.chronometron = self.variant('Chronometron', 25)

    def check_rules(self):
        super(Cryptek, self).check_rules()
        self.gear = [] if self.art.solar.value else [Gear('Staff of Light')]

    def get_unique_gear(self):
        result = self.art.description
        if self.rel:
            result += self.rel.description
        return result


class DestroyerLord(Unit):
    type_name = u'Destroyer Lord'
    type_id = 'destroyerlord_v3'

    def __init__(self, parent):
        super(DestroyerLord, self).__init__(parent=parent, points=110)
        melee = Melee(self)
        Technoarcana(self)
        self.art = Artefacts(self, melee)

    def get_unique_gear(self):
        return self.art.get_unique()


class Nemesor(Unit):
    type_name = u'Nemesor Zahndrekh, Overlord of Gidrim'
    type_id = 'nemesorzahndrekh_v3'

    def __init__(self, parent):
        super(Nemesor, self).__init__(parent=parent, name='Nemesor Zandrekh',
                                      points=150, unique=True, gear=[
                                          Gear('Phase Shifter'),
                                          Gear('Staff of Light')
                                      ], static=True)


class Vargard(Unit):
    type_name = u'Vargard Obyron, Zandrekh\'s shield'
    type_id = 'vargardobyron_v3'

    def __init__(self, parent):
        super(Vargard, self).__init__(parent=parent, name='Vargard Obyron',
                                      points=120, unique=True, gear=[
                                          Gear('Ghostwalk mantle'),
                                          Gear('Warscythe')
                                      ])


class Illuminor(Unit):
    type_name = u'Illuminor Szeras, Architect of Biotransference'
    type_id = 'illuminorszeras_v3'

    def __init__(self, parent):
        super(Illuminor, self).__init__(parent=parent, name='Illuminor Szeras',
                                        points=110, unique=True, static=True,
                                        gear=[Gear('Eldritch Lance')])


class Orikan(Unit):
    type_name = u'Orikan the Diviner, Seer of the Necrontyr'
    type_id = 'orikanthediviner_v3'

    def __init__(self, parent):
        super(Orikan, self).__init__(parent=parent, points=120,
                                     name='Orikan the Diviner', unique=True,
                                     gear=[
                                         Gear('Phase Shifter'),
                                         Gear('Staff of Tomorrow')
                                     ], static=True)


class Anrakyr(Unit):
    type_name = u'Anrakyr the Traveller, Lord of the Pyrrhyan Legions'
    type_id = 'anrakyrthetraveller_v3'

    def __init__(self, parent):
        super(Anrakyr, self).__init__(parent=parent, points=160, unique=True,
                                      gear=[Gear('Tachyon Arrow'), Gear('Warscythe')],
                                      name='Anrakyr the Traveller')


class Trazyn(Unit):
    type_name = u'Trazyn the Infinite, Archeovist of the Solemnace Galleries'
    type_id = 'trazintheinfinite_v3'

    def __init__(self, parent):
        super(Trazyn, self).__init__(parent=parent, points=130, unique=True,
                                     gear=[Gear('Empathic Obliterator')],
                                     name='Trazin the Infinite', static=True)


class CommandBarge(Unit):
    type_name = u'Catacomb Command Barge'
    type_id = 'catacombcommandbarge_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CommandBarge.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Tesla Cannon', 0)
            self.variant('Gauss Cannon', 0)

    class Barge(Unit):
        def __init__(self, parent):
            super(CommandBarge.Barge, self).__init__(parent, CommandBarge.type_name,
                                                     135 - 80, [Gear('Quantum shielding')])
            CommandBarge.Weapon(self)

    def __init__(self, parent):
        super(CommandBarge, self).__init__(parent)
        SubUnit(self, Overlord(parent=self))
        SubUnit(self, self.Barge(parent=None))
