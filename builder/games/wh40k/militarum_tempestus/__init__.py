__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Wh40kKillTeam, PrimaryDetachment,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, TroopsSection,\
    FastSection, UnitType, Formation
from builder.games.wh40k.fortifications import Fort

from builder.games.wh40k.escalation.imperial_guard import LordsOfWar
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusSection
from hq import *
from troops import *
from fast import *


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        UnitType(self, Commissar)
        UnitType(self, CommandSquad)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, Scions)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, TauroxPrime)
        UnitType(self, ValkyrieSquadron)


class HQ(BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class FastAttack(BaseFastAttack):
    pass


class Lords(CerastusSection, Titans, LordsOfWar):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman)


class MilitarumTempestusBase(Wh40kBase):
    army_id = 'militarum_tempestus_v1_base'
    army_name = 'Militarum Tempestus'

    def __init__(self):
        super(MilitarumTempestusBase, self).__init__(
            hq=HQ(parent=self),
            troops=Troops(parent=self),
            fast=FastAttack(parent=self),
            fort=Fort(parent=self),
            lords=Lords(parent=self)
        )


class MilitarumTempestusCAD(MilitarumTempestusBase, CombinedArmsDetachment):
    army_id = 'militarum_tempestus_v1_cad'
    army_name = 'Militarum Tempestus (Combined arms detachment)'


class MilitarumTempestusAD(MilitarumTempestusBase, AlliedDetachment):
    army_id = 'militarum_tempestus_v1_ad'
    army_name = 'Militarum Tempestus (Allied detachment)'


class MilitarumTempestusPA(MilitarumTempestusBase, PlanetstrikeAttacker):
    army_id = 'militarum_tempestus_v1_pa'
    army_name = 'Militarum Tempestus (Planetstrike attacker detachment)'


class MilitarumTempestusPD(MilitarumTempestusBase, PlanetstrikeDefender):
    army_id = 'militarum_tempestus_v1_pd'
    army_name = 'Militarum Tempestus (Planetstrike defender detachment)'


class MilitarumTempestusSA(MilitarumTempestusBase, SiegeAttacker):
    army_id = 'militarum_tempestus_v1_sa'
    army_name = 'Militarum Tempestus (Siege War attacker detachment)'


class MilitarumTempestusSD(MilitarumTempestusBase, SiegeDefender):
    army_id = 'militarum_tempestus_v1_sd'
    army_name = 'Militarum Tempestus (Siege War defender detachment)'


class MilitarumTempestusKillTeam(Wh40kKillTeam):
    army_id = 'militarum_tempestus_v1_kt'
    army_name = 'Militarum Tempestus'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(MilitarumTempestusKillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [Scions])

    class KTFast(FastSection):
        def __init__(self, parent):
            super(MilitarumTempestusKillTeam.KTFast, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [TauroxPrime])

    def __init__(self):
        super(MilitarumTempestusKillTeam, self).__init__(
            self.KTTroops(self), None, self.KTFast(self)
        )


class AirborneAssault(Formation):
    army_id = 'militarum_tempestus_v1_airborne'
    army_name = 'Airborne Assault Formation'

    def __init__(self):
        super(AirborneAssault, self).__init__()
        UnitType(self, Commissar, min_limit=1, max_limit=1)
        UnitType(self, CommandSquad, min_limit=1, max_limit=1)
        UnitType(self, Scions, min_limit=3, max_limit=3)
        UnitType(self, Valkyrie, min_limit=4, max_limit=4)


class GroundAssault(Formation):
    army_id = 'militarum_tempestus_v1_ground'
    army_name = 'Ground Assault Formation'

    def __init__(self):
        super(GroundAssault, self).__init__()
        UnitType(self, Commissar, min_limit=1, max_limit=1)
        UnitType(self, CommandSquad, min_limit=1, max_limit=1)
        UnitType(self, Scions, min_limit=3, max_limit=3)
        UnitType(self, TauroxPrime, min_limit=4, max_limit=4)


class HellrainBrigade(Formation):
    army_id = 'militarum_tempestus_v1_hellrain'
    army_name = 'Hellrain Brigade'

    def __init__(self):
        super(HellrainBrigade, self).__init__()
        UnitType(self, Commissar, min_limit=1, max_limit=1)
        UnitType(self, CommandSquad, min_limit=1, max_limit=1)
        UnitType(self, Scions, min_limit=1, max_limit=1)
        UnitType(self, TauroxPrime, min_limit=1, max_limit=1)


faction = 'Militarum Tempestus'


class MilitarumTempestus(Wh40k7ed, Wh40kImperial):
    army_id = 'militarum_tempestus_v1'
    army_name = 'Militarum Tempestus'
    faction = faction

    def __init__(self):
        super(MilitarumTempestus, self).__init__([MilitarumTempestusCAD, AirborneAssault, GroundAssault, HellrainBrigade])


class MilitarumTempestusMissions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'militarum_tempestus_v1_mis'
    army_name = 'Militarum Tempestus'
    faction = faction

    def __init__(self):
        super(MilitarumTempestusMissions, self).__init__([
            MilitarumTempestusCAD, MilitarumTempestusPA,
            MilitarumTempestusPD, MilitarumTempestusSA,
            MilitarumTempestusSD,
            AirborneAssault, GroundAssault, HellrainBrigade])


detachments = [MilitarumTempestusCAD, MilitarumTempestusAD,
               MilitarumTempestusPA,
               MilitarumTempestusPD, MilitarumTempestusSA,
               MilitarumTempestusSD, AirborneAssault, GroundAssault, HellrainBrigade]


unit_types = [
    Commissar, CommandSquad, Scions, TauroxPrime,
    ValkyrieSquadron
]
