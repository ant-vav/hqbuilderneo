__author__ = 'Ivan Truskov'

from builder.core2 import OptionalSubUnit, Gear, OptionsList,\
    UnitList, UnitDescription, SubUnit, ListSubUnit, Count,\
    OneOf
from builder.games.wh40k.roster import Unit
from hq import Mek
from fast import Trukk
from armory import Melee, Choppa, Slugga, Ranged, TlShoota
from heavy import Battlewagon


class BurnaBoyz(Unit):
    type_id = 'burna_v1'
    type_name = u'Burna Boyz'

    model_points = 16
    model_description = UnitDescription(name=type_name, points=model_points, options=[Gear('Burna'), Gear('Stikkbombs')])
    boyz_min_def = 5
    boyz_max_def = 15

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(BurnaBoyz.Transport, self).__init__(parent, 'Transport')
            self.trukk = SubUnit(self, Trukk(parent=None))

    def get_transport_class(self):
        return BurnaBoyz.Transport

    class Upgrades(OptionsList):
        def __init__(self, parent):
            super(BurnaBoyz.Upgrades, self).__init__(parent, 'Upgrades')
            self.to_mek = self.variant('To Mek', 0)

        @property
        def description(self):
            return []

    class BurnaMeks(UnitList):

        def get_mek_class(self, parent):
            class BurnaMek(Mek, ListSubUnit):
                model_points = parent.model_points
                killsaw_cost = 20
                pass
            return BurnaMek

        def __init__(self, parent):
            super(BurnaBoyz.BurnaMeks, self).__init__(parent, self.get_mek_class(parent), min_limit=0, max_limit=3)

    def __init__(self, parent):
        super(BurnaBoyz, self).__init__(parent)
        self.boyz = Count(self, 'Burna Boy', self.boyz_min_def, self.boyz_max_def, self.model_points, gear=self.model_description)

        self.bosses = self.BurnaMeks(self)
        self.transport = self.get_transport_class()(self)
        self.opt = self.Upgrades(self)

    def get_count(self):
        return self.boyz.cur + (self.bosses.count if self.opt.any else 0)

    def check_rules(self):
        self.bosses.visible = self.opt.any
        self.bosses.used = self.opt.any
        self.boyz.min = self.boyz_min_def - (self.bosses.count if self.opt.any else 0)
        self.boyz.max = self.boyz_max_def - (self.bosses.count if self.opt.any else 0)

    def build_statistics(self):
        return self.note_transport(super(BurnaBoyz, self).build_statistics())


class Tankbustas(Unit):
    type_id = 'bustas_v1'
    type_name = u'Tankbustas'

    wargear = [Gear('Rokkit launcha'), Gear('Stikkbombs'), Gear('Tankbusta bombs')]

    class Nob(Unit):

        class Melee(OptionsList):
            def __init__(self, parent):
                super(Tankbustas.Nob.Melee, self).__init__(parent, 'Melee weapon')
                self.variant('Big choppa', 5)
                self.variant('Power klaw', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Tankbustas.Nob.Options, self).__init__(parent, 'Options')
                self.ha = self.variant('Bosspole', 5)

        def __init__(self, parent):
            super(Tankbustas.Nob, self).__init__(parent, name='Boss Nob', points=13 + 10, gear=[Gear('Rokkit launcha'), Gear('Stikkbombs'), Gear('Tankbusta bombs')])
            self.Melee(self)
            self.Options(self)

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Tankbustas.Boss, self).__init__(parent=parent, name='Boss', limit=1)
            SubUnit(self, Tankbustas.Nob(parent=None))

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Tankbustas.Transport, self).__init__(parent, 'Transport')
            self.trukk = SubUnit(self, Trukk(parent=None))

    def __init__(self, parent):
        super(Tankbustas, self).__init__(parent)
        self.models = Count(self, 'Tankbustas', 5, 15, 13, per_model=True)
        self.th = Count(self, 'Tankhammer', 0, 2, 15)
        self.bs = Count(self, 'Bomb squig', 0, 3, 5, per_model=True)
        self.boss = self.Boss(self)
        self.transport = self.Transport(self)

    def get_count(self):
        return self.models.cur + self.boss.count

    def check_rules(self):
        self.models.min = 5 - self.boss.count
        self.models.max = 15 - self.boss.count

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        model = UnitDescription(name='Tankbusta', points=13, options=self.wargear[1:])
        common = model.clone().add(self.wargear[0])
        desc.add(common.set_count(self.models.cur - self.th.cur))
        desc.add(model.clone().add(Gear('Tankhammer')).add_points(15).set_count(self.th.cur))
        desc.add(self.boss.description)
        desc.add(self.bs.description)
        return desc

    def build_statistics(self):
        return self.note_transport(super(Tankbustas, self).build_statistics())


class NobTransport(OptionalSubUnit):
    def __init__(self, parent):
        super(NobTransport, self).__init__(parent, 'Transport', limit=1)
        self.models = [
            SubUnit(self, Trukk(parent=None)),
        ]
        self.wagon = SubUnit(self, Battlewagon(parent=None))
        self.models += [self.wagon]


class Nobz(Unit):
    type_id = 'nobz_v1'
    type_name = u'Nobz'

    class BaseNob(Unit):
        base_points = 18

        class Weapon1(Melee, Choppa):
            pass

        class Weapon2(Ranged, Slugga):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Nobz.BaseNob.Options, self).__init__(parent, 'Options')
                self.Variant(self, 'Bosspole', 5)
                self.Variant(self, 'Ammo runt', 3)

        def __init__(self, parent, name='Nob'):
            super(Nobz.BaseNob, self).__init__(parent, name=name, points=self.base_points, gear=[Gear('Stikkbombz')])
            self.ccw = self.Weapon1(self, 'Melee weapon')
            self.rng = self.Weapon2(self, 'Ranged weapon')
            self.opt = self.Options(self)

        @property
        def base_unit(self):
            # since root_unit is only defined in ListSubUnit, we use this surrogate here
            return self.parent.parent

        def build_description(self):
            desc = super(Nobz.BaseNob, self).build_description()
            desc.add(self.base_unit.nob_opt.description)
            return desc

        def build_points(self):
            return super(Nobz.BaseNob, self).build_points() + self.base_unit.nob_opt.points

    class Nob(BaseNob, ListSubUnit):
        type_name = u'Nob'

        class Banners(OptionsList):
            def __init__(self, parent):
                super(Nobz.Nob.Banners, self).__init__(parent, 'Banner')
                self.waagh = self.variant('Waagh! banner', 20)

        def __init__(self, parent):
            super(Nobz.Nob, self).__init__(parent)
            self.ban = self.Banners(self)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.ban.any

    class NobOptions(OptionsList):
        def __init__(self, parent):
            super(Nobz.NobOptions, self).__init__(parent, 'Unit options')
            self.Variant(self, '\'eavy armour', 4, per_model=True)
            self.bikes = self.Variant(self, 'Warbike', 27, per_model=True)

    def __init__(self, parent):
        super(Nobz, self).__init__(parent, self.type_name)
        self.boss = SubUnit(self, self.BaseNob(parent=None, name='Boss Nob'))
        self.nobz = UnitList(self, unit_class=self.Nob, min_limit=2, max_limit=9)
        self.nob_opt = self.NobOptions(self)
        self.transport = NobTransport(self)

    def count_banners(self):
        return sum(c.count_banners() for c in self.nobz.units)

    def check_rules(self):
        flag = self.count_banners()
        if flag > 1:
            self.error('Only one Nob can take a banner (taken: {})'.format(flag))
        self.transport.visible = self.transport.used = not self.nob_opt.bikes.value

    def build_points(self):
        # points from nob options are already counted for every model, so their cost "on their own" is removed from the result
        return super(Nobz, self).build_points() - self.nob_opt.points

    def get_count(self):
        return self.nobz.count + self.boss.count

    def build_statistics(self):
        return self.note_transport(super(Nobz, self).build_statistics())


class Meganobz(Unit):
    type_id = 'mega_v1'
    type_name = u'Meganobz'

    class BaseMega(Unit):
        base_points = 40

        class Melee(OneOf):
            def __init__(self, parent):
                super(Meganobz.BaseMega.Melee, self).__init__(parent, 'Melee weapon')
                self.variant('Power klaw', 0)
                self.saw = self.variant('Killsaws', 10, gear=[Gear('Killsaw', count=2)])

        class Ranged(TlShoota):
            def __init__(self, parent):
                super(Meganobz.BaseMega.Ranged, self).__init__(parent, name='Ranged weapon')
                self.variant('Kombi-weapon with rokkit launcha', 5)
                self.variant('Kombi-weapon with skorcha', 5)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Meganobz.BaseMega.Options, self).__init__(parent, 'Options')
                self.Variant(self, 'Bosspole', 5)

        def __init__(self, parent, name='Meganob'):
            super(Meganobz.BaseMega, self).__init__(parent, name=name, points=self.base_points, gear=[Gear('Mega armour'), Gear('Stikkbombz')])
            self.ccw = self.Melee(self)
            self.rng = self.Ranged(self)
            self.opt = self.Options(self)

        def check_rules(self):
            self.rng.visible = self.rng.used = self.ccw.saw != self.ccw.cur

    class Meganob(BaseMega, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Meganobz, self).__init__(parent, self.type_name)
        self.boss = SubUnit(self, self.BaseMega(parent=None, name='Boss Meganob'))
        self.nobz = UnitList(self, unit_class=self.Meganob, min_limit=2, max_limit=9)
        self.transport = NobTransport(self)

    def get_count(self):
        return self.nobz.count + self.boss.count

    def build_statistics(self):
        return self.note_transport(super(Meganobz, self).build_statistics())


class Kommandos(Unit):
    type_id = 'komds_v1'
    type_name = u'Kommandos'

    class Nob(Unit):
        class NobMelee(Melee, Choppa):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Kommandos.Nob.Options, self).__init__(parent, 'Options')
                self.ha = self.variant('Bosspole', 5)

        def __init__(self, parent):
            super(Kommandos.Nob, self).__init__(parent, name='Boss Nob', points=10 + 10, gear=[Gear('Slugga'), Gear('Stikkbombs')])
            self.NobMelee(self, 'Melee weapon')
            self.Options(self)

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Kommandos.Boss, self).__init__(parent=parent, name='Boss', limit=1)
            SubUnit(self, Kommandos.Nob(parent=None))

    def __init__(self, parent):
        super(Kommandos, self).__init__(parent)
        self.models = Count(self, 'Kommando', 5, 15, 10)
        self.rl = Count(self, 'Rokkit launcha', 0, 2, 5)
        self.bs = Count(self, 'Big shoota', 0, 2, 5)
        self.brn = Count(self, 'Burna', 0, 2, 15)
        self.boss = self.Boss(self)

    def get_count(self):
        return self.models.cur + self.boss.count

    def check_rules(self):
        self.models.min = 5 - self.boss.count
        self.models.max = 15 - self.boss.count
        Count.norm_counts(0, 2, [self.bs, self.rl, self.brn])

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        model = UnitDescription(name='Kommando', points=10, options=[Gear('Choppa'), Gear('Stikkbombs')])
        common = model.clone().add(Gear('Slugga')).set_count(self.models.cur - self.bs.cur - self.rl.cur - self.brn.cur)
        desc.add(common)
        desc.add(model.clone().add(Gear('Big shoota')).add_points(5).set_count(self.bs.cur))
        desc.add(model.clone().add(Gear('Rokkit launcha')).add_points(5).set_count(self.rl.cur))
        desc.add(model.clone().add(Gear('Burna')).add_points(15).set_count(self.brn.cur))
        desc.add(self.boss.description)
        return desc


class Snikrot(Unit):
    type_name = u'Boss Snikrot, The Green Ghost'
    type_id = 'snikrot_v1'

    def __init__(self, parent):
        super(Snikrot, self).__init__(parent=parent, points=60, unique=True, gear=[
            Gear('Stikkbombs'),
            Gear('Mork\'s Teeth'),
        ])


class SkrakNobz(Unit):
    type_name = "Skrak's Skull-Nobz"
    type_id = 'skrak_v1'

    def __init__(self, parent):
        common = [Gear("'Eavy Armour"), Gear('Slugga'), Gear('Stikkbombs')]
        super(SkrakNobz, self).__init__(parent, points=190, static=True, gear=[
            UnitDescription('Nob', options=[Gear('Choppa')] + common),
            UnitDescription('Nob', options=[Gear('Kombi-weapon with skorcha'), Gear('Ammo runt')] + common),
            UnitDescription('Nob', options=[Gear('Power klaw'), Gear('Bosspole')] + common, count=2),
            UnitDescription('Skrak Head-smasha', options=[Gear('Choppa'), Gear('Bosspole')] + common)
        ])

    def get_count(self):
        return 5

    def get_unique(self):
        return 'Skrak Head-smasha'
