__author__ = 'Denis Romanow'

from builder.games.wh40k.orks_v3.armory import *
from builder.core2 import OptionsList, Gear, Count, OneOf
from builder.games.wh40k.roster import Unit


class Warboss(Unit):
    type_id = 'warboss'
    type_name = u'Warboss'

    class Weapon1(Melee, Choppa):
        pass

    class Weapon2(Ranged, Slugga):
        pass

    class Armour(OptionsList):
        def __init__(self, parent):
            super(Warboss.Armour, self).__init__(parent, 'Armour', limit=1)
            self.variant('\'eavy armour', 4)
            self.mega = self.variant('Mega armour', 40)

        def is_mega(self):
            return self.mega.value

        def set_mega(self, val):
            self.mega.used = self.mega.active = val

    def __init__(self, parent):
        super(Warboss, self).__init__(parent=parent, points=60, gear=[Gear('Stikkbombs')])
        self.armour = self.Armour(self)
        self.wep1 = self.Weapon1(self, name='Melee weapon')
        self.wep2 = self.Weapon2(self, name='Ranged weapon')
        self.mega1 = MegaMelee(self, name='Melee weapon')
        self.mega2 = MegaRanged(self, name='Ranged weapon')
        RuntsSquigs(self)
        self.wots = KnowWots(self, armour=self.armour)
        self.gifts = Gifts(self, armour=self.armour)

    def check_rules(self):
        super(Warboss, self).check_rules()
        for w in [self.wep1, self.wep2]:
            w.visible = w.used = not self.armour.is_mega()
        for w in [self.mega1, self.mega2]:
            w.visible = w.used = self.armour.is_mega()

    def get_unique_gear(self):
        return self.gifts.description


class BigMek(Unit):
    type_id = 'big_mek_v1'
    type_name = u'Big Mek'

    class Armour(OptionsList):
        def __init__(self, parent):
            super(BigMek.Armour, self).__init__(parent, 'Armour', limit=1)
            self.variant('\'eavy armour', 4)
            self.mega = self.variant('Mega armour', 40, gear=[Gear('Mega armour'), Gear('Power klaw')])

        def is_mega(self):
            return self.mega.value

    class Melee(Melee, Choppa):
        def __init__(self, parent, name):
            super(BigMek.Melee, self).__init__(parent, name=name)
            self.variant('Killsaw', 30)

    class Ranged(MekWeapon, Slugga):
        def __init__(self, parent, name):
            super(BigMek.Ranged, self).__init__(parent, name=name)
            self.variant('Kustom force field', 50)
            self.variant('Shokk attack gun', 50)

    class MegaGear(OptionsList):
        def __init__(self, parent):
            super(BigMek.MegaGear, self).__init__(parent, name='Options', limit=1)
            self.variant('Tellyport blasta', 25)
            self.variant('Kustom force field', 50)

    class MegaRanged(OneOf):
        def __init__(self, parent, name):
            super(BigMek.MegaRanged, self).__init__(parent, name=name)
            self.variant('Kustom mega-blasta', 0)
            self.variant('Shoota', 0)
            self.variant('Twin-linked shoota', 3)
            self.variant('Kombi-weapon with rokkit launcha', 5)
            self.variant('Kombi-weapon with skorcha', 10)

    def __init__(self, parent):
        super(BigMek, self).__init__(parent=parent, points=35, gear=[Gear('Stikkbombs'), Gear('Mek\'s tools')])
        self.armour = self.Armour(self)
        self.wep1 = self.Melee(self, name='Melee weapon')
        self.wep2 = self.Ranged(self, name='Ranged weapon')
        self.mega2 = self.MegaRanged(self, name='Ranged weapon')
        self.opt = self.MegaGear(self)
        RuntsSquigs(self, mek=True)
        self.wots = KnowWots(self, armour=self.armour)
        self.gifts = Gifts(self, mek=True, armour=self.armour)

    def check_rules(self):
        super(BigMek, self).check_rules()
        for w in [self.wep1, self.wep2]:
            w.visible = w.used = not self.armour.is_mega()
        for w in [self.mega2, self.opt]:
            w.visible = w.used = self.armour.is_mega()

    def get_unique_gear(self):
        return self.gifts.description


class Weirdboy(Unit):
    type_id = 'weirdboy_v1'
    type_name = u'Weirdboy'

    class Psy(OneOf):
        def __init__(self, parent):
            super(Weirdboy.Psy, self).__init__(parent, name='Psyker')
            self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 25)

    def __init__(self, parent):
        super(Weirdboy, self).__init__(parent=parent, points=45, gear=[Gear('Weirdboy staff')])
        self.mastery = self.Psy(self)

    def count_charges(self):
        return 1 + (self.mastery.cur == self.mastery.lvl2)

    def build_statistics(self):
        return self.note_charges(super(Weirdboy, self).build_statistics())


class Painboy(Unit):
    type_id = 'painboy_v1'
    type_name = u'Painboy'

    def __init__(self, parent):
        super(Painboy, self).__init__(parent=parent, points=50, gear=[Gear('\'Urty syringe'), Gear('Dok\'s tools')])
        KnowWots(self)
        RuntsSquigs(self, doc=True)


class Mek(Unit):
    type_id = 'mek_v1'
    type_name = u'Mek'

    model_points = 15
    killsaw_cost = 30

    class Weapon(MekWeapon, Slugga):
        pass

    class Melee(Choppa):
        def __init__(self, parent):
            super(Mek.Melee, self).__init__(parent, name='Melee weapon')
            self.variant('Killsaw', self.parent.killsaw_cost)

    class Oiler(OptionsList):
        def __init__(self, parent):
            super(Mek.Oiler, self).__init__(parent, '')
            self.variant('Grot Oiler', 5)

    def __init__(self, parent):
        super(Mek, self).__init__(parent=parent, points=self.model_points, gear=[Gear('Stikkbombs'), Gear('Mek\'s tools')])
        self.Melee(self)
        self.Weapon(self, name='Ranged weapon')
        self.Oiler(self)

    def build_statistics(self):
        return {'Models': 1}


class Grotsnik(Unit):
    type_name = u'Mad Dok Grotsnik, Da Painboss'
    type_id = 'grotsnik_v1'

    def __init__(self, parent):
        super(Grotsnik, self).__init__(parent=parent, name='Mad Dok Grotsnik', points=160, unique=True, gear=[
            Gear('Slugga'),
            Gear('Power klaw'),
            Gear('\'Urty syringe'),
            Gear('Dok\'s tools'),
            Gear('Cybork body')
        ])


class Badrukk(Unit):
    type_name = u'Kaptin Badrukk, Da Freeboota King'
    type_id = 'badrukk_v1'

    def __init__(self, parent):
        super(Badrukk, self).__init__(parent=parent, name='Kaptin Badrukk', points=110, unique=True, gear=[
            Gear('Slugga'),
            Gear('Choppa'),
            Gear('Stikkbombs'),
            Gear('Bosspole'),
            Gear('Gitfinda'),
            Gear('Goldtoof Armour'),
            Gear('Da Rippa'),
        ])
        Count(self, 'Ammo runt', 0, 3, points=3)


class Zagstruk(Unit):
    type_name = u'Boss Zagstruk'
    type_id = 'zagstruk_v1'

    def __init__(self, parent):
        super(Zagstruk, self).__init__(parent=parent, points=65, unique=True, gear=[
            Gear('Slugga'),
            Gear('Choppa'),
            Gear('\'Eavy armour'),
            Gear('Stikkbombs'),
            Gear('Cybork body'),
            Gear('Rokkit pack'),
            Gear('Da Vulcha\'s Klaws'),
        ])


class Grukk(Unit):
    type_name = u'Grukk Face-Rippa, Scourge of the Sanctus Reach'
    type_id = 'grukk_v1'

    def __init__(self, parent):
        super(Grukk, self).__init__(parent=parent, name='Grukk Face-Rippa', points=130, unique=True, gear=[
            Gear('\'Eavy armour'),
            Gear('Kombi-weapon with rokkit launcha'),
            Gear('Attack squig'),
            Gear('Bosspole'),
            Gear('Stikkbombs'),
            Gear('Git-rippa'),
        ])
