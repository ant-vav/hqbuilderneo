__author__ = 'Ivan Truskov'
ia_suffix = ' (IA vol.1)'


from builder.core2 import OneOf, Gear
from builder.games.wh40k.unit import Unit
from armory import VehicleOptions


class SalamanderCommand(Unit):
    clear_name = 'Salamander Command Vehicle'
    type_name = clear_name + ia_suffix
    type_id = 'salamander_command_ia_v1'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SalamanderCommand.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Heavy flamer')
            self.variant('Heavy bolter')

    def __init__(self, parent):
        super(SalamanderCommand, self).__init__(parent=parent, name='Salamander Command', points=60,
                                                gear=[Gear('Heavy bolter'), Gear('Auspex Surveyour'),
                                                      Gear('Searchlight'), Gear('Smoke launchers')])
        self.Weapon(self)
        VehicleOptions(self, netting=20)


class Atlas(Unit):
    clear_name = 'Atlas Recovery Tank'
    type_name = clear_name + ia_suffix
    type_id = 'atlas_ia_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Atlas, self).__init__(parent, 'Atlas', 85,
                                    [Gear('Heavy bolter'),
                                     Gear('Searchlight'),
                                     Gear('Smoke launchers')])
        VehicleOptions(self, netting=20)
