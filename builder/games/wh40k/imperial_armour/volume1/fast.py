__author__ = 'Ivan Truskov'
ia_suffix = ' (IA vol.1)'


from builder.core2 import OptionsList, OneOf, Gear, OptionalSubUnit, SubUnit
from builder.games.wh40k.unit import Unit
from armory import VehicleOptions, CamoSquadron


class Chimera(Unit):
    type_name = 'Chimera' + ia_suffix
    type_id = 'chimera_ia_v1'
    imperial_armour = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Heavy bolter', 0)
            self.variant('Heavy flamer', 0)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Multi-laser', 0)
            self.variant('Heavy bolter', 0)
            self.variant('Heavy flamer', 0)
            self.variant('Autocannon', 5)
            self.variant('Twin-linked heavy bolters', 10)

    def __init__(self, parent):
        super(Chimera, self) .__init__(parent=parent, name='Chimera', points=55, gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.Weapon1(self)
        self.Weapon2(self)
        VehicleOptions(self, netting=20)


class Hellhound(Unit):
    type_name = 'Hellhound'

    def __init__(self, parent):
        super(Hellhound, self).__init__(parent=parent)
        self.type = self.Type(self)
        self.Weapon(self)
        VehicleOptions(self, light=1)

    class Type(OneOf):
        def __init__(self, parent):
            super(Hellhound.Type, self).__init__(parent=parent, name='Type')
            self.hellhound = self.variant('Hellhound', 130, gear=[Gear('Inferno cannon')])
            self.devildog = self.variant('Devil dog', 120, gear=[Gear('Melta cannon')])
            self.banewolf = self.variant('Banewolf', 130, gear=[Gear('Chem cannon')])

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Hellhound.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.multymelta = self.variant('Multi-melta', 15)

    def build_description(self):
        desc = super(Hellhound, self).build_description()
        desc.name = self.type.cur.title
        return desc


class HellhoundSquadron(CamoSquadron):
    clear_name = 'Hellhound Squadron'
    type_name = clear_name + ia_suffix
    type_id = 'hellhounds_ia_v1'
    unit_class = Hellhound
    imperial_armour = True
    launchers = 5


class Centaur(Unit):
    type_name = 'Centaur Carrier'
    type_id = 'centaur_ia_v1'

    class VehicleOptions(OptionsList):
        def __init__(self, parent):
            super(Centaur.VehicleOptions, self).__init__(parent, 'Options')
            self.variant('Hunter-killer missile', 10)
            self.variant('Dozer blade', 5)
            self.variant('Extra armour', 10)

    def __init__(self, parent):
        super(Centaur, self).__init__(parent, points=40, gear=[
            Gear('Heavy stubber'), Gear('Searchlight'), Gear('Smoke launchers')
        ])
        self.VehicleOptions(self)


class CentaurSquadron(CamoSquadron):
    clear_name = 'Centaur Carrier Squadron'
    type_name = clear_name + ia_suffix
    type_id = 'centaurs_ia_v1'
    unit_class = Centaur
    imperial_armour = True


class Salamander(Unit):
    type_name = 'Salamander Scout'

    def __init__(self, parent):
        super(Salamander, self).__init__(parent=parent, points=55,
                                         gear=[Gear('Heavy bolter'), Gear('Autocannon'),
                                               Gear('Searchlight'), Gear('Smoke launchers')])
        VehicleOptions(self)


class SalamanderSquadron(CamoSquadron):
    clear_name = 'Salamander Recon Squadron'
    type_name = clear_name + ia_suffix
    type_id = 'salamanders_ia_v1'
    unit_class = Salamander
    imperial_armour = True


class Tauros(Unit):
    type_name = 'Tauros'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Tauros.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Heavy flamer')
            self.variant('Tauros grenade launcher', 5)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Tauros.Weapon2, self).__init__(parent, 'Weapon')
            self.variant('Twin-linked multi-laser')
            self.variant('Twin-linked lascannon', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Tauros.Options, self).__init__(parent, 'Options')
            self.up = self.variant('Upgrade to Tauros Venator', 20, gear=[])
            self.variant('Hunter-killer missile', 10)
            self.variant('Extra armour', 15)

    def __init__(self, parent):
        super(Tauros, self).__init__(parent, points=40,
                                     gear=[Gear('Searchlight')])
        self.w1 = self.Weapon1(self)
        self.w2 = self.Weapon2(self)
        self.opt = self.Options(self)

    def check_rules(self):
        super(Tauros, self).check_rules()
        self.w1.used = self.w1.visible = not self.opt.up.value
        self.w2.used = self.w2.visible = self.opt.up.value

    def build_description(self):
        res = super(Tauros, self).build_description()
        if self.opt.up.value:
            res.name = 'Tauros Venator'
        return res


class TaurosSquadron(CamoSquadron):
    clear_name = 'Tauros Squadron'
    type_name = clear_name + ia_suffix
    type_id = 'tauroses_ia_v1'
    unit_class = Tauros
    launchers = 5
    netting = 10
    imperial_armour = True


class DropSentinel(Unit):
    type_name = 'Drop Sentinel'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DropSentinel.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Heavy bolter')
            self.variant('Heavy flamer')
            self.variant('Multi-melta', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DropSentinel.Options, self).__init__(parent, 'Options')
            self.variant('Searchlight', 1)
            self.variant('Hunter-killer missile', 10)

    def __init__(self, parent):
        super(DropSentinel, self).__init__(parent, points=55)
        self.Weapon(self)
        self.Options(self)


class DropSquadron(CamoSquadron):
    clear_name = 'Drop Sentinel Squadron'
    type_name = clear_name + ia_suffix
    type_id = 'dropsents_ia_v1'
    unit_class = DropSentinel
    launchers = 5
    netting = 10
    imperial_armour = True

    class SkyTalon(Unit):
        type_name = 'Valkyrie Sky Talon'
        type_id = 'skytalon_ia_v1'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(DropSquadron.SkyTalon.Weapon, self).__init__(parent, 'Missiles')
                self.variant('Two hellstrike missiles',
                             gear=[Gear('Hellstrike missile', count=2)])
                self.variant('Two multiple rocket pods', 30,
                             gear=[Gear('Multiple rocket pod', count=2)])

        def __init__(self, parent):
            super(DropSquadron.SkyTalon, self).__init__(parent, 'Sky Talon', 70,
                                                        [Gear('Heavy bolter'),
                                                         Gear('Searchlight'),
                                                         Gear('Extra armour')])
            self.Weapon(self)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(DropSquadron.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, DropSquadron.SkyTalon(parent=None))

    def __init__(self, parent):
        super(DropSquadron, self).__init__(parent)
        self.transport = self.Transport(self)

    def check_rules(self):
        super(DropSquadron, self).check_rules()
        self.transport.used = self.transport.visible = self.vehicles.count <= 2

    def build_points(self):
        return super(DropSquadron, self).build_points() + self.transport.points

    def build_statistics(self):
        return self.note_transport(super(DropSquadron, self).build_statistics())


class ScoutSentinel(Unit):
    type_name = 'Scout Sentinel'

    def __init__(self, parent):
        super(ScoutSentinel, self).__init__(parent=parent, points=35)
        self.Weapon(self)
        DropSentinel.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ScoutSentinel.Weapon, self).__init__(parent=parent, name='Weapon')
            self.multilaser = self.variant('Multi-laser', 0)
            self.heavyflamer = self.variant('Heavy flamer', 5)
            self.autocannon = self.variant('Autocannon', 5)
            self.lascannon = self.variant('Lascannon', 15)
            self.missilelauncher = self.variant('Missile launcher', 10)
            self.variant('Multiple rocjet pod', 10)


class ScoutSentinelSquadron(CamoSquadron):
    clear_name = 'Scout Sentinel Squadron'
    type_name = clear_name + ia_suffix
    type_id = 'scoutsentinels_ia_v1'
    unit_class = ScoutSentinel
    launchers = 5
    netting = 10
    imperial_armour = True


class ArmouredSentinel(Unit):
    type_name = 'Armoured Sentinel'

    def __init__(self, parent):
        super(ArmouredSentinel, self).__init__(parent=parent, points=40)
        self.Weapon(self)
        DropSentinel.Options(self)

    class Weapon(ScoutSentinel.Weapon):
        def __init__(self, parent):
            super(ArmouredSentinel.Weapon, self).__init__(parent=parent)
            self.plasmacannon = self.variant('Plasma cannon', 20)


class ArmouredSentinelSquadron(CamoSquadron):
    clear_name = 'Armoured Sentinel Squadron'
    type_name = clear_name + ia_suffix
    type_id = 'armouredsentinels_ia_v1'
    unit_class = ArmouredSentinel
    launchers = 5
    netting = 10
    imperial_armour = True
