__author__ = 'Ivan Truskov'
from builder.core.unit import Unit


class MalcadorInfernus(Unit):
    name = 'Malcador \'Infernus\''
    base_points = 270
    gear = ['Searchlight', 'Smoke launchers', 'Inferno Gun']

    def __init__(self):
        Unit.__init__(self)
        self.amm = self.opt_one_of('Inferno Gun Ammunition', [
            ['Standard Flammable Fuel', 0, 'sff'],
            ['Chemical Ammunition', 10, 'camm']
        ])
        self.spons = self.opt_one_of('Sponson-mounted weapons', [
            ['Heavy Stubber', 0, 'hstb'],
            ['Heavy Bolter', 5, 'hbgun'],
            ['Heavy flamer', 5, 'hflame'],
            ['Autocannon', 20, 'acan'],
            ['Lascannon', 30, 'lcan']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Pintle-mounted Heavy Stubber', 10, 'hstb'],
            ['Hunter-killer Missile', 10, 'hkm']
        ])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(exclude=[self.spons.id])
        self.description.add('Sponson-mounted ' + self.spons.get_selected(), 2)


class Praetor(Unit):
    name = 'Praetor Armored Assault Launcher'
    base_points = 300
    gear = ['Praetor Launcher']

    def __init__(self):
        Unit.__init__(self)
        self.spons1 = self.opt_one_of('Sponson-mounted weapon', [
            ['Heavy Bolter', 0, 'hbgun'],
            ['Heavy flamer', 0, 'hflame'],
            ['Autocannon', 5, 'acan'],
            ['Lascannon', 10, 'lcan']
        ])
        self.spons2 = self.opt_one_of('Sponson-mounted weapon', [
            ['Heavy Bolter', 0, 'hbgun'],
            ['Heavy flamer', 0, 'hflame'],
            ['Autocannon', 5, 'acan'],
            ['Lascannon', 10, 'lcan']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Hunter-killer Missile', 10, 'hkm'],
            ['Dozer Blade', 10, 'dbld']
        ])
        self.pintle = self.opt_options_list('', [
            ['Pintle-mounted Heavy Stubber', 10, 'hstb'],
            ['Pintle-mounted Storm Bolter', 10, 'hsbgun']
        ], 1)


class Crassus(Unit):
    name = 'Crassus Armored Assault Transport'
    base_points = 250
    gear = ['Searchlight', 'Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.spons1 = self.opt_one_of('Sponson-mounted weapon', [
            ['Heavy Bolter', 0, 'hbgun'],
            ['Heavy flamer', 0, 'hflame'],
            ['Autocannon', 5, 'acan'],
            ['Lascannon', 10, 'lcan']
        ])
        self.spons2 = self.opt_one_of('Sponson-mounted weapon', [
            ['Heavy Bolter', 0, 'hbgun'],
            ['Heavy flamer', 0, 'hflame'],
            ['Autocannon', 5, 'acan'],
            ['Lascannon', 10, 'lcan']
        ])
        self.side = self.opt_one_of('Side equipment', [
            ['Two side sponsons with Heavy Bolters', 0, 'ssp'],
            ['Side armour plates', 0, 'sarm']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Hunter-killer Missile', 10, 'hkm'],
            ['Dozer Blade', 10, 'dbld']
        ])
        self.pintle = self.opt_options_list('', [
            ['Pintle-mounted Heavy Stubber', 10, 'hstb'],
            ['Pintle-mounted Storm Bolter', 10, 'hsbgun']
        ], 1)
