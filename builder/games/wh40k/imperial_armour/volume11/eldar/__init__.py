__author__ = 'Denis Romanow'

from hq import *
from fast import *
from heavy import *
from superheavy import *
from elites import *

from builder.games.wh40k.eldar_v3.elites import Wraithblades, Banshees,\
    Scorpions, Dragons
from builder.games.wh40k.eldar_v3.troops import GuardianDefenders,\
    StormGuardians, Rangers, Avengers, GuardianHeavyWeaponTeam
from builder.games.wh40k.eldar_v3.hq import Autarch, Farseer, Warlocks,\
    Spiritseer
from builder.games.wh40k.eldar_v3.heavy import Battery, Wraithlord,\
    DarkReapers, WarWalkers, FirePrisms, NightSpinners
from builder.games.wh40k.eldar_v3.fast import Hawks, Spiders, Spears,\
    VyperSquadron
from builder.games.wh40k.eldar_v3.lords import Wraithknight
from builder.games.wh40k.harlequins.troops import Troupe
from builder.games.wh40k.corsairs_v2.troops import ReaverBand

from builder.games.wh40k.section import LordsOfWarSection, HQSection,\
    HeavySection, FastSection, ElitesSection, Formation, UnitType


class IA11HQSection(HQSection):
    def __init__(self, parent):
        super(IA11HQSection, self).__init__(parent)

        self.irillyth = UnitType(self, Irillyth)
        self.belannath = UnitType(self, BelAnnath)
        self.wraithseer = UnitType(self, Wraithseer)


class IA11FastSection(FastSection):
    def __init__(self, parent):
        super(IA11FastSection, self).__init__(parent)
        self.spectres = UnitType(self, Spectres)
        UnitType(self, Hornets)
        UnitType(self, Nightwing)
        UnitType(self, Phoenix)


class IA11ElitesSection(ElitesSection):
    def __init__(self, parent):
        super(IA11ElitesSection, self).__init__(parent)
        UnitType(self, Wasps)


class IA11HeavySection(HeavySection):
    def __init__(self, parent):
        super(IA11HeavySection, self).__init__(parent)
        UnitType(self, Lynx)
        UnitType(self, WarpHunters)


class IA11LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(IA11LordsOfWar, self).__init__(parent)
        UnitType(self, Scorpion)
        UnitType(self, Cobra)
        UnitType(self, VampireRaider)
        UnitType(self, SkathachWraithknight)
        UnitType(self, Revenant)
        UnitType(self, Phantom)


class UndyingHostLord(Formation):
    army_name = 'Lord of the Undying Host'
    army_id = 'ia11_host_lord'
    imperial_armour = True

    def __init__(self):
        super(UndyingHostLord, self).__init__()
        UnitType(self, Wraithseer, min_limit=1, max_limit=1)
        UnitType(self, Wraithblades, min_limit=1, max_limit=3)


class SpectresShrine(Formation):
    army_name = 'Shadow Spectres Shrine'
    army_id = 'ia11_spectre_shrine'
    imperial_armour = True

    def __init__(self):
        super(SpectresShrine, self).__init__()
        self.spectres = UnitType(self, Spectres, min_limit=3, max_limit=3)

    def check_rules(self):
        super(SpectresShrine, self).check_rules()
        exarch_count = sum(u.has_exarch() for u in self.spectres.units)
        if exarch_count > 1:
            self.error("Only one unit in the Formation may include an Exarch")


class HawksSquadron(Formation):
    army_name = 'Khaine\'s hawks squadron'
    army_id = 'ia11_hawks'
    imperial_armour = True

    class Nightwing(Nightwing):
        def __init__(self, parent):
            super(HawksSquadron.Nightwing, self).__init__(parent)
            self.models.min, self.models.max = (2, 3)

    def __init__(self):
        super(HawksSquadron, self).__init__()
        UnitType(self, self.Nightwing, min_limit=1, max_limit=1)


class PhoenixSquadron(Formation):
    army_name = 'Fires of the Phoenix'
    army_id = 'ia11_phoenixes'
    imperial_armour = True

    class Phoenix(Phoenix):
        def __init__(self, parent):
            super(PhoenixSquadron.Phoenix, self).__init__(parent)
            self.models.min = 2

    def __init__(self):
        super(PhoenixSquadron, self).__init__()
        UnitType(self, self.Phoenix, min_limit=1, max_limit=1)


class WarpHunterSquadron(Formation):
    army_name = 'Fist of Vaul'
    army_id = 'ia11_warp_hunters'
    imperial_armour = True

    class WarpHunters(WarpHunters):
        def __init__(self, parent):
            super(WarpHunterSquadron.WarpHunters, self).__init__(parent)
            self.models.update_range(2, 3)

    def __init__(self):
        super(WarpHunterSquadron, self).__init__()
        self.wh = UnitType(self, self.WarpHunters, min_limit=1, max_limit=1)

    # def check_rules(self):
    #     super(WarpHunterSquadron, self).check_rules()
    #     cannon_count = 0
    #     unit_count = 0
    #     for unit in self.wh.units:
    #         unit_count += unit.get_count
    #         cannon_count += sum(u.has_cannon() for u in unit.models.units)
    #     if unit_count != cannon_count and cannon_count > 0:
    #         self.error("All Warp Hunters in this formation must have the same upgrades")


class HornetSquadron(Formation):
    army_name = 'Hornet Swarm'
    army_id = 'ia11_hornets'
    imperial_armour = True

    class Hornets(Hornets):
        def __init__(self, parent):
            super(HornetSquadron.Hornets, self).__init__(parent)
            self.models.update_range(3, 6)

    def __init__(self):
        super(HornetSquadron, self).__init__()
        UnitType(self, self.Hornets, min_limit=1, max_limit=1)


class WaspPhalanx(Formation):
    army_name = 'Wasp Phalanx'
    army_id = 'ia11_wasps'
    imperial_armour = True

    class Wasps(Wasps):
        def __init__(self, parent):
            super(WaspPhalanx.Wasps, self).__init__(parent)
            self.models.update_range(3, 6)

    def __init__(self):
        super(WaspPhalanx, self).__init__()
        UnitType(self, self.Wasps, min_limit=1, max_limit=1)


class HammerOfVaul(Formation):
    army_name = 'Hammer of Vaul'
    army_id = 'ia11_hammer'
    imperial_armour = True

    def __init__(self):
        super(HammerOfVaul, self).__init__()
        cobra = UnitType(self, Cobra)
        scorp = UnitType(self, Scorpion)
        self.add_type_restriction([cobra, scorp], 1, 1)


class Skyhunters(Formation):
    army_name = 'Skyhunters squadron'
    army_id = 'ia11_skyhunters'
    imperial_armour = True

    def __init__(self):
        super(Skyhunters, self).__init__()
        self.lynx = UnitType(self, Lynx, min_limit=1, max_limit=3)

    def check_rules(self):
        super(Skyhunters, self).check_rules()
        if not OptionsList.same_options([unit.opt for unit in self.lynx.units]):
            self.error("All Lynx grav-tanks from this formation must take the same upgrades")


class Skyreavers(Formation):
    army_name = 'Skyreaver Raiding echelon'
    army_id = 'ia11_skureavers'
    imperial_armour = True

    def __init__(self):
        super(Skyreavers, self).__init__()
        UnitType(self, VampireRaider, min_limit=1, max_limit=1)
        UnitType(self, Autarch, min_limit=1, max_limit=1)
        self.battery = UnitType(self, Battery, min_limit=1, max_limit=1)
        self.guards = UnitType(self, GuardianDefenders)
        self.storm = UnitType(self, StormGuardians)

    def check_rules(self):
        super(Skyreavers, self).check_rules()
        if self.guards.count < self.battery.count:
            self.error("This Formation may not include more Vaul\s Wrath Support Batteries then it does Guardian Defender units")
        if self.guards.count + self.storm.count < 1:
            self.error("This formation must include at least one Guardian Defender or Storm Guardian unit")


class WraithTitans(Formation):
    army_name = 'Wraith Titans'
    army_id = 'ia11_titans'
    imperial_armour = True

    def __init__(self):
        super(WraithTitans, self).__init__()
        self.add_type_restriction([
            UnitType(self, Revenant),
            UnitType(self, Phantom)
        ], 1, 1)
        UnitType(self, Wraithknight, min_limit=0, max_limit=2)


class PaleCourtsBattlehost(Formation):
    army_name = 'Pale Courts Battlehost'
    army_id = 'pale_court_v1'

    class SupplementOptions(OneOf):
        def __init__(self, parent):
            super(PaleCourtsBattlehost.SupplementOptions, self).__init__(parent=parent, name='Codex')
            self.base = self.variant(name=PaleCourtsBattlehost.army_name)
            self.iyanden = self.variant(name='Iyanden')

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_iyanden(self):
        return self.codex.cur == self.codex.iyanden

    class Traits(OptionsList):
        def __init__(self, parent):
            super(PaleCourtsBattlehost.Traits, self).__init__(
                parent, 'Craftworld traits', limit=2)
            self.khaine = self.variant('Children of Khaine')
            self.lonely = self.variant('On lonely paths')
            self.crossroads = self.variant('Crossroads of Eternity')
            self.vaul = self.variant('Disciples of Vaul')
            self.graveyard = self.variant('Graveyard of Dreams')
            self.tomb = self.variant('Tomb-ship of Fallen Heroes')
            self.aspect = self.variant('Aspect Lord-shrine')
            self.fortress = self.variant('Fortress of Discipline')
            self.anger = self.variant('Swift to Anger')
            self.martial = self.variant('Halls of Martial Splendour')
            self.strong = self.variant('The Strong Stand Alone')

        # should execute before all rules checks
        def check_rules(self):
            super(PaleCourtsBattlehost.Traits, self).check_rules()
            self.parent.set_default_restrictions()
            if self.lonely.value:
                self.parent.ranger.min_limit, self.parent.ranger.max_limit = (0, 3)
            if self.crossroads.value:
                self.parent.troupe.min_limit, self.parent.troupe.max_limit = (0, 1)

                self.parent.reavers.min_limit, self.parent.reavers.max_limit = (0, 1)

            if self.vaul.value:
                self.parent.battery.min_limit, self.parent.battery.max_limit = (1, 3)
            for unit in self.parent.defenders.units:
                unit.more_guns = self.vaul.value

            if self.tomb.value:
                self.parent.farseer.min_limit, self.parent.farseer.max_limit = (0, 0)
                self.parent.spirit.max_limit = self.parent.wraithseer.max_limit = 1
                self.parent.add_type_restriction([self.parent.spirit, self.parent.wraithseer], 1, 1)
                self.parent.wraithlord.min_limit, self.parent.wraithlord.max_limit = (0, 1)

            if self.graveyard.value:
                self.parent.defenders.min_limit = 0
                self.parent.blades.min_limit = self.parent.blades.max_limit = 3

            if self.aspect.value:
                self.parent.defenders.min_limit = 0
                for aspect_type in self.parent.aspects:
                    aspect_type.max_limit = 3

            # 1 or 1-3 part is handled later on
            if self.fortress.value:
                self.parent.walkers.min_limit, self.parent.walkers.max_limit = (0, 3)
                self.parent.wasp.max_limit = 1

            if self.anger.value:
                self.parent.vipers.min_limit, self.parent.vipers.max_limit = (0, 3)
                self.parent.hornet.max_limit = 1

            if self.martial.value:
                self.parent.farseer.min_limit, self.parent.farseer.max_limit = (0, 0)
                self.parent.autarch.min_limit, self.parent.autarch.max_limit = (1, 1)

            if self.strong.value:
                for engine_type in self.parent.engines:
                    engine_type.max_limit = 1
                self.parent.add_type_restriction(self.parent.engines, 1, 1)

            if self.khaine.value:
                self.parent.storm.min_limit = self.parent.defenders.min_limit
                self.parent.storm.max_limit = self.parent.defenders.max_limit
                self.parent.defenders.max_limit, self.parent.defenders.min_limit = (0, 0)

            self.parent.set_allowances()

    class VaulDefenders(GuardianDefenders):
        def __init__(self, parent):
            super(PaleCourtsBattlehost.VaulDefenders, self).__init__(parent)
            self.more_guns = False
            self.hwt3 = GuardianHeavyWeaponTeam(parent=self, discount=self.discount)
            self.hwt4 = GuardianHeavyWeaponTeam(parent=self, discount=self.discount)

        def check_rules(self):
            if self.more_guns:
                self.hwt2.used = self.hwt2.visible = True
                self.hwt3.used = self.hwt3.visible = self.get_count() >= 15
                self.hwt4.used = self.hwt4.visible = self.get_count() == 20
            else:
                self.hwt3.used = self.hwt3.visible = False
                self.hwt4.used = self.hwt4.visible = False
                super(PaleCourtsBattlehost.VaulDefenders, self).check_rules()

    def __init__(self):
        super(PaleCourtsBattlehost, self).__init__()
        self.codex = self.SupplementOptions(self)
        self.traits = self.Traits(self)
        self.farseer = UnitType(self, Farseer)
        self.defenders = UnitType(self, self.VaulDefenders)
        self.council = UnitType(self, Warlocks, min_limit=0, max_limit=1)
        self.storm = UnitType(self, StormGuardians)
        self.ranger = UnitType(self, Rangers)
        self.troupe = UnitType(self, Troupe)
        self.reavers = UnitType(self, ReaverBand)
        self.battery = UnitType(self, Battery)
        self.blades = UnitType(self, Wraithblades)
        self.spirit = UnitType(self, Spiritseer)
        self.wraithseer = UnitType(self, Wraithseer)
        self.wraithlord = UnitType(self, Wraithlord)
        self.dire = UnitType(self, Avengers)
        self.aspects = [
            self.dire,
            UnitType(self, Hawks),
            UnitType(self, Spiders),
            UnitType(self, Spears),
            UnitType(self, Banshees),
            UnitType(self, Scorpions),
            UnitType(self, Dragons),
            UnitType(self, DarkReapers),
            UnitType(self, Spectres)
        ]
        self.walkers = UnitType(self, WarWalkers)
        self.wasp = UnitType(self, Wasps)
        self.vipers = UnitType(self, VyperSquadron)
        self.hornet = UnitType(self, Hornets)
        self.autarch = UnitType(self, Autarch)
        self.engines = [
            UnitType(self, FirePrisms),
            UnitType(self, NightSpinners),
            UnitType(self, WarpHunters)
        ]

    def set_default_restrictions(self):
        self.farseer.min_limit, self.farseer.max_limit = (1, 1)
        self.defenders.min_limit, self.defenders.max_limit = (3, 3)
        for other_type in [
                self.storm, self.ranger, self.troupe, self.reavers,
                self.battery, self.blades, self.spirit, self.wraithseer,
                self.wraithlord, self.walkers, self.wasp, self.vipers,
                self.hornet, self.autarch] + self.aspects + self.engines:
            other_type.min_limit, other_type.max_limit = (0, 0)

    def set_allowances(self):
        for other_type in [
                self.storm, self.ranger, self.troupe, self.reavers,
                self.battery, self.blades, self.spirit, self.wraithseer,
                self.wraithlord, self.walkers, self.wasp, self.vipers,
                self.hornet, self.autarch, self.farseer, self.defenders]\
                + self.aspects + self.engines:
            other_type.active = other_type.max_limit > 0

    def check_rules(self):
        super(PaleCourtsBattlehost, self).check_rules()
        if self.traits.fortress.value:
            if (self.walkers.count > 0 and self.wasp.count > 0) or\
               (self.walkers.count == 0 and self.wasp.count == 0):
                self.error('Battlehost must include 1-3 units of War Walkers or 1 unit of Wasp Assault Walkers')
        if self.traits.anger.value:
            if (self.vipers.count > 0 and self.hornet.count > 0) or\
               (self.vipers.count == 0 and self.hornet.count == 0):
                self.error('Battlehost must include 1-3 units of Vypers or 1 unit of Hornets')
        if self.traits.aspect.value:
            aspect_count = 0
            for aspect_type in self.aspects:
                aspect_count += aspect_type.count
                if aspect_count not in [0, 3]:
                    break
            if aspect_count != 3:
                self.error('Battlehost must include 3 units of the same aspect')
