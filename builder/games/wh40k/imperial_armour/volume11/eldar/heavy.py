__author__ = 'Denis Romanov'

from builder.core2 import OneOf, Gear, ListSubUnit, UnitList,\
    UnitDescription
from builder.games.wh40k.eldar_v3.armory import VehicleEquipment
from builder.games.wh40k.roster import Unit

ia_id = ' (IA vol.11)'


class Lynx(Unit):
    clear_name = 'Lynx'
    type_id = 'ia11_lynx_v1'
    type_name = 'Eldar Lynx' + ia_id
    imperial_armour = True

    def __init__(self, parent):
        super(Lynx, self).__init__(parent=parent, points=255, name=self.clear_name)
        self.Weapon1(self)
        self.Weapon2(self)
        self.opt = VehicleEquipment(self, squadron=False)
        #self.Options(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Lynx.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.pulsar = self.variant('Pulsar', 0)
            self.soniclance = self.variant('Sonic Lance', 0)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Lynx.Weapon2, self).__init__(parent=parent, name='')
            self.shurikencannon = self.variant('Shuriken Cannon', 0)
            self.scatterlaser = self.variant('Scatter Laser', 0)
            self.starcannon = self.variant('Starcannon', 5)
            self.brightlance = self.variant('Bright Lance', 5)


class WarpHunters(Unit):
    type_name = 'Eldar Warp Hunter Squadron' + ia_id
    type_id = "warphunters_v2"

    class WarpHunter(ListSubUnit):
        type_name = "Warp Hunter"
        type_id = "warphunter_v2"

        class Weapon(OneOf):
            def __init__(self, parent):
                super(WarpHunters.WarpHunter.Weapon, self).__init__(parent, 'Secondary weapon')
                self.variant('Twin-linked shuriken catapult')
                self.cannon = self.variant('Shuriken cannon', 10)

        def __init__(self, parent):
            super(WarpHunters.WarpHunter, self).__init__(parent=parent, points=185, gear=[
                Gear('D-Flail')
            ], )
            self.wep = self.Weapon(self)

        def build_description(self):
            desc = super(WarpHunters.WarpHunter, self).build_description()
            desc.add(self.root_unit.opt.description).add_points(self.root_unit.opt.points)
            return desc

        @ListSubUnit.count_gear
        def has_cannon(self):
            return self.wep.cur == self.wep.cannon

    def __init__(self, parent):
        super(WarpHunters, self).__init__(parent, name='Warp Hunters Squadron')
        self.models = UnitList(self, self.WarpHunter, 1, 3)
        self.opt = VehicleEquipment(self)

    def build_points(self):
        return super(WarpHunters, self).build_points()\
            + self.opt.points * (self.models.count - 1)

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        res.add(self.models.description)
        return res

    def get_count(self):
        return self.models.count
