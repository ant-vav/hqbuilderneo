__author__ = 'Ivan Truskov'

from builder.core2 import Gear, UpgradeUnit
from builder.games.wh40k.space_marines_v3.elites import SternguardVeteranSquad as CodexSternguards
from builder.games.wh40k.roster import Unit


class Culln(Unit):
    type_name = 'Commander Carab Culln, Captain of the 1st company  (IA vol.4)'
    type_id = 'cap_culln_v1'
    imperial_armour = True
    chapter = 'scorpions'

    def __init__(self, parent):
        super(Culln, self).__init__(parent, 'Commander Carab Culln', 165, [
            Gear('Master-crafted storm bolter'),
            Gear('Iron Halo'),
            Gear('Tears of the Scorpion (sword)'),
            Gear('Terminator armour'),
            Gear('Teleport homer')
        ], static=True)

    def get_unique(self):
        return 'Carab Culln'


class Halar(Unit):
    type_name = 'Dreadnought-brother Halar, Martyr of Endikon  (IA vol.4)'
    type_id = 'halar_v1'
    imperial_armour = True
    chapter = 'scorpions'

    def __init__(self, parent):
        super(Halar, self).__init__(
            parent=parent, name='Halar',
            points=265, unique=True, gear=[
                Gear('Extra armour'),
                Gear('Smoke launcher'),
                Gear('Searchlight'),
                Gear('Power fist'),
                Gear('Storm bolter'),
                Gear('Flamestorm cannon')
            ], static=True)


class HaasSternguards(CodexSternguards):

    class Haas(UpgradeUnit, CodexSternguards.Sergeant):
        def __init__(self, parent):
            super(HaasSternguards.Haas, self).__init__(
                parent, 'Veteran Sergeant Haas', 45, [
                    Gear('Power sword'), Gear('Bolt pistol'),
                    Gear('Frag grenades'), Gear('Krak grenades'),
                    Gear('Artificer armour')])

        def check_rules(self):
            self.up.used = self.up.visible = self.roster.is_scorpions()\
                                             and self.roster.ia_enabled
            if not self.up.used:
                self.up.value = False
            super(HaasSternguards.Haas, self).check_rules()

    def get_leader(self):
        return self.Haas

    def get_unique(self):
        return 'Veteran Sergeant Haas' if self.leader.unit.up.any else None

    # @property
    # def chapter(self):
    #     return 'scorpions' if self.leader.unit.up.any else None
