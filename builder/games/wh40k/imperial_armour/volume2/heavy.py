from builder.core2 import *
from options import SpaceMarinesBaseVehicle

__author__ = 'dante'


class LandRaiderHelios(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_helios_v1'
    type_name = "Land Raider Helios (IA vol.2)"
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent, armour=True):
            super(LandRaiderHelios.Options, self).__init__(parent, 'Options')
            self.hkm = self.variant('Hunter-killer missile', 10)
            self.dblade = self.variant('Dozer blade', 5)
            if armour:
                self.exarm = self.variant('Extra armour', 10)
            self.sbgun = self.variant('Storm bolter', 5)
            self.melta = self.variant('Multi-melta', 10)

        def check_rules(self):
            self.process_limit([self.melta, self.sbgun], 1)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(LandRaiderHelios.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Whirlwind missile launcher', 0)
            self.variant('Hyperios air defence missile launcher', 20)

    def __init__(self, parent):
        super(LandRaiderHelios, self).__init__(name="Land Raider Helios", parent=parent, points=260, gear=[
            Gear('Twin-linked lascannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.Weapon(self)
        self.Options(self)


class LandRaiderProteus(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_proteus_v1'
    type_name = "Land Raider Proteus (IA vol.2)"
    imperial_armour = True

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(LandRaiderProteus.Weapon, self).__init__(parent, 'Options')
            self.bolt = self.variant('Twin-linked heavy bolter', 20)
            self.flame = self.variant('Twin-linked heavy flamer', 20)
            self.variant('Explorator Auguty Web', 50)

        def check_rules(self):
            self.process_limit([self.bolt, self.flame], 1)

    def __init__(self, parent):
        super(LandRaiderProteus, self).__init__(name="Land Raider Proteus", parent=parent, points=200, gear=[
            Gear('Twin-linked lascannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.Weapon(self)
        LandRaiderHelios.Options(self)


class LandRaiderAchilles(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_achilles_v1'
    type_name = "Land Raider Achilles (IA vol.2)"
    imperial_armour = True

    def __init__(self, parent):
        super(LandRaiderAchilles, self).__init__(parent=parent, name="Land Raider Achilles", points=325, gear=[
            Gear('Thunderfire cannon'),
            Gear('Twin-linked multi-melta', count=2),
            Gear('Extra armour'),
            Gear('Armoured Ceramite'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)

        LandRaiderHelios.Options(self, armour=False)


class DeimosPredator(SpaceMarinesBaseVehicle):
    type_name = "Deimos Predator (IA vol.2)"
    type_id = "deimos_predator_v1"
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(DeimosPredator.Options, self).__init__(parent, 'Options')
            self.hkm = self.variant('Hunter-killer missile', 10)
            self.dblade = self.variant('Dozer blade', 5)
            self.sbgun = self.variant('Storm bolter', 10)
            self.exarm = self.variant('Extra armour', 10)

    class Turret(OneOf):
        def __init__(self, parent):
            super(DeimosPredator.Turret, self).__init__(parent=parent, name='Turret')
            self.variant('Autocannon', 0)
            self.beamer = self.variant('Heavy conversion beamer', 65)
            self.plasma = self.variant('Plasma destroyer', 35)
            self.variant('Magna-melta cannon', 45)
            self.variant('Flamestorm cannon', 15)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(DeimosPredator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.variant('Heavy bolters', 20, gear=Gear('Heavy bolter', count=2))
            self.variant('Heavy flamers', 20, gear=Gear('Heavy flamer', count=2))
            self.variant('Lascannons', 40, gear=Gear('Lascannon', count=2))

    def __init__(self, parent):
        super(DeimosPredator, self).__init__(parent=parent, name="Deimos Predator", points=75, gear=[
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.turret = self.Turret(self)
        self.side = self.Sponsons(self)
        self.opt = self.Options(self)

    def is_relic(self):
        return self.turret.cur in [self.turret.beamer, self.turret.plasma]


class SicaranTank(SpaceMarinesBaseVehicle):
    type_name = "Relic Sicaran Battle Tank (IA vol.2)"
    type_id = "sicaran_tank_v1"
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(SicaranTank.Options, self).__init__(parent, 'Options')
            self.variant('Hunter-killer missile', 10)
            self.variant('Dozer blade', 5)
            self.variant('Armoured Ceramite', 20)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(SicaranTank.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.variant('Heavy bolters', 20, gear=Gear('Heavy bolter', count=2))
            self.variant('Lascannons', 40, gear=Gear('Lascannon', count=2))

    def __init__(self, parent):
        super(SicaranTank, self).__init__(parent=parent, points=135, name="Relic Sicaran Tank", gear=[
            Gear('Twin-linked accelerator cannon'),
            Gear('Heavy bolter'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Extra armour'),
        ], tank=True)
        self.side = self.Sponsons(self)
        self.opt = self.Options(self)

    def is_relic(self):
        return True


class WhirlwindHyperios(SpaceMarinesBaseVehicle):
    type_name = "Whirlwind Hyperios Air Defence Tank (IA vol.2)"
    type_id = "whirlwind_hyperios_v1"
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(WhirlwindHyperios.Options, self).__init__(parent, 'Options')
            self.hkm = self.variant('Hunter-killer missile', 10)
            self.dblade = self.variant('Dozer blade', 5)
            self.sbgun = self.variant('Storm bolter', 5)
            self.exarm = self.variant('Extra armour', 10)

    def __init__(self, parent):
        super(WhirlwindHyperios, self).__init__(parent=parent, name="Whirlwind Hyperios", points=115, gear=[
            Gear('Hyperios missile launcher'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = self.Options(self)


class WhirlwindScorpius(SpaceMarinesBaseVehicle):
    type_name = "Relic Whirlwind Scorpius (IA vol.2)"
    type_id = "whirlwind_scorpius_v1"
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(WhirlwindScorpius.Options, self).__init__(parent, 'Options')
            self.hkm = self.variant('Hunter-killer missile', 10)
            self.dblade = self.variant('Dozer blade', 5)
            self.exarm = self.variant('Extra armour', 10)

    def __init__(self, parent):
        super(WhirlwindScorpius, self).__init__(parent=parent, name="Whirlwind Scorpius", points=125, gear=[
            Gear('Scorpius multi-launcher'),
            Gear('Storm bolter'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = self.Options(self)

    def is_relic(self):
        return True


class Spartan(SpaceMarinesBaseVehicle):
    type_name = "Spartan Assault Tank (IA vol.2)"
    type_id = "spartan_v1"
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(Spartan.Options, self).__init__(parent, 'Options')
            self.variant('Frag assault launchers', 10)
            self.variant('Hunter-killer missile', 10)
            self.variant('Armoured Ceramite', 20)
            self.weapon = [
                self.variant('Storm bolter', 5),
                self.variant('Heavy bolter', 15),
                self.variant('Heavy flamer', 15),
                self.variant('Multi-melta', 20),
            ]

        def check_rules(self):
            self.process_limit(self.weapon, 1)

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(Spartan.Sponsons, self).__init__(parent, 'Weapon')
            self.variant('Quad lascannons', 0, gear=Gear('Quad lascannon', count=2))
            self.variant('Laser destroyers', 0, gear=Gear('Laser destroyer', count=2))

    class Hull(OneOf):
        def __init__(self, parent):
            super(Spartan.Hull, self).__init__(parent, '')
            self.variant('Twin-linked heavy bolter', 0)
            self.variant('Twin-linked heavy flamer', 0)

    def __init__(self, parent):
        super(Spartan, self) .__init__(parent=parent, name='Spartan', points=295, gear=[
            Gear('Extra armour'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.Hull(self)
        self.Sponsons(self)
        self.Options(self)


class StormEagleGunship(SpaceMarinesBaseVehicle):
    type_name = 'Storm Eagle Assault Gunship (IA vol.2)'
    type_id = 'storm_eagle_gunship_v1'
    imperial_armour = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(StormEagleGunship.Weapon1, self).__init__(parent, 'Weapon')
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.twinlinkedmultimelta = self.variant('Twin-linked multi-melta', 15)
            self.typhoonmissilelauncher = self.variant('Typhoon missile launcher', 25)

    class Options(OptionsList):
        def __init__(self, parent):
            super(StormEagleGunship.Options, self).__init__(parent, 'Options')
            self.hell = self.variant('Four hellstrike missiles', 40, gear=[Gear('Hellstrike missile', count=4)])
            self.las = self.variant('Two twin-linked lascannon', 60, gear=[Gear('Twin-linked lascannon', count=2)])
            self.searchlight = self.variant('Searchlight', 1)
            self.extraarmour = self.variant('Extra armour', 15)

        def check_rules(self):
            self.process_limit([self.hell, self.las], 1)

    def __init__(self, parent):
        super(StormEagleGunship, self).__init__(parent=parent, name='Storm Eagle', points=225, gear=[
            Gear('Vengeance launcher'),
            Gear('Armoured Ceramite'),
        ], transport=True, storm_eagle=True)
        self.wep1 = self.Weapon1(self)
        self.opt = self.Options(self)


class StormEagleGunshipRoc(SpaceMarinesBaseVehicle):
    type_name = 'Storm Eagle Assault Gunship - Roc Pattern (IA vol.2)'
    type_id = 'storm_eagle_gunship_roc_v1'
    imperial_armour = True
    chapter = 'minotaurs'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(StormEagleGunshipRoc.Weapon1, self).__init__(parent, 'Weapon')
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.twinlinkedmultimelta = self.variant('Twin-linked multi-melta', 15)
            self.typhoonmissilelauncher = self.variant('Typhoon missile launcher', 25)

    class Options(OptionsList):
        def __init__(self, parent):
            super(StormEagleGunshipRoc.Options, self).__init__(parent, 'Options')
            self.searchlight = self.variant('Searchlight', 1)
            self.extraarmour = self.variant('Extra armour', 15)

    def __init__(self, parent):
        super(StormEagleGunshipRoc, self).__init__(parent=parent, name='Storm Eagle (Roc)', points=295, gear=[
            Gear('Twin-linked vengeance launcher with Roc warheads'),
            Gear('Twin-linked lascannon', count=2),
            Gear('Armoured Ceramite'),
        ], transport=True, storm_eagle=True)
        self.wep1 = self.Weapon1(self)
        self.opt = self.Options(self)


class FireRaptor(SpaceMarinesBaseVehicle):
    type_name = 'Fire Raptor Gunship (IA vol.2)'
    type_id = 'fire_raptor_gunship_v1'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(FireRaptor.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Quad heavy bolters', 0, gear=[Gear('Quad heavy bolter', count=2)])
            self.variant('Twin-linked autocannons', 0, gear=[Gear('Twin-linked autocannon', count=2)])

    def __init__(self, parent):
        super(FireRaptor, self).__init__(parent=parent, name='Fire Raptor', points=225, gear=[
            Gear('Twin-linked Avenger bolt cannon'),
            Gear('Stormstrike missile', count=4),
            Gear('Extra armour'),
        ])
        self.weapon = self.Weapon(self)

    def is_relic(self):
        return True


class DeathStormDropPod(SpaceMarinesBaseVehicle):
    type_name = 'Deathstorm Drop Pod (IA vol.2)'
    type_id = 'DeathstormDropPod_v1'
    imperial_armour = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DeathStormDropPod.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Automated whirlwind launchers', 0)
            self.variant('Automated assault cannon', 20)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DeathStormDropPod.Options, self).__init__(parent, 'Options')
            self.variant('Drop Pod Assault', 25)

    def __init__(self, parent):
        super(DeathStormDropPod, self).__init__(parent=parent, name='Deathstorm Drop Pod', points=75, deep_strike=True)
        self.wep1 = self.Weapon1(self)
        self.opt = self.Options(self)


class Tarantula(Unit):
    type_name = 'Tarantula Sentry Gun Battery (IA vol.2)'
    type_id = 'Tarantula_v1'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(Tarantula.Options, self).__init__(parent, 'Options')
            self.hyperios = self.variant('Hyperios missile launchers', 20, per_model=True, gear=[])
            self.camo = self.variant('Camo Netting', 10, per_model=True)
            self.deep = self.variant('Deep Strike', 10, per_model=True)

        @property
        def points(self):
            return super(Tarantula.Options, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(Tarantula, self).__init__(parent, name='Tarantula Sentry Gun Battery')
        self.models = Count(self, 'Tarantula Sentry Gun', 1, 3, 15, per_model=True, gear=[])
        self.wep = [
            Count(
                self, 'Twin-linked lascannon', 0, 1, 10, per_model=True,
                gear=UnitDescription('Tarantula Sentry Gun', points=25, options=[Gear('Twin-linked lascannon')])
            ),
            Count(
                self, 'Multi-melta and searchlight', 0, 1, 5, per_model=True,
                gear=UnitDescription('Tarantula Sentry Gun', points=20, options=[Gear('Multi-melta'), Gear('Searchlight')])
            )
        ]
        self.opt = self.Options(self)
        self.command = Count(
            self, 'Hyperios command platform', 0, 1, 10, per_model=True,
            gear=UnitDescription('Tarantula Sentry Gun', points=45, options=[Gear('Hyperios command platform')])
        )

    def check_rules(self):
        Count.norm_counts(0, self.get_count(), self.wep)
        self.command.max = self.get_count()
        for u in self.wep:
            u.visible = u.used = not self.opt.hyperios.value
        self.command.visible = self.command.used = self.opt.hyperios.value

    def build_description(self):
        desc = super(Tarantula, self).build_description()
        if self.opt.hyperios:
            desc.add(
                UnitDescription(
                    'Tarantula Sentry Gun', points=35, options=[Gear('Hyperios missile launcher')],
                    count=self.models.cur - self.command.cur
                )
            )
        else:
            desc.add(
                UnitDescription(
                    'Tarantula Sentry Gun', points=15, options=[Gear('Hyperios missile launcher')],
                    count=self.models.cur - sum(o.cur for o in self.wep)
                )
            )
        return desc

    def get_count(self):
        return self.models.cur


class Rapier(Unit):
    type_name = 'Space Marine Rapier Weapon Battery (IA vol.2)'
    type_id = 'Rapier_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Rapier, self).__init__(parent, name='Rapier Weapon Battery')
        self.models = Count(self, 'Rapier Carrier and two Space Marine Crew', 1, 3, 40, per_model=True, gear=[])
        self.las = Count(self, 'Laser destroyer', 0, 1, 15, per_model=True, gear=[])

    def check_rules(self):
        self.las.max = self.get_count()

    def build_description(self):
        desc = super(Rapier, self).build_description()
        desc.add(UnitDescription(
            'Space Marine Crew', count=self.models.cur * 2,
            options=[Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Bolt pistol')],
        ))
        desc.add(UnitDescription(
            'Rapier Carrier', count=self.las.cur, points=40 + 15,
            options=[Gear('Laser destroyer')],
        ))
        desc.add(UnitDescription(
            'Rapier Carrier', count=self.models.cur - self.las.cur, points=40,
            options=[Gear('Quad heavy bolter')],
        ))
        return desc

    def get_count(self):
        return self.models.cur
