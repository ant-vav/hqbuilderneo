__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear,\
    Count
from builder.games.wh40k.roster import Unit


class KustomFortress(Unit):
    type_name = "'Kustom' Battle Fortress"
    type_id = 'kustom_fortress_v2'
    imperial_armour = True

    class Options1(OptionsList):
        def __init__(self, parent):
            super(KustomFortress.Options1, self).__init__(parent, 'Options')
            self.variant('\'Ard case', 25)
            self.variant("Boarding plank", 10)

    class Options2(OptionsList):
        def __init__(self, parent):
            super(KustomFortress.Options2, self).__init__(parent, '', limit=1)
            self.variant("Wreckin' ball", 15)
            self.variant("Grabbin' klaw", 15)

    class Options3(OptionsList):
        def __init__(self, parent):
            super(KustomFortress.Options3, self).__init__(parent, 'Missile weapons', limit=1)
            self.variant("Three Grot Bomms", 60, gear=[Gear('Grot Bomm', count=3)])
            self.variant("Three Supa-Rokkits", 45, gear=[Gear('Supa-Rokkit', count=3)])

    class Weapon(OneOf):
        def __init__(self, parent, zzap=True):
            super(KustomFortress.Weapon, self).__init__(
                parent, 'Big gun')
            if zzap:
                self.variant('Zzap gun', 0)
                self.variant('Kannon', 0)
            else:
                self.variant('Kannon', 0)
                self.variant('Zzap gun', 0)
            self.variant('Lobba', 0)

            self.variant('Killkannon', 35)
            self.variant('Big lobba', 25)
            self.variant('Flakka-gunz', 35)
            self.variant('Supa-kannon', 65)
            self.variant('Supa-lobba', 45)

    class PintleWeapon(OneOf):
        def __init__(self, parent, number):
            super(KustomFortress.PintleWeapon, self).__init__(
                parent, 'Pintle weapon {}'.format(number))
            self.variant('(None)', 0, gear=[])
            self.variant('Big shoota', 5)
            self.variant('Skorcha', 5)
            self.variant('Rokkit launcha', 10)
            self.variant('Twin-linked big shoota', 15)
            self.variant('Twin-linked rokkit launcha', 20)

    def __init__(self, parent):
        super(KustomFortress, self).__init__(parent, points=355)
        self.Options1(self)
        self.Options2(self)
        Count(self, 'Grot sponsons', 0, 4, 5)
        self.Options3(self)
        self.Weapon(self)
        self.Weapon(self)
        self.Weapon(self, zzap=False)
        [
            self.PintleWeapon(self, i + 1) for i in range(0, 4)
        ]


class KillKrusha(Unit):
    type_name = 'Kill Krusha Tank'
    type_id = 'kill_krusha'
    imperial_armour = True

    def __init__(self, parent):
        super(KillKrusha, self).__init__(parent, 'Kill Krusha', 275,
                                         [Gear('Krusha kannon')])
        self.extra = [
            Count(self, 'Big shoota', 0, 5, 5),
            Count(self, 'Skorcha', 0, 5, 5),
            Count(self, 'Rokkit launcha', 0, 5, 10),
            Count(self, 'Twin big shoota', 0, 5, 10),
            Count(self, 'Twin rokkit launcha', 0, 5, 15)
        ]
        Count(self, 'Grot bomb', 0, 2, 20)
        Count(self, 'Grot sponsons', 0, 2, 5)

    def check_rules(self):
        super(KillKrusha, self).check_rules()
        Count.norm_counts(0, 5, self.extra)


class KillBursta(Unit):
    type_name = 'Kill Bursta'
    type_id = 'kill_bursta'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(KillBursta.Weapon, self).__init__(parent, 'Main weapon')
            self.variant('Belly gun', 0)
            self.variant('Bursta kannon', 50)

    class Options(OptionsList):
        def __init__(self, parent):
            super(KillBursta.Options, self).__init__(parent, 'Options')
            self.variant('Grot gunners', 25)

    def __init__(self, parent):
        super(KillBursta, self).__init__(parent, 'Kill Blasta', 350,
                                         [Gear('Grot riggers'), Gear('Twin-linked big shoota')])
        self.Weapon(self)
        self.extra = [
            Count(self, 'Big shoota', 0, 5, 5),
            Count(self, 'Skorcha', 0, 5, 5),
            Count(self, 'Rokkit launcha', 0, 5, 10),
            Count(self, 'Twin big shoota', 0, 5, 10),
            Count(self, 'Twin rokkit launcha', 0, 5, 15)
        ]
        Count(self, 'Grot bomb', 0, 2, 20)
        Count(self, 'Grot sponsons', 0, 2, 5)
        self.Options(self)

    def check_rules(self):
        super(KillBursta, self).check_rules()
        Count.norm_counts(0, 5, self.extra)


class KillBlasta(Unit):
    type_name = 'Kill Blasta'
    type_id = 'kill_blasta'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(KillBlasta.Options, self).__init__(parent, 'Options')
            self.variant('Grot gunners', 25)

    def __init__(self, parent):
        super(KillBlasta, self).__init__(parent, 'Kill Blasta', 330,
                                         [Gear('Gigashoota'), Gear('Twin-linked big shoota')])
        self.extra = [
            Count(self, 'Big shoota', 0, 5, 5),
            Count(self, 'Skorcha', 0, 5, 5),
            Count(self, 'Rokkit launcha', 0, 5, 10),
            Count(self, 'Twin big shoota', 0, 5, 10),
            Count(self, 'Twin rokkit launcha', 0, 5, 15)
        ]
        Count(self, 'Grot bomb', 0, 2, 20)
        Count(self, 'Grot sponsons', 0, 2, 5)
        self.Options(self)

    def check_rules(self):
        super(KillBlasta, self).check_rules()
        Count.norm_counts(0, 5, self.extra)
