__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear
from options import CommonOption, ChaosSpaceMarinesBaseVehicle
from builder.games.wh40k.roster import Unit

ia_id = ' (IA vol.13)'


class Fellblade(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Fellblade Super-Heavy Tank (IA vol.13)'
    wikilink = 'https://sites.google.com/site/wh40000rules/codices/chaos-space-marines/profiles#TOC-Chaos-Fellblade-Super-heavy-Tank'  # AUTO-ADDED
    type_id = 'fellblade_v1'
    imperial_armour = True

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(Fellblade.Sponsons, self).__init__(parent, 'Sponsons')
            self.variant('Quad lascannon', 0,
                         gear=[Gear('Quad lascannon', count=2)])
            self.variant('Laser destroyer', 0,
                         gear=[Gear('Laser destroyer', count=2)])

    class HullMounted(OneOf):
        def __init__(self, parent):
            super(Fellblade.HullMounted, self).__init__(
                parent, 'Hull-mounted weapon')
            self.variant('Twin-linked heavy bolter')
            self.variant('Twin-linked heavy flamer')

    class PintleMounted(OptionsList):
        def __init__(self, parent):
            super(Fellblade.PintleMounted, self).__init__(
                parent, 'Pintle-mounted weapon', limit=1)
            self.variant('Heavy flamer', 15)
            self.variant('Heavy bolter', 15)
            self.variant('Multi-melta', 20)

    def __init__(self, parent):
        super(Fellblade, self).__init__(parent, 'Chaos Fellblade', 540,
                                        gear=[Gear('Demolished cannon'),
                                              Gear('Twin-linked Fellblade Accelerator cannon'),
                                              Gear('Searchlight'), Gear('Snoke launcher')],
                                        tank=True, super_heavy=True)
        self.Sponsons(self)
        self.HullMounted(self)
        self.PintleMounted(self)
        CommonOption(self, ceramite=20, posession=30, malefic=15)


class Typhon(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Typhon Heavy Siege Tank (IA vol.13)'
    type_id = 'typhon_v1'
    imperial_armour = True

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(Typhon.Sponsons, self).__init__(parent, 'Sponsons')
            self.variant('Heavy bolters', 20,
                         gear=[Gear('Heavy bolters', count=2)])
            self.variant('Lascannon', 40,
                         gear=[Gear('Lascannon', count=2)])

    class PintleMounted(OptionsList):
        def __init__(self, parent):
            super(Typhon.PintleMounted, self).__init__(
                parent, 'Pintle-mounted weapon', limit=1)
            self.variant('Heavy flamer', 15)
            self.variant('Heavy bolter', 15)
            self.variant('Multi-melta', 20)

    def __init__(self, parent):
        super(Typhon, self).__init__(parent, 'Chaos Typhon', 350,
                                        gear=[Gear('Dreadhammer siege-cannon'),
                                              Gear('Searchlight'), Gear('Snoke launcher')],
                                     tank=True, super_heavy=True)
        self.Sponsons(self)
        self.PintleMounted(self)
        CommonOption(self, ceramite=20, posession=30, malefic=40)


class Spartan(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Spartan Assault Tank (IA vol.13)'
    type_id = 'spartan_v1'
    imperial_armour = True

    class PintleMounted(OptionsList):
        def __init__(self, parent):
            super(Spartan.PintleMounted, self).__init__(
                parent, 'Pintle-mounted weapon', limit=1)
            self.variant('Combi-bolter', 5)
            self.variant('Havok launcher', 12)
            self.variant('Heavy flamer', 15)
            self.variant('Heavy bolter', 15)
            self.variant('Multi-melta', 20)

    class Option(OptionsList):
        def __init__(self, parent):
            super(Spartan.Option, self).__init__(parent, 'Options')
            self.variant('Frag assault launchers', 10)
            self.variant('Armoured Ceramite', 20)
            self.variant('Daemonic Posession', 25)
            self.variant('Dirge caster', 5)
            self.variant('Warpflame Gargoyles', 5)
            self.variant('Malefic Ammunition', 35)

    def __init__(self, parent):
        super(Spartan, self).__init__(parent, 'Chaos Spartan', 285,
                                        gear=[Gear('Searchlight'), Gear('Snoke launcher')],
                                      tank=True, transport=True)
        Fellblade.Sponsons(self)
        Fellblade.HullMounted(self)
        self.PintleMounted(self)
        self.Option(self)


class ThunderhawkGunship(ChaosSpaceMarinesBaseVehicle):
    type_id = 'thunderhawk_gunship_v1'
    type_name = 'Chaos Space Marine Thunderhawk Gunship (IA vol.13)'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ThunderhawkGunship.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Thunderhawk cannon', 0)
            self.variant('Turbo-laser destructor', 90)

    class Bombs(OneOf):
        def __init__(self, parent):
            super(ThunderhawkGunship.Bombs, self).__init__(parent=parent, name='')
            self.variant('Hellstrike missiles', 0, gear=Gear('Hellstrike missile', count=6))
            self.variant('Thunderhawk cluster bombs', 60, gear=Gear('Thunderhawk cluster bomb', count=6))

    class Options(OptionsList):
        def __init__(self, parent):
            super(ThunderhawkGunship.Options, self).__init__(parent=parent, name='Options')
            self.variant('Daemonic Transport', 50)
            self.variant('Malefic Ammunition', 55)

    def __init__(self, parent):
        super(ThunderhawkGunship, self) .__init__(parent=parent, name='Chaos Thunderhawk Gunship', points=685, gear=[
            Gear('Twin-linked heavy bolter', count=4),
            Gear('Lascannon', count=2),
            Gear('Armoured Ceramite'),
        ], super_heavy=True, transport=True)
        self.Weapon(self)
        self.Bombs(self)
        self.Options(self)


class GreaterScorpion(Unit):
    type_name = 'Greater Brass Scorpion of Khorne (IA vol.13)'
    type_id = 'brass_scorpion_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(GreaterScorpion, self).__init__(
            parent, 'Greater Brass Scorpion', 700, [
                Gear('Scorpion cannon'), Gear('Soulburner cannon'),
                Gear('Hellmaw cannon', count=2)
            ], static=True)


class ChaosReaverTitan(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Reaver Battle Titan (IA vol.13)'
    type_id = 'chaos_reaver_titan_v1'
    imperial_armour = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(ChaosReaverTitan.Weapon1, self).__init__(parent, name='Carapace weapon')
            self.variant('Double-barrelled turbo laser destructor')
            self.variant('Plasma blastgun')
            self.variant('Inferno gun')
            self.variant('Vulcan mega-bolter')
            self.variant('Apocalypse missile-launcher')
            self.variant('Vortex missile')

    class Weapon2(OneOf):
        def __init__(self, parent, name):
            super(ChaosReaverTitan.Weapon2, self).__init__(parent, name=name)
            self.variant('Gatling blaster')
            self.variant('Melta cannon')
            self.variant('Vulcano cannon')
            self.variant('Laser blaster')
            self.variant('Titan power first')

    class Dedication(OptionsList):
        def __init__(self, parent):
            super(ChaosReaverTitan.Dedication, self).__init__(parent, 'Dedication', limit=1)
            self.variant('Daemon Titan of Khorne', 100)
            self.nurgle = self.variant('Daemon Titan of Nurgle', 200)
            self.variant('Daemon Titan of Slaanesh', 150)
            self.variant('Daemon Titan of Tzeentch', 200)

    def __init__(self, parent):
        super(ChaosReaverTitan, self).__init__(parent, 'Chaos Reaver', 1460,
                                               [Gear('Dirge casters'), Gear('Void shield', count=4)],
                                               walker=True, super_heavy=True)
        self.Weapon1(self)
        self.Weapon2(self, name='Arm weapon')
        self.Weapon2(self, name='')
        self.god = self.Dedication(self)

    def not_nurgle(self):
        return self.god.any and not self.god.nurgle.value


class ChaosWarhoundTitan(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Warhound Titan (IA vol.13)'
    type_id = 'chaos_warhound_titan_v1'
    imperial_armour = True

    class Weapon1(OneOf):
        def __init__(self, parent, name):
            super(ChaosWarhoundTitan.Weapon1, self).__init__(parent, name=name)
            self.variant('Double-barrelled turbo laser destructor')
            self.variant('Plasma blastgun')
            self.variant('Inferno gun')
            self.variant('Vulcan mega-bolter')

    class Dedication(OptionsList):
        def __init__(self, parent):
            super(ChaosWarhoundTitan.Dedication, self).__init__(parent, 'Dedication', limit=1)
            self.variant('Daemon Titan of Khorne', 50)
            self.nurgle = self.variant('Daemon Titan of Nurgle', 100)
            self.variant('Daemon Titan of Slaanesh', 75)
            self.variant('Daemon Titan of Tzeentch', 50)

    def __init__(self, parent):
        super(ChaosWarhoundTitan, self).__init__(parent, 'Chaos Warhound',
                                                 730, [Gear('Dirge caster'), Gear('Void shield', count=4)],
                                                 walker=True, super_heavy=True)
        self.Weapon1(self, name='Arm weapon')
        self.Weapon1(self, name='')
        self.god = self.Dedication(self)

    def not_nurgle(self):
        return self.god.any and not self.god.nurgle.value


class Scabeiathrax(Unit):
    clear_name = 'Daemon Lord - Scabeiathrax the Bloated'
    type_name = clear_name + ia_id
    type_id = 'scabeiathrax_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Scabeiathrax, self).__init__(parent, name='Scabeiathrax', points=777, gear=[
            Gear('Blade of Decay'),
            Gear('Icon of Chaos '),
        ], static=True, unique=True)


class Anggrath(Unit):
    clear_name = 'Daemon Lord - An\'ggrath the Unbound'
    type_name = clear_name + ia_id
    type_id = 'anggrath_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Anggrath, self).__init__(parent, name="An'ggrath", points=888, gear=[
            Gear('Axe of Khorne'),
            Gear('Bloodlash of Khorne'),
            Gear('Daemonic Armour'),
            Gear('Icon of Chaos '),
        ], unique=True, static=True)


class Zarakynel(Unit):
    clear_name = 'Daemon Lord - Zarakynel'
    type_name = clear_name + ia_id
    type_id = 'zarakynel_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Zarakynel, self).__init__(parent, name='Zarakynel', points=666, gear=[
            Gear('The Souleater sword'),
            Gear('Icon of Chaos '),
        ], unique=True, statix=True)


class Aetaosraukeres(Unit):
    clear_name = 'Daemon Lord - Aetaos\'rau\'keres'
    type_name = clear_name + ia_id
    type_id = 'aetaosraukeres_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Aetaosraukeres, self).__init__(parent, name='Aetaos\'rau\'keres', points=999, gear=[
            Gear('Staff of Cataclysm'),
            Gear('Icon of Chaos '),
            Gear('Psyker (Mastery Level 4)')
        ], unique=True, static=True)
