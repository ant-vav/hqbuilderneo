__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear, Count,\
    UnitDescription
from options import ChaosSpaceMarinesBaseVehicle
from builder.games.wh40k.roster import Unit


class StormEagleGunship(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Storm Eagle Assault Gunship (IA vol.13)'
    type_id = 'storm_eagle_gunship_v1'
    imperial_armour = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(StormEagleGunship.Weapon1, self).__init__(parent, 'Weapon')
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.twinlinkedmultimelta = self.variant('Twin-linked multi-melta', 15)
            self.typhoonmissilelauncher = self.variant('Havok launcher', 5)
            self.reaper = self.variant('Reaper autocannon')

    class Options(OptionsList):
        def __init__(self, parent, weapon):
            super(StormEagleGunship.Options, self).__init__(parent, 'Options')
            self.weapon = weapon
            self.hell = self.variant('Four hellstrike missiles', 40,
                                     gear=[Gear('Hellstrike missile', count=4)])
            self.las = self.variant('Two twin-linked lascannon',
                                    60, gear=[Gear('Twin-linked lascannon', count=2)])
            self.inc = self.variant('Four Balefire incendiary missiles', 50,
                                    gear=[Gear('Balefire incendiary missile', count=4)])
            self.extraarmour = self.variant('Extra armour', 10)
            self.variant('Dirge caster', 5)
            self.variant('Warpflame Gargoyles', 5)
            self.variant('Daemonic Posession', 15)
            self.mal = self.variant('Malefic Ammunition', 20)

        def check_rules(self):
            self.process_limit([self.hell, self.las, self.inc], 1)
            if self.weapon.cur == self.weapon.twinlinkedheavybolter:
                self.mal.visible = self.mal.used = True
                self.mal.points = 20
            else:
                if self.weapon.cur == self.weapon.reaper:
                    self.mal.visible = self.mal.used = True
                    self.mal.points = 15
                else:
                    self.mal.visible = self.mal.used = False

    def __init__(self, parent):
        super(StormEagleGunship, self).__init__(
            parent=parent, name='Chaos Storm Eagle', points=205, gear=[
                Gear('Vengeance launcher'),
                Gear('Armoured Ceramite'),
            ], transport=True)
        self.wep1 = self.Weapon1(self)
        self.opt = self.Options(self, self.wep1)


class HellBlade(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Hell Blade (IA vol.13)'
    type_id = 'hellblade_v1'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(HellBlade.Options, self).__init__(parent, 'Options')
            self.variant('Daemonic posession', 20)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(HellBlade.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Reaper autocannons', 0,
                         gear=[Gear('Two Reaper autocannon', count=2)])
            self.variant('Two Helstorm autocannons', 15,
                         gear=[Gear('Helstorm autocannon', count=2)])

    def __init__(self, parent):
        super(HellBlade, self).__init__(parent, 'Hell Blade', 100)
        self.Options(self)
        self.Weapon(self)


class HellTalon(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Hell Talon (IA vol.13)'
    type_id = 'helltalon_v1'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(HellTalon.Options, self).__init__(parent, 'Options')
            self.variant('Daemonic posession', 20)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(HellTalon.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Reaper autocannon', 0)
            self.variant('Havok launcher', 0)
            self.variant('Helstorm autocannon', 15)

    def __init__(self, parent):
        super(HellTalon, self).__init__(parent, 'Hell Talon', 160,
                                        [Gear('Twin-linked lascannon')])
        self.Options(self)
        self.Weapon(self)
        self.bombs = [
            Count(self, 'Warp-pulse bomb', 0, 8, 15),
            Count(self, 'Alchem cluster bomb', 0, 8, 10),
            Count(self, 'Baletalon shatter charge', 0, 8, 0)
        ]

    def check_rules(self):
        super(HellTalon, self).check_rules()
        Count.norm_counts(0, 8, self.bombs)

    def build_description(self):
        res = super(HellTalon, self).build_description()
        bcount = 8 - sum(c.cur for c in self.bombs)
        if bcount:
            res.add(Gear('Pyrax bomb', count=bcount))
        return res


class Dreadclaw(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Dreadclaw Drop Pod (IA vol.13)'
    type_id = 'dreadclaw_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Dreadclaw, self).__init__(parent, 'Chaos Dreadclaw', 100,
                                        [Gear('Frag assault launchers')],
                                        transport=True)


class Kharybdis(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Kharybdis Assault Claw (IA vol.13)'
    type_id = 'Kharybdis_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Kharybdis, self).__init__(parent, 'Kharybdis Assault Claw', 260,
                                        [Gear('Frag assault launchers'), Gear('Melta-ram'),
                                         Gear('Kharybdis storm launchers', count=5)],
                                        transport=True)

    def is_relic(self):
        return True


class BlightDrones(Unit):
    type_name = 'Blight Drones (IA vol.13)'
    type_id = 'blight_drone_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(BlightDrones, self).__init__(parent, name='Blight Drones')
        self.models = Count(self, 'Blight Drones', 1, 3, 150, True,
                            gear=UnitDescription('Blight Drone', 150, options=[
                                Gear('Mawcannon'), Gear('Reaper autocannon')
                            ]))

    def get_count(self):
        return self.models.cur
