__author__ = 'dvarh'

from builder.core2 import UnitType
from builder.games.wh40k.section import LordsOfWarSection
from chaos_knights import ChaosErrant, ChaosPaladin, KhorneErrant,\
    KhornePaladin, Kytan


class ChaosKnights(LordsOfWarSection):
    def __init__(self, parent):
        super(ChaosKnights, self).__init__(parent)
        UnitType(self, ChaosPaladin)
        UnitType(self, ChaosErrant)
        UnitType(self, Kytan)


class KhorneKnights(LordsOfWarSection):
    def __init__(self, parent):
        super(KhorneKnights, self).__init__(parent)
        UnitType(self, KhornePaladin)
        UnitType(self, KhorneErrant)
        UnitType(self, Kytan)
