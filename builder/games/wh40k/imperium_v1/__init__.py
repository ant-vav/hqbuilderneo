from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    HeavySection, UnitType, Formation, LordsOfWarSection, FastSection
from builder.games.wh40k.cult_mechanicus import Cawl, Dominus, Techpriest as CMTechpriest, Fulgurites, Corpuscarii, \
    Breachers, Destroyers, KastelanManiple
from builder.games.wh40k.astra_militarum import Techpriest, Servitors, Commissar as AMCommissar, Yarrick, \
    CommandSquad as AMCommandSquad, TankCommander, LordCommissar, Priest as AMPriest, PrimarisPsyker as AMPsyker, \
    InfantryPlatoon, VeteranSquad, BullgrynSquad, MilitarumTempestusPlatoon, OgrynSquad, RatlingSquad, \
    PsykerSquad, ArmouredSentinelSquad, HellhoundSquadron, Riders, ScoutSentinelSquad, VendettaSquadron, Valkyries, \
    BasiliskSquadron, Deathstrike, HydraSquadron, LemanRussSquadron, Manticore, WyvernSquadron
from builder.games.wh40k.militarum_tempestus import Commissar as MTCommissar, CommandSquad as MTCommandSquad, Scions, \
    TauroxPrime, ValkyrieSquadron
from builder.games.wh40k.adepta_sororitas_v4 import Celestine, Priest as IAPriest, Canoness, \
    CommandSquad as IACommandSquad, Jacobus, Celestians, Repentia, PenitentEngine, Exorcist, Retributors, Sisters, \
    Dominions, Seraphims
from builder.games.wh40k.obsolete.adepta_sororitas_v3 import Priest as ASPrist, Conclave,\
    CommandSquad as ASCommandSquad
from builder.games.wh40k.inquisition_v2 import Greyfax, Coteaz, Karamazov, Malleus as IAMalleus,\
    Xenos as IAXenos, Hereticus as IAHereticus
from builder.games.wh40k.inquisition import Malleus, Xenos, Hereticus, Warband
from builder.games.wh40k.imperial_knight_v2 import Paladin, Errant, Warden, Crusader, Gallant
from builder.games.wh40k.skitarii import Infiltrators, Rangers, Vanguard, Dragoons, Ironstriders, Dunecrawlers, \
    Ruststalkers
from builder.games.wh40k.space_marines_v3 import SupplementOptions, TerminatorCaptain, Captain, Chaplain,\
    Grimaldus, Champion, Helbrecht, Librarian, Techmarine, CataphractiiTerminatorSquad, CenturionAssault, \
    CommandSquad as SMCommandSquad, HonourGuardSquad, IronDreds, SternguardVeteranSquad, TerminatorSquad, \
    TerminatorAssaultSquad, VanguardVeteranSquad, VenDreadnoughts, CrusaderSquad, ScoutBikers, ScoutSquad,\
    TacticalSquad, Dreadnoughts, Stormhawks, AssaultSquad, AttackBikeSquad, BikeSquad, DropPod, LandSpeederStorm, \
    LandSpeederSquadron, Razorback, Rhino, Stormtalons, ContemptorDreadnoughts, CenturionDevastators, \
    Devastators, Hunters, LandRaider, LandRaiderRedeemer, LandRaiderCrusader, Predators, Stalkers, \
    Stormravens, Thunderfire, Vindicators, Whirlwinds
from builder.games.wh40k.astra_telepathica import PrimarisPsyker, WyrdvanePsykers as ATWyrdvanePsykers
from builder.games.wh40k.officio_assasinorum import VindicareAssasin, CulexusAssasin, CallidusAssasin, EversorAssasin
from builder.games.wh40k.escalation.imperial_guard import Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, \
    Shadowsword, Stormlord, Stormsword

from itertools import chain


class MTValkyries(ValkyrieSquadron):
    #trajic reetition of type_id!
    type_id = 'valkyrie_squadron_mt_v1'


class Triumvirate(Formation):
    army_name = 'Triumvirate of the Imperium'
    army_id = 'imperium_v1_triumvirate'

    def __init__(self):
        super(Triumvirate, self).__init__()
        UnitType(self, Cawl, min_limit=1, max_limit=1)
        UnitType(self, Celestine, min_limit=1, max_limit=1)
        UnitType(self, Greyfax, min_limit=1, max_limit=1)


class Triumvirate2(Formation):
    army_name = 'Triumvirate of the Primarch'
    army_id = 'imperium_v1_primarch_triforce'

    def __init__(self):
        super(Triumvirate2, self).__init__()
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman, min_limit=1, max_limit=1)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus, min_limit=1, max_limit=1)
        from builder.games.wh40k.dataslates.cypher import Cypher
        UnitType(self, Cypher, min_limit=1, max_limit=1)


class GrandConvocationHQ(HQSection):
    def __init__(self, parent):
        super(GrandConvocationHQ, self).__init__(parent, 2, 4)
        UnitType(self, Cawl)
        UnitType(self, Dominus)
        UnitType(self, CMTechpriest)
        self.tech = UnitType(self, Techpriest, slot=0)
        self.serv = UnitType(self, Servitors, slot=0)


class GrandConvocationElites(ElitesSection):
    def __init__(self, parent):
        super(GrandConvocationElites, self).__init__(parent, 0, 6)
        UnitType(self, Corpuscarii, group=Corpuscarii.faction)
        UnitType(self, Fulgurites, group=Fulgurites.faction)
        UnitType(self, Infiltrators, group=Infiltrators.faction)
        UnitType(self, Ruststalkers, group=Ruststalkers.faction)


class GrandConvocationTroops(TroopsSection):
    def __init__(self, parent):
        super(GrandConvocationTroops, self).__init__(parent, 4, None)
        UnitType(self, Breachers, group=Breachers.faction)
        UnitType(self, Destroyers, group=Destroyers.faction)
        UnitType(self, Rangers, group=Rangers.faction)
        UnitType(self, Vanguard, group=Vanguard.faction)


class GrandConvocationFast(FastSection):
    def __init__(self, parent):
        super(GrandConvocationFast, self).__init__(parent, 0, 6)
        UnitType(self, Dragoons)


class GrandConvocationHeavySupport(HeavySection):
    def __init__(self, parent):
        super(GrandConvocationHeavySupport, self).__init__(parent, 0, 6)
        UnitType(self, KastelanManiple, group=KastelanManiple.faction)
        UnitType(self, Ironstriders, group=Ironstriders.faction)
        UnitType(self, Dunecrawlers, group=Dunecrawlers.faction)


class GrandConvocationLordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(GrandConvocationLordsOfWar, self).__init__(parent, 0, 3)
        UnitType(self, Paladin)
        UnitType(self, Errant)
        UnitType(self, Warden)
        UnitType(self, Crusader)
        UnitType(self, Gallant)


class CastellansOfTheImperiumHQ(HQSection):
    def __init__(self, parent):
        super(CastellansOfTheImperiumHQ, self).__init__(parent, 2, 4)
        UnitType(self, Greyfax, group=Greyfax.faction + " (IA)")
        UnitType(self, Celestine, group=Celestine.faction + " (IA)")
        UnitType(self, TerminatorCaptain, group=TerminatorCaptain.faction)
        UnitType(self, AMCommissar, slot=0, group=AMCommissar.faction)
        UnitType(self, MTCommissar, group=MTCommissar.faction)
        UnitType(self, Yarrick, group=Yarrick.faction)
        UnitType(self, AMCommandSquad, group=AMCommandSquad.faction)

        self.tech = UnitType(self, Techpriest, slot=0, group=Techpriest.faction)
        self.serv = UnitType(self, Servitors, slot=0, group=Servitors.faction)
        UnitType(self, CMTechpriest, group=CMTechpriest.faction)

        class PaskTankCommander(TankCommander):
            allow_pask = True

        UnitType(self, PaskTankCommander, group=PaskTankCommander.faction)
        UnitType(self, LordCommissar, group=LordCommissar.faction)
        UnitType(self, AMPriest, slot=0, group=AMPriest.faction)
        UnitType(self, ASPrist, slot=0, group=ASPrist.faction)
        UnitType(self, IAPriest, group=IAPriest.faction + " (IA)")
        UnitType(self, AMPsyker, group=AMPsyker.faction + " (Codex)")
        UnitType(self, PrimarisPsyker, group=PrimarisPsyker.faction)
        UnitType(self, Canoness, group=Canoness.faction + " (IA)")
        UnitType(self, Conclave, group=Conclave.faction + " (Codex)")
        UnitType(self, ASCommandSquad, group=ASCommandSquad.faction + " (Codex)")
        UnitType(self, IACommandSquad, group=IACommandSquad.faction + " (IA)")
        UnitType(self, Jacobus, group=Jacobus.faction + " (IA)")
        UnitType(self, Coteaz, group=Coteaz.faction + " (IA)")
        UnitType(self, Karamazov, group=Karamazov.faction + " (IA)")
        UnitType(self, Hereticus, group=Hereticus.faction + " (Codex)")
        UnitType(self, IAHereticus, group=IAHereticus.faction + " (IA)")
        UnitType(self, Malleus, group=Malleus.faction + " (Codex)")
        UnitType(self, IAMalleus, group=IAMalleus.faction + " (IA)")
        UnitType(self, Xenos, group=Xenos.faction + " (Codex)")
        UnitType(self, IAXenos, group=IAXenos.faction + " (IA)")
        UnitType(self, MTCommandSquad, group=MTCommandSquad.faction)
        cap = UnitType(self, Captain, group=Captain.faction)
        chap = UnitType(self, Chaplain, group=Chaplain.faction)
        UnitType(self, Grimaldus, group=Grimaldus.faction)
        UnitType(self, Champion, group=Champion.faction)
        UnitType(self, Helbrecht, group=Helbrecht.faction)
        self.librarian = UnitType(self, Librarian, group=Librarian.faction)
        tech = UnitType(self, Techmarine, group=Techmarine.faction)
        self.bike_hq = [cap, chap, self.librarian, tech]

    def has_biker_hq(self):
        return any([unit.has_bike() for unit in
                    chain(*[_type.units for _type in self.bike_hq])])


class CastellansOfTheImperiumElites(ElitesSection):
    def __init__(self, parent):
        super(CastellansOfTheImperiumElites, self).__init__(parent, 0, 6)
        UnitType(self, CallidusAssasin, group=CallidusAssasin.faction)
        UnitType(self, CulexusAssasin, group=CulexusAssasin.faction)
        UnitType(self, EversorAssasin, group=EversorAssasin.faction)
        UnitType(self, VindicareAssasin, group=VindicareAssasin.faction)
        UnitType(self, CataphractiiTerminatorSquad, group=CataphractiiTerminatorSquad.faction)
        UnitType(self, BullgrynSquad, group=BullgrynSquad.faction)
        UnitType(self, MilitarumTempestusPlatoon, group=MilitarumTempestusPlatoon.faction)
        UnitType(self, OgrynSquad, group=OgrynSquad.faction)
        UnitType(self, RatlingSquad, group=RatlingSquad.faction)
        UnitType(self, ATWyrdvanePsykers, group=ATWyrdvanePsykers.faction)
        UnitType(self, PsykerSquad, group=PsykerSquad.faction)
        UnitType(self, Celestians, group=Celestians.faction)
        UnitType(self, Repentia, group=Repentia.faction)
        UnitType(self, Warband, group=Warband.faction)
        UnitType(self, CenturionAssault, group=CenturionAssault.faction)
        UnitType(self, SMCommandSquad, group=SMCommandSquad.faction)
        UnitType(self, Dreadnoughts, group=Dreadnoughts.faction)
        UnitType(self, HonourGuardSquad, group=HonourGuardSquad.faction)
        UnitType(self, IronDreds, group=IronDreds.faction)
        UnitType(self, SternguardVeteranSquad, group=SternguardVeteranSquad.faction)
        UnitType(self, TerminatorSquad, group=TerminatorSquad.faction)
        UnitType(self, TerminatorAssaultSquad, group=TerminatorAssaultSquad.faction)
        UnitType(self, VanguardVeteranSquad, group=VanguardVeteranSquad.faction)
        UnitType(self, VenDreadnoughts, group=VenDreadnoughts.faction)


class CastellansOfTheImperiumTroops(TroopsSection):
    def __init__(self, parent):
        super(CastellansOfTheImperiumTroops, self).__init__(parent, 4, None)
        UnitType(self, InfantryPlatoon, group=InfantryPlatoon.faction)
        UnitType(self, VeteranSquad, group=VeteranSquad.faction)
        UnitType(self, Sisters, group=Sisters.faction)
        UnitType(self, Scions, group=Scions.faction)
        UnitType(self, CrusaderSquad, group=CrusaderSquad.faction)
        UnitType(self, ScoutSquad, group=ScoutSquad.faction)
        UnitType(self, TacticalSquad, group=TacticalSquad.faction)
        self.bike_squad = UnitType(self, BikeSquad, active=False, group=BikeSquad.faction)



class CastellansOfTheImperiumFast(FastSection):
    def __init__(self, parent):
        super(CastellansOfTheImperiumFast, self).__init__(parent, 0, 6)
        UnitType(self, ArmouredSentinelSquad, group=ArmouredSentinelSquad.faction)
        UnitType(self, HellhoundSquadron, group=HellhoundSquadron.faction)
        UnitType(self, Riders, group=Riders.faction)
        UnitType(self, ScoutSentinelSquad, group=ScoutSentinelSquad.faction)
        UnitType(self, VendettaSquadron, group=VendettaSquadron.faction)
        UnitType(self, Valkyries, group=Valkyries.faction)
        UnitType(self, Dominions, group=Dominions.faction)
        UnitType(self, Seraphims, group=Seraphims.faction)
        UnitType(self, Stormhawks, group=Stormhawks.faction)
        UnitType(self, TauroxPrime, group=TauroxPrime.faction)
        UnitType(self, MTValkyries, group=MTValkyries.faction)
        UnitType(self, AssaultSquad, group=AssaultSquad.faction)
        UnitType(self, AttackBikeSquad, group=AttackBikeSquad.faction)
        self.bikes = UnitType(self, BikeSquad, group=BikeSquad.faction)
        UnitType(self, DropPod, group=DropPod.faction)
        UnitType(self, LandSpeederStorm, group=LandSpeederStorm.faction)
        UnitType(self, LandSpeederSquadron, group=LandSpeederSquadron.faction)
        UnitType(self, Razorback, group=Razorback.faction)
        UnitType(self, Rhino, group=Rhino.faction)
        UnitType(self, ScoutBikers, group=ScoutBikers.faction)
        UnitType(self, Stormtalons, group=Stormtalons.faction)


class CastellansOfTheImperiumHeavySupport(HeavySection):
    def __init__(self, parent):
        super(CastellansOfTheImperiumHeavySupport, self).__init__(parent, 0, 6)
        UnitType(self, ContemptorDreadnoughts, group=ContemptorDreadnoughts.faction)
        UnitType(self, BasiliskSquadron, group=BasiliskSquadron.faction)
        UnitType(self, Deathstrike, group=Deathstrike.faction)
        UnitType(self, HydraSquadron, group=HydraSquadron.faction)
        UnitType(self, LemanRussSquadron, group=LemanRussSquadron.faction)
        UnitType(self, Manticore, group=Manticore.faction)
        UnitType(self, WyvernSquadron, group=WyvernSquadron.faction)
        UnitType(self, Exorcist, group=Exorcist.faction)
        UnitType(self, PenitentEngine, group=PenitentEngine.faction)
        UnitType(self, Retributors, group=Retributors.faction)
        UnitType(self, CenturionDevastators, group=CenturionDevastators.faction)
        UnitType(self, Devastators, group=Devastators.faction)
        UnitType(self, Hunters, group=Hunters.faction)
        UnitType(self, LandRaider, group=LandRaider.faction)
        UnitType(self, LandRaiderCrusader, group=LandRaiderCrusader.faction)
        UnitType(self, LandRaiderRedeemer, group=LandRaiderRedeemer.faction)
        UnitType(self, Predators, group=Predators.faction)
        UnitType(self, Stalkers, group=Stalkers.faction)
        UnitType(self, Stormravens, group=Stormravens.faction)
        UnitType(self, Thunderfire, group=Thunderfire.faction)
        UnitType(self, Vindicators, group=Vindicators.faction)
        UnitType(self, Whirlwinds, group=Whirlwinds.faction)


class CastellansOfTheImperiumLordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(CastellansOfTheImperiumLordsOfWar, self).__init__(parent, 0, 3)
        UnitType(self, Paladin, group=Paladin.faction)
        UnitType(self, Errant, group=Errant.faction)
        UnitType(self, Warden, group=Warden.faction)
        UnitType(self, Crusader, group=Crusader.faction)
        UnitType(self, Gallant, group=Gallant.faction)
        UnitType(self, Baneblade, group=Baneblade.faction)
        UnitType(self, Banehammer, group=Banehammer.faction)
        UnitType(self, Banesword, group=Banesword.faction)
        UnitType(self, Doomhammer, group=Doomhammer.faction)
        UnitType(self, Hellhammer, group=Hellhammer.faction)
        UnitType(self, Shadowsword, group=Shadowsword.faction)
        UnitType(self, Stormlord, group=Stormlord.faction)
        UnitType(self, Stormsword, group=Stormsword.faction)


class MultiFaction(Wh40kBase):
    def check_rules(self):
        super(MultiFaction, self).check_rules()
        facset = set()
        for sec in self.sections:
            for unit in sec.units:
                facset.add(unit.faction)
        if len(facset) < 2:
            self.error("This Detachment must include units that have at least 2 different factions")

    def allow_relics(self):
        '''For Knights in detachment'''
        return False

    def is_cadia(self):
        '''For Astra Militarum'''
        return True


class GrandConvocationDetachment(MultiFaction):
    army_id = 'imperium_v1_grand_convocation_detachment'
    army_name = 'Grand Convocation Detachment'
    allow_cadia_arcana = True

    def __init__(self):
        self.hq = GrandConvocationHQ(parent=self)
        super(GrandConvocationDetachment, self).__init__(
            sections=[
                self.hq,
                GrandConvocationTroops(parent=self),
                GrandConvocationElites(parent=self),
                GrandConvocationFast(parent=self),
                GrandConvocationHeavySupport(parent=self),
                GrandConvocationLordsOfWar(parent=self)
            ]
        )

    def check_rules(self):
        super(GrandConvocationDetachment, self).check_rules()
        self.check_unit_limit(self.hq.tech.count, self.hq.serv,
                              "You may include one unit of Servitors for every Enginseer (AM)")


class CastellansOfTheImperiumDetachment(MultiFaction):
    army_id = 'imperium_v1_castellans_of_the_imperium_detachment'
    army_name = 'Castellans Of The Imperium Detachment'
    allow_cadia_holy = True
    ia_enabled = False

    def __init__(self):
        self.hq = CastellansOfTheImperiumHQ(parent=self)
        self.troops = CastellansOfTheImperiumTroops(parent=self)
        self.fast = CastellansOfTheImperiumFast(parent=self)
        super(CastellansOfTheImperiumDetachment, self).__init__(
            sections=[self.hq,
                      CastellansOfTheImperiumElites(parent=self),
                      self.troops,
                      self.fast,
                      CastellansOfTheImperiumHeavySupport(parent=self),
                      CastellansOfTheImperiumLordsOfWar(parent=self)]
        )
        self.cm_codex = SupplementOptions(self)

    def check_rules(self):
        super(CastellansOfTheImperiumDetachment, self).check_rules()
        self.check_unit_limit(self.hq.tech.count, self.hq.serv,
                              "You may include one unit of Servitors for every Enginseer (AM)")
        biker_troops = self.hq.has_biker_hq()
        self.troops.bike_squad.active = biker_troops
        self.move_units(biker_troops, self.fast.bikes, self.troops.bike_squad)
        if not biker_troops and self.troops.bike_squad.count > 0:
            self.error("Space Maine bikers can only be taken as troops there is an HQ selection on a bike in the army")
        # no librarians for templars!
        is_templar = self.is_templars()
        self.hq.librarian.active = not is_templar
        if is_templar and self.hq.librarian.count > 0:
            self.error("Black Templars detachments cannot include librarians")


    def is_base_codex(self):
        return self.cm_codex.cur in [self.cm_codex.base, self.cm_codex.scars,
                                     self.cm_codex.ravens, self.cm_codex.karchodons,
                                     self.cm_codex.hawks, self.cm_codex.scorpions, self.cm_codex.salamanders,
                                     self.cm_codex.fists,
                                     self.cm_codex.hands]

    def is_fists(self):
        return self.cm_codex.cur == self.cm_codex.fists

    def is_hands(self):
        return self.cm_codex.cur == self.cm_codex.hands

    def is_scars(self):
        return self.cm_codex.cur == self.cm_codex.scars

    def is_ravens(self):
        return self.cm_codex.cur == self.cm_codex.ravens

    def is_scorpions(self):
        return self.cm_codex.cur == self.cm_codex.scorpions

    def is_karchodons(self):
        return self.cm_codex.cur == self.cm_codex.karchodons

    def is_hawks(self):
        return self.cm_codex.cur == self.cm_codex.hawks

    def is_minotaurs(self):
        return self.cm_codex.cur == self.cm_codex.minotaurs

    def is_salamanders(self):
        return self.cm_codex.cur == self.cm_codex.salamanders

    def is_templars(self):
        return self.cm_codex.cur == self.cm_codex.templars

    def is_ultra(self):
        return self.cm_codex.cur == self.cm_codex.ultra


faction = 'Armies of the Imperium'


class ImperiumV1(Wh40k7ed):
    army_id = 'imperium_v1'
    army_name = 'Armies of the Imperium'
    faction = faction

    def __init__(self):
        super(ImperiumV1, self).__init__([
            Triumvirate,
            Triumvirate2,
            GrandConvocationDetachment,
            CastellansOfTheImperiumDetachment
        ])


detachments = [
    Triumvirate,
    Triumvirate2,
    GrandConvocationDetachment,
    CastellansOfTheImperiumDetachment
]
