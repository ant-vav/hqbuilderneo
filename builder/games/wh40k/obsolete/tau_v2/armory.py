__author__ = 'maria'

from builder.core2 import OptionsList, Count


class Ritual(OptionsList):
    def __init__(self, parent, ritual_points=1):
        super(Ritual, self).__init__(parent=parent, name='Options', limit=None)
        opt_name = 'Bonding knife ritual'
        self.base_ritual = self.variant(opt_name, ritual_points, per_model=True)
        self.enclaves_ritual = self.variant(opt_name, ritual_points, per_model=True, active=False)
        self.enclaves_ritual.value = True

    def check_rules(self):
        self.base_ritual.used = self.base_ritual.visible = self.roster.is_base
        self.enclaves_ritual.visible = self.enclaves_ritual.used = self.roster.is_enclaves

    @property
    def points(self):
        return super(Ritual, self).points * self.parent.get_count()


class Vehicle(OptionsList):
    def __init__(self, parent):
        super(Vehicle, self).__init__(parent, 'Options')
        self.blacksunfilter = self.variant('Blacksun filter', 1)
        self.decoylaunchers = self.variant('Decoy launchers', 3)
        self.automatedrepairsystem = self.variant('Automated repair system', 5)
        self.sensorspines = self.variant('Sensor spines', 5)
        self.flechettedischarger = self.variant('Flechette discharger', 10)
        self.pointdefencetargetingrelay = self.variant('Point defence targeting relay', 10)
        self.advancedtargetingsystem = self.variant('Advanced targeting system', 10)
        self.disruptionpod = self.variant('Disruption pod', 15)


class SupportSystem(OptionsList):
    def __init__(self, parent, slots=1, broadside=False, riptide=False):
        super(SupportSystem, self).__init__(parent=parent, name='Support system', limit=slots)

        self.advancedtargetingsystem = self.variant('Advanced targeting system', 3)
        self.counterfiresystem = self.variant('Counter fire system', 5)
        self.earlywarningoverride = self.variant('Early warning override', 5)
        self.positionalrelay = self.variant('Positional relay', 5)
        self.targetlock = self.variant('Target lock', 5)
        if not broadside and not riptide:
            self.vectoredretrothrusters = self.variant('Vectored retro-thrusters', 5, node_id='vrt')
        self.dronecontroller = self.variant('Drone controller', 8)
        self.velocitytracker = self.variant('Velocity tracker', 20)
        if riptide:
            self.stimulantinjector = self.variant('Stimulant injector', 35)
        else:
            self.stimulantinjector = self.variant('Stimulant injector', 15)
            self.shieldgenerator = self.variant('Shield generator', 25)

    def count_slots(self):
        if not self.used:
            return 0
        return self.count

    def set_free_slots(self, free_slots):
        self.limit = self.count + free_slots


class Drones(object):
    def __init__(self, parent, limit=2, broadside=False):
        super(Drones, self).__init__()

        self.limit = limit
        self.gundrone = Count(parent, 'Gun drone', min_limit=0, max_limit=0, points=12)
        self.markerdrone = Count(parent, 'Marker drone', min_limit=0, max_limit=0, points=12)
        self.shielddrone = Count(parent, 'Shield drone', min_limit=0, max_limit=0, points=12)
        self.drones = [self.gundrone, self.markerdrone, self.shielddrone]
        if broadside:
            self.missiledrone = Count(parent, 'Missile drone', min_limit=0, max_limit=0, points=12)
            self.drones.append(self.missiledrone)

    def check_rules(self):
        Count.norm_counts(0, self.limit, self.drones)

    @property
    def visible(self):
        return any(c.visible for c in self.drones)

    @visible.setter
    def visible(self, val):
        for c in self.drones:
            c.visible = val

    @property
    def used(self):
        return any(c.used for c in self.drones)

    @used.setter
    def used(self, val):
        for c in self.drones:
            c.used = val


class SignatureSystem(OptionsList):

    def __init__(self, parent, slots=3, commander=False):
        super(SignatureSystem, self).__init__(parent=parent, name='Signature system', limit=slots)
        self.commander = commander

        self.neurowebsystemjammer = self.variant('Neuroweb System Jammer', 2)
        self.onagergauntlet = self.variant('Onager gauntlet', 5)
        self.failsafedetonator = self.variant('Failsafe detonator', 10)
        self.repulsorimpactfield = self.variant('Repulsor impact field', 10)
        self.commandandcontrolnode = self.variant('Command and control node', 15)
        self.puretideengramneurochip = self.variant('Puretide Engram Neurochip', 15)
        self.multispectrumsensorsuit = self.variant('Multi-spectrum Sensor Suit', 20)
        self.xv802crisisiridiumbattlesuit = self.variant('XV8-02 Crisis \'Iridium\' Battlesuit', 25)
        self.base = [
            self.neurowebsystemjammer, self.onagergauntlet, self.failsafedetonator, self.repulsorimpactfield,
            self.commandandcontrolnode, self.puretideengramneurochip, self.multispectrumsensorsuit,
            self.xv802crisisiridiumbattlesuit
        ]

        self.themirrorcodex = self.variant('The Mirrorcodex', 50)
        self.seismicfibrillatornode = self.variant('Seismic Fibrillator Node', 45)
        self.warscaperdrone = self.variant('Warscaper Drone', 35)
        self.earthcastepilotarray = self.variant('Earth Caste Pilot Array', 30)
        self.thetalismanofarthasmoloch = self.variant('The Talisman of Arthas Moloch', 25)
        self.enclaves = [
            self.themirrorcodex, self.seismicfibrillatornode, self.warscaperdrone, self.earthcastepilotarray,
            self.thetalismanofarthasmoloch
        ]
        if self.commander:
            self.fusionblades = self.variant('Fusion Blades', 30)
            self.enclaves.append(self.fusionblades)

    def count_slots(self):
        if self.used:
            return self.count
        return 0

    def set_free_slots(self, free_slots):
        OptionsList.process_limit(
            options=self.base if self.roster.is_base else self.enclaves,
            limit=(self.count + free_slots) if self.used else len(self.options)
        )
        if not self.used:
            for o in self.options:
                o.value = False

    def check_rules(self):
        if self.commander and not self.parent.weapon.has_fusion() and self.fusionblades.value and \
                self.roster.is_enclaves:
            self.parent.error('Fusion blades available for Commander with a twin-linked fusion blaster only')
        for o in self.base:
            o.visible = o.used = self.roster.is_base
        for o in self.enclaves:
            o.visible = o.used = self.roster.is_enclaves

    def get_unique_gear(self):
        if self.used:
            return self.description
        return []


class Weapon(OptionsList):
    def __init__(self, parent, name='Weapon', slots=3):
        self.flamer = Count(parent, 'Flamer', min_limit=0, max_limit=slots, points=5)
        self.burstcannon = Count(parent, 'Burst cannon', min_limit=0, max_limit=slots, points=10)
        self.fusionblaster = Count(parent, 'Fusion blaster', min_limit=0, max_limit=slots, points=15)
        self.missilepod = Count(parent, 'Missile pod', min_limit=0, max_limit=slots, points=15)
        self.plasmarifle = Count(parent, 'Plasma rifle', min_limit=0, max_limit=slots, points=15)
        self.one_slot = [self.flamer, self.burstcannon, self.fusionblaster, self.missilepod, self.plasmarifle]

        self.twinlinkedflamer = Count(parent, 'Twin-linked flamer', min_limit=0, max_limit=slots/2, points=10)
        self.twinlinkedburstcannon = Count(parent, 'Twin-linked burst cannon', min_limit=0, max_limit=slots/2,
                                           points=15)
        self.twinlinkedfusionblaster = Count(parent, 'Twin-linked fusion blaster', min_limit=0, max_limit=slots/2,
                                             points=20)
        self.twinlinkedmissilepod = Count(parent, 'Twin-linked missile pod', min_limit=0, max_limit=slots/2, points=20)
        self.twinlinkedplasmarifle = Count(parent, 'Twin-linked plasma rifle', min_limit=0, max_limit=slots/2, points=20)
        self.two_slot = [self.twinlinkedburstcannon, self.twinlinkedflamer, self.twinlinkedfusionblaster,
                         self.twinlinkedmissilepod, self.twinlinkedplasmarifle]

        super(Weapon, self).__init__(parent, name)
        self.airburstingfragmentationprojector = self.variant('Airbursting fragmentation projector', 15)
        self.cyclicionblaster = self.variant('Cyclic ion blaster', 15)

    def count_slots(self):
        if not self.used:
            return 0
        return self.count + sum(c.cur for c in self.one_slot) + sum(c.cur for c in self.two_slot) * 2

    def set_free_slots(self, free_slots):
        for c in self.one_slot:
            c.max = c.cur + free_slots
        for c in self.two_slot:
            c.max = c.cur + free_slots / 2
        self.process_limit([self.airburstingfragmentationprojector, self.cyclicionblaster], self.count + free_slots)

    def has_fusion(self):
        return self.twinlinkedfusionblaster.cur

    def get_unique_gear(self):
        return self.description
