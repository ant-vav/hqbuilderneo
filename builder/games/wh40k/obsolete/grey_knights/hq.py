__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, StaticUnit, ListSubUnit


class Draigo(StaticUnit):
    name = 'Lord Kaldor Draigo'
    base_points = 275
    gear = ['Terminator armour', 'Storm bolter', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades',
            'The Titansword', 'Storm shield']


class Mordrak(Unit):
    name = 'Grand Master Mordrak'
    base_points = 200
    gear = ['Terminator armour', 'Storm bolter', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades',
            'Master-crafted Nemesis Daemon hammer', 'Iron Halo']

    def get_unique(self):
        return self.name

    class GhostKnights(Unit):
        name = 'Ghost Knights'

        class GhostKnight(ListSubUnit):
            name = 'Ghost Knight'
            gear = ['Terminator armour', 'Storm bolter', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']
            base_points = 40

            def __init__(self):
                ListSubUnit.__init__(self)
                self.weapon = self.opt_one_of('Weapon', [
                    ['Nemesis force sword', 0, 'nsw'],
                    ['Nemesis force halberd', 0, 'nhal'],
                    ['Nemesis Daemon hammer', 0, 'nham'],
                    ['Pair of Nemesis falcions', 5, 'nfalc'],
                    ['Brotherhood banner', 25, 'ban']
                ])

            def have_banner(self):
                return self.weapon.get_cur() == 'ban'

            def set_enable_banner(self, val):
                self.weapon.set_active_options(['ban'], (val or self.have_banner()) and self.count.get() == 1)

        def __init__(self):
            Unit.__init__(self)
            self.warriors = self.opt_units_list(self.GhostKnight.name, self.GhostKnight, 1, 5)

        def check_rules(self):
            self.warriors.update_range()
            have_banner = reduce(lambda val, u: u.have_banner() or val, self.warriors.get_units(), False)
            for u in self.warriors.get_units():
                u.set_enable_banner(not have_banner)
            self.points.set(self.build_points(count=1))
            self.build_description()

        def get_count(self):
            return self.warriors.get_count()

    def __init__(self):
        Unit.__init__(self)
        self.knights = self.opt_optional_sub_unit(self.GhostKnights.name, [self.GhostKnights()])


class Stern(StaticUnit):
    name = 'Brother-Captein Stern'
    base_points = 200
    gear = ['Terminator armour', 'Storm bolter', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades',
            'Nemesis force sword', 'Iron Halo']


class Crowe(StaticUnit):
    name = 'Castellan Crowe'
    base_points = 150
    gear = ['Artificer armour', 'Storm bolter', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades',
            'The Blade of Antwyr', 'Iron Halo']


class GrandMaster(Unit):
    name = 'Grand Master'
    base_points = 175
    gear = ['Terminator armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades', 'Iron Halo']

    def __init__(self):
        Unit.__init__(self)
        self.mastery = self.opt_one_of('Psyker', [
            ['Mastery level 1', 0, 'ml1'],
            ['Mastery level 2', 35, 'ml2']
        ])
        self.rng = self.opt_one_of('Weapon', [
            ['Storm bolter', 0, 'sbgun'],
            ['Incinerator', 5, 'inc'],
            ['Psilencer', 35, 'psl'],
            ['Psycannon', 45, 'pscan']
        ])
        self.rngup = self.opt_options_list('', [['Make master-crafted', 5, 'wup']])
        self.mlwep = self.opt_one_of('', [
            ['Nemesis force sword', 0, 'nsw'],
            ['Nemesis force halberd', 5, 'nhal'],
            ['Nemesis Daemon hammer', 5, 'nham'],
            ['Pair of Nemesis falcions', 10, 'nfalc'],
            ['Nemesis warding stave', 35, 'nws']
        ])
        self.mlwup = self.opt_options_list('', [['Make master-crafted', 5, 'wup']])
        self.sculls = self.opt_count('Servo-sculls', 0, 3, 5)
        self.opt = self.opt_options_list('Options', [
            ['Blind grenades', 5, 'bg'],
            ['Meltabombs', 5, 'mb'],
            ['Digital weapons', 5, 'dw'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Empyrean brain mines', 10, 'ebm'],
            ['Psychotroke grenades', 15, 'pg'],
            ['Rad grenades', 15, 'rg'],
            ['Orbital strike relay', 50, 'psr']
        ])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(exclude=[self.rng.id, self.rngup.id, self.mlwep.id, self.mlwup.id])
        if self.rngup.get('wup'):
            self.description.add('Master crafted ' + self.rng.get_selected())
        else:
            self.description.add(self.rng.get_selected())
        if self.mlwup.get('wup'):
            self.description.add('Master crafted ' + self.mlwep.get_selected())
        else:
            self.description.add(self.mlwep.get_selected())


class Captain(Unit):
    name = 'Brother-Captain'
    base_points = 150
    gear = ['Terminator armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades', 'Iron Halo']

    def __init__(self):
        Unit.__init__(self)
        self.rng = self.opt_one_of('Weapon', [
            ['Storm bolter', 0, 'sbgun'],
            ['Incinerator', 5, 'inc'],
            ['Psilencer', 30, 'psl'],
            ['Psycannon', 40, 'pscan']
        ])
        self.rngup = self.opt_options_list('', [['Make master-crafted', 5, 'wup']])
        self.mlwep = self.opt_one_of('', [
            ['Nemesis force sword', 0, 'nsw'],
            ['Nemesis force halberd', 5, 'nhal'],
            ['Nemesis Daemon hammer', 5, 'nham'],
            ['Pair of Nemesis falcions', 10, 'nfalc'],
            ['Nemesis warding stave', 35, 'nws']
        ])
        self.mlwup = self.opt_options_list('', [['Make master-crafted', 5, 'wup']])
        self.sculls = self.opt_count('Servo-sculls', 0, 3, 5)
        self.opt = self.opt_options_list('Options', [
            ['Blind grenades', 5, 'bg'],
            ['Meltabombs', 5, 'mb'],
            ['Digital weapons', 5, 'dw'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Empyrean brain mines', 10, 'ebm'],
            ['Psychotroke grenades', 15, 'pg'],
            ['Rad grenades', 15, 'rg'],
            ['Orbital strike relay', 50, 'psr']
        ])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(exclude=[self.rng.id, self.rngup.id, self.mlwep.id, self.mlwup.id])
        if self.rngup.get('wup'):
            self.description.add('Master crafted ' + self.rng.get_selected())
        else:
            self.description.add(self.rng.get_selected())
        if self.mlwup.get('wup'):
            self.description.add('Master crafted ' + self.mlwep.get_selected())
        else:
            self.description.add(self.mlwep.get_selected())


class Champion(Unit):
    name = 'Brotherhood Champion'
    base_points = 100
    gear = ['Artificer armour', 'Storm bolter', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades',
            'Anointed blade', 'Iron Halo']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Digital weapons', 5, 'dw'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Empyrean brain mines', 10, 'ebm'],
            ['Psychotroke grenades', 15, 'pg'],
            ['Rad grenades', 15, 'rg'],
            ['Orbital strike relay', 50, 'psr']
        ])


class Librarian(Unit):
    name = 'Librarian'
    base_points = 150
    gear = ['Terminator armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades', 'Psychic hood', 'Storm bolter']

    def __init__(self):
        Unit.__init__(self)
        self.mastery = self.opt_one_of('Psyker', [
            ['Mastery level 2', 0, 'ml2'],
            ['Mastery level 3', 50, 'ml3']
        ])
        self.mlwep = self.opt_one_of('Weapon', [
            ['Nemesis force sword', 0, 'nsw'],
            ['Nemesis force halberd', 5, 'nhal'],
            ['Nemesis Daemon hammer', 5, 'nham'],
            ['Pair of Nemesis falcions', 10, 'nfalc'],
            ['Nemesis warding stave', 35, 'nws']
        ])
        self.mlwup = self.opt_options_list('', [['Make master-crafted', 5, 'wup']])
        self.sculls = self.opt_count('Servo-sculls', 0, 3, 5)
        self.opt = self.opt_options_list('Options', [
            ['Digital weapons', 5, 'dw'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Empyrean brain mines', 10, 'ebm'],
            ['Teleport homer', 15, 'th']
        ])
        self.pwr = self.opt_options_list('Powers', [
            ['Dark Excommunication', 5, 'de'],
            ['Quicksilver', 5, 'q'],
            ['Might of Titan', 5, 'mot'],
            ['Sanctuary', 5, 'san'],
            ['Smite', 5, 'smt'],
            ['The Shrouding', 5, 'shr'],
            ['The Summoning', 5, 'smn'],
            ['Vortex of Doom', 5, 'vod'],
            ['Warp Rift', 5, 'wr']
        ])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(exclude=[self.mlwep.id, self.mlwup.id])
        if self.mlwup.get('wup'):
            self.description.add('Master crafted ' + self.mlwep.get_selected())
        else:
            self.description.add(self.mlwep.get_selected())


class Coteaz(StaticUnit):
    name = 'Inquisitor Coteaz'
    base_points = 100
    gear = ['Artificer armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades',
            'Master-crafted Nemesis Daemon hammer', 'Psyber-eagle']


class Karamazov(StaticUnit):
    name = 'Inquisitor Karamazov'
    base_points = 200
    gear = ['Master-crafted multi melta', 'Master-crafted power sword', 'Frag grenades', 'Krak grenades',
            'Psyk-out grenades', 'Rad grenades', 'Orbital strike relay', 'Throne of Judgement']


class Valeria(StaticUnit):
    name = 'Inquisitor Valeria'
    base_points = 140
    gear = ['Power armour', 'Laspistol', 'Graviton beamer', 'The Dagger of Midnight', 'Frag grenades', 'Krak grenades',
            'Psyk-out grenades', 'Forcefield', 'Hyperstone maze', 'Runes of Destiny']


class Malleus(Unit):
    name = 'Ordo Malleus inquisitor'
    base_points = 25

    def __init__(self):
        Unit.__init__(self)
        weaponlist = [
            ['Power sword', 10, 'psw'],
            ['Plasma pistol', 10, 'ppist'],
            ['Combi-flamer', 10, 'cflame'],
            ['Combi-melta', 10, 'cmgun'],
            ['Combi-plasma', 10, 'cpgun'],
            ['Power fist', 15, 'pfist'],
            ['Incinerator', 15, 'inc'],
            ['Nemesis Daemon hammer', 15, 'hnam'],
            ['Daemonblade', 15, 'dblade'],
            ['Hellrifle', 15, 'hgun']
        ]
        self.rngwep = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + weaponlist +
                                                [['Force sword', 0, 'fsw']])
        self.mlwep = self.opt_one_of('', [['Chainsword', 0, 'csw']] + weaponlist + [['Force sword', 0, 'fsw']])
        self.trng = self.opt_one_of('Weapon', [
            ['Storm bolter', 0, 'sbgun'],
            ['Combi-flamer', 10, 'cflame'],
            ['Combi-melta', 10, 'cmgun'],
            ['Combi-plasma', 10, 'cpgun'],
            ['Psycannon', 15, 'psycan'],
            ['Force sword', 0, 'fsw']
        ])
        self.tmel = self.opt_one_of('', [
            ['Nemesis Daemon hammer', 0, 'hnam'],
            ['Force sword', 0, 'fsw']
        ])
        self.armour = self.opt_one_of('Armour', [
            ['Caparace armour', 0, 'cap'],
            ['Power armour', 8, 'pwr'],
            ['Terminator armour', 40, 'tda']
        ])
        self.sculls = self.opt_count('Servo-sculls', 0, 3, 3)
        self.opt = self.opt_options_list('Options', [
            ['Psybolt ammunition', 5, 'pba'],
            ['Empyrean brain mines', 10, 'ebm']
        ])
        self.psy = self.opt_options_list('Become Psyker', [
            ['Mastery level 1', 30, 'ml1']
        ])

    def check_rules(self):
        is_tda = self.armour.get_cur() == 'tda'
        self.rngwep.set_visible(not is_tda)
        self.mlwep.set_visible(not is_tda)
        self.tmel.set_visible(is_tda)
        self.trng.set_visible(is_tda)
        if is_tda:
            self.gear = ['Psyk-out grenades']
        else:
            self.gear = ['Frag grenades', 'Krak grenades', 'Psyk-out grenades']
        is_psy = self.psy.get('ml1')
        for select in [self.tmel, self.trng, self.rngwep, self.mlwep]:
            select.set_active_options(['fsw'], is_psy)
        Unit.check_rules(self)


class Hereticus(Unit):
    name = 'Ordo Hereticus inquisitor'
    base_points = 25
    gear = ['Frag grenades', 'Krak grenades', 'Psyk-out grenades']

    def __init__(self):
        Unit.__init__(self)
        weaponlist = [
            ['Power sword', 10, 'psw'],
            ['Plasma pistol', 10, 'ppist'],
            ['Combi-flamer', 10, 'cflame'],
            ['Combi-melta', 10, 'cmgun'],
            ['Combi-plasma', 10, 'cpgun'],
            ['Power fist', 15, 'pfist'],
            ['Condemnor boltgun', 15, 'cbbgun'],
            ['Thunder hammer', 20, 'ham'],
            ['Null rod', 25, 'rod']
        ]
        self.rngwep = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + weaponlist +
                                                [['Force sword', 0, 'fsw']])
        self.mlwep = self.opt_one_of('', [['Chainsword', 0, 'csw']] + weaponlist + [['Force sword', 0, 'fsw']])
        self.armour = self.opt_one_of('Armour', [
            ['Caparace armour', 0, 'cap'],
            ['Power armour', 8, 'pwr']
        ])
        self.sculls = self.opt_count('Servo-sculls', 0, 3, 3)
        self.opt = self.opt_options_list('Options', [
            ['Psyocculum', 25, 'occ']
        ])
        self.psy = self.opt_options_list('Become Psyker', [
            ['Mastery level 1', 30, 'ml1']
        ])

    def check_rules(self):
        is_psy = self.psy.get('ml1')
        for select in [self.rngwep, self.mlwep]:
            select.set_active_options(['fsw'], is_psy)
        Unit.check_rules(self)


class Xenos(Unit):
    name = 'Ordo Xenos inquisitor'
    base_points = 25
    gear = ['Frag grenades', 'Krak grenades', 'Psyk-out grenades']

    def __init__(self):
        Unit.__init__(self)
        weaponlist = [
            ['Power sword', 10, 'psw'],
            ['Plasma pistol', 10, 'ppist'],
            ['Combi-flamer', 10, 'cflame'],
            ['Combi-melta', 10, 'cmgun'],
            ['Combi-plasma', 10, 'cpgun'],
            ['Needle pistol', 15, 'npist'],
            ['Scythian venom talon', 15, 'talon'],
            ['Conversion beamer', 45, 'beam']
        ]
        self.rngwep = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + weaponlist +
                                                [['Force sword', 0, 'fsw']])
        self.mlwep = self.opt_one_of('', [['Chainsword', 0, 'csw']] + weaponlist + [['Force sword', 0, 'fsw']])
        self.armour = self.opt_one_of('Armour', [
            ['Caparace armour', 0, 'cap'],
            ['Power armour', 8, 'pwr']
        ])
        self.sculls = self.opt_count('Servo-sculls', 0, 3, 3)
        self.opt = self.opt_options_list('Options', [
            ['Digital weapons', 5, 'dgw'],
            ['Ulumeathi Plasma Syphon', 10, 'syph'],
            ['Rad grenades', 15, 'rgr'],
            ['Psychotroke grenades', 15, 'psgr']
        ])
        self.psy = self.opt_options_list('Become Psyker', [
            ['Mastery level 1', 30, 'ml1']
        ])

    def check_rules(self):
        is_psy = self.psy.get('ml1')
        for select in [self.rngwep, self.mlwep]:
            select.set_active_options(['fsw'], is_psy)
        Unit.check_rules(self)
