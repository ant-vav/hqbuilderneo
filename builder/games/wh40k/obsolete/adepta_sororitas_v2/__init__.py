from builder.games.wh40k.obsolete.adepta_sororitas import troops, elites, fast, hq

__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, HQSection, ElitesSection, TroopsSection, FastSection, HeavySection,\
    Fort
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar
from builder.games.wh40k.obsolete.adepta_sororitas import heavy


Celestine = Unit.norm_core1_unit(hq.Celestine)
Jacobus = Unit.norm_core1_unit(hq.Jacobus)
Canoness = Unit.norm_core1_unit(hq.Canoness)
CommandSquad = Unit.norm_core1_unit(hq.CommandSquad)
Priest = Unit.norm_core1_unit(hq.Priest)
Conclave = Unit.norm_core1_unit(hq.Conclave)


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.celestine = UnitType(self, Celestine)
        self.jacobus = UnitType(self, Jacobus)
        self.canoness = UnitType(self, Canoness)
        self.commandsquad = UnitType(self, CommandSquad, slot=0)
        self.priest = UnitType(self, Priest, slot=0)
        self.conclave = UnitType(self, Conclave, slot=0)


Celestians = Unit.norm_core1_unit(elites.Celestians)
Repentia = Unit.norm_core1_unit(elites.Repentia)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        self.celestians = UnitType(self, Celestians)
        self.repentia = UnitType(self, Repentia)


Sisters = Unit.norm_core1_unit(troops.Sisters)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.sisters = UnitType(self, Sisters)


Seraphims = Unit.norm_core1_unit(fast.Seraphims)
Dominions = Unit.norm_core1_unit(fast.Dominions)


class FastAttack(FastSection):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        self.seraphims = UnitType(self, Seraphims)
        self.dominions = UnitType(self, Dominions)


Retributors = Unit.norm_core1_unit(heavy.Retributors)
Exorcist = Unit.norm_core1_unit(heavy.Exorcist)
PenitentEngine = Unit.norm_core1_unit(heavy.PenitentEngine)


class HeavySupport(HeavySection):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        self.retributors = UnitType(self, Retributors)
        self.exorcist = UnitType(self, Exorcist)
        self.penitentengine = UnitType(self, PenitentEngine)


class AdeptaSororitasV2Base(Wh40kBase):
    army_id = 'adepta_sororitas_v2'
    army_name = 'Adepta Sororitas'

    def __init__(self):

        super(AdeptaSororitasV2Base, self).__init__(
            hq=HQ(parent=self),
            elites=Elites(parent=self),
            troops=Troops(parent=self),
            fast=FastAttack(parent=self),
            heavy=HeavySupport(parent=self),
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )

    def check_rules(self):
        super(AdeptaSororitasV2Base, self).check_rules()

        self.check_unit_limit(
            self.hq.jacobus.count + self.hq.priest.count, self.hq.conclave,
            "You can't have more Battle Conclaves then Confessors (including Uriah Jacobus)."
        )
        self.check_unit_limit(
            self.hq.canoness.count, self.hq.commandsquad,
            "You can't have more Sororitas Command Squads then Canonesses."
        )
        if self.hq.priest.count > 5:
            self.error('You can\'t take more then 5 Preachers.')

class AdeptaSororitasV2CAD(AdeptaSororitasV2Base, CombinedArmsDetachment):
    army_id = 'adepta_sororitas_v2_cad'
    army_name = 'Adepta Sororitas (Combined arms detachment)'

class AdeptaSororitasV2AD(AdeptaSororitasV2Base, AlliedDetachment):
    army_id = 'adepta_sororitas_v2_ad'
    army_name = 'Adepta Sororitas (Allied detachment)'

faction = 'Adepta_sororotas'

class AdeptaSororitasV2(Wh40k7ed):
    army_id = 'adepta_sororitas_v2'
    army_name = 'Adepta Sororitas'
    faction = faction
    obsolete = True
    def __init__(self):
        super(AdeptaSororitasV2, self).__init__([AdeptaSororitasV2CAD])

detachments = [AdeptaSororitasV2CAD, AdeptaSororitasV2AD]
