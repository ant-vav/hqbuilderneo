__author__ = 'Ivan Truskov'

from builder.core import get_ids

special_gear = [
    ['Melta bombs', 5, 'mbomb'],
    ['Blight grenades', 5, 'blgr'],
    ['Sigil of corruption', 25, 'sig']
]

ride = [
    ['Jump pack', 15, 'jp'],
    ['Chaos bike', 20, 'cbike'],
    ['Juggernaut of Khorne', 35, 'jug'],
    ['Disk of Tzeentch', 30, 'disc'],
    ['Palanquin of Nurgle', 40, 'pal'],
    ['Steed of Slaanesh', 20, 'steed']
]

reward = [
    ['Ichor blood', 5, 'ibl'],
    ['Gift of mutation', 10, 'gom'],
    ['Aura of dark glory', 15, 'adg'],
    ['Combat familiar', 15, 'cfam'],
    ['Spell familiar', 15, 'sfam']
]

artefact_weapon = [
    ['Burning Brand of Skalathrax', 30, 'brand'],
    ['Axe of Blind Fury', 35, 'blind'],
    ['The Murder Sword', 35, 'msword'],
    ['The Black Mace', 45, 'bmace']
]
artefact_weapon_ids = get_ids(artefact_weapon)

artefact_option = [
    ['Dimensional Key', 25, 'dkey'],
    ['Scrolls of Magnus', 45, 'msc']
]
artefact_option_ids = get_ids(artefact_option)

marks = [
    ['Mark of Khorne', 10, 'kh'],
    ['Mark of Tzeentch', 15, 'tz'],
    ['Mark of Nurgle', 15, 'ng'],
    ['Mark of Slaanesh', 15, 'sl']
]

vehicle_upgrades = [
    ["Combi-bolter", 5, 'cbgun'],
    ["Dirge caster", 5, 'dcstr'],
    ['Warpflame gargoyles', 5, 'wfgarg'],
    ['Combi-melta', 10, 'cmelta'],
    ['Combi-flamer', 10, 'cflame'],
    ['Combi-plasma', 10, 'cplasma'],
    ["Extra armour", 10, 'exarm'],
    ['Havok launcher', 12, 'hvklnch'],
    ['Daemonic possession', 15, 'pos']
]

tank_upgrades = vehicle_upgrades + [
    ["Dozer blade", 5, 'dblade'],
    ['Destroyer blades', 15, 'desblade']
]

melee = [
    ['Chainaxe', 8, 'chaxe'],
    ['Lightning claw', 15, 'lclaw'],
    ['Power weapon', 15, 'pwep'],
    ['Power fist', 25, 'pfist']
]

ranged = [
    ['Combi-bolter', 3, 'cbgun'],
    ['Combi-melta', 10, 'cmelta'],
    ['Combi-flamer', 10, 'cflame'],
    ['Combi-plasma', 10, 'cplasma'],
    ['Plasma pistol', 15, 'ppist']
]
ranged_ids = get_ids(ranged)

tda_melee = [
    ['Lightning claw', 5, 'lclaw'],
    ['Power fist', 10, 'pfist'],
    ['Chainfist', 15, 'chfist']
]

tda_ranged = [
    ['Combi-melta', 7, 'cmelta'],
    ['Combi-flamer', 7, 'cflame'],
    ['Combi-plasma', 7, 'cplasma'],
    ['Power weapon', 12, 'pwep'],
    ['Lightning claw', 17, 'lclaw'],
    ['Power fist', 22, 'pfist'],
    ['Chainfist', 27, 'chfist']
]

bl_artefacts = [
    ['The Spineshiver Blade', 30, 'blade'],
    ['The Crucible of Lies', 25, 'cruc'],
    ['The Eye of Night', 75, 'eye'],
    ['The Skull of Ker\'ngar', 40, 'skull'],
    ['The Hand of Darkness', 50, 'hand'],
]

bl_psy_artefacts = bl_artefacts + [
    ['Last Memory of the Yuranthos', 30, 'mem'],
]
