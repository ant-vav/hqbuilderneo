__author__ = 'Denis Romanov'

from builder.core.unit import Unit
from builder.games.wh40k.obsolete.tau.armory import *
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor


class Devilfish(Unit):
    name = 'Devilfish'
    base_points = 80
    gear = ['Burst cannon']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon system', [
            ['Two Gun Drones', 0, 'gd'],
            ['Twin linked smart missile system', 10, 'sms']
        ])
        self.opt = self.opt_options_list('Options', vehicle)
        self.mis = self.opt_count('Seeker missile', 0, 2, 8)

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description(exclude=([self.wep.id] if self.wep.get_cur() == 'gd' else []))
        if self.wep.get_cur() == 'gd':
            self.description.add('Gun Drone', 2)


class FireWarriors(Unit):
    name = 'Fire Warrior team'

    class Shasui(Unit):
        name = 'Shas\'ui'
        base_points = 19
        base_gear = ['Combat armour', 'Photon grenades']

        def __init__(self):
            Unit.__init__(self)
            self.opt_one_of('Weapon', [
                ['Pulse rifle', 0],
                ['Pulse carbine', 0],
            ])
            self.opt = self.opt_options_list('Options', [
                ['Markerlight and target lock', 15, 'ml']
            ])
            self.drones = [self.opt_count(tp[0], 0, 2, tp[1]) for tp in drones]

        def set_grenades(self, val):
            self.gear = self.base_gear + (['EMP grenades'] if val else [])
            norm_counts(0, 2, self.drones)
            self.base_points = 19 + (2 if val else 0)
            self.points.set(self.build_points())
            self.build_description(exclude=[self.opt.id])
            if self.opt.get('ml'):
                self.description.add('Markerlight')
                self.description.add('Target lock')

        def check_rules(self):
            pass

    def __init__(self, enclaves=False):
        Unit.__init__(self)
        self.leader = self.opt_optional_sub_unit('Team leader', self.Shasui())
        self.team = self.opt_count('Fire warrior', 6, 12, 9)
        self.pc = self.opt_count('Pulse carbine', 0, 6, 0)
        self.enclaves = enclaves
        if self.enclaves:
            self.opt = self.opt_options_list('Options', [
                ['EMP grenades', 2, 'emp'],
            ])
        else:
            self.opt = self.opt_options_list('Options', [
                ['EMP grenades', 2, 'emp'],
                ['Bonding knife ritual', 1, 'bk'],
            ])
        self.transport = self.opt_optional_sub_unit('Transport', Devilfish())

    def check_rules(self):
        leader = self.leader.get_count()
        self.team.update_range(6 - leader, 12 - leader)
        self.pc.update_range(0, self.team.get())
        if leader:
            self.leader.get_unit().get_unit().set_grenades(self.opt.get('emp'))
        if self.enclaves:
            self.set_points(self.build_points(exclude=[self.opt.id], count=1) +
                            (self.opt.points() + 1) * self.team.get())
        else:
            self.set_points(self.build_points(exclude=[self.opt.id], count=1) + self.opt.points() * self.team.get())
        self.build_description(
            exclude=[self.team.id, self.pc.id, self.transport.id, self.opt.id],
            count=1,
            gear=['Bonding knife ritual'] if self.opt.get('bk') or self.enclaves else []
        )
        desc = ModelDescriptor('Fire warrior', gear=['Combat armour', 'Photon grenades'], points=9)
        if self.opt.get('emp'):
            desc.add_gear('EMP grenades', points=2)
        self.description.add(desc.clone().add_gear('Pulse carbine').build(self.pc.get()))
        self.description.add(desc.clone().add_gear('Pulse rifle').build(self.team.get() - self.pc.get()))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.team.get() + self.leader.get_count()


class EnclavesFireWarriors(FireWarriors):
    def __init__(self):
        FireWarriors.__init__(self, enclaves=True)


class KrootSquad(Unit):
    name = "Kroot Carnivore squad"

    class Shaper(Unit):
        name = 'Shaper'
        base_points = 21
        base_gear = ['Kroot armour']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Kroot rifle', 0, 'kr'],
                ['Pulse rifle', 4, 'pr'],
                ['Pulse carbine', 4, 'pc']
            ])

        def has_kroot_rifle(self):
            return self.wep.get_cur() == 'kr'

        def set_rounds(self, val):
            self.gear = self.base_gear + (['Sniper rounds'] if val and self.has_kroot_rifle() else [])
            self.base_points = 21 + (1 if val and self.has_kroot_rifle() else 0)
            Unit.check_rules(self)

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_optional_sub_unit('Leader', self.Shaper())
        self.team = self.opt_count('Kroot', 10, 20, 6)
        self.dog = self.opt_count('Kroot Hound', 0, 10, 5)
        self.cav = self.opt_count('Krootox rider', 0, 3, 25)
        self.opt = self.opt_options_list('Options', [['Sniper rounds', 1, 'sr']])

    def check_rules(self):
        self.team.update_range(10 - self.leader.get_count(), 20 - self.leader.get_count())
        if self.leader.get_unit():
            self.leader.get_unit().get_unit().set_rounds(self.opt.get('sr'))
        self.set_points(self.build_points(count=1, exclude=[self.opt.id]) + self.opt.points() * self.team.get())
        self.build_description(count=1, options=[self.leader])
        desc = ModelDescriptor('Kroot', points=6, gear=['Kroot armour', 'Kroot rifle'])
        if self.opt.get('sr'):
            desc.add_gear_opt(self.opt)
        self.description.add(desc.build(count=self.team.get()))
        self.description.add(
            ModelDescriptor('Krootox rider', points=25, gear=['Kroot armour', 'Kroot gun']).build(self.cav.get()))
        self.description.add(ModelDescriptor('Kroot hound', points=5).build(self.dog.get()))

    def get_count(self):
        return self.team.get() + self.dog.get() + self.cav.get() + self.leader.get_count()
