from builder.core2 import OptionsList, OneOf, Gear

__author__ = 'Ivan Truskov'


class VehicleEquipment(OptionsList):
    def __init__(self, parent, blade=True):
        super(VehicleEquipment, self).__init__(parent, 'Options')
        self.sbgun = self.Variant(self, 'Storm bolter', 5)
        if blade:
            self.dblade = self.Variant(self, 'Dozer blade', 5)
        self.hkm = self.Variant(self, 'Hunter-killer missile', 10)
        self.exarm = self.Variant(self, 'Extra armour', 10)


class BaseWeapon(OneOf):
    def __init__(self, parent, name, **kwargs):
        self.armour = kwargs.pop('armour', None)
        self.pwr_weapon = []
        self.tda_weapon = []
        self.relic_options = []
        super(BaseWeapon, self).__init__(parent, name, **kwargs)

    def check_rules(self):
        super(BaseWeapon, self).check_rules()
        if self.armour:
            if self.armour.is_tda():
                visible = self.tda_weapon
                invisible = self.pwr_weapon
            else:
                visible = self.pwr_weapon
                invisible = self.tda_weapon

            for wep in visible:
                wep.visible = True
            for wep in invisible:
                wep.visible = False

    def get_unique(self):
        if self.used and self.cur in self.relic_options:
            return self.description
        return []


class BoltPistol(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(BoltPistol, self).__init__(*args, **kwargs)
        self.boltpistol = self.variant('Bolt pistol', 0)
        self.pwr_weapon += [self.boltpistol]


class Chainsword(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Chainsword, self).__init__(*args, **kwargs)
        self.chainsword = self.variant('Chainsword', 0)
        self.pwr_weapon += [self.chainsword]


class Crozius(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Crozius, self).__init__(*args, **kwargs)
        self.roz = self.variant('Crozius Arcanum', 0)


class MeleePlain(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(MeleePlain, self).__init__(*args, **kwargs)
        self.mle = self.variant('Melee weapon', 0)
        self.pwr_weapon += [self.mle]


class Ranged(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Ranged, self).__init__(*args, **kwargs)
        self.sb = self.variant('Storm bolter', 5)
        self.cf = self.variant('Combi-flamer', 10)
        self.cm = self.variant('Combi-melta', 10)
        self.cp = self.variant('Combi-plasma', 10)
        self.pp = self.variant('Plasma pistol', 15)
        self.pwr_weapon += [self.sb, self.cf, self.cm, self.cp, self.pp]


class Melee(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Melee, self).__init__(*args, **kwargs)
        self.pwr = self.variant('Power weapon', 15)
        self.ss = self.variant('Storm shield', 15)
        self.fs = self.variant('Frost sword', 20)
        self.fa = self.variant('Frost axe', 20)
        # check for 2 wolf claws is made separately for price correction
        self.wcl = self.variant('Wolf claw', 20)
        self.fist = self.variant('Power fist', 25)
        self.ham = self.variant('Thunder hammer', 30)
        self.pwr_weapon += [self.pwr, self.ss, self.fs, self.fa, self.wcl,
                            self.fist, self.ham]

    def is_melee(self):
        return self.cur in [self.pwr, self.ss, self.fs, self.fa, self.wcl,
                            self.fist, self.ham]


class TerminatorRanged(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(TerminatorRanged, self).__init__(*args, **kwargs)
        self.tda_sb = self.variant('Storm Bolter', 0)
        self.tda_cf = self.variant('Combi-flamer', 5)
        self.tda_cm = self.variant('Combi-melta', 5)
        self.tda_cp = self.variant('Combi-plasma', 5)
        self.tda_wc = self.variant('Wolf claw', 15)
        self.tda_ham = self.variant('Thunder hammer', 25)
        self.tda_weapon += [self.tda_sb, self.tda_cf, self.tda_cm,
                            self.tda_cp, self.tda_wc, self.tda_ham]


class TerminatorMelee(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(TerminatorMelee, self).__init__(*args, **kwargs)
        self.tda_pwr = self.variant('Power weapon', 0)
        self.tda_ss = self.variant('Storm shield', 0)
        self.tda_fs = self.variant('Frost sword', 5)
        self.tda_fa = self.variant('Frost axe', 5)
        # check for 2 wolf claws is made separately for price correction
        self.tda_wc2 = self.variant('Wolf claw', 10)
        self.tda_fist = self.variant('Power fist', 10)
        self.tda_cfist = self.variant('Chainfist', 15)
        self.tda_ham2 = self.variant('Thunder hammer', 15)
        self.tda_weapon += [self.tda_pwr, self.tda_ss, self.tda_fs,
                            self.tda_fa, self.tda_wc2, self.tda_fist,
                            self.tda_cfist, self.tda_ham2]


class Boltgun(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Boltgun, self).__init__(*args, **kwargs)
        self.boltgun = self.variant('Boltgun', 0)
        self.pwr_weapon += [self.boltgun]


class Heavy(BaseWeapon):

    class Flakk(OptionsList):
        def __init__(self, parent):
            super(Heavy.Flakk, self).__init__(parent=parent, name='', limit=None)
            self.flakkmissiles = self.variant('Flakk missiles', 10)

    def __init__(self, parent, scout=False, *args, **kwargs):
        super(Heavy, self).__init__(parent, *args, **kwargs)

        self.heavybolter = self.variant('Heavy bolter', 10)
        self.missilelauncher = self.variant('Missile launcher', 15)
        if not scout:
            self.multimelta = self.variant('Multi-melta', 10)
            self.plasmacannon = self.variant('Plasma cannon', 15)
            self.lascannon = self.variant('Lascannon', 20)

        self.heavy = [self.heavybolter, self.missilelauncher]
        if not scout:
            self.heavy += [self.multimelta, self.plasmacannon,
                           self.lascannon]
        self.flakk = self.Flakk(parent=parent)

    def is_heavy(self):
        return self.cur in self.heavy

    def check_rules(self):
        super(Heavy, self).check_rules()
        self.flakk.visible = self.flakk.used = self.cur == self.missilelauncher
        # if self.flakk.visible:
        #     pdb.set_trace()


class Special(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Special, self).__init__(*args, **kwargs)

        self.flamer = self.variant('Flamer', 5)
        self.meltagun = self.variant('Meltagun', 10)
        self.plasmagun = self.variant('Plasma gun', 15)
        self.spec = [self.flamer, self.meltagun, self.plasmagun]

    def is_spec(self):
        return self.cur in self.spec


class DredWeapon(OneOf):
    def __init__(self, parent):
        super(DredWeapon, self).__init__(parent, 'Dreadnought weapon')
        self.variant('Multi-melta', 0)
        self.variant('Twin-linked autocannon', 5)
        self.variant('Twin-linked heavy bolter', 5)
        self.variant('Twin-linked heavy flamer', 5)
        self.variant('Plasma cannon', 10)
        self.variant('Assault cannon', 20)
        self.variant('Hellfrost cannon', 20)
        self.variant('Twin-linked lascannon', 25)


class MeltaBombs(OptionsList):
    def __init__(self, parent):
        super(MeltaBombs, self).__init__(parent, "Options")
        self.variant("Melta bombs", 5)


class Options(MeltaBombs):
    def __init__(self, parent, armour=None, iron_priest=False, wolf_mount=0):
        super(Options, self).__init__(parent)
        self.armour = armour
        self.ride = []

        self.digitalweapons = self.variant('Digital weapons', 10)

        self.spacemarinebike = self.variant('Space Marine Bike', 20)
        self.ride += [self.spacemarinebike]

        if not iron_priest:
            self.jumppack = self.variant('Jump pack', 15)
            self.ride += [self.jumppack]

        if wolf_mount:
            self.wolf = self.variant('Thunderwolf mount', 50)
            self.ride += [self.wolf]

    def check_rules(self):
        super(Options, self).check_rules()
        if self.armour is not None:
            for opt in self.ride:
                opt.visible = opt.used = not self.armour.is_tda()
        self.process_limit(self.ride, 1)

    def has_bike(self):
        return self.spacemarinebike.value


class RelicOption(OptionsList):
    def __init__(self, parent):
        self.flag = parent.roster.is_champion()

        super(RelicOption, self).__init__(parent, 'Relic', limit=1)

        self.helm = self.variant('The Helm of Durfast', 20)
        self.stone = self.variant('The Wulfen Stone', 40)
        base = [self.helm, self.stone]
        self.pelt = self.variant('The Pelt of Balewolf', 10)
        self.teeth = self.variant('Fellclaw\'s Teeth', 15)
        supplement = [self.pelt, self.teeth]
        # hide unused
        if self.flag:
            da_list = base
        else:
            da_list = supplement
        for relic in da_list:
            relic.visible = relic.used = False

    def get_unique(self):
        if self.used:
            return self.description
        return []


class RelicWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(RelicWeapon, self).__init__(*args, **kwargs)
        self.flag = self.parent.roster.is_champion()
        self.bite = self.variant('The bite of Fenris', 10)
        self.death = self.variant('Black Death', 25)
        self.fangsword = self.variant('Fangsword of the Ice Wolf', 25)

        self.relic_weapon = [self.bite, self.death, self.fangsword]
        self.relic_options += self.relic_weapon[:]

        self.fury = self.variant('Frostfury', 15)
        self.bone = self.variant('Krakenbone sword', 35)
        self.mclaws = self.variant('Morkai\'s claws', 45)

        self.champion_weapon = [self.fury, self.bone, self.mclaws]
        self.relic_options += self.champion_weapon

        if self.flag:
            da_list = self.relic_weapon
        else:
            da_list = self.champion_weapon
        for relic in da_list:
            relic.visible = relic.used = False

    def has_claws(self):
        return self.cur == self.mclaws


class PowerFist(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerFist, self).__init__(*args, **kwargs)
        self.fist = self.variant('Power fist', 25)
        self.pwr_weapon += [self.fist]


class Armour(OneOf):
    power_armour_set = [Gear('Power armour'),
                        Gear('Frag grenades'),
                        Gear('Krak grenades')]
    runic_armour_set = [Gear('Runic armour'),
                        Gear('Frag grenades'),
                        Gear('Krak grenades')]
    scout_armour_set = [Gear('Scout armor'),
                        Gear('Frag grenades'),
                        Gear('Krak grenades')]

    def __init__(self, parent, art=0, tda=0, relic=False):
        super(Armour, self).__init__(parent, 'Armour')
        self.pwr = self.variant('Power armour', 0, gear=self.power_armour_set)
        self.run = art and self.variant('Runic armour',
                                        art, gear=self.runic_armour_set)
        self.the_armour = self.variant('The Armour of Russ', 35,
                                       gear=[Gear('The Armour of Russ'),
                                             Gear('Frag grenades'),
                                             Gear('Krak grenades')])
        self.tda = tda and self.variant('Terminator armour', tda)
        self.the_armour2 = self.variant('Armour of Asvald Stormwrack', 50)
        if relic:
            self.the_armour.visible = self.the_armour.used = not parent.roster.is_champion()
            self.the_armour2.visible = self.the_armour2.used = parent.roster.is_champion()
        else:
            self.the_armour.visible = self.the_armour.used = False
            self.the_armour2.visible = self.the_armour2.used = False

    def is_tda(self):
        return self.used and self.cur in [self.tda, self.the_armour2]

    def get_unique(self):
        if self.used and self.cur in [self.the_armour, self.the_armour2]:
            return self.description[0:1]
        return []


class PowerWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerWeapon, self).__init__(*args, **kwargs)
        self.pwr = self.variant('Power weapon', 15)
        self.pwr_weapon += [self.pwr]


class PlasmaPistol(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PlasmaPistol, self).__init__(*args, **kwargs)
        self.ppist = self.variant('Plasma pistol', 15)
        self.pwr_weapon += [self.ppist]

# 
# 
# class ForceWeapon(BaseWeapon):
#     def __init__(self, *args, **kwargs):
#         super(ForceWeapon, self).__init__(*args, **kwargs)
#         self.force = self.variant('Force weapon', 0)
# 
# 

# 
# class LightningClaw(BaseWeapon):
#     def __init__(self, *args, **kwargs):
#         super(LightningClaw, self).__init__(*args, **kwargs)
#         self.claw = self.variant('Lightning claw', 15)
#         self.pwr_weapon += [self.claw]
# 
# 
# class ThunderHammer(BaseWeapon):
#     def __init__(self, *args, **kwargs):
#         super(ThunderHammer, self).__init__(*args, **kwargs)
#         self.hummer = self.variant('Thunder hammer', 30)
#         self.pwr_weapon += [self.hummer]


