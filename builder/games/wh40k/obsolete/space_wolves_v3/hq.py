__author__ = 'Ivan Truskov'

from builder.core2 import Unit, Gear, Count, OneOf, OptionalSubUnit,\
    SubUnit
from armory import Melee, Chainsword, BoltPistol, Ranged,\
    Armour, Options, RelicOption, RelicWeapon, TerminatorRanged,\
    TerminatorMelee, BaseWeapon, Crozius, Boltgun
from fast import DropPod, WolfTransport


class WolfLord(Unit):
    type_id = 'wolf_lord_v3'
    type_name = 'Wolf Lord'

    class MeleeWeapon(RelicWeapon, Ranged, Melee,
                      TerminatorMelee, Chainsword):
        pass

    class RangedWeapon(RelicWeapon, Ranged, Melee,
                       TerminatorRanged, BoltPistol):
        def __init__(self, *args, **kwargs):
            super(WolfLord.RangedWeapon, self).__init__(*args, **kwargs)
            self.tda_ss2 = self.variant('Storm shield', 15)
            self.tda_weapon += [self.tda_ss2]

    def __init__(self, parent):
        super(WolfLord, self).__init__(parent, self.type_name, 105,
                                       [Gear('Belt of Russ')])
        self.armour = Armour(self, 20, 40, relic=True)
        self.wep1 = self.MeleeWeapon(self, name='Weapon', armour=self.armour)
        self.wep2 = self.RangedWeapon(self, name='Weapon', armour=self.armour)
        self.opt = Options(self, self.armour, wolf_mount=50)
        self.relic = RelicOption(self)
        self.wolves = Count(self, 'Fenrisian Wolf', 0, 2, 8, True)

    def build_points(self):
        res = super(WolfLord, self).build_points()
        if self.wep1.cur == self.wep1.wcl and self.wep2.cur == self.wep2.wcl:
            res = res - 10
        return res

    def check_rules(self):
        super(WolfLord, self).check_rules()
        self.wep1.used = self.wep1.visible = not self.wep2.has_claws()
        self.wep2.used = self.wep2.visible = not self.wep1.has_claws()
        rel_cnt = len(self.get_unique_gear())
        if rel_cnt > 1:
            self.error("Cannot carry more then one relic; taken: {}".format(rel_cnt))

    def get_unique_gear(self):
        return self.wep1.get_unique() + self.wep2.get_unique() +\
            self.relic.get_unique() + self.armour.get_unique()


class Ragnar(Unit):
    type_id = 'ragnar_v3'
    type_name = 'Ragnar Blackmane, The Young King'

    def __init__(self, parent):
        super(Ragnar, self).__init__(parent, name='Ragnar Blackmane',
                                     points=195, unique=True,
                                     gear=[
                                         Gear('Power armour'),
                                         Gear('Bolt pistol'),
                                         Gear('Frostfang'),
                                         Gear('Frag grenades'),
                                         Gear('Krak grenades'),
                                         Gear('Melta bombs'),
                                         Gear('Belt of Russ')
                                     ])
        self.wolves = Count(self, 'Fenrisian Wolf', 0, 2, 8, True)


class Harald(Unit):
    type_id = 'harald_v3'
    type_name = 'Harald Deathwolf, Lord of the wolfkin'

    def __init__(self, parent):
        super(Harald, self).__init__(parent, name='Harald Deathwolf',
                                     points=190, unique=True,
                                     gear=[
                                         Gear('Power armour'),
                                         Gear('Bolt pistol'),
                                         Gear('Frost axe'),
                                         Gear('Mantle of the Ice Troll King'),
                                         Gear('Frag grenades'),
                                         Gear('Krak grenades'),
                                         Gear('Storm shield'),
                                         Gear('Thunderwolf mount')
                                     ])
        self.wolves = Count(self, 'Fenrisian Wolf', 0, 2, 8, True)


class Canis(Unit):
    type_id = 'canis_v3'
    type_name = 'Canis Wolfborn, The Feral Knight'

    def __init__(self, parent):
        super(Canis, self).__init__(parent, name='Canis Wolfborn',
                                    points=185, unique=True,
                                    gear=[
                                        Gear('Power armour'),
                                        Gear('Bolt pistol'),
                                        Gear('Wolf claw', count=2),
                                        Gear('Frag grenades'),
                                        Gear('Krak grenades'),
                                        Gear('Thunderwolf mount')
                                    ])
        self.wolves = Count(self, 'Fenrisian Wolf', 0, 2, 8, True)


class RunePriest(Unit):
    type_id = 'rune_priest_v3'
    type_name = 'Rune Priest'

    class Runic(OneOf):
        def __init__(self, parent, name):
            super(RunePriest.Runic, self).__init__(parent, name)
            self.variant('Runic axe', 0)
            self.variant('Runic sword', 0)
            self.variant('Runic stave', 0)

    # i believe we cannot exchange runic weapon for ranged
    class MeleeWeapon(RelicWeapon, Runic):
        pass

    class PriestRanged(BaseWeapon):
        def __init__(self, *args, **kwargs):
            super(RunePriest.PriestRanged, self).__init__(*args, **kwargs)
            none = self.variant('(No weapon)', 0, gear=[])
            sb = self.variant('Storm bolter', 5)
            cf = self.variant('Combi-flamer', 10)
            cm = self.variant('Combi-melta', 10)
            cp = self.variant('Combi-plasma', 10)
            self.tda_weapon += [none, sb, cf, cm, cp]

    class RangedWeapon(RelicWeapon, PriestRanged, Ranged, BoltPistol):
        pass

    class Options(Options):
        def __init__(self, parent, armour):
            super(RunePriest.Options, self).__init__(parent, armour, iron_priest=True)
            self.variant('Psychic hood', 10)

    class Mastery(OneOf):
        def __init__(self, parent):
            super(RunePriest.Mastery, self).__init__(parent, 'Psyker')
            self.variant('Mastery level 1', 0)
            self.variant('Mastery level 2', 25)

    def __init__(self, parent):
        super(RunePriest, self).__init__(parent, self.type_name, 60)
        self.armour = Armour(self, 25, 25, relic=True)
        self.wep1 = self.MeleeWeapon(self, name='Weapon', armour=self.armour)
        self.wep2 = self.RangedWeapon(self, name='Weapon', armour=self.armour)
        self.opt = self.Options(self, self.armour)
        self.mastery = self.Mastery(self)
        self.relic = RelicOption(self)

    def check_rules(self):
        super(RunePriest, self).check_rules()
        self.wep1.used = self.wep1.visible = not self.wep2.has_claws()
        self.wep2.used = self.wep2.visible = not self.wep1.has_claws()
        rel_cnt = len(self.get_unique_gear())
        if rel_cnt > 1:
            self.error("Cannot carry more then one relic; taken: {}".format(rel_cnt))

    def get_unique_gear(self):
        return self.wep1.get_unique() + self.wep2.get_unique() +\
            self.relic.get_unique() + self.armour.get_unique()


class Njal(Unit):
    type_id = 'njal_v3'
    type_name = 'Njal Stormcaller, The tempest that walks'

    class Armour(OneOf):
        runic_armour_set = [Gear('Runic armour'),
                            Gear('Frag grenades'),
                            Gear('Krak grenades')]

        def __init__(self, parent):
            super(Njal.Armour, self).__init__(parent, 'Armour')
            self.run = self.variant('Runic armour', 0, gear=self.runic_armour_set)
            self.tda = self.variant('Terminator armour', 0)

    def __init__(self, parent):
        super(Njal, self).__init__(parent, 'Njal Storlmcaller', 180,
                                   unique=True, gear=[
                                       Gear('Bolt pistol'),
                                       Gear('Psychic hood'),
                                       Gear('Nightwing'),
                                       Gear('Staff of the Stormcaller')
                                   ])
        self.Armour(self)


class WolfPriest(Unit):
    type_id = 'wolf_priest_v3'
    type_name = 'Wolf Priest'

    class PriestRanged(BaseWeapon):
        def __init__(self, *args, **kwargs):
            super(WolfPriest.PriestRanged, self).__init__(*args, **kwargs)
            self.tda_sb = self.variant('Storm Bolter', 0)
            self.tda_cf = self.variant('Combi-flamer', 5)
            self.tda_cm = self.variant('Combi-melta', 5)
            self.tda_cp = self.variant('Combi-plasma', 5)
            self.tda_weapon += [self.tda_sb, self.tda_cf, self.tda_cm,
                                self.tda_cp]

    class MeleeWeapon(RelicWeapon, Crozius):
        pass

    class RangedWeapon(RelicWeapon, PriestRanged, Ranged, BoltPistol):
        pass

    def __init__(self, parent):
        super(WolfPriest, self).__init__(parent, self.type_name, 110,
                                         gear=[Gear('Healing balms'), Gear('Wolf amulet')])
        self.armour = Armour(self, 20, 30, relic=True)
        self.wep1 = self.MeleeWeapon(self, 'Weapon', armour=self.armour)
        self.wep2 = self.RangedWeapon(self, 'Weapon', armour=self.armour)
        Options(self, armour=self.armour)
        self.relic = RelicOption(self)

    def check_rules(self):
        super(WolfPriest, self).check_rules()
        self.wep1.used = self.wep1.visible = not self.wep2.has_claws()
        self.wep2.used = self.wep2.visible = not self.wep1.has_claws()
        rel_cnt = len(self.get_unique_gear())
        if rel_cnt > 1:
            self.error("Cannot carry more then one relic; taken: {}".format(rel_cnt))

    def get_unique_gear(self):
        return self.wep1.get_unique() + self.wep2.get_unique() +\
            self.relic.get_unique() + self.armour.get_unique()


class Ulrik(Unit):
    type_id = 'ulrik_v3'
    type_name = 'Ulrik the Slayer, Wolf High Priest'

    def __init__(self, parent):
        super(Ulrik, self).__init__(parent, 'Ulrik the Slayer', 145,
                                   unique=True, static=True, gear=[
                                       Gear('Power armour'),
                                       Gear('Plasma pistol'),
                                       Gear('Crozius arcanum'),
                                       Gear('Frag grenades'),
                                       Gear('Krak grenades'),
                                       Gear('Healing balms'),
                                       Gear('Wolf amulet'),
                                       Gear('Wolf Helm of Russ')
                                   ])


class BattleLeader(Unit):
    type_id = 'guard_leader_v3'
    type_name = 'Wolf Guard Battle Leader'

    class MeleeWeapon(RelicWeapon, Ranged, Melee,
                      TerminatorMelee, Chainsword):
        pass

    class RangedWeapon(RelicWeapon, Ranged, Melee,
                       TerminatorRanged, Boltgun, BoltPistol):
        def __init__(self, *args, **kwargs):
            super(BattleLeader.RangedWeapon, self).__init__(*args, **kwargs)
            self.tda_ss2 = self.variant('Storm shield', 25)
            self.tda_weapon += [self.tda_ss2]

    def __init__(self, parent):
        super(BattleLeader, self).__init__(parent, self.type_name, 50)
        self.armour = Armour(self, 25, 40, relic=True)
        self.wep1 = self.MeleeWeapon(self, name='Weapon', armour=self.armour)
        self.wep2 = self.RangedWeapon(self, name='Weapon', armour=self.armour)
        self.opt = Options(self, self.armour, wolf_mount=50)
        self.relic = RelicOption(self)
        self.wolves = Count(self, 'Fenrisian Wolf', 0, 2, 8, True)

    def build_points(self):
        res = super(BattleLeader, self).build_points()
        if self.wep1.cur == self.wep1.wcl and self.wep2.cur == self.wep2.wcl:
            res = res - 10
        return res

    def check_rules(self):
        super(BattleLeader, self).check_rules()
        self.wep1.used = self.wep1.visible = not self.wep2.has_claws()
        self.wep2.used = self.wep2.visible = not self.wep1.has_claws()
        rel_cnt = len(self.get_unique_gear())
        if rel_cnt > 1:
            self.error("Cannot carry more then one relic; taken: {}".format(rel_cnt))

    def get_unique_gear(self):
        return self.wep1.get_unique() + self.wep2.get_unique() +\
            self.relic.get_unique() + self.armour.get_unique()


class Bjorn(Unit):
    type_id = 'bjorn_v3'
    type_name = 'Bjorn the Fell-Handed, Last of the Company of Russ'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Bjorn.Weapon, self).__init__(parent, 'Ranged weapon')
            self.variant('Assault cannon', 0)
            self.variant('Plasma cannon', 0)
            self.variant('Helfrost cannon', 0)
            self.variant('Twin-linked lascannon', 5)

    def __init__(self, parent):
        super(Bjorn, self).__init__(parent, 'Bjorn the Fell-Handed',
                                    220, unique=True, gear=[
                                        Gear('Trueclaw'),
                                        Gear('Searchlight'),
                                        Gear('Smoke launchers')
                                    ])
        self.Weapon(self)
        WolfTransport(self, transport_types=[DropPod])
