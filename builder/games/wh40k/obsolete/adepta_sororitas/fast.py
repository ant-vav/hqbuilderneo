__author__ = 'Ivan Truskov'

from builder.core.unit import Unit
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.adepta_sororitas.troops import Rhino, Immolator
from builder.games.wh40k.obsolete.adepta_sororitas.armory import melee, ranged


class Seraphims(Unit):
    name = 'Seraphim Squad'
    common_gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Jump pack']
    model_points = 15

    class Superior(Unit):
        name = 'Seraphim Superior'

        def __init__(self):
            self.base_points = Seraphims.model_points + 10
            self.gear = Seraphims.common_gear
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Bolt pistol', 0, 'bpist'],
                ['Chainsword', 0, 'csw'],
                ['Power sword', 15, 'psw'],
            ])
            self.wep2 = self.opt_one_of('', [
                ['Bolt pistol', 0, 'bpist'],
                ['Plasma pistol', 15, 'ppist']
            ])
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.squad = self.opt_count('Seraphim', 5, 10, self.model_points)
        self.sup = self.opt_optional_sub_unit('', self.Superior())
        self.fl = self.opt_count('Hand flamers', 0, 2, 10)
        self.inf = self.opt_count('Inferno pistols', 0, 2, 30)

    def check_rules(self):
        norm_counts(0, 2, [self.fl, self.inf])
        self.squad.update_range(5 - self.sup.get_count(), 10 - self.sup.get_count())
        self.set_points(self.build_points(count=1))
        self.build_description(options=[self.sup])
        desc = ModelDescriptor('Seraphim', points=self.model_points, gear=self.common_gear)
        self.description.add(desc.clone().add_gear('Hand flamer', count=2, points=10).build(self.fl.get()))
        self.description.add(desc.clone().add_gear('Inferno pistol', count=2, points=30).build(self.inf.get()))
        self.description.add(desc.clone().add_gear('Bolt pistol',
                                                   count=2).build(self.squad.get() - self.inf.get() - self.fl.get()))

    def get_count(self):
        return self.squad.get() + self.sup.get_count()


class Dominions(Unit):
    name = 'Dominion Squad'
    common_gear = ['Power armour', 'Frag grenades', 'Krak grenades']
    model_points = 13

    class Superior(Unit):
        name = 'Dominion Superior'

        def __init__(self):
            self.gear = Dominions.common_gear
            self.base_points = Dominions.model_points
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [['Boltgun', 0, 'bgun']] + melee + ranged)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist']] + melee + ranged)
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
            ])
            self.vet = self.opt_options_list('', [
                ['Veteran', 10, 'vet']
            ])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Dominion Superior', exclude=[self.vet.id])
            else:
                self.build_description()

    def __init__(self):
        Unit.__init__(self)
        self.sup = self.opt_sub_unit(self.Superior())
        self.squad = self.opt_count('Dominion', 4, 9, self.model_points)
        self.opt = self.opt_options_list('Options', [
            ['Simulacrum imperialis', 10, 'simp'],
        ], limit=1)
        self.sb = self.opt_count('Storm Bolter', 0, 4, 5)
        self.fl = self.opt_count('Flamer', 0, 4, 5)
        self.mg = self.opt_count('Meltagun', 0, 4, 10)
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')

    def check_rules(self):
        free = 3 if self.squad.get() == 4 and self.opt.is_any_selected() else 4
        norm_counts(0, free, [self.fl, self.sb, self.mg])
        self.points.set(self.build_points(count=1))
        self.build_description(options=[self.sup])
        desc = ModelDescriptor('Dominion', points=self.model_points, gear=self.common_gear + ['Bolt Pistol'])
        count = self.squad.get()
        if self.opt.is_any_selected():
            self.description.add(desc.clone().add_gear('Boltgun').add_gear_opt(self.opt).build(1))
            count -= 1
        self.description.add(desc.clone().add_gear('Storm Bolter', points=5).build(self.sb.get()))
        self.description.add(desc.clone().add_gear('Flamer', points=5).build(self.fl.get()))
        self.description.add(desc.clone().add_gear('Meltagun', points=10).build(self.mg.get()))
        self.description.add(desc.clone().add_gear('Boltgun').build(count - self.fl.get() - self.sb.get() -
                                                                    self.mg.get()))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.squad.get() + 1
