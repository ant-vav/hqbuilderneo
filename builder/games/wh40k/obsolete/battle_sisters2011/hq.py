__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.games.wh40k.obsolete.battle_sisters2011.troops import Rhino,Immolator
from builder.core.options import norm_counts
class Celestine(StaticUnit):
    name = 'Saint Celestine'
    base_points = 115
    gear = ['Armour of Saint Katherine','The Ardent Blade','Frag grenades','Krak grenades','Jump pack']

class Canoness(Unit):
    name = 'Canoness'
    base_points = 65
    gear = ['Power armour','Frag grenades','Krak grenades']
    def __init__(self):
        Unit.__init__(self)
        weaponlist = [
            ['Chainsword',0,'chsw'],
            ['Storm Bolter',3,'sbgun'],
            ['Power sword',10,'psw'],
            ['Combi-melta', 10, 'cmelta' ],
            ['Combi-flamer', 10, 'cflame' ],
            ['Combi-plasma', 10, 'cplasma' ],
            ['Condemnor boltgun',15,'cbgun'],
            ['Plasma pistol',15,'ppist'],
            ['Inferno pistol',15,'ipist'],
            ['Evisecrator',25,'evsc']
        ]
        self.wep1 = self.opt_one_of('Weapon', [['Boltgun',0,'bgun']] + weaponlist)
        self.wep2 = self.opt_one_of('Weapon',[['Bolt pistol',0,'bpist']] + weaponlist)
        self.opt = self.opt_options_list('Options',[
            ['Melta bombs',5,'mbomb'],
            ['Rosarius', 25, 'rs']
        ])

common_gear = ['Power armour','Bolt pistol','Frag grenades','Krak grenades']

class CommandSquad(Unit):
    name = 'Sororitas command squad'
    base_points = 115

    class Hospitaler(StaticUnit):
        name = 'Hospitaler'
        gear = common_gear + ['Chirurgeon\'s tools']

    class Dialogus(StaticUnit):
        name = 'Dialogus'
        gear = common_gear + ['Loud hailer']

    class Celestian(Unit):
        name = 'Celestian'
        gear = common_gear
        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                    ['Boltgun',0,'bgun'],
                    ['Chainsword',0,'chsw'],
                    ['Storm Bolter',3,'sbgun'],
                    ['Flamer', 5, 'flame' ],
                    ['Heavy bolter', 5,'hbgun'],
                    ['Meltagun', 10, 'mgun' ],
                    ['Multi-melta',10, 'mmgun'],
                    ['Heavy flamer',20,'hflame']
            ])
            self.flag = self.opt_options_list('Options',[
                    ['Simulacrum imperialis',20,'simp'],
                    ['Blessed banner',15,'bban']
            ],1)
            self.have_flag = False
        def check_rules(self):
            if not len(self.flag.get_selected()) > 0:
                self.flag.set_active_options(self.flag.get_all_ids(), not self.have_flag)
            Unit.check_rules(self)
    def __init__(self):
        Unit.__init__(self)
        self.doc = self.opt_sub_unit(self.Hospitaler())
        self.speaker = self.opt_sub_unit(self.Dialogus())
        self.cel = [self.opt_sub_unit(self.Celestian()) for i in range(0,3)]
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')
    def check_rules(self):
        flag = reduce(lambda val, c: val + (1 if len(c.get_unit().flag.get_selected()) > 0 else 0), self.cel, 0)
        for c in self.cel:
            c.get_unit().have_flag = (flag > 0)
            c.get_unit().check_rules()
        Unit.check_rules(self)

class Kyrinov(StaticUnit):
    name = 'Arch-Confessor Kyrinov'
    base_points = 90
    gear = ['Flak armour','Bolt pistol','Mace of Valaan','Frag grenades','Krak grenades','Icon of Chiros','Rosarius']

class Jacobus(StaticUnit):
    name = 'Uriah Jacobus, Protector of the Faith'
    base_points = 90
    gear = ['Flak armour','Bolt pistol','The Redeemer','Chainsword','Frag grenades','Krak grenades','Banner of Sanctity','Rosarius']

class Confessor(Unit):
    name = 'Eccleiarchy Confessor'
    base_points = 75
    gear = ['Flak armour','Frag grenades','Krak grenades','Rosarius']
    def __init__(self):
        Unit.__init__(self)
        weaponlist = [
            ['Boltgun',2,'bgun'],
            ['Bolt pistol',2,'bpist'],
            ['Shotgun',2,'sgun'],
            ['Storm Bolter',3,'sbgun'],
            ['Power sword',10,'psw'],
            ['Combi-melta', 10, 'cmelta' ],
            ['Combi-flamer', 10, 'cflame' ],
            ['Combi-plasma', 10, 'cplasma' ],
            ['Condemnor boltgun',15,'cbgun'],
            ['Plasma pistol',15,'ppist'],
            ['Evisecrator',25,'evsc']
        ]
        self.wep1 = self.opt_one_of('Weapon', [['Laspistol',0,'lpist']] + weaponlist)
        self.wep2 = self.opt_one_of('Weapon',[['Chainsword',0,'chsw']] + weaponlist)
        self.opt = self.opt_options_list('Options',[
            ['Melta bombs',5,'mbomb'],
            ['Plasma gun', 15, 'pgun']
        ])

class Conclave(Unit):
    name = 'Battle Conclave'
    class Assassin(ListSubUnit):
        min = 0
        max = 10
        base_points = 15
        name = 'Death Cult Assassin'
        gear = ['Flak armour','Power weapon','Power weapon']
    class Flagellant(ListSubUnit):
        min = 0
        max = 10
        base_points = 15
        name = 'Arco-flagellant'
        gear = ['Arco-flails']
    class Crusader(ListSubUnit):
        min = 0
        max = 10
        base_points = 15
        name = 'Crusader'
        gear = ['Flak armour','Power weapon','Storm shield']

    def __init__(self):
        Unit.__init__(self)
        self.ass = self.opt_sub_unit(self.Assassin())
        self.arc = self.opt_sub_unit(self.Flagellant())
        self.crus = self.opt_sub_unit(self.Crusader())
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')

    def check_rules(self):
        norm_counts(3,10,[self.ass.get_unit().count,self.arc.get_unit().count,self.crus.get_unit().count])
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return self.ass.get_count() + self.arc.get_count() + self.crus.get_count()
