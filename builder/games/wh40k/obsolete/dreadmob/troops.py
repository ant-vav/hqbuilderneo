__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, ListUnit, ListSubUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.core.options import norm_counts


class Trukk(Unit):
    name = "Scrap Trukk"
    base_points = 35
    gear = ['Armour plates', 'Grabbin\' Klaw']

    def __init__(self):
        Unit.__init__(self)
        self.rng = self.opt_one_of('Weapon', [
            ["Big shoota", 0],
            ["Rokkit launcha", 5],
        ])
        self.opt = self.opt_options_list('Options', [
            ["Red paint job", 5],
            ["Grot riggers", 5],
            ["Stikkbomb chukka", 5],
            ["Boarding plank", 5],
            ["Wreckin' ball", 10],
            ["Reinforces ram", 5],
        ])


class Boyz(Unit):
    name = 'Spanna Boyz'

    class Mek(Unit):
        name = 'Mek'
        base_points = 16

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Slugga and Choppa', 0, 'sc'],
                ['Shoota', 0, 'sht'],
                ['Burna', 10, 'bc'],
                ["Rokkit launcha", 10, 'rl'],
                ["Big shoota", 5, 'bs'],
                ["Kustom mega-blasta", 10, 'kmb']
            ])

            self.opt = self.opt_options_list('Options', [
                ["Grot oiler", 5, 'go']
            ])

        def set_options(self, shoota_boys, bombs, cyborg):
            self.gear = []
            if cyborg:
                self.gear += ['Cybork body']
                self.base_points += 5
            self.base_points += (1 if bombs else 0)
            self.gear += (['Stikkbombs'] if bombs else [])
            self.wep.set_active_options(['sht'], shoota_boys)
            Unit.check_rules(self)

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.boyz = self.opt_count("Boyz", 10, 20, 6)

        self.weapon = self.opt_one_of('Weapon', [
            ['Choppa and Slugga', 0, 'cs'],
            ["Shoota", 0, 'shoota'],
        ])

        self.opt = self.opt_options_list('Options', [
            ["Stikkbombs", 1, 'bombs']
        ])

        self.mek = self.opt_options_list('Boss', [
            ["Mek", 10, 'up'],
        ])
        self.mek_unit = self.opt_sub_unit(self.Mek())
        self.ride = self.opt_optional_sub_unit('Transport', [Trukk()])
        self.cyborg = self.opt_options_list('Da Dok is in', [
            ["Cybork body", 3, 'cyborg'],
        ])

    def check_rules(self):
        self.mek_unit.set_active(self.mek.get('up'))
        self.ride.set_active(self.get_count() <= 12)

        mek = 1 if self.mek.get('up') else 0
        if mek:
            self.mek_unit.get_unit().set_options(self.weapon.get_cur() == 'shoota',
                                                 self.opt.get('bombs'), self.cyborg.get('cyborg'))
        norm_counts(10 - mek, 20 - mek, [self.boyz])

        self.set_points(self.build_points(exclude=[self.opt.id, self.mek.id, self.cyborg.id],
                                          count=1) + self.get_count() * (self.opt.points() + self.cyborg.points()))
        self.build_description(options=[self.mek_unit, self.ride])
        desc = ModelDescriptor('Spanna Boy', points=6).add_gear_opt(self.opt).add_gear_opt(self.cyborg)
        minor = desc.clone()
        if self.weapon.get_cur() == 'cs':
            minor.add_gear('Choppa').add_gear('Slugga')
        else:
            minor.add_gear('Shoota')
        self.description.add(minor.build(self.boyz.get()))

    def get_count(self):
        return self.boyz.get() + self.mek_unit.get_count()

    def are_cyb(self):
        return self.cyborg.get('cyborg')


class Gretchins(Unit):
    name = 'Gretchin Scavenger Mob'

    def __init__(self):
        Unit.__init__(self)

        self.count = self.opt_count('Gretchin', 10, 30, 3)
        self.runth = self.opt_count('Runtheard', 1, 1, 10)
        self.prod = self.opt_count('Grot-prod', 0, 1, 5)

    def check_rules(self):
        runth = int(self.count.get() / 10)
        norm_counts(runth, runth, [self.runth])
        norm_counts(0, runth, [self.prod])
        self.set_points(self.build_points(count=1,))
        self.build_description(options=[])
        ork = ModelDescriptor('Runtheard', points=10, gear=['Slugga', 'Squig hound'])
        self.description.add(ork.clone().add_gear('Grabba stikk').build(self.runth.get() - self.prod.get()))
        self.description.add(ork.add_gear('Grot-prod', 5).build(self.prod.get()))
        self.description.add(ModelDescriptor('Gretchins', points=3, gear=['Blasta', 'Firebombz'])
            .build(self.count.get()))

    def get_count(self):
        return self.count.get() + self.runth.get()


class DeffDreadMob(ListUnit):
    name = 'Deff Dread Mob'

    class DeffDread(ListSubUnit):
        name = 'Deff Dread'
        base_points = 85
        gear = ["Drednought CCW", "Drednought CCW"]

        def __init__(self):
            ListSubUnit.__init__(self, min_models=1, max_models=3)
            self.w1 = self.opt_one_of('Weapon', [
                ["Big Shoota", 5],
                ["Rokkit Launcha", 5],
                ["Kustom mega-blasta", 10],
                ["Scorcha", 5],
                ["Drednought CCW", 10]
            ])
            self.w2 = self.opt_one_of('', [
                ["Big Shoota", 5],
                ["Rokkit Launcha", 5],
                ["Kustom mega-blasta", 10],
                ["Scorcha", 5],
                ["Drednought CCW", 10]
            ])

            self.opt = self.opt_options_list('Options', [
                ["Grot riggers", 5],
                ["Armor plates", 10],
            ])

    def __init__(self):
        ListUnit.__init__(self, self.DeffDread, min_num=1, max_num=3)