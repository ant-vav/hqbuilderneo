__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit, ListUnit

class Spears(Unit):
    name = 'Shining Spears'
    gear = ['Laser lance','Eldar Jetbike','Jetbike-mounted twin-linked shuriken catapults']
    min = 3
    max = 5

    class Exarch(Unit):
        name = 'Exarch'
        base_points = 35 + 12
        gear = ['Eldar Jetbike']
        def __init__(self):
            Unit.__init__(self)
            self.bikew = self.opt_one_of('Bike weapon',[
                ['Twin-linked shuriken catapults',0,'tlshc'],
                ['Shuriken cannon',15,'scan']
            ])
            self.wep = self.opt_one_of('Weapon',[
                ['Laser lance',0,'llance'],
                ['Power weapon',0,'pwep'],
                ['Star lance',15,'slance']
            ])

            self.opt = self.opt_options_list('Warrior powers',[
                ['Skilled rider', 10, 'skill'],
                ['Withdraw', 25, 'run']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Shining Spear', self.min, self.max,35)
        self.leader = self.opt_optional_sub_unit(self.Exarch.name, self.Exarch())

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min - ldr, self.max - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
        
class Spiders(Unit):
    name = 'Warp Spiders'
    gear = ['Warp jump generator','Death spinner']
    min = 5
    max = 10

    class Exarch(Unit):
        name = 'Exarch'
        base_points = 22 + 12
        gear = ['Warp jump generator']
        def __init__(self):
            Unit.__init__(self)
            self.bikew = self.opt_options_list('Melee weapon',[
                ['Powerblades',10,'pbld']
            ])
            self.wep = self.opt_one_of('Weapon',[
                ['Death spinner',0,'dsp'],
                ['2 Death spinners',5,'2dsp'],
                ['Spinneret rifle',5,'sprifl']
            ])

            self.opt = self.opt_options_list('Warrior powers',[
                ['Surprise assault', 10, 'sass'],
                ['Withdraw', 15, 'run']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Warp Spider', self.min, self.max,22)
        self.leader = self.opt_optional_sub_unit(self.Exarch.name, self.Exarch())

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min - ldr, self.max - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
        
class Hawks(Unit):
    name = 'Swooping Hawks'
    gear = ['Plasma grenades','Haywire grenades','Swooping Hawk grenade pack','Lasblaster']
    min = 5
    max = 10

    class Exarch(Unit):
        name = 'Exarch'
        base_points = 21 + 12
        gear = ['Plasma grenades','Haywire grenades','Swooping Hawk grenade pack']
        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_options_list('Melee weapon',[
                ['Power weapon',10,'pwep']
            ])
            self.rng = self.opt_one_of('Ranged weapon',[
                ['Lasblaster',0,'lblast'],
                ['Hawk\'s talon',10,'talon'],
                ['Sunrifle',5,'srifl']
            ])

            self.opt = self.opt_options_list('Warrior powers',[
                ['Intercept', 5, 'int'],
                ['Skyleap', 15, 'leap']
            ])
        def check_rules(self):
            self.rng.set_active_options(['talon','srifl'],not self.wep.get('pwep'))
            self.wep.set_active(self.rng.get_cur() == 'lblast')
            Unit.check_rules(self)
            
    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Swooping Hawk', self.min, self.max,21)
        self.leader = self.opt_optional_sub_unit(self.Exarch.name, self.Exarch())

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min - ldr, self.max - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
        
class VyperSquadron(ListUnit):
    name = 'Vyper Squadron'

    class Vyper(ListSubUnit):
        min = 1
        max = 3
        base_points = 45
        name = 'Vyper'
        def __init__(self):
            ListSubUnit.__init__(self)
            self.costy = self.opt_one_of('Primary weapon',[
                            ['Shuriken cannon',5,'shcan'],
                            ['Scatter laser',15,'sclas'],
                            ['Eldar missile launcher',20,'eml'],
                            ['Starcannon',25,'scan'],
                            ['Bright lances',30,'blance']
                                ])
            self.free = self.opt_one_of('Secondary weapon',[
                            ['Twin-linked shuriken catapults',0,'tlshc'],
                            ['Shuriken cannon',10,'shcan']
                                ])
            self.opt = self.opt_options_list('Options',[
                            ['Vectored engines',20,'veng'],
                            ['Star engines',15,'seng'],
                            ['Spirit stones',10,'spst'],
                            ['Holo-fields',35,'holo']
                                ])
    unit_class = Vyper