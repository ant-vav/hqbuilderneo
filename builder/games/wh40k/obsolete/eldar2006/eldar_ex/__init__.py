__author__ = 'Ivan Truskov'

from hq import *
from fast import *
from heavy import *
from superheavy import *

ia_hq = [Irillyth, BelAnnath, Wraithseer]
ia_fast = [Spectres, Wasps, Hornets]
ia_heavy = [WHunter]
ia_super_heavy = [Lynx, Scorpion, Cobra, VampireRaider, VampireHunter, Revenant, Phantom]
