__author__ = 'Ivan Truskov'

from builder.core.unit import Unit


class Lynx(Unit):
    name = 'Lynx'
    base_points = 320

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Pulsar', 0, 'pls'],
            ['Sonic Lance', 0, 'slnc']
        ])
        self.wep2 = self.opt_one_of('', [
            ['Shuriken Cannon', 0, 'scan'],
            ['Scatter Laser', 10, 'scat'],
            ['Starcannon', 25, 'scan'],
            ['Bright Lance', 30, 'blnc']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Vector Engines', 40, 'veng'],
            ['Star Engines', 30, 'seng']
        ])


class Scorpion(Unit):
    name = 'Scorpion'
    base_points = 500
    gear = ['Twin-linked Pulsars']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Shuriken Cannon', 0, 'scan'],
            ['Scatter Laser', 10, 'scat'],
            ['Starcannon', 20, 'scan'],
            ['Bright Lance', 30, 'blnc']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Vector Engines', 40, 'veng'],
            ['Star Engines', 30, 'seng']
        ])


class Cobra(Unit):
    name = 'Cobra'
    base_points = 600
    gear = ['D-Cannon']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Shuriken Cannon', 0, 'scan'],
            ['Scatter Laser', 10, 'scat'],
            ['Eldar Missile Launcher', 15, 'eml'],
            ['Starcannon', 20, 'scan'],
            ['Bright Lance', 30, 'blnc']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Vector Engines', 40, 'veng'],
            ['Star Engines', 30, 'seng']
        ])


class VampireRaider(Unit):
    name = 'Vampire'
    base_points = 730
    gear = ['Scatter Laser']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapons', [
            ['Two Twin-linked Pulse Lasers', 0, 'tlpl'],
            ['Two Twin-linked Phoenix Missile Launchers', 0, 'tlml'],
            ['Pulsar', 0, 'pls']
        ])


class VampireHunter(Unit):
    name = 'Vampire Hunter'
    base_points = 730
    gear = ['Twin-linked Pulsar', 'Twin-linked Phoenix Missile Launcher', 'Scatter Laser']


class Revenant(Unit):
    name = 'Revenant Titan'
    base_points = 800
    gear = ['Revenant Missile Launcher']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapons', [
            ['Two Pulsars', 0, 'pls'],
            ['Two Sonic Lances', 0, 'slnc']
        ])


class Phantom(Unit):
    name = 'Phantom Titan'
    base_points = 2500
    gear = ['Phantom Titan Missile Launcher', 'Phantom Titan AA Launcher on Anti-Aircraft Mount']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Caparace Mounted Weapon', [
            ['Phantom Titan Starcannon', 0, 'ptscan'],
            ['Pulse Laser', 0, 'plas']
        ])
        self.wep2 = self.opt_one_of('Arm weapon', [
            ['Phantom Titan Pulsar', 0, 'ptp'],
            ['Phantom Titan Distortion Cannon', 0, 'ptdcan']
        ])
        self.wep3 = self.opt_one_of('Arm weapon', [
            ['Phantom Titan Pulsar', 0, 'ptp'],
            ['Phantom Titan Distortion Cannon', 0, 'ptdcan'],
            ['Titan Close Combat Weapon with Twin-linked Phantom Starcannons', 0, 'ccw']
        ])
