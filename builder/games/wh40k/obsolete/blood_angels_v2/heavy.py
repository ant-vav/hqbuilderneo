__author__ = 'Denis Romanov'

from builder.core2 import *
from transport import Transport, DreadnoughtTransport
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedDreadnought, IATransportedUnit


class Devastators(IATransportedUnit):
    type_name = 'Devastator Squad'
    type_id = 'devastator_v1'

    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 16
    model = UnitDescription('Space Marine', points=model_points, options=model_gear + [Gear('Bolt pistol')])

    class MarineCount(Count):
        normalizer = None

        @property
        def description(self):
            return [Devastators.model.clone().add(Gear('Boltgun')).set_count(self.cur - self.normalizer())]

    def __init__(self, parent):
        super(Devastators, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(None))
        self.spacemarine = self.MarineCount(self, 'Space Marine', min_limit=4, max_limit=9, points=self.model_points)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=1, points=g['points'],
                  gear=Devastators.model.clone().add(Gear(g['name'])).add_points(g['points']))
            for g in [
                dict(name='Heavy bolter', points=10),
                dict(name='Multi-melta', points=10),
                dict(name='Missile launcher', points=10),
                dict(name='Plasma cannon', points=15),
                dict(name='Lascannon', points=25),
            ]
        ]
        self.spacemarine.normalizer = lambda: sum(c.cur for c in self.spec)
        self.transport = Transport(self)

    def check_rules(self):
        super(Devastators, self).check_rules()
        Count.norm_counts(0, 4, self.spec)

    def get_count(self):
        return self.spacemarine.cur + 1

    class Sergeant(Unit):
        type_name = 'Space Marine Sergeant'
        type_id = 'spacemarinesergeant_v1'

        def __init__(self, parent):
            super(Devastators.Sergeant, self).__init__(
                parent=parent, name='Space Marine Sergeant', points=90 - Devastators.model_points * 4,
                gear=Devastators.model_gear + [Gear('Signum')]
            )
            self.Weapon(self, 'Weapon', pistol=True)
            self.Weapon(self, '', boltgun=True)
            self.Options(self)

        class Weapon(OneOf):
            def __init__(self, parent, name, pistol=False, boltgun=False):
                super(Devastators.Sergeant.Weapon, self).__init__(parent=parent, name=name)
                if boltgun:
                    self.boltgun = self.variant('Boltgun', 0)
                if pistol:
                    self.boltpistol = self.variant('Bolt pistol', 0)
                self.chainsword = self.variant('Chainsword', 0)
                self.stormbolter = self.variant('Stormbolter', 3)
                self.combimelta = self.variant('Combi-melta', 10)
                self.combiflamer = self.variant('Combi-flamer', 10)
                self.combiplasma = self.variant('Combi-plasma', 10)
                self.plasmapistol = self.variant('Plasma pistol', 15)
                self.powerweapon = self.variant('Power weapon', 15)
                self.powerfist = self.variant('Power fist', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Devastators.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)


class Dreadnought(IATransportedDreadnought):
    type_name = 'Dreadnought'
    type_id = 'dreadnought_v1'

    def __init__(self, parent):
        super(Dreadnought, self).__init__(parent=parent, points=105, gear=[Gear('Smoke launchers')], walker=True)
        self.Weapon1(self)
        self.Weapon2(self)
        self.buildin = self.BuildIn(self)
        self.Options(self)
        self.transport = DreadnoughtTransport(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.multimelta = self.variant('Multi-melta', 0)
            self.twinlinkedheavyflamer = self.variant('Twin-linked heavy flamer', 0)
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 5)
            self.twinlinkedautocannon = self.variant('Twin-linked autocannon', 10)
            self.plasmacannon = self.variant('Plasma cannon', 10)
            self.assaultcannon = self.variant('Assault cannon', 10)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 30)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.bloodfist = self.variant('Blood fist', 0)
            self.twinlinkedautocannon = self.variant('Twin-linked autocannon', 10)
            self.missilelauncher = self.variant('Missile launcher', 10)

        def check_rules(self):
            super(Dreadnought.Weapon2, self).check_rules()
            self.parent.buildin.visible = self.parent.buildin.used = self.cur == self.bloodfist

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(Dreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant('Storm bolter', 0)
            self.builtinheavyflamer = self.variant('Heavy flamer', 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Dreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.extraarmour = self.variant('Extra armour', 15)
            self.searchlight = self.variant('Searchlight', 1)


class StormravenGunship(SpaceMarinesBaseVehicle):
    type_name = 'Stormraven Gunship'
    type_id = 'stormravengunship_v1'

    def __init__(self, parent):
        super(StormravenGunship, self).__init__(
            parent=parent, points=200, gear=[Gear('Bloodstrike missile', count=4), Gear('Ceramite plating')])
        self.Weapon1(self)
        self.Weapon2(self)
        self.Sponsons(self)
        self.Options(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.twinlinkedmultimelta = self.variant('Twin-linked multi-melta', 0)
            self.typhoonmissilelauncher = self.variant('Typhoon missile launcher', 25)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon2, self).__init__(parent=parent, name='')
            self.twinlinkedplasmacannon = self.variant('Twin-linked plasma cannon', 0)
            self.twinlinkedassaultcannon = self.variant('Twin-linked assault cannon', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 0)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(StormravenGunship.Sponsons, self).__init__(parent=parent, name='Side sponsons')
            self.hurricanebolters = self.variant('Hurricane bolters', 30, gear=[Gear('Hurricane bolter', count=2)])

    class Options(OptionsList):
        def __init__(self, parent):
            super(StormravenGunship.Options, self).__init__(parent=parent, name='Options')
            self.searchlight = self.variant('Searchlight', 1)
            self.locatorbeacon = self.variant('Locator beacon', 15)
            self.extraarmour = self.variant('Extra armour', 15)


class Predator(SpaceMarinesBaseVehicle):
    type_name = 'Predator'
    type_id = 'predator_v1'

    def __init__(self, parent):
        super(Predator, self).__init__(parent=parent, points=70, gear=[Gear('Smoke launchers')], tank=True)
        self.Weapon(self)
        self.Sponsons(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Predator.Weapon, self).__init__(parent=parent, name='Weapon')
            self.autocannon = self.variant('Autocannon', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 45)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(Predator.Sponsons, self).__init__(parent=parent, name='Side sponsons', limit=1)
            self.heavybolters = self.variant('Heavy Bolters', 30, gear=[Gear('Heavy bolter')])
            self.lascannons = self.variant('Lascannons', 65, gear=[Gear('Lascannon')])

    class Options(OptionsList):
        def __init__(self, parent):
            super(Predator.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.dozenblade = self.variant('Dozen blade', 5)
            self.stormbolter = self.variant('Storm bolter', 10)
            self.hinterkillermissile = self.variant('Hinter-killer missile', 10)
            self.extraarmour = self.variant('Extra armour', 15)
            self.searchlight = self.variant('Searchlight', 1)


class Whirlwind(SpaceMarinesBaseVehicle):
    type_name = 'Whirlwind'
    type_id = 'whirlwind_v1'

    def __init__(self, parent):
        super(Whirlwind, self).__init__(parent=parent, points=90, gear=[Gear('Whirlwind multiple missile launcher'),
                                                                        Gear('Smoke launchers')], tank=True)
        Predator.Options(self)


class Vindicator(SpaceMarinesBaseVehicle):
    type_name = 'Vindicator'
    type_id = 'vindicator_v1'

    def __init__(self, parent):
        super(Vindicator, self).__init__(
            parent=parent, points=145, tank=True,
            gear=[Gear('Demolisher cannon'), Gear('Storm bolter'), Gear('Smoke launchers')]
        )
        self.Options(self)

    class Options(Predator.Options):
        def __init__(self, parent):
            super(Vindicator.Options, self).__init__(parent=parent)
            self.siegeshield = self.variant('Siege shield', 10)
