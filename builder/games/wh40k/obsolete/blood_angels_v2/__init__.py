__author__ = 'Denis Romanov'

from builder.games.wh40k.old_roster import Wh40k
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment, AlliedDetachment, HQSection, ElitesSection, TroopsSection, FastSection, HeavySection, Fort
from builder.games.wh40k.escalation.space_marines import LordsOfWar as MarinesLordsOfWar
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.imperial_armour import volume2
from builder.games.wh40k.dataslates.cypher import Cypher
from hq import *
from elites import *
from troops import *
from fast import *
from heavy import *


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.dante = UnitType(self, Dante)
        UnitType(self, Gabriel)
        self.astorath = UnitType(self, Astorath)
        UnitType(self, Sanguinor)
        UnitType(self, Mephiston)
        UnitType(self, Tycho)
        UnitType(self, Librarian)
        UnitType(self, Reclusiarch)
        UnitType(self, Captain)
        self.honour_guards = UnitType(self, HonourGuards, slot=0)
        UnitType(self, Cypher, slot=0)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        UnitType(self, Chaplain)
        self.sanguinary_guards = UnitType(self, SanguinaryGuards)
        UnitType(self, FuriosoDreadnought)
        UnitType(self, TerminatorSquad)
        UnitType(self, TerminatorAssaultSquad)
        UnitType(self, SternguardVeteranSquad)
        UnitType(self, Techmarine)
        self.priest = UnitType(self, SanguinaryPriest, slot=1/3)
        self.corbulo = UnitType(self, Corbulo, slot=1/3)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.tactical_squad = UnitType(self, TacticalSquad)
        self.death_company = UnitType(self, DeathCompany)
        self.death_company_dreadnought = UnitType(self, DeathCompanyDreadnought)
        self.scout_squad = UnitType(self, ScoutSquad)
        self.assault_squad = UnitType(self, AssaultSquad)
        self.sanguinary_guards = UnitType(self, SanguinaryGuards, active=True)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, VanguardVeteranSquad)
        UnitType(self, LandSpeederSquadron)
        UnitType(self, BaalPredator)
        UnitType(self, AttackBikeSquad)
        UnitType(self, SpaceMarineBikers)
        UnitType(self, ScoutBikers)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, Dreadnought)
        UnitType(self, StormravenGunship)
        UnitType(self, Predator)
        UnitType(self, Devastators)
        UnitType(self, Whirlwind)
        UnitType(self, Vindicator)


class HQ(volume2.SpaceMarineHQ, BaseHQ):
    pass


class FastAttack(volume2.SpaceMarineFast, BaseFastAttack):
    pass


class Elites(volume2.BloodAngelsElites, BaseElites):
    pass


class HeavySupport(volume2.SpaceMarineHeavySupport, BaseHeavySupport):
    pass


class LordsOfWar(volume2.SpaceMarineLordsOfWar, Titans, MarinesLordsOfWar):
    pass


#class BloodAngelsV2(Wh40k):
#    army_id = 'blood_angels_v2'
#    army_name = 'Blood Angels'
#
#    def __init__(self, secondary=False):
#        from builder.games.wh40k.roster import Ally
#        self.hq = HQ(parent=self)
#        self.elites = Elites(parent=self)
#        self.troops = Troops(parent=self)
#        self.fast = FastAttack(parent=self)
#        self.heavy = HeavySupport(parent=self)
#        super(BloodAngelsV2, self).__init__(
#            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
#            fort=Fort(parent=self), lords=LordsOfWar(parent=self),
#            ally=Ally(parent=self, ally_type=Ally.BA),
#            secondary=secondary
#        )
#
#    def check_rules(self):
#        super(BloodAngelsV2, self).check_rules()
#        self.move_units(self.hq.dante.count > 0, self.elites.sanguinary_guards, self.troops.sanguinary_guards)
#
#        hg = self.hq.honour_guards.count
#        if hg > len(self.hq.units) - hg:
#            self.error("You can't have more Honour Guards units then other units in HQ.")
#
#        astropath = self.hq.astorath.count > 0
#        if not astropath and self.troops.death_company.count > 1:
#            self.error("You can't have more then one Death Company without Astorath the Grim in HQ.")
#
#        dc_dred_limit = int(sum(u.get_count() for u in self.troops.death_company.units) / 5)
#        self.troops.death_company_dreadnought.active = dc_dred_limit > 0
#        dc_dred_count = self.troops.death_company_dreadnought.count
#        if dc_dred_limit and dc_dred_count > dc_dred_limit:
#            self.error("You can't have more then {0} Death Company Dreadnought.".format(dc_dred_limit))
#        elif dc_dred_limit == 0 and dc_dred_count > 0:
#            self.error("You can't have Death Company Dreadnought without at least 5 Death Company.")

class BloodAngelsV2Base(Wh40kBase):
    army_id = 'blood_angels_v2_base'
    army_name = 'Blood Angels'
    ia_enabled = True

    def __init__(self, secondary=False):

        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(BloodAngelsV2Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self), lords=LordsOfWar(parent=self)
        )

    def check_rules(self):
        super(BloodAngelsV2Base, self).check_rules()
        self.move_units(self.hq.dante.count > 0, self.elites.sanguinary_guards, self.troops.sanguinary_guards)

        hg = self.hq.honour_guards.count
        if hg > len(self.hq.units) - hg:
            self.error("You can't have more Honour Guards units then other units in HQ.")

        astropath = self.hq.astorath.count > 0
        if not astropath and self.troops.death_company.count > 1:
            self.error("You can't have more then one Death Company without Astorath the Grim in HQ.")

        dc_dred_limit = int(sum(u.get_count() for u in self.troops.death_company.units) / 5)
        self.troops.death_company_dreadnought.active = dc_dred_limit > 0
        dc_dred_count = self.troops.death_company_dreadnought.count
        if dc_dred_limit and dc_dred_count > dc_dred_limit:
            self.error("You can't have more then {0} Death Company Dreadnought.".format(dc_dred_limit))
        elif dc_dred_limit == 0 and dc_dred_count > 0:
            self.error("You can't have Death Company Dreadnought without at least 5 Death Company.")

class BloodAngelsV2CAD(BloodAngelsV2Base, CombinedArmsDetachment):
    army_id = 'blood_angels_v2_cad'
    army_name = 'Blood Angels (combined arms detachment)'
    
class BloodAngelsV2AD(BloodAngelsV2Base, CombinedArmsDetachment):
    army_id = 'blood_angels_v2_ad'
    army_name = 'Blood Angels (Allied detachment)'

faction = 'Blood_Angels'

class BloodAngelsV2(Wh40k7ed):
    army_id = 'blood_angels_v2'
    army_name = 'Blood Angels'
    faction = faction
    obsolete = True

    def __init__(self):
        super(BloodAngelsV2, self).__init__([BloodAngelsV2CAD])

detachments = [BloodAngelsV2CAD, BloodAngelsV2AD]
