__author__ = 'Denis Romanov'

from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor
from builder.core.unit import Unit
from builder.games.wh40k.obsolete.tyranids.elites import MyceticSpore

class Warrior(Unit):
    name = 'Tyranid Warrior Brood'
    base_points = 30
    gear = ['Hardened carapace']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Warrior', 3, 9, self.base_points)
        self.wep1 = self.opt_one_of('Weapon',
            [
                ['Scything talons', 0],
                ['Rending claws', 5],
                ['A pair of boneswords', 10],
                ['Lash whip and bone sword', 15],
                ], 'wep1')

        self.wep2 = self.opt_one_of('Weapon',
            [
                ['Devourer', 0],
                ['Scything talons', 0],
                ['Rending claws', 0],
                ['Spinefists', 0],
                ['Deathspitter', 5],
                ], 'wep2')
        self.heavy = self.opt_options_list('Heavy Weapon', [
                ['Barbed strangler', 10, 'bs'],
                ['Venom cannon', 15, 'vc'],
            ], limit=1)
        self.opt1 = self.opt_options_list('Options', [
            ["Adrenal glands", 5],
            ["Toxin sacs", 5],
            ], id='opt1')

        self.spore = self.opt_options_list('Drop', [["Mycetic Spore", 40, 'ms']])
        self.spore_unit = self.opt_sub_unit(MyceticSpore())

    def check_rules(self):
        self.spore_unit.set_active(self.spore.get('ms'))
        heavy = self.heavy.get('bs') or self.heavy.get('vc')
        self.wep2.set_visible(not heavy)
        self.points.set((self.base_points + self.wep1.points() + self.wep2.points() + self.opt1.points()) * self.count.get()
                        + self.heavy.points() + self.spore_unit.points())

        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(self.wep1.get_selected(), self.count.get())
        self.description.add(self.wep2.get_selected(), self.count.get())
        self.description.add(self.heavy.get_selected())
        if heavy:
            self.description.add('Devourer', self.count.get() - 1)
        self.description.add(self.opt1.get_selected(), self.count.get())
        self.description.add(self.spore_unit.get_selected())
        self.set_description(self.description)


class Broodlord(Unit):
    name = 'Broodlord'
    base_points = 60
    gear = ['Hardened carapace',  'Rending claws']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Scything talons', 2],
            ["Implant attack", 15],
            ["Acid blood", 15],
            ])

class Genestealer(Unit):
    name = 'Genestealer Brood'
    base_points = 14
    gear = ['Reinforced chitin',  'Rending claws']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Genestealer', 5, 20, self.base_points)
        self.wep = self.opt_options_list('Weapon', [
            ['Scything talons', 2, 'st'],
            ])
        self.opt = self.opt_options_list('Options', [
            ["Adrenal glands", 5],
            ["Toxin sacs", 5],
            ])

        self.lord = self.opt_options_list('Broodlord', [['Upgrade', 46, 'up'],])
        self.lord_unit = self.opt_sub_unit(Broodlord())
        self.spore = self.opt_options_list('Drop', [["Mycetic Spore", 40, 'ms']])
        self.spore_unit = self.opt_sub_unit(MyceticSpore())

    def check_rules(self):
        self.lord_unit.set_active(self.lord.get('up'))
        self.spore_unit.set_active(self.spore.get('ms'))
        lord = 1 if self.lord.get('up') else 0
        norm_counts(5 - lord, 20 - lord, [self.count])
        self.points.set((self.base_points + self.wep.points()) * self.count.get() +
                        self.opt.points() * (lord + self.count.get()) +
                        self.lord_unit.points() + self.spore_unit.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(self.wep.get_selected(), self.count.get())
        self.description.add(self.lord_unit.get_selected())
        self.description.add(self.opt.get_selected(), self.count.get() + lord)
        self.description.add(self.spore_unit.get_selected())
        self.set_description(self.description)

    def get_count(self):
        return self.count.get() + (1 if self.lord.get('up') else 0)

class Termagant(Unit):
    name = 'Termagant Brood'
    base_points = 5
    gear = ['Chitin', 'Claws and Teeth']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Termagant', 10, 30, self.base_points)
        self.wep = self.opt_one_of('Weapon', [
            ['Fleshborer', 0],
            ['Spinefists', 1],
            ['Spike rifle', 1],
            ['Devourer', 5]
            ])
        self.hw = self.opt_count('Strangleweb', 0, int(self.count.get()/10), 10)
        self.opt = self.opt_options_list('Options', [
            ["Adrenal glands", 1],
            ["Toxin sacs", 1],
            ])

        self.spore = self.opt_options_list('Drop', [["Mycetic Spore", 40, 'ms']])
        self.spore_unit = self.opt_sub_unit(MyceticSpore())

    def check_rules(self):
        self.spore.set_active_options(['ms'], self.count.get() <= 20)
        self.spore_unit.set_active(self.spore.get('ms'))
        norm_counts(0, int(self.count.get() / 10), [self.hw])
        self.wep.set_visible(self.hw.get() == 0)

        self.points.set((self.base_points + self.wep.points() + self.opt.points()) * self.count.get() +
                        self.hw.points() + self.spore_unit.points())

        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(self.wep.get_selected(), self.count.get())
        self.description.add(self.hw.get_selected())
        if self.hw.get() > 0:
            self.description.add('Fleshborer', self.count.get() - self.hw.get())
        self.description.add(self.opt.get_selected(), self.count.get())
        self.description.add(self.spore_unit.get_selected())
        self.set_description(self.description)

class Hormagant(Unit):
    name = 'Hormagant Brood'
    base_points = 6
    gear = ['Chitin', 'Scything talons']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Hormagant', 10, 30, self.base_points)
        self.opt = self.opt_options_list('Options', [
            ["Adrenal glands", 2],
            ["Toxin sacs", 2],
            ])
        self.spore = self.opt_options_list('Drop', [["Mycetic Spore", 40, 'ms']])
        self.spore_unit = self.opt_sub_unit(MyceticSpore())

    def check_rules(self):
        self.spore.set_active_options(['ms'], self.count.get() <= 20)
        self.spore_unit.set_active(self.spore.get('ms'))

        self.points.set((self.base_points + self.opt.points()) * self.count.get() + self.spore_unit.points())

        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(self.opt.get_selected(), self.count.get())
        self.description.add(self.spore_unit.get_selected())
        self.set_description(self.description)


class Ripper(Unit):
    name = 'Ripper Swarm Brood'
    base_points = 10
    base_gear = ['Chitin', 'Claws and Teeth']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Ripper Swarm', 3, 9, self.base_points)
        self.opt = self.opt_options_list('Options', [
            ["Spinefists", 5],
            ["Adrenal glands", 4],
            ["Toxin sacs", 4],
            ["Tunnel swarm", 2],
        ])

    def check_rules(self):
        self.points.set(self.build_points(exclude=[self.count.id]))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Ripper Swarm', gear=self.base_gear,
                                             points=self.base_points).add_gear_opt(self.opt).build(self.get_count()))
