__author__ = 'Ivan Truskov'

from builder.games.wh40k.obsolete.chaos_daemons.troops import CountUnit, BaseDaemon


class Crushers(BaseDaemon):
    name = "Bloodcrushers of Khorne"

    def __init__(self):
        BaseDaemon.__init__(
            self,
            unit_name=self.name,
            base_name='Bloodcrusher',
            leader_name='Bloodhunter',
            base_gear=['Hellblade'],
            base_points=45,
            banner_name='Banner of blood',
            min_count=3,
            max_count=9
        )


class Flamers(BaseDaemon):
    name = "Flamers of Tzeentch"

    def __init__(self):
        BaseDaemon.__init__(
            self,
            unit_name=self.name,
            base_name='Flamer of Tzeentch',
            leader_name='Pyrocaster',
            base_gear=['Flames of Tzeentch'],
            base_points=23,
            banner_name=None,
            min_count=3,
            max_count=9,
            icon_available=False

        )


class Beasts(CountUnit):
    name = "Beasts of Nurgle"

    def __init__(self):
        CountUnit.__init__(self, 1, 9, 52)


class Fiends(CountUnit):
    name = "Fiends of Slaanesh"

    def __init__(self):
        CountUnit.__init__(self, 3, 9, 35)
