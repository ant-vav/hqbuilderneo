__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.core2.section import Section
from builder.games.wh40k.old_roster import Wh40k, Ally
from builder.games.wh40k.fortifications import Fort
from elites import TheDamned


class DamnedSection(Section):
    def __init__(self, parent):
        super(DamnedSection, self).__init__(parent, 'lod', 'Elites', min_limit=1, max_limit=4)
        UnitType(self, TheDamned)


class DamnedLegion(Wh40k):
    army_id = 'damned_v1'
    army_name = 'Legion of the Damned'
    obsolete = True

    def __init__(self, secondary=False):
        self.lod = DamnedSection(parent=self)
        super(DamnedLegion, self).__init__(
            added=[self.lod],
            fort=Fort(parent=self),
            ally=Ally(parent=self, ally_type=Ally.LOD),
            secondary=secondary
        )

    def check_rules(self):
        super(DamnedLegion, self).check_rules()
        root = self.root
        if root.foc.cur == root.foc.double:
            self.lod.min = 2
            self.lod.max = 8
        elif root.foc.cur != root.foc.custom and root.foc.cur != root.foc.unlimited:
            self.lod.min = 1
            self.lod.max = 4
        elif root.foc.cur == root.foc.unlimited:
            self.lod.min = 0
            self.lod.max = None
