__author__ = 'Ivan Truskov'
from builder.core.unit import Unit


class Phoenix(Unit):
    name = 'Corsair Phoenix Bomber'
    base_points = 225
    gear = ['Shuriken Cannon with Anti-aircraft Mounts' for _ in range(2)]

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Pulse Laser', 0, 'plas'],
            ['Twin-linked Bright Lances', 0, 'tlbl'],
            ['Twin-linked Starcannons', 0, 'tlscan']
        ])
        self.mis = self.opt_one_of('Missile Launchers', [
            ['Two Phoenix Missile Launchers', 0, 'phml'],
            ['Two Nightfire Missile Launchers', 10, 'nfml']
        ])


class WHunter(Unit):
    name = 'Corsair Warp Hunter'
    base_points = 125
    gear = ['D-cannon']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Twin-linked Shuriken Catapult', 0, 'tlshc'],
            ['Shuriken Cannon', 10, 'shcan']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Vectored Engines', 20, 'veng'],
            ['Star Engines', 15, 'seng'],
            ['Holo-fields', 35, 'hf'],
            ['Spirit Stones', 10, 'ss']
        ])


class Firestorm(Unit):
    name = 'Corsair Firestorm'
    base_points = 180
    gear = ['Twin-linked Firestorm Scatter Lasers on an Anti-Aircraft Mount']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Twin-linked Shuriken Catapult', 0, 'tlshc'],
            ['Shuriken Cannon', 10, 'shcan']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Vectored Engines', 20, 'veng'],
            ['Star Engines', 15, 'seng'],
            ['Holo-fields', 35, 'hf'],
            ['Spirit Stones', 10, 'ss']
        ])
