__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, StaticUnit, ListSubUnit
from troops import Raider,Venom

class Vect(Unit):
    name = 'Asdrubael Vect'
    base_points = 240
    unique = True
    gear = ['Shadow field','Ghostplate armour','Splinter pistol','Plasma grenades','Haywire grenades','Obsidian orbs','Sceptre of the Dark City']
    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('',[
            ['The Dais of Destruction',200,'pimp_ride']
            ])

class Malys(StaticUnit):
    name = 'Lady Malys'
    base_points = 130
    gear = ['Plasma grenades','Kabalite armour','Steel fan','The Crystal Heart','The Lady\'s Blade']

class Drazhar(StaticUnit):
    name = 'Drazhar, Master of blades'
    base_points = 230
    gear = ['Ancient Incubus warsuit','Demiclaives']

class Lelith(StaticUnit):
    name = 'Lelith Hesperax'
    base_points = 175
    gear = ['Wicked blades and barbed hair','Wychsuit','Plasma grenades']

class Rakarth(StaticUnit):
    name = 'Urien Rakarth'
    base_points = 190
    gear = ['Close combat weapon','Gnarlskin','Casket of Flensing','Clone field','Ichor gauntlet']
    def check_arcane(self,val):
        return 1 if val == 'coflns' else 0
class Sliscus(StaticUnit):
    name = 'Duke Sliscus the Serpent'
    base_points = 150
    gear = ['Shadow field','Blast pistol','Ghostplate armour','Plasma grenades','Combat drugs','The Serpent\'s bite']

class Decapitator(StaticUnit):
    name = 'Khaedruakh, The Decapitator'
    base_points = 140
    gear = ['Decapitator']

class Sathonyx(StaticUnit):
    name = 'Baron Sathonyx'
    base_points = 105
    gear = ['Shadow field','Splinter pistol','Wychsuit','Phantasm grenade launcher','Custom skyboard','Bones of the seer','Twilight shroud']

class Archon(Unit):
    name = 'Archon'
    base_points = 60
    gear = ['Plasma grenades']
    def __init__(self):
        Unit.__init__(self)
        weaponlist = [
            ['Venom blade',5,'vsw'],
            ['Blast pistol',15,'blpist'],
            ['Power weapon', 15, 'pwep' ],
            ['Blaster', 15, 'blast'],
            ['Agoniser', 20, 'agon' ],
            ['Electrocorrosive whip', 20, 'ewhip' ],
            ['Huskblade',35,'hsw'],
                ]
        self.armr = self.opt_one_of('Armour',[
            ['Kabalite armour',0,'kabarmr'],
            ['Ghostplate armour',10,'ghostarmr']
                ])
        self.wep1 = self.opt_one_of('Weapon',
            [['Close combat weapon',0,'csw']] + weaponlist,id='w1')
        self.wep2 = self.opt_one_of('Weapon',
            [['Splinter pistol',0,'splpist']] + weaponlist,id='w2')
        self.field = self.opt_options_list('Field',[
            ["Clone field", 20, 'clfld'],
            ["Shadow field", 30, 'shfld']
                ],1)
        self.opt = self.opt_options_list('Options',[
            ["Haywire grenades", 5, 'hwrgrnd'],
            ["Combat drugs", 10, 'drug'],
            ["Soul-trap", 10, 'strap'],
            ["Djin blade", 20, 'djsw'],
            ["Phantasm grenade launcher", 25, 'phgrlnch'],
            ["Webway portal", 35, 'wwp']
                ])

class Court(Unit):
    name = 'The Court of Archon'

    class Lhamaean(ListSubUnit):
        max = 2
        name = 'Lhamaean'
        base_points = 10
        gear = ['Kabalite armour','Splinter pistol','Poisoned weapon']

    class Medusae(ListSubUnit):
        max = 2
        name = 'Medusae'
        base_points = 15
        gear = ['Kabalite armour']

    class Sslyth(ListSubUnit):
        max = 3
        name = 'Sslyth'
        base_points = 35
        gear = ['Kabalite armour','Splinter pistol','Shardcarbine','Close combat weapon']

    class UrGhul(ListSubUnit):
        max = 5
        name = 'Ur-Ghul'
        base_points = 15
        gear = ['Claws and needle-teeth']

    def __init__(self):
        Unit.__init__(self)
        self.lhm = self.opt_sub_unit(self.Lhamaean())
        self.med = self.opt_sub_unit(self.Medusae())
        self.ssl = self.opt_sub_unit(self.Sslyth())
        self.urgh = self.opt_sub_unit(self.UrGhul())
        self.transport = self.opt_optional_sub_unit('Transport', [Raider(), Venom()], id='trans')

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return self.lhm.get_count() + self.med.get_count() + self.ssl.get_count() + self.urgh.get_count()

class Succubus(Unit):
    name = 'Succubus'
    base_points = 65
    gear = ['Plasma grenades','Wychsuit','Combat drugs']
    def __init__(self):
        Unit.__init__(self)
        weaponlist = [
            ['Venom blade',5,'vsw'],
            ['Power weapon', 15, 'blast' ],
            ['Agoniser', 20, 'agon' ],
            ['Electrocorrosive whip', 20, 'ewhip' ]
                ]
        self.wep1 = self.opt_one_of('Weapon',
            [['Close combat weapon',0,'csw']] + weaponlist,id='w1')
        self.wep2 = self.opt_one_of('Weapon',
            [['Splinter pistol',0,'splpist'], ['Blast pistol',15,'blpist']] +  weaponlist,id='w2')
        self.twohnd = self.opt_options_list('Replace both weapons',[
            ['Razorflails', 10, 'rzf' ],
            ['Hydra gauntlets', 5, 'hydra' ],
            ['Shardnet and impaler', 5, 'net' ]
            ],1)
        self.opt = self.opt_options_list('Options',[
            ["Haywire grenades", 5, 'hwrgrnd']
            ])
    def check_rules(self):
        spec = len(self.twohnd.get_all()) > 0
        self.wep1.set_visible(not spec)
        self.wep2.set_visible(not spec)
        Unit.check_rules(self)


class Haemunculus(Unit):
    name = 'Haemonculus'
    base_points = 50
    gear = ['Gnarlskin']
    def __init__(self):
        Unit.__init__(self)
        self.upgrade = self.opt_options_list('',[['Ancient Haemonculus',30,'up']])
        weaponlist = [
            ['Venom blade',5,'vsw'],
            ['Stinger pistol',5,'stpist'],
            ['Power weapon', 10, 'blast' ],
            ['Mindphase gauntlet', 10, 'mphgnt' ],
            ['Flesh gauntlet', 20, 'flshgnt' ],
            ['Agoniser', 20, 'agon' ],
            ['Electrocorrosive whip', 20, 'ewhip' ],
            ['Huskblade',35,'hsw'],
            ]
        self.wep1 = self.opt_one_of('Weapon',
            [['Close combat weapon',0,'csw']] + weaponlist,id='w1')
        self.wep2 = self.opt_one_of('Weapon',
            [['Splinter pistol',0,'splpist']] + weaponlist,id='w2')
        self.opt = self.opt_options_list('Arcane wargear',[
            ["Animus vitae", 5, 'avit'],
            ['Casket of Flensing', 10, 'coflns'],
            ['Liquefier gun',10,'liqgun'],
            ["Soul-trap", 10, 'strap'],
            ['Scissorhand', 15, 'schnd'],
            ['Archangel of Pain', 15, 'apain'],
            ['Hexrifle', 15, 'hexgun'],
            ['Shattershard', 15, 'sshard'],
            ['Crucible of malediction', 20, 'crucm'],
            ['Orb of despair', 20, 'dorb'],
            ['Dark gate',25,'dgate'],
            ["Webway portal", 35, 'wwp']
        ], limit = 2)

    def check_arcane(self,val):
        return 1 if self.opt.get(val) else 0

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(exclude=[self.upgrade.id], name='Ancient ' + self.name if self.upgrade.get('up') else self.name)
