__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.orks.troops import Trukk
from builder.games.wh40k.obsolete.orks.heavy import Battlewagon


class Meganobz(Unit):
    name = 'Meganobz'

    class Meganob(ListSubUnit):
        name = 'Meganobz'
        base_points = 40

        def __init__(self):
            ListSubUnit.__init__(self)
            self.weapon = self.opt_one_of('Weapon', [
                ["Twin-linked Shoota", 0],
                ["Shoota/Rokkit", 5],
                ["Shoota/Skorcha", 5],
            ])

        def set_cyborg(self, cyborg):
            self.gear = ["Mega armor", 'Power klaw']
            if cyborg:
                self.gear += ['Cybork body']
                self.base_points += 5
            ListSubUnit.check_rules(self)

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.meganobz = self.opt_units_list(self.name, self.Meganob, 3, 10)

        self.wheels = self.opt_options_list('Transport', [
            ["Trukk", 35, 'trukk'],
            ["Battlewagon", 90, 'bw'],
        ], limit=1)
        self.trukk = self.opt_sub_unit(Trukk())
        self.bw = self.opt_sub_unit(Battlewagon(True))
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        self.meganobz.update_range(3, 10)
        if self.wheels.get('trukk') and self.get_count() > 6:
            self.error('Trukk overloaded! It can transport only 6 meganobz. (taken: {0})'.format(self.get_count()))
        self.trukk.set_active(self.wheels.get('trukk'))
        self.bw.set_active(self.wheels.get('bw'))
        for unit in self.meganobz.get_units():
            unit.set_cyborg(self.cyborg.get('cyborg'))
        self.points.set(self.build_points(exclude=[self.wheels.id, self.cyborg.id], count=1))
        self.build_description(exclude=[self.wheels.id, self.cyborg.id])

    def get_count(self):
        return self.meganobz.get_count()


class Nobz(Unit):
    name = 'Nobz'

    class Nob(ListSubUnit):
        name = 'Nob'
        base_points = 20

        def __init__(self):
            ListSubUnit.__init__(self)
            self.ccw = self.opt_one_of('Close combat weapon', [
                ['Choppa', 0, 'choppa'],
                ['Big choppa', 5, 'bc'],
                ['Power klaw', 25, 'klaw'],
            ])
            self.rng = self.opt_one_of('Ranged weapon', [
                ["Slugga", 0, 'slugga'],
                ["Shoota/Rokkit", 5],
                ["Shoota/Skorcha", 5],
                ["Twin-linked Shoota", 5],
            ])
            self.opt = self.opt_options_list('Options', [
                ["Bosspole", 5],
                ["'eavy armour", 5],
                ["Waaagh! banner", 15],
                ["Ammo runt", 5],
            ])

        def set_options(self, opt, bikes):
            self.base_points = 20 + opt.points() + (25 if bikes else 0)
            self.gear = ['Warbike'] if bikes else []
            ListSubUnit.check_rules(self)
            self.description.add(opt.get_selected())

        def check_rules(self):
            pass

    class Painboy(Unit):
        name = 'Painboy'
        base_points = 50
        base_gear = ['Urty syringe', "Dok's tools"]

        def __init__(self):
            Unit.__init__(self)
            self.opt = self.opt_options_list('Options', [["Grot orderly", 5]])

        def set_options(self, opt, bikes):
            self.base_points = 50 + opt.points() + (25 if bikes else 0)
            self.gear = self.base_gear + (['Warbike'] if bikes else [])
            Unit.check_rules(self)
            self.description.add(opt.get_selected())

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.nobz = self.opt_units_list(self.name, self.Nob, 3, 10)
        self.pain = self.opt_options_list('', [["Painboy", 30, 'up']])
        self.pain_unit = self.opt_sub_unit(self.Painboy())
        self.opt = self.opt_options_list('Options', [
            ["Stikkbombs", 1],
            ["Cybork body", 5, 'cyborg'],
        ])
        self.wheels = self.opt_options_list('Transport', [
            ["Bikes", 25, 'bike'],
            ["Trukk", 35, 'trukk'],
            ["Battlewagon", 90, 'bw'],
        ], limit=1)
        self.trukk = self.opt_sub_unit(Trukk())
        self.bw = self.opt_sub_unit(Battlewagon(True))

    def check_rules(self):
        self.pain.set_active_options(['up'], self.nobz.get_count() < 10)
        self.pain_unit.set_active(self.pain.get('up'))
        painboy = 1 if self.pain.get('up') else 0
        self.nobz.update_range(3 - painboy, 10 - painboy)

        self.opt.set_active_options(['cyborg'], self.pain.get('up') or self.get_roster().has_grotsnik())
        self.trukk.set_active(self.wheels.get('trukk'))
        self.bw.set_active(self.wheels.get('bw'))
        for unit in self.nobz.get_units() + ([self.pain_unit.get_unit()] if self.pain.get('up') else []):
            unit.set_options(self.opt, self.wheels.get('bike'))
        self.set_points(self.build_points(exclude=[self.wheels.id, self.pain.id, self.opt.id], count=1))
        self.build_description(exclude=[self.wheels.id, self.pain.id, self.opt.id], count=1)

    def get_count(self):
        return self.nobz.get_count() + self.pain_unit.get_count()


class BurnaBoyz(Unit):
    name = 'Burna Boyz'

    class Mek(ListSubUnit):
        name = 'Mek'
        base_points = 15

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ["Kustom mega-blasta", 0],
                ["Slugga and Choppa", 0],
                ["Big Shoota", 0],
                ["Rokkit launcha", 5],
            ])
            self.opt = self.opt_options_list('', [['Grot Oiler', 5]])

        def set_cyborg(self, cyborg):
            self.gear = ["Mek's Tools"]
            if cyborg:
                self.gear += ['Cybork body']
                self.base_points += 5
            ListSubUnit.check_rules(self)

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count("Burna Boyz", 5, 15, 15)
        self.meks = self.opt_options_list("", [['Meks', 15, 'add']])
        self.meks_unit = self.opt_units_list('Meks Unit', self.Mek, 0, 3)
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        have_meks = self.meks.get('add')
        self.meks_unit.set_visible(have_meks)
        self.meks_unit.set_active(have_meks)
        meks = 0
        if have_meks:
            self.meks_unit.update_range(1, 3)
            meks = self.meks_unit.get_count()
            for unit in self.meks_unit.get_units():
                unit.set_cyborg(self.cyborg.get('cyborg'))
        norm_counts(5 - meks, 15 - meks, [self.count])
        self.set_points(self.build_points(count=1, exclude=[self.meks.id, self.cyborg.id]) +
                        self.count.get() * self.cyborg.points())
        self.build_description(options=[self.meks_unit], count=1)
        self.description.add(ModelDescriptor('Burna Boyz', points=15, count=self.count.get(), gear=['Burna'])
            .add_gear_opt(self.cyborg).build())

    def get_count(self):
        return self.count.get() + self.meks_unit.get_count()


class Lootas(Unit):
    name = 'Lootas'

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count("Lootas", 5, 15, 15)
        self.meks = self.opt_options_list("", [['Meks', 15, 'add']])
        self.meks_unit = self.opt_units_list('Meks Unit', BurnaBoyz.Mek, 0, 3)
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        have_meks = self.meks.get('add')
        self.meks_unit.set_visible(have_meks)
        self.meks_unit.set_active(have_meks)
        meks = 0
        if have_meks:
            self.meks_unit.update_range(1, 3)
            meks = self.meks_unit.get_count()
            for unit in self.meks_unit.get_units():
                unit.set_cyborg(self.cyborg.get('cyborg'))
        norm_counts(5 - meks, 15 - meks, [self.count])
        self.set_points(self.build_points(count=1, exclude=[self.meks.id, self.cyborg.id]) +
                        self.count.get() * self.cyborg.points())
        self.build_description(options=[self.meks_unit], count=1)
        self.description.add(ModelDescriptor('Lootas', points=15, count=self.count.get(), gear=['Deffgun'])
            .add_gear_opt(self.cyborg).build())

    def get_count(self):
        return self.count.get() + self.meks_unit.get_count()


class Tankbustas(Unit):
    name = 'Tankbustas'

    class Nob(Unit):
        name = 'Nob'
        base_points = 25
        gear = ['Tankbusta bombz']

        def __init__(self):
            Unit.__init__(self)

            self.wep = self.opt_one_of('Weapon', [
                ['Rokkit launcha', 0],
                ['Power klaw', 25],
            ])

            self.opt = self.opt_options_list('Options', [
                ["Bosspole", 5, 'pole'],
                ["'eavy armour", 5, 'armour'],
            ])

        def set_cyborg(self, cyborg):
            self.gear = ['Tankbusta bombz']
            if cyborg:
                self.gear += ['Cybork body']
                self.base_points += 5
            Unit.check_rules(self)

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Tankbustas', 5, 15, 15)
        self.th = self.opt_count("Tankhammer", 0, 2, 0)
        self.bs = self.opt_count("Bomb-squigs", 0, 3, 5)

        self.boss = self.opt_options_list('Boss', [
            ["Nob", self.Nob.base_points, 'nob'],
        ])
        self.nob_unit = self.opt_sub_unit(self.Nob())
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        self.nob_unit.set_active(self.boss.get('nob'))
        boss = 1 if self.boss.get('nob') else 0
        norm_counts(5 - boss, 15 - boss, [self.count])
        if self.nob_unit.get_unit():
            self.nob_unit.get_unit().set_cyborg(self.cyborg.get('cyborg'))
        self.set_points(self.build_points(exclude=[self.boss.id, self.cyborg.id], count=1) +
                        self.count.get() * self.cyborg.points())
        self.build_description(options=[self.nob_unit, self.bs])
        desc = ModelDescriptor('Tankbustas', gear=['Tankbusta bombz'], points=15).add_gear_opt(self.cyborg)
        self.description.add(desc.clone().add_gear('Tankhammer').build(self.th.get()))
        self.description.add(desc.add_gear('Rokkit launcha').build(self.count.get() - self.th.get()))

    def get_count(self):
        return self.count.get() + self.nob_unit.get_count()


class Kommandos(Unit):
    name = 'Kommandos'

    class Nob(Unit):
        name = 'Nob'
        base_points = 20
        gear = ['Stikkbombz', 'Slugga']

        def __init__(self):
            Unit.__init__(self)

            self.ccw = self.opt_one_of('Weapon', [
                ['Choppa', 0, 'choppa'],
                ['Big choppa', 5, 'bc'],
                ['Power klaw', 25, 'klaw'],
            ])

            self.opt = self.opt_options_list('Options', [
                ["Bosspole", 5, 'pole'],
                ["'eavy armour", 5, 'armour'],
            ])

        def set_cyborg(self, cyborg):
            self.gear = ['Stikkbombz', 'Slugga']
            if cyborg:
                self.gear += ['Cybork body']
                self.base_points += 5
            Unit.check_rules(self)

        def check_rules(self):
            pass

    class BossSnikrot(StaticUnit):
        name = 'Boss Snikrot'
        base_points = 75
        gear = ["Mork's Teeth", 'Stikkbombz']

    def __init__(self):
        Unit.__init__(self)

        self.count = self.opt_count('Kommandos', 5, 15, 10)

        self.bs = self.opt_count("Big shoota", 0, 2, 5)
        self.rl = self.opt_count("Rokkit launcha", 0, 2, 10)
        self.br = self.opt_count("Burna", 0, 2, 15)

        self.boss = self.opt_options_list('Boss', [
            ["Nob", self.Nob.base_points, 'nob'],
            ["Boss Snikrot", self.BossSnikrot.base_points, 'snik'],
        ], limit=1)
        self.nob_unit = self.opt_sub_unit(self.Nob())
        self.snik_unit = self.opt_sub_unit(self.BossSnikrot())
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        self.nob_unit.set_active(self.boss.get('nob'))
        self.snik_unit.set_active(self.boss.get('snik'))
        boss = 1 if self.boss.get('nob') else 0
        norm_counts(5 - boss, 15 - boss, [self.count], 4)
        norm_counts(0, 2, [self.bs, self.rl, self.br])
        if self.nob_unit.get_unit():
            self.nob_unit.get_unit().set_cyborg(self.cyborg.get('cyborg'))
        self.set_points(self.build_points(exclude=[self.boss.id, self.cyborg.id], count=1) +
                        self.count.get() * self.cyborg.points())
        self.build_description(options=[self.nob_unit, self.snik_unit])
        desc = ModelDescriptor('Kommandos', gear=['Stikkbombz'], points=10).add_gear_opt(self.cyborg)
        self.description.add(desc.clone().add_gear("Big shoota", 5).build(self.bs.get()))
        self.description.add(desc.clone().add_gear("Rokkit launcha", 10).build(self.rl.get()))
        self.description.add(desc.clone().add_gear("Burna", 15).build(self.br.get()))
        self.description.add(desc.add_gear('Slugga').add_gear('Choppa')
            .build(self.count.get() - self.bs.get() - self.rl.get() - self.br.get()))

    def get_unique(self):
        if self.boss.get('snik'):
            return self.BossSnikrot.name
        return None

    def get_count(self):
        return self.count.get() + self.nob_unit.get_count() + self.snik_unit.get_count()
