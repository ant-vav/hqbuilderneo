__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.games.wh40k.obsolete.blood_angels.transport import DropPod, Rhino, Razorback, LandRaider, LandRaiderCrusader, LandRaiderRedeemer

class Dante(StaticUnit):
    name = "Commander Dante"
    base_points = 225
    gear = ['Artificer Armour', 'Infernus Pistol', 'The Axe Mortalis', 'Jump Pack', 'Frag and Krak grenades',
            'Iron Halo', 'The Belt of Russ', 'Death Mask of Sanguinius']

class Gabriel(StaticUnit):
    name = "Chapter Master Gabriel Seth"
    base_points = 160
    gear = ['Power Armour', 'Bolt Pistol', 'Blood Reaver', 'Frag and Krak grenades', 'Iron Halo']

class Astropath(StaticUnit):
    name = "Astorath the Grim"
    base_points = 220
    gear = ['Artificer Armour', 'Bolt Pistol', "The Executoner's Axe", 'Frag and Krak grenades', 'Jump Pack', 'Rosarius']

class Sanguinor(StaticUnit):
    name ="The Sanguinor, Exemplar of the Host"
    base_points = 275
    gear = ['Artificer Armour', "Glave Encarmine", 'Frag and Krak grenades', 'Jump Pack']

class Mephiston(StaticUnit):
    name = "Mephiston, Lord  of Death"
    base_points = 250
    gear = ['Artificer Armour', 'Plasma Pistol', "Force Sword", 'Frag and Krak grenades', 'Psychic Hood']

class Tycho(StaticUnit):
    name = "Captain Tycho"
    base_points = 175
    gear = ['Artificer Armour', 'Blood Song', 'Bolt Pistol', "The Dead Man's Hand", 'Frag and Krak grenades', 'Iron Halo']

class Librarian(Unit):
    name = "Librarian"
    base_points = 100
    def __init__(self):
        Unit.__init__(self)
        self.armor = self.opt_one_of('Armour', [
            ["Power Armour", 0, "power"],
            ["Terminator Armour", 25, "tda"],
            ])

        self.wep1 = self.opt_one_of('Weapon', [
            ["Bolt Pistol", 0, "pistol"],
            ["Boltgun", 0, "gun"],
            ["Storm Bolter", 3, "storm"],
            ["Hand flamer", 10, "hflame"],
            ["Combi-flamer", 10, "cflame"],
            ["Combi-melta", 10, "cmelta"],
            ["Combi-plasma", 10, "cplasma"],
            ["Plasma Pistol", 15, "plasma"],
            ["Infernus Pistol", 15, "infernus"],
            ])

        self.tda_wep1 = self.opt_one_of('Weapon', [
            ["Storm Bolter", 0, "storm"],
            ["Combi-flamer", 5, "cflame"],
            ["Combi-melta", 5, "cmelta"],
            ["Combi-plasma", 5, "cplasma"],
            ["Storm Shield", 20, "shield"],
            ])

        self.mount = self.opt_options_list("Mount", [
            ["Jump Pack", 25, "jump"],
            ["Space Marine Bike", 35, "bike"],
            ], limit = 1)

        self.opt = self.opt_options_list("Options", [
            ["Blood Boil", 0, "boil"],
            ["Fear of the Darkness", 0, "fear"],
            ["Might of Heroes", 0, "might"],
            ["Shackle Soul", 0, "shackle"],
            ["Shield of Sanguinius", 0, "sangshield"],
            ["Smite", 0, "smite"],
            ["The Blood Lance", 0, "lance"],
            ["The Sanguine Sword", 0, "sword"],
            ["Unleash Rage", 0, "rage"],
            ["Wings of Sanguinius", 0, "wings"],
            ], limit=2)
        self.opt = self.opt_options_list("", [
            ['Epistolary', 50]
        ])


    def check_rules(self):
        tda = self.armor.get_cur() == 'tda'
        self.wep1.set_visible(not tda)
        self.mount.set_visible(not tda)
        self.tda_wep1.set_visible(tda)
        self.gear = ["Force Weapon", 'Psychic Hood']
        if not tda:
            self.gear += ['Frag and Krak Grenades']
        Unit.check_rules(self)

class Reclusiarch(Unit):
    name = "Reclusiarch"
    base_points = 130

    def __init__(self):
        Unit.__init__(self)
        self.armor = self.opt_one_of('Armour', [
            ["Power Armour", 0, "power"],
            ["Terminator Armour", 25, "tda"],
            ])

        self.wep1 = self.opt_one_of('Weapon', [
            ["Bolt Pistol", 0, "pistol"],
            ["Boltgun", 0, "gun"],
            ["Storm Bolter", 3, "storm"],
            ["Hand flamer", 10, "hflame"],
            ["Combi-flamer", 10, "cflame"],
            ["Combi-melta", 10, "cmelta"],
            ["Combi-plasma", 10, "cplasma"],
            ["Plasma Pistol", 15, "plasma"],
            ["Infernus Pistol", 15, "infernus"],
            ["Power Fist", 15, "fist"],
            ])

        self.tda_wep1 = self.opt_one_of('Weapon', [
            ["Storm Bolter", 0, "storm"],
            ["Combi-flamer", 5, "cflame"],
            ["Combi-melta", 5, "cmelta"],
            ["Combi-plasma", 5, "cplasma"],
            ])

        self.mount = self.opt_options_list("Mount", [
            ["Jump Pack", 25, "jump"],
            ["Space Marine Bike", 35, "bike"],
            ], limit = 1)

        self.opt = self.opt_options_list("Options", [
            ["Melta Bombs", 5, "bomb"],
            ])

    def check_rules(self):
        tda = self.armor.get_cur() == 'tda'
        self.wep1.set_visible(not tda)
        self.mount.set_visible(not tda)
        self.tda_wep1.set_visible(tda)
        self.gear = ["Rosarius", 'Crozius Arcanum']
        if not tda:
            self.gear += ['Frag and Krak Grenades']
        Unit.check_rules(self)


class Captian(Unit):
    name = "Captain"
    base_points = 100

    def __init__(self):
        Unit.__init__(self)
        self.armor = self.opt_one_of('Armour', [
            ["Power Armour", 0, "power"],
            ["Terminator Armour", 40, "tda"],
            ])

        wep = [
            ["Boltgun", 0, "gun"],
            ["Storm Bolter", 3, "storm"],
            ["Hand flamer", 10, "hflame"],
            ["Combi-flamer", 10, "cflame"],
            ["Combi-melta", 10, "cmelta"],
            ["Combi-plasma", 10, "cplasma"],
            ["Plasma Pistol", 15, "plasma"],
            ["Power Weapon", 15, "pw"],
            ["Lightning Claw", 15, "claw"],
            ["Infernus Pistol", 15, "infernus"],
            ["Storm Shield", 20, "shield"],
            ["Power Fist", 25, "fist"],
            ["Thunder Hummer", 30, "hummer"],
            ]

        self.wep1 = self.opt_one_of('Weapon', [["Bolt Pistol", 0, "pistol"]] + wep)

        self.wep2 = self.opt_one_of('', [["Chain Sword", 0, "ccw"]] + wep)

        self.tda_wep1 = self.opt_one_of('Weapon', [
            ["Storm Bolter", 0, "storm"],
            ["Combi-flamer", 5, "cflame"],
            ["Combi-melta", 5, "cmelta"],
            ["Combi-plasma", 5, "cplasma"],
            ["Lightning Claw", 10, "claw"],
            ["Thunder Hummer", 20, "hummer"],
            ])

        self.tda_wep2 = self.opt_one_of('', [
            ["Power Sword", 0, "power"],
            ["Lightning Claw", 5, "claw"],
            ["Power Fist", 10, "fist"],
            ["Thunder Hummer", 15, "hummer"],
            ["Storm Shield", 15, "shield"],
            ["Chain Fist", 15, "chain"],
            ])

        self.mount = self.opt_options_list("Mount", [
            ["Jump Pack", 25, "jump"],
            ["Space Marine Bike", 35, "bike"],
            ], limit = 1)
        self.bombs = self.opt_options_list("Options", [
            ["Melta bombs", 5, "bomb"],
        ])

    def check_rules(self):
        tda = self.armor.get_cur() == 'tda'
        self.wep1.set_visible(not tda)
        self.wep2.set_visible(not tda)
        self.mount.set_visible(not tda)
        self.tda_wep1.set_visible(tda)
        self.tda_wep2.set_visible(tda)
        self.gear = ["Iron Halo"]
        if not tda:
            self.gear += ['Frag and Krak Grenades']
        Unit.check_rules(self)

class HonourGuards(Unit):
    name = "Honour Guard"

    class HonorGuard(Unit):
        base_points = 23
        name = 'Honour Guard'

        def __init__(self):
            Unit.__init__(self)
            wep = [
                ["Storm Bolter", 3, "storm"],
                ["Flamer", 5, "flame"],
                ["Meltagun", 10, "melta"],
                ["Combi-flamer", 10, "cflame"],
                ["Combi-melta", 10, "cmelta"],
                ["Combi-plasma", 10, "cplasma"],
                ["Hand flamer", 10, "hflame"],
                ["Plasma Pistol", 15, "plasma"],
                ["Plasma Gun", 15, "pgun"],
                ["Infernus Pistol", 15, "infernus"],
                ["Power Weapon", 15, "pw"],
                ["Lightning Claw", 15, "claw"],
                ["Storm Shield", 20, "shield"],
                ["Power Fist", 25, "fist"],
                ["Thunder Hummer", 30, "hummer"],
                ]
            self.wep1 = self.opt_one_of('Weapon', [["Bolt Pistol", 0, "pistol"], ["Boltgun", 0, "gun"]] + wep)
            self.wep2 = self.opt_one_of('', [["Chain Sword", 0, "ccw"]] + wep)
            self.blood = self.opt_options_list('', [["Blood Champion", 20, "bch"]])
            self.opt = self.opt_options_list('Options', [["Melta-bombs", 5, "bombs"]])
            self.flag = self.opt_options_list('Standard', [
                ["Company Standard", 15, "cs"],
                ["Chapter Banner", 30, "cb"],
            ], limit=1)
            self.have_flag = False
            self.have_bch = False
            self.have_jp = False

        def check_rules(self):
            bch = self.blood.get('bch')
            self.wep1.set_visible(not bch)
            self.wep2.set_visible(not bch)
            self.opt.set_visible(not bch)
            self.flag.set_visible(not bch)

            self.blood.set_active_options(['bch'], not self.have_bch or bch)
            if not self.flag.get_all():
                self.flag.set_active_options(self.flag.get_all_ids(), not self.have_flag)
            self.gear = ["Power Armour", 'Frag and Krak Grenades']
            if self.have_jp:
                self.gear += ['Jump Pack']
            self.set_points(self.build_points())
            if bch:
                self.gear += ['Power Weapon', 'Combat Shield']
                self.build_description(name='Blood Champion', exclude=[self.blood.id])
            else:
                self.build_description()

    class SanguinaryNovitiate(Unit):
        base_points = 23
        name = 'Sanguinary Novitiate'

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [["Bolt Pistol", 0, "pistol"], ["Boltgun", 0, "gun"]])
            self.have_jp = False
        def check_rules(self):
            self.gear = ["Power Armour", 'Frag and Krak Grenades', 'Blood Chalice']
            if self.have_jp:
                self.gear += ['Jump Pack']
            Unit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.hg = [self.opt_sub_unit(self.HonorGuard()) for i in range(0,4)]
        self.sv = self.opt_sub_unit(self.SanguinaryNovitiate())
        self.jp = self.opt_options_list('', [['Jump Packs', 50, 'jp']])
        self.trans = self.opt_optional_sub_unit('Transport', [Rhino(), Razorback(), DropPod(),
                                                              LandRaider(), LandRaiderCrusader(), LandRaiderRedeemer()])

    def check_rules(self):
        bch = reduce(lambda val, c: val + (1 if c.get_unit().blood.get('bch') else 0), self.hg, 0)
        flag = reduce(lambda val, c: val + (1 if c.get_unit().flag.get_all() else 0), self.hg, 0)
        jp = self.jp.get('jp')
        for hg in self.hg:
            hg.get_unit().have_bch = (bch > 0)
            hg.get_unit().have_flag = (flag > 0)
            hg.get_unit().have_jp = jp
            hg.get_unit().check_rules()
        self.trans.get_options().set_active_all(not jp)
        self.jp.set_active_all(self.trans.get_count() == 0)
        self.points.set(self.build_points())
        self.build_description(exclude=[self.jp.id])

    def get_unique(self):
        if reduce(lambda val, c: val + (1 if c.get_unit().flag.get('cb') else 0), self.hg, 0):
            return "Chapter Banner"
