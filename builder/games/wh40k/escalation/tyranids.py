__author__ = 'Denis Romanow'

from builder.core2 import *
from builder.games.wh40k.roster import LordsOfWarSection


class Harridan(Unit):
    type_id = 'harridan_v1'
    type_name = 'Harridan'

    def __init__(self, parent):
        super(Harridan, self) .__init__(parent=parent, points=735, gear=[
            Gear('Bio-cannon', count=2),
            Gear('Scything talons'),
        ])


class Hierodule(Unit):
    type_id = 'hierodule_v1'
    type_name = 'Barbed Hierodule'

    def __init__(self, parent):
        super(Hierodule, self) .__init__(parent=parent, points=565, gear=[
            Gear('Bio-cannon', count=2),
            Gear('Scything talons'),
        ])


class Hierophant(Unit):
    type_id = 'hierophant_v1'
    type_name = 'Hierophant Bio-titan'

    def __init__(self, parent):
        super(Hierophant, self) .__init__(parent=parent, points=1000, gear=[
            Gear('Bio-cannon', count=2),
            Gear('Lash whip'),
            Gear('Scything talons'),
            Gear('Regeneration'),
            Gear('Toxic miasma'),
        ])


class LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent=parent)
        UnitType(self, Hierodule)
        UnitType(self, Harridan)
        UnitType(self, Hierophant)
