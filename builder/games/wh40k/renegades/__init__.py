__author__ = 'Ivan Truskov'
__summaty = ['Imperial Armour 13: Renegades and heretics list',
             'Imperial Armour 5-7: Siege of Vraks Renegades of Vraks list']

from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, PrimaryDetachment
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, LordsOfWarSection, UnitType, Section
from builder.games.wh40k.fortifications import Fort
from hq import EnforcerCadre, PsykerCoven, RenegadeCommand
from troops import Mutants, RenegadePlatoon, Veterans, Zombies
from elites import Disciples, Maradeurs, OgrynBrutes,\
    RenegadeBlightDrones, RenegadeSlaughterers, Spawn
from fast import ArvusSquadron, HellhoundSquadron, SalamanderSquadron,\
    SentinelSquadron, ValkyrieSquadron
from heavy import ArtilleryBattery, BombardBattery, FieldBattery,\
    HydraBattery, OrdnanceBattery, Rapiers, StrikeBattery, SupportSquad,\
    TankSquadron
from lords import Malkador, Minotaur, MalkadorDefender, Baneblade,\
    SpinedBeasts, GiantSpawns, SpinedBeast, GiantChaosSpawn
from builder.games.wh40k.imperial_armour.volume5_7 import RenegadeCharacters


class HQ(HQSection):

    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.command = UnitType(self, RenegadeCommand)
        self.coven = UnitType(self, EnforcerCadre)
        self.cadre = UnitType(self, PsykerCoven)

    def check_rules(self):
        super(HQ, self).check_rules()
        if self.command.count < 1:
            self.error("At least one Renegade Command Squad must be taken")
        devcount = sum(u.is_devoted() for u in self.command.units)
        if devcount > 1:
            self.error("Only one Demagogue in the army can gain a Devotion")
        if self.coven.count > 1:
            self.error("At most one Rogue Psykers Coven can be taken as HQ")
        if self.cadre.count > 1:
            self.error("At most one Renegade Enforcers Cadre may be taken")


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.disc = UnitType(self, Disciples)
        self.brutes = UnitType(self, OgrynBrutes)


class BaseTroops(TroopsSection):
    def __init__(self, parent):
        super(BaseTroops, self).__init__(parent)
        self.platoon = UnitType(self, RenegadePlatoon)
        self.mutants = UnitType(self, Mutants)
        self.veterans = UnitType(self, Veterans)


class Fast(FastSection):
    def __init__(self, parent):
        super(Fast, self).__init__(parent)
        UnitType(self, HellhoundSquadron)
        UnitType(self, SentinelSquadron)
        UnitType(self, SalamanderSquadron)
        if not self.roster.is_vraks:
            UnitType(self, ArvusSquadron)
            UnitType(self, ValkyrieSquadron)


class BaseHeavy(HeavySection):
    def __init__(self, parent):
        super(BaseHeavy, self).__init__(parent)
        UnitType(self, TankSquadron)
        UnitType(self, ArtilleryBattery)
        UnitType(self, StrikeBattery)
        UnitType(self, HydraBattery)
        UnitType(self, BombardBattery)
        UnitType(self, OrdnanceBattery)
        self.support = UnitType(self, SupportSquad)
        UnitType(self, Rapiers)
        UnitType(self, FieldBattery)


class BaseLords(LordsOfWarSection):
    def __init__(self, parent):
        super(BaseLords, self).__init__(parent)
        UnitType(self, Malkador)
        UnitType(self, Minotaur)
        UnitType(self, MalkadorDefender)
        UnitType(self, Baneblade)


class PrimarySection(Section):
    """
    Section in Renegades Primary detachment
    """
    def __init__(self, parent):
        super(PrimarySection, self).__init__(parent)
        # must consist of tuples (<list of unit types>, <precondition>, <error message>, <limit>, <error names 2>)
        self.primary_type_data = []

    def check_rules(self):
        super(PrimarySection, self).check_rules()
        for tup in self.primary_type_data:
            if isinstance(self.roster.parent.parent, PrimaryDetachment)\
               and tup[1](self.roster.demagogue):
                for ut in tup[0]:
                    ut.active = True
                limit = tup[3](self, tup[0])
                if limit:
                    total = sum(ut.count for ut in tup[0])
                    if total > limit:
                        self.error("No more then {} of {} may be taken (taken: {})".format(limit, tup[4], total))
            else:
                for ut in tup[0]:
                    ut.active = False
                    if ut.count > 0:
                        self.error(tup[2])


class VraksHQ(RenegadeCharacters, HQ):
    pass


class BasePrimaryElites(PrimarySection, BaseElites):
    def __init__(self, parent):
        super(BasePrimaryElites, self).__init__(parent)
        witches = UnitType(self, PsykerCoven)
        maradeurs = UnitType(self, Maradeurs)
        self.slaughterers = UnitType(self, RenegadeSlaughterers)
        drones = UnitType(self, RenegadeBlightDrones)
        self.primary_type_data += [
            ([witches], lambda demagogue: demagogue.is_witch(),
             "Only primaris-rogue witch may include psyker covens as elites",
             lambda sec, types: False, ""),
            ([maradeurs], lambda demagogue: not demagogue.has_covenant(),
             "Only Demagogue with no chaos covenant may take Renegade maradeurs",
             lambda sec, types: 3, "Renegade Maradeurs"),
            ([self.slaughterers], lambda demagogue: demagogue.khorne_covenant(),
             "Only Demagogue with Khorne covenant may take Blood Slaughterers",
             lambda sec, types: 1, "Blood Slaughterers"),
            ([drones], lambda demagogue: demagogue.nurgle_covenant(),
             "Onle Demagogue with Nurgle covenant may take Blight Drones",
             lambda sec, types: 1, "Blight Drones")
        ]


class PrimaryElites(BasePrimaryElites):
    def __init__(self, parent):
        super(PrimaryElites, self).__init__(parent)
        from builder.games.wh40k.imperial_armour.volume13.elites import Decimator
        decimator = UnitType(self, Decimator)
        from builder.games.wh40k.imperial_armour.volume13.elites import SonicDreadnought
        from builder.games.wh40k.chaos_marines_v2.elites import NoiseMarines
        noise = [
            UnitType(self, SonicDreadnought),
            UnitType(self, NoiseMarines)
        ]
        self.primary_type_data += [
            ([decimator], lambda demagogue: demagogue.is_heretek(),
             "Only heretek magus may include decimators as elites",
             lambda sec, types: False, ""),
            (noise, lambda demagogue: demagogue.slaanesh_covenant(),
             "Only Demagogue with Slaanesh covenant may take Noise Marines or Sonic Dreadnought",
             lambda sec, types: sum(ut.count > 0 for ut in types), "Noise Marines and/or Sonic Dreadnoughts")
        ]


class VraksElites(BasePrimaryElites):
    def __init__(self, parent):
        super(VraksElites, self).__init__(parent)
        artillery = [
            UnitType(self, ArtilleryBattery),
            UnitType(self, StrikeBattery),
            UnitType(self, BombardBattery),
            UnitType(self, OrdnanceBattery)
        ]
        self.primary_type_data += [
            (artillery, lambda demagogue: demagogue.is_tyrant(),
             "Only Ordnance Tyrant may include artillery units as elites",
             lambda sec, types: False, "")
        ]


class PrimaryTroops(PrimarySection, BaseTroops):
    def __init__(self, parent):
        super(PrimaryTroops, self).__init__(parent)
        self.spawn = UnitType(self, Spawn)
        self.primary_type_data += [
            ([self.spawn], lambda demagogue: demagogue.is_mutant() or demagogue.tzeentch_covenant(),
             "Only Mutant Overlord or a Demagogue with Tzeentch covenant may take Chaos Spawn",
             lambda sec, types: sec.roster.demagogue.is_mutant() + 3 * sec.roster.demagogue.tzeentch_covenant(), 'Chaos Spawn'),
            ([UnitType(self, Zombies)], lambda demagogue: demagogue.nurgle_covenant(),
             "Only Demagogue with Nurgle covenant may take Plague Zombies",
             lambda sec, types: 1, 'Plague Zombies')
        ]

    def check_rules(self):
        super(PrimaryTroops, self).check_rules()
        if self.roster.demagogue.is_mutant():
            if self.mutants.count < 2:
                self.error("Army led by Mutant Overlord must include at least 2 Mutant Rabble units")
        if self.roster.demagogue.is_horde():
            if self.platoon.count < 2:
                self.error("Army led by Master of the Horde must include at least 2 Renegade Infantry Platoons")
        if len(self.units) - self.spawn.count < self.min and self.spawn.count:
            self.error("Chaos Spawns cannot be compulsory Troop choices")


class VraksTroops(PrimarySection, BaseTroops):
    def __init__(self, parent):
        super(VraksTroops, self).__init__(parent)
        self.battery = UnitType(self, FieldBattery)
        self.zombies = UnitType(self, Zombies)
        self.brutes = UnitType(self, OgrynBrutes)
        self.primary_type_data += [
            ([self.battery], lambda demagogue: demagogue.is_tyrant(),
             "Only Ordnance Tyrant may take Field Artillery Batteries as Troops choices",
             lambda sec, types: False, ""),
            ([self.brutes], lambda demagogue: demagogue.is_shock(),
             "Only Shock Legion Taskmaster may take Ogryn Brutes as Troops choice",
             lambda sec, types: False, ''),
            ([self.zombies], lambda demagogue: demagogue.nurgle_covenant(),
             "Only Demagogue with Nurgle covenant may take Plague Zombies",
             lambda sec, types: False, '')
        ]

    def check_rules(self):
        super(VraksTroops, self).check_rules()
        if self.roster.demagogue.is_horde():
            if self.platoon.count < 2:
                self.error("Army led by Master of the Horde must include at least 2 Renegade Infantry Platoons")
        if len(self.units) - self.battery.count < self.min and self.battery.count:
            self.error("Field Artillery Batteries cannot be compulsory Troop choices")
        if self.roster.demagogue.is_shock() and self.brutes.count < self.min:
            self.error("Ogryn Brutes must be compulsory Troops")


class PrimaryHeavy(PrimarySection, BaseHeavy):
    def __init__(self, parent):
        super(PrimaryHeavy, self).__init__(parent)
        from builder.games.wh40k.chaos_marines_v2.heavy import Defiler
        self.primary_type_data += [
            ([UnitType(self, Defiler)], lambda demagogue: demagogue.is_heretek(),
             "Only Heretek Magus may take Defilers",
             lambda sec, types: False, "")
        ]


class PrimaryLords(PrimarySection, BaseLords):
    def __init__(self, parent):
        super(PrimaryLords, self).__init__(parent)
        from builder.games.wh40k.escalation.chaos import LordOfSkulls
        from builder.games.wh40k.imperial_armour.volume13 import GreaterScorpion,\
            Scabeiathrax, Anggrath, Zarakynel, Aetaosraukeres, ChaosWarhoundTitan,\
            ChaosReaverTitan
        self.lords = [
            UnitType(self, Scabeiathrax),
            UnitType(self, Anggrath),
            UnitType(self, Zarakynel),
            UnitType(self, Aetaosraukeres)
        ]
        beasts = [
            UnitType(self, SpinedBeasts),
            UnitType(self, GiantSpawns)
        ]
        from builder.games.wh40k.imperial_armour.volume1.lords import Macharius,\
            MachariusVanquisher, MachariusVulcan, MachariusOmega, Valdor
        tanks = [
            UnitType(self, Macharius),
            UnitType(self, MachariusVanquisher),
            UnitType(self, MachariusVulcan),
            UnitType(self, MachariusOmega)
        ]
        titans = [
            UnitType(self, Valdor),
            UnitType(self, ChaosWarhoundTitan),
            UnitType(self, ChaosReaverTitan)
        ]
        self.primary_type_data += [
            ([UnitType(self, LordOfSkulls)], lambda demagogue: demagogue.is_reaver(),
             "Only bloody-handed Reaver may take Khorne Lord of Skulls",
             lambda sec, types: False, ""),
            ([UnitType(self, GreaterScorpion)], lambda demagogue: demagogue.is_reaver() or demagogue.is_heretek(),
             "Only heretek magus or bloody-handed Reaver may take Greater Brass Scorpion",
             lambda sec, types: False, ""),
            (self.lords, lambda demagogue: demagogue.is_witch(),
             "Only primaris-rogue witch may take Daemon Lords",
             lambda sec, types: False, ""),
            (beasts, lambda demagogue: demagogue.is_mutant(),
             "Only mutant overlord may take giant chaos spawns or spined beasts",
             lambda sec, types: False, ""),
            (tanks, lambda demagogue: demagogue.is_revolutionary(),
             "Only arch-heretic revolutionary may take any Macharius tank",
             lambda sec, types: False, ""),
            (titans, lambda demagogue: demagogue.is_heretek(),
             "Only heretek magus may take Chaos Titan or Valdor Tank hunter",
             lambda sec, types: False, "")
        ]


class VraksLords(BaseLords):
    def __init__(self, parent):
        super(VraksLords, self).__init__(parent)
        from builder.games.wh40k.escalation.chaos import LordOfSkulls
        self.los = UnitType(self, LordOfSkulls)
        from builder.games.wh40k.imperial_armour.volume13 import GreaterScorpion,\
            Scabeiathrax, Anggrath
        self.scorp = UnitType(self, GreaterScorpion)
        UnitType(self, Scabeiathrax)
        self.anggrath = UnitType(self, Anggrath)
        self.beast = UnitType(self, SpinedBeast)
        self.spawn = UnitType(self, GiantChaosSpawn)
        from builder.games.wh40k.imperial_armour.volume1.lords import Macharius,\
            MachariusVanquisher, MachariusVulcan, MachariusOmega, Valdor
        UnitType(self, Macharius)
        UnitType(self, MachariusVanquisher)
        UnitType(self, MachariusVulcan)
        UnitType(self, MachariusOmega)
        UnitType(self, Valdor)


class PurgeSection(Section):
    def __init__(self, parent):
        super(PurgeSection, self).__init__(parent)
        self.undevoted = []

    def check_rules(self):
        super(PurgeSection, self).check_rules()
        for ut in self.undevoted:
            for un in ut.units:
                if un.not_nurgle():
                    self.error("{} may not be dedicated to Chaos Gods who are not Grandpa Nurgle!".format(un.name))


class PurgeHq(PurgeSection, VraksHQ):
    def __init__(self, parent):
        super(PurgeHq, self).__init__(parent)
        self.undevoted += [
            self.command
        ]


class PurgeElites(PurgeSection, VraksElites):
    def __init__(self, parent):
        super(PurgeElites, self).__init__(parent)
        self.slaughterers.used = self.slaughterers.visible = False
        self.undevoted += [
            self.disc, self.brutes
        ]


class PurgeTroops(PurgeSection, VraksTroops):
    def __init__(self, parent):
        super(PurgeTroops, self).__init__(parent)
        # check for zombiesis removed here
        self.primary_type_data = self.primary_type_data[:-1]
        self.undevoted += [
            self.brutes,
            self.platoon,
            self.mutants,
            self.veterans
        ]


class PurgeFast(PurgeSection, Fast):
    pass


class PurgeHeavy(PurgeSection, BaseHeavy):
    def __init__(self, parent):
        super(PurgeHeavy, self).__init__(parent)
        self.undevoted += [
            self.support
        ]


class PurgeLords(PurgeSection, VraksLords):
    def __init__(self, parent):
        super(PurgeLords, self).__init__(parent)
        self.anggrath.used = self.anggrath.visible = False
        self.los.used = self.los.visible = False
        self.scorp.used = self.scorp.visible = False
        self.undevoted += [
            self.beast, self.spawn,
        ]


class BaseRenegades(Wh40kBase):
    imperial_armour = True
    ia_enabled = True

    def check_rules_chain(self):
        self.find_demagogue()
        super(BaseRenegades, self).check_rules_chain()

    class FalseDemagogue(object):

        def is_heretek(self):
            return False

        def is_witch(self):
            return False

        def is_reaver(self):
            return False

        def is_mutant(self):
            return False

        def is_revolutionary(self):
            return False

        def is_horde(self):
            return False

        def is_tyrant(self):
            return False

        def is_shock(self):
            return False

        def has_covenant(self):
            return False

        def khorne_covenant(self):
            return False

        def nurgle_covenant(self):
            return False

        def slaanesh_covenant(self):
            return False

        def tzeentch_covenant(self):
            return False

        def not_nurgle(self):
            return False

    def find_demagogue(self):
        self.demagogue = self.FalseDemagogue()

    # for inclusion of Noise Marines
    @property
    def is_base(self):
        return True

    @property
    def is_black(self):
        return False

    @property
    def is_crimson(self):
        return False

    @property
    def is_alpha(self):
        return False

    @property
    def is_iron(self):
        return False

    @property
    def is_guard(self):
        return False

    @property
    def is_child(self):
        return False

    @property
    def is_thousand(self):
        return False

    @property
    def is_night(self):
        return False

    @property
    def is_word(self):
        return False

    @property
    def is_eater(self):
        return False

    @property
    def is_vraks(self):
        return False


class RenegadesPrimaryCAD(BaseRenegades, CombinedArmsDetachment):
    army_name = 'Renegades and Heretics (Combined arms detachment)'
    army_id = 'renegades_primary_v1_cad'

    def find_demagogue(self):
        if self.hq.command.count:
            self.demagogue = self.hq.command.units[0]
        else:
            self.demagogue = self.FalseDemagogue()

    def __init__(self):
        super(RenegadesPrimaryCAD, self).__init__(
            hq=HQ(self), elites=PrimaryElites(self), troops=PrimaryTroops(self),
            fast=Fast(self), heavy=PrimaryHeavy(self), fort=Fort(self), lords=PrimaryLords(self))

    def check_rules(self):
        super(RenegadesPrimaryCAD, self).check_rules()
        from itertools import chain
        lord_points = sum(u.points for u in chain(*[ut.units for ut in self.lords.lords]))

        def daemon_lord_points_check(roster):
            if roster.points < 4 * lord_points:
                roster.error("Daemon Lords may not make up more then 25% of army\'s total point value")
        self.root.extra_checks['renegade_demon_lords'] = daemon_lord_points_check


class VraksPrimaryCAD(BaseRenegades, CombinedArmsDetachment):
    army_name = 'Renegades of Vraks (Combined arms detachment)'
    army_id = 'vraks_renegades_primary_v1_cad'

    def find_demagogue(self):
        if self.hq.command.count:
            self.demagogue = self.hq.command.units[0]
        else:
            self.demagogue = self.FalseDemagogue()

    def __init__(self):
        super(VraksPrimaryCAD, self).__init__(
            hq=VraksHQ(self), elites=VraksElites(self), troops=VraksTroops(self),
            fast=Fast(self), heavy=BaseHeavy(self), fort=Fort(self), lords=VraksLords(self))

    @property
    def is_vraks(self):
        return True


class Purge(BaseRenegades, CombinedArmsDetachment):
    army_name = 'The Purge'
    army_id = 'purge_v1'

    def find_demagogue(self):
        if self.hq.command.count:
            self.demagogue = self.hq.command.units[0]
        else:
            self.demagogue = self.FalseDemagogue()

    def __init__(self):
        super(Purge, self).__init__(
            hq=PurgeHq(self), elites=PurgeElites(self), troops=PurgeTroops(self),
            fast=PurgeFast(self), heavy=PurgeHeavy(self), fort=Fort(self), lords=PurgeLords(self))
        self.troops.min, self.troops.max = (0, 8)
        self.elites.min, self.elites.max = (2, 6)
        self.heavy.max = 4

    @property
    def is_vraks(self):
        return True

    def check_rules(self):
        super(Purge, self).check_rules()
        self.troops.zombies.active = ((type(self.parent.parent) is PrimaryDetachment) and
                                     (self.demagogue.nurgle_covenant() > 0))
        if self.troops.zombies.count > 0:
            if self.demagogue.not_nurgle()\
               or not (type(self.parent.parent) is PrimaryDetachment):
                self.error("You can't have Plague Zombie without Demagogue with devotion to Nurgle as a Warlord")


class UnendingHost(VraksPrimaryCAD):
    army_name = 'Vraks Renegade Unending Host'
    army_id = 'vraks_renegades_primary_v1_host'

    def __init__(self):
        super(UnendingHost, self).__init__()
        self.troops.min, self.troops.max = (4, 8)
        self.elites.max = 2
        self.fast.max = 1
        self.heavy.max = 1

    def check_rules(self):
        super(UnendingHost, self).check_rules()
        if not self.demagogue.is_horde():
            self.error("The Army's Warlord must be a Renegade Demagogue with the Master of the Horde Devotion")


class RenegadesCAD(BaseRenegades, CombinedArmsDetachment):
    army_name = 'Renegades and Heretics (Combined arms detachment)'
    army_id = 'renegades_v1_cad'

    def __init__(self):
        super(RenegadesCAD, self).__init__(
            hq=HQ(self), elites=BaseElites(self), troops=BaseTroops(self),
            fast=Fast(self), heavy=BaseHeavy(self), fort=Fort(self), lords=BaseLords(self))


class VraksCAD(BaseRenegades, CombinedArmsDetachment):
    army_name = 'Renegades of Vraks (Combined arms detachment)'
    army_id = 'vraks_renegades_v1_cad'

    def __init__(self):
        super(VraksCAD, self).__init__(
            hq=VraksHQ(self), elites=BaseElites(self), troops=BaseTroops(self),
            fast=Fast(self), heavy=BaseHeavy(self), fort=Fort(self), lords=VraksLords(self))

    @property
    def is_vraks(self):
        return True


class RenegadesAD(BaseRenegades, AlliedDetachment):
    army_name = 'Renegades and Heretics (Allied detachment)'
    army_id = 'renegades_v1_cad'

    def __init__(self):
        super(RenegadesAD, self).__init__(
            hq=HQ(self), elites=BaseElites(self), troops=BaseTroops(self),
            fast=Fast(self), heavy=BaseHeavy(self))


class VraksAD(BaseRenegades, AlliedDetachment):
    army_name = 'Renegades of Vraks (Allied detachment)'
    army_id = 'vraks_renegades_v1_cad'

    def __init__(self):
        super(VraksAD, self).__init__(
            hq=VraksHQ(self), elites=BaseElites(self), troops=BaseTroops(self),
            fast=Fast(self), heavy=BaseHeavy(self))

    @property
    def is_vraks(self):
        return True


faction = 'Renegades and Heretics'


class Renegades(Wh40k7ed):
    army_name = 'Renegades and Heretics'
    army_id = 'renegades_v1'
    imperial_armour = True
    faction = faction

    def __init__(self):
        super(Renegades, self).__init__([RenegadesPrimaryCAD, VraksPrimaryCAD,
                                         UnendingHost, Purge], [])

    @property
    def imperial_armour_on(self):
        return self.imperial_armour


detachments = [
    RenegadesCAD,
    RenegadesAD,
    VraksCAD,
    VraksAD,
    Purge,
    UnendingHost
]
