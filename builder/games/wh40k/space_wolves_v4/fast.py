__author__ = 'Denis Romanov'

# from builder.games.wh40k.obsolete.space_wolves import fast
from builder.core2 import ListSubUnit, OneOf, OptionsList,\
    Count, Gear, OptionalSubUnit, SubUnit, UpgradeUnit, UnitList,\
    UnitDescription
from builder.games.wh40k.imperial_armour.volume2.options\
    import SpaceMarinesBaseVehicle, SpaceMarinesBaseSquadron
from armory import IronwolvesOption, VehicleEquipment, PlasmaPistol, Chainsword,\
    Melee, Special, BoltPistol, MeltaBombs, PowerFist, PowerWeapon,\
    Boltgun, Ranged
from builder.games.wh40k.unit import Unit


class WolfTransport(OptionalSubUnit):
    def __init__(self, parent, transport_types):
        super(WolfTransport, self).__init__(parent=parent, name='Transport', limit=1)
        self.transports = []
        self.ia_transports = []
        for ttype in transport_types:
            subunit = SubUnit(self, ttype(parent=self))
            self.transports.append(subunit)
            if hasattr(ttype, 'imperial_armour') and ttype.imperial_armour:
                self.ia_transports.append(subunit)

    def get_unique_gear(self):
        return sum((u.unit.get_unique_gear() for u in
                    self.transports), [])

    def count_glory_legacies(self):
        return sum((u.unit.count_glory_legacies() for u in self.transports if u and u.used))


class DropPod(SpaceMarinesBaseVehicle):
    type_name = u'Drop Pod'
    type_id = 'droppod_v4'
    model_points = 35

    def build_points(self):
        res = super(DropPod, self).build_points()
        if hasattr(self.roster, 'discount') and self.roster.discount is True:
            res -= self.model_points
        return res

    def check_rules(self):
        super(DropPod, self).check_rules()
        self.points = self.build_points()

    def __init__(self, parent, points=model_points):
        super(DropPod, self).__init__(parent=parent, points=points, deep_strike=True, transport=True)
        self.Weapon(self)
        self.Options(self)

    class Options(OptionsList, IronwolvesOption):
        def __init__(self, parent):
            super(DropPod.Options, self).__init__(parent, 'Options')
            self.variant('Locator beacon', 10 * self.freeflag)

    class Weapon(OneOf, IronwolvesOption):
        def __init__(self, parent):
            super(DropPod.Weapon, self).__init__(parent=parent, name='Weapon')
            self.stormbolter = self.variant('Storm Bolter', 0)
            self.deathwindmissilelauncher = self.variant('Deathwind Missile Launcher', 15 * self.freeflag)


class Rhino(SpaceMarinesBaseVehicle):
    type_id = 'rhino_v4'
    type_name = "Rhino"

    def __init__(self, parent, points=35):
        super(Rhino, self).__init__(parent=parent, points=points, gear=[
            Gear('Storm bolter'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.opt = VehicleEquipment(self)


class Razorback(SpaceMarinesBaseVehicle):
    type_id = 'razorback_v4'
    type_name = u'Razorback'

    class Weapon(OneOf, IronwolvesOption):
        def __init__(self, parent):
            super(Razorback.Weapon, self).__init__(parent, 'Weapon')
            self.tlhbgun = self.variant('Twin-linked heavy bolter', 0)
            self.tlhflame = self.variant('Twin-linked heavy flamer', 0)
            self.tlasscan = self.variant('Twin-linked assault cannon', 20 * self.freeflag)
            self.tllcannon = self.variant('Twin-linked lascannon', 20 * self.freeflag)
            self.lascplasgun = self.variant('Lascannon and twin-linked plasma gun', 20 * self.freeflag, gear=[
                Gear('Lascannon'), Gear('Twin-linked plasma gun')
            ])

    def __init__(self, parent):
        super(Razorback, self).__init__(parent=parent, points=55, gear=[
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.weapon = Razorback.Weapon(self)
        self.opt = VehicleEquipment(self)


class Stormwolf(SpaceMarinesBaseVehicle):
    type_name = u'Stormwolf'
    type_id = 'stormwolf_v4'

    class Weapon1(OneOf, IronwolvesOption):
        def __init__(self, parent):
            super(Stormwolf.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Two Twin-linked heavy bolters', 0,
                         gear=[Gear('Twin-linked heavy bolter', count=2)])
            self.variant('Skyhammer missile launcher', 0)
            self.variant('Two twin-linked Multi-meltas', 20 * self.freeflag,
                         gear=[Gear('Twin-linked multi-melta', count=2)])

    def __init__(self, parent):
        super(Stormwolf, self).__init__(parent=parent, points=215, gear=[
            Gear('Ceramite plating'),
            Gear('Twin-linked helfrost cannon'),
            Gear('Twin-linked lascannon')
        ], transport=True)
        self.wep1 = self.Weapon1(self)


class Stormwolves(SpaceMarinesBaseSquadron):
    type_name = u'Stormwolves'
    type_id = 'stormwolves_v4'
    unit_class = Stormwolf
    unit_max = 4


class LandSpeeder(SpaceMarinesBaseVehicle):
    class BaseWeapon(OneOf, IronwolvesOption):
        def __init__(self, parent):
            super(LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy Bolter', 0)
            self.heavyflamer = self.variant('Heavy Flamer', 0)
            self.multimelta = self.variant('Multi-melta', 10 * self.freeflag)

    class UpWeapon(OptionsList, IronwolvesOption):
        def __init__(self, parent):
            super(LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Upgrade', limit=1)
            self.heavybolter = self.variant('Heavy Bolter', 10 * self.freeflag)
            self.heavyflamer = self.variant('Heavy Flamer', 10 * self.freeflag)
            self.multimelta = self.variant('Multi-melta', 20 * self.freeflag)
            self.typhoonmissilelauncher = self.variant('Typhoon Missile Launcher', 25 * self.freeflag)
            self.assaultcannon = self.variant('Assault Cannon', 30 * self.freeflag)

    def __init__(self, parent):
        super(LandSpeeder, self).__init__(parent=parent, points=50, name="Land Speeder")
        self.wep = self.BaseWeapon(self)
        self.up = self.UpWeapon(self)


class LandSpeederSquadron(SpaceMarinesBaseSquadron):
    type_name = "Land Speeders"
    type_id = "land_speeders_v4"
    unit_class = LandSpeeder


class Swiftclaws(Unit):
    type_name = "Swiftclaws"
    type_id = "swiftclaws_v4"
    model_gear = [Gear('Power armour'), Gear('Frag grenades'),
                  Gear('Krak grenades'), Gear('Space marine bike')]

    class Marine(ListSubUnit):

        class Weapon(Special, PlasmaPistol, Melee, Chainsword, BoltPistol):
            pass

        def __init__(self, parent):
            super(Swiftclaws.Marine, self).__init__(
                parent=parent, name='Swiftclaw Biker',
                points=20, gear=Swiftclaws.model_gear)
            self.wep1 = self.Weapon(self, name='Weapon')

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep1.is_spec() or self.wep1.cur == self.wep1.ppist

        @ListSubUnit.count_gear
        def has_ccw(self):
            return self.wep1.is_melee()

    class AttackBike(Unit):
        model_name = 'Swiftclaw Attack Bike'
        model_points = 35

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Swiftclaws.AttackBike.Weapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy bolter', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        def __init__(self, parent):
            super(Swiftclaws.AttackBike, self).__init__(
                parent=parent, name=self.model_name, points=self.model_points,
                gear=Swiftclaws.model_gear + [Gear('Bolt pistol')])
            self.wep = self.Weapon(self)

    class OptUnits(OptionalSubUnit):
        def __init__(self, parent):
            super(Swiftclaws.OptUnits, self).__init__(parent=parent, name='')
            self.attackbike = SubUnit(self, Swiftclaws.AttackBike(parent=None))

    class WolfGuardBike(UpgradeUnit, Marine):

        @ListSubUnit.count_gear
        def is_guard(self):
            return self.up.any

        class WeaponRanged(Ranged, Melee, BoltPistol):
            pass

        def __init__(self, parent):
            super(Swiftclaws.WolfGuardBike, self).__init__(
                parent, upgraded_name='Wolf Guard Bike leader',
                upgrade_cost=10, upgraded_gear=Swiftclaws.model_gear)

        def make_upgraded_options(self):
            super(Swiftclaws.WolfGuardBike, self).make_upgraded_options()
            wep = self.WeaponRanged(self, name="Weapon")
            opt = MeltaBombs(self)
            self.upgraded_options += [wep, opt]

    def __init__(self, parent):
        super(Swiftclaws, self).__init__(parent)
        self.marines = UnitList(self, self.WolfGuardBike, 3, 10)
        self.attack = self.OptUnits(self)

    def check_rules(self):
        super(Swiftclaws, self).check_rules()

        guards = sum(u.is_guard() for u in self.marines.units)
        if guards > 1:
            self.error("Only one model can be upgraded to Wolf Guard Bike Leader")

        if sum(u.has_spec() for u in self.marines.units) > 1:
            self.error('Only one Swiftclaw Biker may upgrade his bolt pistol')
        if sum(u.has_ccw() for u in self.marines.units) > 1:
            self.error('Only one Swiftclaw Biker may upgrade his close combat weapon')

    def get_count(self):
        return self.marines.count + self.attack.count


class WolfPack(Unit):
    type_name = "Fenrisian Wolf Pack"
    type_id = "fenrisian_wolf_pack_v4"

    def __init__(self, parent):
        super(WolfPack, self).__init__(parent)
        self.wolves = Count(
            self, 'Fenrisian Wolf', 5, 15, 8,
            gear=UnitDescription('Fenrisian Wolf', points=8)
        )
        self.opt = self.Options(self)

    def check_rules(self):
        super(WolfPack, self).check_rules()
        Count.norm_counts(5 - self.opt.any, 15 - self.opt.any, [self.wolves])

    class Options(OptionsList):
        def __init__(self, parent):
            super(WolfPack.Options, self).__init__(parent=parent, name='Options')
            self.variant('Cyberwolf', 18,
                         gear=UnitDescription('Cyberwolf', points=18))

    def get_count(self):
        return self.wolves.cur + self.opt.count


class Skyclaws(Unit):
    type_name = "Skyclaws"
    type_id = "skyclaws__v4"
    model_gear = [Gear('Power armour'), Gear('Frag grenades'),
                  Gear('Krak grenades'), Gear('Jump pack')]

    class Marine(ListSubUnit):

        class WeaponRanged(Special, PlasmaPistol, BoltPistol):
            pass

        class WeaponMelee(PowerFist, PowerWeapon, Chainsword):
            pass

        def __init__(self, parent):
            super(Skyclaws.Marine, self).__init__(
                parent=parent, name='Skyclaw', points=15,
                gear=Skyclaws.model_gear)
            self.wep1 = self.WeaponMelee(self, name='Melee weapon')
            self.wep2 = self.WeaponRanged(self, name='Ranged weapon')

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep2.cur != self.wep2.boltpistol

        @ListSubUnit.count_gear
        def has_ccw(self):
            return self.wep1.cur != self.wep1.chainsword

    class SkyWolfGuard(UpgradeUnit, Marine):

        @ListSubUnit.count_gear
        def is_guard(self):
            return self.up.any

        class WeaponRanged(Ranged, Melee, BoltPistol):
            pass

        class WeaponMelee(Ranged, Melee, Chainsword):
            pass

        def __init__(self, parent):
            super(Skyclaws.SkyWolfGuard, self).__init__(
                parent, upgrade_cost=10, upgraded_name='Wolf Guard Sky leader',
                upgraded_gear=Skyclaws.model_gear)

        def make_upgraded_options(self):
            super(Skyclaws.SkyWolfGuard, self).make_upgraded_options()
            wep1 = self.WeaponMelee(self, name="Weapon")
            wep2 = self.WeaponRanged(self, name="")
            opt = MeltaBombs(self)
            self.upgraded_options += [wep1, wep2, opt]

    def __init__(self, parent):
        super(Skyclaws, self).__init__(parent)
        self.marines = UnitList(self, self.SkyWolfGuard, 5, 10)

    def check_rules(self):
        super(Skyclaws, self).check_rules()
        guards = sum(u.is_guard() for u in self.marines.units)
        if guards > 1:
            self.error("Only one model can be upgraded to Wolf Guard Sky Leader")
        if sum(u.has_spec() for u in self.marines.units) > 2:
            self.error('Only two Skyclaws may upgrade his bolt pistol')
        if sum(u.has_ccw() for u in self.marines.units) > 1:
            self.error('Only one Skyclaw may upgrade his close combat weapon')

    def get_count(self):
        return self.marines.count


class ThunderwolfCavalry(Unit):
    type_name = u'Thunderwolf Cavalry'
    type_id = 'thunderwolf_calvalry_v4'

    class Leader(Unit):
        type_name = u'Thunderwolf Cavalry Pack Leader'

        class MeleeWeapon(Melee, Chainsword):
            pass

        class RangedWeapon(Melee, PlasmaPistol, Boltgun, BoltPistol):
            pass

        def __init__(self, parent):
            super(ThunderwolfCavalry.Leader, self).__init__(
                parent, self.type_name, 40, gear=[
                    Gear('Power armour'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades'),
                    Gear('Thunderwolf mount')
                ]
            )
            self.MeleeWeapon(self, name='Weapon')
            self.RangedWeapon(self, name='')
            MeltaBombs(self)

    class Marine(Leader, ListSubUnit):
        type_name = u'Thunderwolf Cavalry'

    def __init__(self, parent):
        super(ThunderwolfCavalry, self).__init__(parent, self.type_name)
        SubUnit(self, self.Leader(parent=None))
        self.marines = UnitList(self, self.Marine, 2, 5)

    def get_count(self):
        return 1 + self.marines.count
