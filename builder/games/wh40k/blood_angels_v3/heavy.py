__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'


from builder.core2 import Count, OneOf, OptionsList, SubUnit, Gear,\
    UnitDescription
from transport import Transport
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle,\
    SpaceMarinesBaseSquadron
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit
from armory import Boltgun, BoltPistol, Heavy, CommonRanged, Melee,\
    VehicleEquipment
from builder.games.wh40k.unit import Unit


class StormravenGunship(SpaceMarinesBaseVehicle):
    type_name = u'Stormraven Gunship'
    type_id = 'stormravengunship_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Stormraven-Gunship')

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.twinlinkedmultimelta = self.variant('Twin-linked multi-melta', 0)
            self.typhoonmissilelauncher = self.variant('Typhoon missile launcher', 25)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon2, self).__init__(parent=parent, name='')
            self.twinlinkedassaultcannon = self.variant('Twin-linked assault cannon', 0)
            self.twinlinkedplasmacannon = self.variant('Twin-linked plasma cannon', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 0)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(StormravenGunship.Sponsons, self).__init__(parent=parent, name='Side sponsons')
            self.hurricanebolters = self.variant('Hurricane bolters', 30, gear=[Gear('Hurricane bolter', count=2)])

    class Options(OptionsList):
        def __init__(self, parent):
            super(StormravenGunship.Options, self).__init__(parent=parent, name='Options')
            self.searchlight = self.variant('Searchlight', 1)
            self.extraarmour = self.variant('Extra armour', 5)
            self.locatorbeacon = self.variant('Locator beacon', 10)

    def __init__(self, parent):
        super(StormravenGunship, self).__init__(
            parent=parent, points=200, gear=[Gear('Stormstrike missile', count=4), Gear('Ceramite plating')])
        self.Weapon1(self)
        self.Weapon2(self)
        self.Sponsons(self)
        self.Options(self)


class Stormravens(SpaceMarinesBaseSquadron):
    type_name = u'Stormraven Gunships'
    type_id = 'stormraven_wing_v3'
    unit_class = StormravenGunship
    unit_max = 4


class Devastators(IATransportedUnit):
    type_name = u'Devastator Squad'
    type_id = 'devastator_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Devastator-Squad')

    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 14
    model = UnitDescription('Space Marine', points=model_points, options=model_gear + [Gear('Bolt pistol')])

    class MarineCount(Count):
        normalizer = None

        @property
        def description(self):
            return [Devastators.model.clone().add(Gear('Boltgun')).set_count(self.cur - self.normalizer())]

    class DevWeapon(Heavy, Boltgun):
        def __init__(self, *args, **kwargs):
            super(Devastators.DevWeapon, self).__init__(*args, **kwargs)
            self.variant('Grav cannon and grav-amp', 35, gear=[
                Gear('Grav cannon'), Gear('Grav-amp')
            ])

    class Sergeant(Unit):
        type_name = u'Space Marine Sergeant'
        type_id = 'spacemarinesergeant_v3'

        class Weapon1(CommonRanged, Melee, BoltPistol):
            pass

        class Weapon2(Melee, Boltgun):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Devastators.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.up = self.variant('Upgrade to Veteran Sergeant', 10, gear=[])

        def __init__(self, parent):
            super(Devastators.Sergeant, self).__init__(
                parent=parent, name=self.type_name, points=Devastators.model_points,
                gear=Devastators.model_gear + [Gear('Signum')]
            )
            self.Weapon1(self, 'Weapon')
            self.Weapon2(self, '')
            self.opt = self.Options(self)

        def check_rules(self):
            self.name = 'Veteran Sergeant' if self.opt.up.value\
                        else self.type_name

    class Cherub(OptionsList):
        def __init__(self, parent):
            super(Devastators.Cherub, self).__init__(parent, 'Options', order=-1)
            self.variant('Armorium cherub', 5)

    def __init__(self, parent):
        super(Devastators, self).__init__(parent=parent)

        self.sergeant = SubUnit(self, self.Sergeant(None))
        self.spacemarine = self.MarineCount(self, 'Space Marine', min_limit=4, max_limit=9, points=self.model_points)
        wep = self.DevWeapon(self, 'Heavy weapon')
        self.hvy = [wep] + [self.DevWeapon(self, '') for i in range(0, 3)]

        self.spacemarine.normalizer = lambda: sum(c.cur != c.boltgun for c in self.hvy)
        self.transport = Transport(self)
        self.opt = self.Cherub(self)

    def get_count(self):
        return self.spacemarine.cur + 1

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count,
                               self.opt.description + self.sergeant.description + self.spacemarine.description)
        for wep in self.hvy:
            if wep.cur != wep.boltgun:
                desc.add_dup(self.model.clone().add(wep.description).add_points(wep.points))
        desc.add(self.transport.description)
        return desc


class BaalPredator(SpaceMarinesBaseVehicle):
    type_name = u'Baal Predator'
    type_id = 'baal_predator_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Baal-Predator')

    class Weapon(OneOf):
        def __init__(self, parent):
            super(BaalPredator.Weapon, self).__init__(parent=parent, name='Weapon')
            self.assault = self.variant('Twin-linked assault cannon', 0)
            self.flamestorm = self.variant('Flamestorm cannon', 0)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(BaalPredator.Sponsons, self).__init__(parent=parent, name='Side sponsons', limit=1)
            self.heavybolters = self.variant('Heavy Bolters', 20, gear=[Gear('Heavy bolter', count=2)])
            self.heavyflamers = self.variant('Heavy flamers', 20, gear=[Gear('Heavy flamer', count=2)])

    def __init__(self, parent):
        super(BaalPredator, self).__init__(parent=parent, points=115,
                                           gear=[
                                               Gear('Smoke launchers'),
                                               Gear('Searchlight'),
                                               Gear('Overcharged engines')
                                           ], tank=True)
        self.Weapon(self)
        self.Sponsons(self)
        VehicleEquipment(self)


class BaalPredators(SpaceMarinesBaseSquadron):
    type_name = u'Baal Predator'
    type_id = 'baal_predatorss_v3'
    unit_class = BaalPredator


class Predator(SpaceMarinesBaseVehicle):
    type_name = u'Predator'
    type_id = 'predator_v3'

    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Predator')

    engines = False

    class Options(VehicleEquipment):
        def __init__(self, parent):
            super(Predator.Options, self).__init__(parent)
            if not parent.engines:
                self.variant('Overcharged engines', 10)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Predator.Weapon, self).__init__(parent=parent, name='Weapon')
            self.autocannon = self.variant('Autocannon', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 25)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(Predator.Sponsons, self).__init__(parent=parent, name='Side sponsons', limit=1)
            self.heavybolters = self.variant('Heavy Bolters', 20,
                                             gear=[Gear('Heavy bolter', count=2)])
            self.lascannons = self.variant('Lascannons', 40,
                                           gear=[Gear('Lascannon', count=2)])

    def __init__(self, parent):
        super(Predator, self).__init__(parent=parent, points=75,
                                       gear=[Gear('Smoke launchers'),
                                             Gear('Searchlight')] + ([Gear('Overcharged engines')] if self.engines else []),
                                       tank=True)
        self.Weapon(self)
        self.Sponsons(self)
        self.Options(self)


class Predators(SpaceMarinesBaseSquadron):
    type_name = u'Predator'
    type_id = 'predatorss_v3'
    unit_class = Predator


class Vindicator(SpaceMarinesBaseVehicle):
    type_name = u'Vindicator'
    type_id = 'vindicator_v3'

    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Vindicator')

    class Options(VehicleEquipment):
        def __init__(self, parent):
            super(Vindicator.Options, self).__init__(parent=parent)
            self.siegeshield = self.variant('Siege shield', 10)
            self.engine = self.variant('Overcharged engines', 10)

    def __init__(self, parent):
        super(Vindicator, self).__init__(
            parent=parent, points=145, tank=True,
            gear=[Gear('Demolisher cannon'), Gear('Smoke launchers'), Gear('Searchlight')]
        )
        self.Options(self)


class Vindicators(SpaceMarinesBaseSquadron):
    type_name = u'Vindicator'
    type_id = 'vindicators_v3'
    unit_class = Vindicator


class Whirlwind(SpaceMarinesBaseVehicle):
    type_name = u'Whirlwind'
    type_id = 'whirlwind_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Whirlwind')

    class Options(VehicleEquipment):
        def __init__(self, parent):
            super(Whirlwind.Options, self).__init__(parent=parent)
            self.engine = self.variant('Overcharged engines', 10)

    def __init__(self, parent):
        super(Whirlwind, self).__init__(parent=parent, points=65, gear=[
            Gear('Whirlwind multiple missile launcher'),
            Gear('Smoke launchers'), Gear('Searchlight')], tank=True)
        self.Options(self)


class Whirlwinds(SpaceMarinesBaseSquadron):
    type_name = u'Whirlwind'
    type_id = 'whirlwindss_v3'
    unit_class = Whirlwind


from builder.games.wh40k.imperial_armour.dataslates.space_marines import BaseDeimos


class DeimosVindicator(BaseDeimos, SpaceMarinesBaseVehicle):
    def get_options(self):
        class Options(VehicleEquipment):
            def __init__(self, parent):
                super(Options, self).__init__(parent=parent)
                self.siegeshield = self.variant('Siege shield', 10)
        return Options
