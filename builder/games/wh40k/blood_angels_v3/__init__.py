__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'
__summary__ = ['Codex Blood Angels 7th Edition',
               'Shield of Baal: Exterminatus',
               'Shield of Baal: Deathstorm',
               'Angel\'s Blade']


from builder.core2.node import Node
from builder.core2.roster import get_unit_names
from builder.core2 import SubUnit
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, PrimaryDetachment, Wh40kKillTeam,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, Detachment, Formation, LordsOfWarSection,\
    FlyerSection, CompositeFormation
from builder.games.wh40k.fortifications import Fort
from builder.games.wh40k.escalation.space_marines import LordsOfWar as MarinesLordsOfWar
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.imperial_armour import volume2, volume9_10
from builder.games.wh40k.imperial_armour.dataslates.space_marines import Deredeo, Xiphon,\
    Leviathan
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusSection
from builder.games.wh40k.imperial_armour.aeronautika.space_marines import HyperiosBattery
from builder.games.wh40k.dataslates.command_tanks import SpaceMarineCommandTanks
from hq import Captain, Tycho, DCTycho, Sanguinor, Librarian, Mephiston,\
    Priest, Corbulo, Chaplain, Astorath, Techmarine,\
    LibrarianDreadnought, Karlaen, TerminatorCaptain,\
    DeathCompanyChaplain
from elites import CommandSquad, DeathCompany, Lemartes, SanguinaryGuards,\
    Dreadnought, DeathCompanyDreadnought, FuriosoDreadnought, TerminatorSquad,\
    TerminatorAssaultSquad, VanguardVeteranSquad, SternguardVeteranSquad,\
    SquadAlphaeus
from troops import TacticalSquad, ScoutSquad, RaphenDeathCompany, Cassor
from fast import OldAssaultSquad, AssaultSquad, SpaceMarineBikers, ScoutBikers,\
    AttackBikeSquad, LandSpeederSquadron
from heavy import StormravenGunship, Devastators, BaalPredator,\
    Predator, Vindicator, Whirlwind, Stormravens, BaalPredators,\
    Predators, Vindicators, Whirlwinds, DeimosVindicator
from transport import DropPod, Rhino, Razorback, LandRaider, LandRaiderCrusader,\
    LandRaiderRedeemer
from lords import Dante, Seth


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        UnitType(self, Astorath)
        UnitType(self, Sanguinor)
        UnitType(self, Mephiston)
        UnitType(self, Tycho)
        UnitType(self, DCTycho)
        UnitType(self, Karlaen)
        UnitType(self, Librarian)
        UnitType(self, LibrarianDreadnought)
        UnitType(self, Captain)
        self.reclusiarch = UnitType(self, Chaplain)
        UnitType(self, TerminatorCaptain)
        UnitType(self, DeathCompanyChaplain)
        UnitType(self, Corbulo)
        UnitType(self, Priest)
        UnitType(self, Techmarine)
        UnitType(self, SpaceMarineCommandTanks)


class HQ(volume9_10.BloodAngelsBadabHq, volume2.SpaceMarineHQ, BaseHQ):
    pass


class HQwithCypher(HQ):
    def __init__(self, parent):
        super(HQwithCypher, self).__init__(parent)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.sanguinary_guards = UnitType(self, SanguinaryGuards)
        UnitType(self, TerminatorSquad)
        UnitType(self, TerminatorAssaultSquad)
        UnitType(self, SternguardVeteranSquad)
        UnitType(self, DeathCompany)
        UnitType(self, Lemartes)
        UnitType(self, VanguardVeteranSquad)
        UnitType(self, Dreadnought)
        UnitType(self, DeathCompanyDreadnought)
        UnitType(self, FuriosoDreadnought)
        UnitType(self, CommandSquad)
        UnitType(self, SquadAlphaeus)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.tactical_squad = UnitType(self, TacticalSquad)
        self.scout_squad = UnitType(self, ScoutSquad)
        UnitType(self, RaphenDeathCompany)
        UnitType(self, Cassor)


class Elites(volume2.BloodAngelsElites, BaseElites):
    pass


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, DropPod)
        UnitType(self, Rhino)
        UnitType(self, Razorback)
        UnitType(self, LandSpeederSquadron)
        UnitType(self, OldAssaultSquad, visible=False)
        UnitType(self, AssaultSquad)
        UnitType(self, SpaceMarineBikers)
        UnitType(self, AttackBikeSquad)
        UnitType(self, ScoutBikers)


class FastAttack(volume2.SpaceMarineFast, BaseFastAttack):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        UnitType(self, HyperiosBattery)
        UnitType(self, Xiphon)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, LandRaider)
        UnitType(self, LandRaiderRedeemer)
        UnitType(self, LandRaiderCrusader)
        raven = UnitType(self, StormravenGunship)
        raven.visible = False
        UnitType(self, Stormravens)
        UnitType(self, Devastators)
        UnitType(self, BaalPredator, visible=False)
        UnitType(self, BaalPredators)
        UnitType(self, Predator, visible=False)
        UnitType(self, Predators)
        UnitType(self, Whirlwind, visible=False)
        UnitType(self, Whirlwinds)
        UnitType(self, Vindicator, visible=False)
        UnitType(self, Vindicators)


class HeavySupport(volume2.BloodAngelsHeavySupport, BaseHeavySupport):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        self.relics.append(UnitType(self, Deredeo))
        UnitType(self, DeimosVindicator)
        self.relics.append(UnitType(self, Leviathan))


class BaseLords(LordsOfWarSection):
    def __init__(self, parent):
        super(BaseLords, self).__init__(parent)
        self.dante = UnitType(self, Dante)
        self.seth = UnitType(self, Seth)

    def chapter_master_count(self):
        return self.dante.count + self.seth.count


class LordsOfWar(CerastusSection, volume2.SpaceMarineLordsOfWar,
                 Titans, MarinesLordsOfWar, BaseLords):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman)


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [Stormravens])


class BloodAngelsCommon(Node):
    def check_rules(self):
        super(BloodAngelsCommon, self).check_rules()
        if 'The Crown Angelic' in self._build_unique_map(get_unit_names).keys():
            if not (type(self.parent.parent) is PrimaryDetachment):
                self.error('Only Warlord may take The Crown Angelic')

    @property
    def supplement(self):
        return None


class BaalStrikeForce(volume2.SpaceMarinesRelicRoster,
                      BloodAngelsCommon):
    army_id = 'baal_strike_force_v3'
    army_name = 'Baal Strike Force'

    def __init__(self):

        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.elites.min, self.elites.max = (1, 4)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        self.lords = LordsOfWar(parent=self)
        super(BaalStrikeForce, self).__init__(
            sections=[self.hq, self.elites, self.troops, self.fast,
                      self.heavy, Fort(parent=self), self.lords]
        )

    def has_keeper(self):
        return self.hq.reclusiarch.count > 0

    def check_rules(self):
        super(BaalStrikeForce, self).check_rules()
        if self.hq.phoros.count:
            if self.hq.chapter_master_count() + self.lords.chapter_master_count() > 1:
                self.error("Malakim Phoros must be the only Chapter Master in detachment")
            if not type(self.parent.parent) is PrimaryDetachment:
                self.error("Malakim Phoros must be your army\'s Warlord")


class FleshTearersStrikeForce(volume2.SpaceMarinesRelicRoster,
                              BloodAngelsCommon):
    army_id = 'tearer_strike_force_v3'
    army_name = 'Flesh Tearers Strike Force'

    @property
    def supplement(self):
        return "tearer"

    def __init__(self):

        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.troops.min, self.troops.max = (1, 4)
        self.fast = FastAttack(parent=self)
        self.fast.min, self.fast.max = (1, 6)
        self.heavy = HeavySupport(parent=self)
        super(FleshTearersStrikeForce, self).__init__(
            sections=[self.hq, self.elites, self.troops, self.fast,
                      self.heavy, Fort(parent=self), LordsOfWar(parent=self)]
        )

    def has_keeper(self):
        return self.hq.reclusiarch.count > 0


class ArchangelStrikeForce(Wh40kBase, volume2.SpaceMarinesLegaciesRoster,
                           BloodAngelsCommon):
    army_id = 'blood_angels_v3_strike_archangel'
    army_name = 'Archangel Strike Force'

    class HQ(HQSection):
        def __init__(self, parent):
            super(ArchangelStrikeForce.HQ, self).__init__(parent)
            UnitType(self, Captain)
            UnitType(self, Karlaen)
            UnitType(self, Chaplain)
            UnitType(self, Librarian)

    class Elites(ElitesSection):
        def __init__(self, parent):
            super(ArchangelStrikeForce.Elites, self).__init__(parent)
            self.veterans = [
                UnitType(self, SternguardVeteranSquad),
                UnitType(self, TerminatorSquad),
                UnitType(self, TerminatorAssaultSquad),
                UnitType(self, VanguardVeteranSquad)
            ]
            self.dred = UnitType(self, FuriosoDreadnought)
            self.min, self.max = (2, 16)

    def __init__(self):
        self.hq = self.HQ(parent=self)
        self.elites = self.Elites(parent=self)
        super(ArchangelStrikeForce, self).__init__(
            sections=[self.hq, self.elites])

    @property
    def supplement(self):
        return 'archangel'

    def check_rules(self):
        super(ArchangelStrikeForce, self).check_rules()
        for u in self.hq.units:
            if not u.armour.is_tda():
                self.error("{} must be equipped with Terminator Armour".format(u.name))
        if self.elites.dred.count > 6:
            self.error("You may not include more then 6 of Furioso Dreadnought")
        if sum(u.count for u in self.elites.veterans) > 10:
            self.error("You may not include more then 10 of Veteran Units")


class ArchangelInterventionForce(volume2.SpaceMarinesLegaciesFormation,
                                 BloodAngelsCommon):
    army_id = 'blood_angels_v3_archangel_intervention'
    army_name = 'Archangel Orbital Intervention Force'

    def __init__(self):
        super(ArchangelInterventionForce, self).__init__()
        term = UnitType(self, TerminatorSquad)
        assault = UnitType(self, TerminatorAssaultSquad)
        self.add_type_restriction([term, assault], 3, 3)


class ArchangelInterventionForceBaal(ArchangelInterventionForce):
    army_id = 'blood_angels_v3_archangel_intervention_baal'
    army_name = 'Archangel Orbital Intervention Force (Shield of Baal)'

    @property
    def supplement(self):
        return 'archangel'

    def __init__(self):
        super(ArchangelInterventionForceBaal, self).__init__()
        self.name = 'Archangel Orbital Intervention Force'


class ArchangelSanguineWing(Formation, BloodAngelsCommon):
    army_id = 'blood_angels_v3_archangel_wing'
    army_name = 'Archangels Sanguine Wing'
    ia_enabled = True

    class VanguardVeteranSquad(VanguardVeteranSquad):
        free_power = True

        def check_rules(self):
            super(ArchangelSanguineWing.VanguardVeteranSquad, self).check_rules()
            if self.marines.count < 9:
                self.error("Must include 10 models")
            if not self.opt.any:
                self.error("Must be equipped with Jump Packs")

    class SternguardVeteranSquad(SternguardVeteranSquad):
        free_ranged = True

        def check_rules(self):
            super(ArchangelSanguineWing.SternguardVeteranSquad, self).check_rules()
            if self.marines.count < 9:
                self.error("Must include 10 models")

    @property
    def supplement(self):
        return 'archangel'

    def __init__(self):
        super(ArchangelSanguineWing, self).__init__()
        UnitType(self, self.VanguardVeteranSquad, min_limit=2, max_limit=2)
        UnitType(self, self.SternguardVeteranSquad, min_limit=1, max_limit=1)
        UnitType(self, StormravenGunship, min_limit=1, max_limit=1)


class ArchangelDemiCompanyBlade(volume2.SpaceMarinesLegaciesFormation,
                                BloodAngelsCommon):
    army_id = 'blood_angels_v3_archangel_demi_blade'
    army_name = 'Archangels Demi-Company'

    def __init__(self):
        super(ArchangelDemiCompanyBlade, self).__init__()
        UnitType(self, TerminatorCaptain, min_limit=1, max_limit=1)
        UnitType(self, FuriosoDreadnought, min_limit=2, max_limit=2)
        veterans = [
            UnitType(self, SternguardVeteranSquad),
            UnitType(self, TerminatorSquad),
            UnitType(self, TerminatorAssaultSquad),
            UnitType(self, VanguardVeteranSquad)
        ]
        self.add_type_restriction(veterans, 5, 5)
        self.name = 'Archangels Demi-Company'


class ArchangelDemiCompany(volume2.SpaceMarinesLegaciesFormation,
                           BloodAngelsCommon):
    army_id = 'blood_angels_v3_archangel_demi'
    army_name = 'Archangels Demi-Company (Shield of Baal)'

    @property
    def supplement(self):
        return 'archangel'

    def __init__(self):
        super(ArchangelDemiCompany, self).__init__()
        cap = UnitType(self, Captain)
        karl = UnitType(self, Karlaen)
        chap = UnitType(self, Chaplain)
        self.hq = [cap, chap]
        self.add_type_restriction(self.hq + [karl], 1, 1)
        UnitType(self, FuriosoDreadnought, min_limit=2, max_limit=2)
        veterans = [
            UnitType(self, SternguardVeteranSquad),
            UnitType(self, TerminatorSquad),
            UnitType(self, TerminatorAssaultSquad),
            UnitType(self, VanguardVeteranSquad)
        ]
        self.add_type_restriction(veterans, 5, 5)
        self.name = 'Archangels Demi-Company'

    def check_rules(self):
        super(ArchangelDemiCompany, self).check_rules()
        for t in self.hq:
            for u in t.units:
                if not u.armour.is_tda():
                    self.error("{} must be equipped with Terminator Armour".format(u.name))


class Archangels(volume2.SpaceMarinesLegaciesFormation,
                 BloodAngelsCommon):
    army_id = 'blood_angels_v3_archangels'
    army_name = 'The Archangels'

    @property
    def supplement(self):
        return 'archangel'

    def __init__(self):
        super(Archangels, self).__init__()
        cap = UnitType(self, Captain)
        karl = UnitType(self, Karlaen)
        self.add_type_restriction([cap, karl], 1, 1)
        UnitType(self, Chaplain, min_limit=1, max_limit=1)
        UnitType(self, FuriosoDreadnought, min_limit=4, max_limit=4)
        veterans = [
            UnitType(self, SternguardVeteranSquad),
            UnitType(self, TerminatorSquad),
            UnitType(self, TerminatorAssaultSquad),
            UnitType(self, VanguardVeteranSquad)
        ]
        self.add_type_restriction(veterans, 10, 10)


class BloodedDemiCompany(volume2.SpaceMarinesLegaciesFormation,
                         BloodAngelsCommon):
    army_id = 'blood_angels_v3_blooded'
    army_name = 'Blooded Demi-Company'

    @property
    def supplement(self):
        return 'archangel'

    def __init__(self):
        super(BloodedDemiCompany, self).__init__()
        self.cap = UnitType(self, Captain)
        self.chap = UnitType(self, Chaplain)
        self.add_type_restriction([self.cap, self.chap], 1, 1)
        self.dread = UnitType(self, FuriosoDreadnought)
        self.comsquad = UnitType(self, CommandSquad)
        UnitType(self, TacticalSquad, min_limit=3, max_limit=3)
        UnitType(self, AssaultSquad, min_limit=1, max_limit=1)
        UnitType(self, Devastators, min_limit=1, max_limit=1)
        UnitType(self, Dreadnought, min_limit=1, max_limit=1)

    def check_rules(self):
        if self.cap.count > 0:
            self.dread.min_limit, self.dread.max_limit = (0, 0)
            self.comsquad.min_limit, self.comsquad.max_limit = (1, 1)
        else:
            self.dread.min_limit, self.dread.max_limit = (1, 1)
            self.comsquad.min_limit, self.comsquad.max_limit = (0, 0)
        super(BloodedDemiCompany, self).check_rules()


class Mortalis(volume2.SpaceMarinesLegaciesFormation, BloodAngelsCommon):
    army_id = 'blood_angels_v3_mortalis'
    army_name = 'Strike Force Mortalis'

    @property
    def supplement(self):
        return 'archangel'

    def __init__(self):
        super(Mortalis, self).__init__()
        UnitType(self, Chaplain, min_limit=1, max_limit=1)
        UnitType(self, DeathCompany, min_limit=3, max_limit=3)
        UnitType(self, DeathCompanyDreadnought, min_limit=2, max_limit=2)
        UnitType(self, StormravenGunship, min_limit=1, max_limit=1)


class AvengingHost(volume2.SpaceMarinesLegaciesFormation, CompositeFormation,
                   BloodAngelsCommon):
    army_id = 'blood_angels_v3_avenging_host'
    army_name = 'Dante\'s Avenging Host'

    @property
    def supplement(self):
        return 'archangel'

    def __init__(self):
        super(AvengingHost, self).__init__()
        UnitType(self, Dante, min_limit=1, max_limit=1)
        UnitType(self, Mephiston, min_limit=1, max_limit=1)
        UnitType(self, Priest, min_limit=1, max_limit=1)
        UnitType(self, SanguinaryGuards, min_limit=1, max_limit=1)
        self.blooded = Detachment.build_detach(self, BloodedDemiCompany, min_limit=1, max_limit=1)
        UnitType(self, StormravenGunship, min_limit=3, max_limit=3)

    def check_rules(self):
        for u in self.blooded.units:
            u.sub_roster.roster.chap.min_limit = 1
            u.sub_roster.roster.chap.max_limit = 1
        super(AvengingHost, self).check_rules()


class VanguardStrikeForce(volume2.SpaceMarinesLegaciesFormation,
                          BloodAngelsCommon):
    army_id = 'tearer_3_vanguard_strike_force'
    army_name = 'Flesh Tearers Vanguard Strike Force'

    @property
    def supplement(self):
        return 'tearer'

    def __init__(self):
        super(VanguardStrikeForce, self).__init__()
        UnitType(self, TacticalSquad, min_limit=3, max_limit=3)
        UnitType(self, AssaultSquad, min_limit=1, max_limit=1)
        UnitType(self, VanguardVeteranSquad, min_limit=1, max_limit=1)
        UnitType(self, FuriosoDreadnought, min_limit=1, max_limit=1)


class ReliefForce(volume2.SpaceMarinesLegaciesFormation, BloodAngelsCommon):
    army_id = 'tearer_3_relief'
    army_name = 'Lysios Relief Force'

    @property
    def supplement(self):
        return 'tearer'

    def __init__(self):
        super(ReliefForce, self).__init__()
        UnitType(self, Seth, min_limit=1, max_limit=1)
        self.tacs = UnitType(self, TacticalSquad, min_limit=1, max_limit=1)
        self.vets = UnitType(self, VanguardVeteranSquad, min_limit=1, max_limit=1)
        UnitType(self, BaalPredator, min_limit=1, max_limit=1)
        UnitType(self, Predator, min_limit=1, max_limit=1)
        UnitType(self, StormravenGunship, min_limit=1, max_limit=1)

    def check_rules(self):
        super(ReliefForce, self).check_rules()
        for unit in self.tacs.units:
            if unit.transport.is_droppod():
                self.error("Tactical squads must be in transports that are not Drop Pods")
        for unit in self.vets.units:
            if unit.transport.used and unit.transport.is_droppod():
                self.error("Veteran squads must be in transports that are not Drop Pods")


class CatherdumDefenders(volume2.SpaceMarinesLegaciesFormation,
                         BloodAngelsCommon):
    army_id = 'tearer_3_defenders'
    army_name = 'The Defenders of the Cathedrum'

    @property
    def supplement(self):
        return 'tearer'

    def __init__(self):
        super(CatherdumDefenders, self).__init__()
        UnitType(self, TerminatorAssaultSquad, min_limit=1, max_limit=1)
        UnitType(self, DeathCompany, min_limit=1, max_limit=1)
        UnitType(self, TacticalSquad, min_limit=2, max_limit=2)
        UnitType(self, AssaultSquad, min_limit=1, max_limit=1)
        UnitType(self, FuriosoDreadnought, min_limit=1, max_limit=1)


class RazorwindForce(volume2.SpaceMarinesLegaciesFormation,
                     CompositeFormation,
                     BloodAngelsCommon):
    army_id = 'tearer_3_razorwind'
    army_name = 'Strike Force Razorwind'

    @property
    def supplement(self):
        return 'tearer'

    def __init__(self):
        super(RazorwindForce, self).__init__()
        Detachment.build_detach(self, CatherdumDefenders, min_limit=1, max_limit=1)
        Detachment.build_detach(self, ReliefForce, min_limit=1, max_limit=1)


class WrathIntervention(Formation):
    army_id = 'blood_angels_v3_wrathintervention'
    army_name = 'Angel\'s Wrath Intervention Force'

    def __init__(self):
        super(WrathIntervention, self).__init__()
        self.vets = UnitType(self, VanguardVeteranSquad, min_limit=1, max_limit=1)
        self.ass = UnitType(self, AssaultSquad, min_limit=2, max_limit=2)

    def check_rules(self):
        super(WrathIntervention, self).check_rules()
        for unit in self.vets.units:
            if not unit.opt.any:
                self.error('All models must be equipped with Jump Packs')
        for unit in self.ass.units:
            if not (unit.pack.visible and unit.pack.any):
                self.error('All models must be equipped with Jump Packs')


class FurySpearhead(volume2.SpaceMarinesLegaciesFormation):
    army_id = 'blood_angels_v3_fury_spearhead'
    army_name = 'Angel\'s Fury Spearhead Force'

    class SpearheadTactical(TacticalSquad):
        def __init__(self, parent):
            super(FurySpearhead.SpearheadTactical, self).__init__(parent)
            self.sergeant.unit.opt.teleporthomer.value = True
            self.sergeant.unit.opt.teleporthomer.points = 0
            self.sergeant.unit.opt.teleporthomer.visible = False
            self.marines.min = 9
            self.transport.used = self.transport.visible = False

    def __init__(self):
        super(FurySpearhead, self).__init__()
        UnitType(self, self.SpearheadTactical, min_limit=3, max_limit=3)
        UnitType(self, StormravenGunship, min_limit=3, max_limit=3)


class Deathstorm(Formation):
    army_id = 'blood_angels_v3_deathstorm'
    army_name = 'Strike Force Deathstorm'

    def __init__(self):
        super(Deathstorm, self).__init__()
        UnitType(self, Karlaen, min_limit=1, max_limit=1)
        UnitType(self, SquadAlphaeus, min_limit=1, max_limit=1)
        UnitType(self, RaphenDeathCompany, min_limit=1, max_limit=1)
        UnitType(self, Cassor, min_limit=1, max_limit=1)


class BattleDemiCompany(volume2.SpaceMarinesLegaciesFormation,
                        BloodAngelsCommon):
    army_id = 'blood_angels_v3_battle_demi'
    army_name = 'Battle Demi-Company'

    def __init__(self):
        super(BattleDemiCompany, self).__init__()
        self.cap = UnitType(self, Captain)
        self.chap = UnitType(self, Chaplain)
        self.add_type_restriction([self.cap, self.chap], 1, 1)
        self.command = UnitType(self, CommandSquad, max_limit=1)
        UnitType(self, TacticalSquad, min_limit=3, max_limit=3)
        fast = [UnitType(self, AssaultSquad),
                UnitType(self, SpaceMarineBikers),
                UnitType(self, AttackBikeSquad),
                UnitType(self, LandSpeederSquadron)]
        self.add_type_restriction(fast, 1, 1)
        UnitType(self, Devastators, min_limit=1, max_limit=1)
        UnitType(self, Dreadnought, min_limit=1, max_limit=1)
        self.fury = UnitType(self, FuriosoDreadnought, max_limit=1)

    def check_rules(self):
        super(BattleDemiCompany, self).check_rules()
        if not(len(self.cap.units) + len(self.command.units) in [0, 2]):
            self.error("Only if Captain leads the Demi-Company, it must include 1 Command Squad")
        self.command.active = len(self.cap.units)
        if not(len(self.chap.units) + len(self.fury.units) in [0, 2]):
            self.error("Only if Chaplain leads the Demi-Company, it must include 1 Furioso Dreadnought")
        self.fury.active = len(self.chap.units)


class AmbushForce(Formation):
    army_name = "10th Company Ambush Force"
    army_id = 'blood_angels_v3_ambush'

    def __init__(self):
        super(AmbushForce, self).__init__()
        scout = UnitType(self, ScoutSquad)
        self.bike = UnitType(self, ScoutBikers)
        self.add_type_restriction([scout, self.bike], 3, 5)

    def check_rules(self):
        super(AmbushForce, self).check_rules()
        for bu in self.bike.units:
            if not bu.mines.any:
                self.error("Scout Bike Squads in this formation must be equipped with cluster mines")
                break


class DeathCompanyStrikeForce(volume2.SpaceMarinesLegaciesFormation,
                              BloodAngelsCommon):
    army_name = 'Death Company Strike Force'
    army_id = 'blood_angels_v3_dc_sf'

    @property
    def supplement(self):
        return 'death_company'

    class ForceDeathCompany(DeathCompany):
        from builder.core2 import SubUnit, OptionalSubUnit
        from transport import Transport

        class DCTransport(Transport):
            def __init__(self, parent):
                super(DeathCompanyStrikeForce.ForceDeathCompany.DCTransport, self).__init__(parent)
                self.raven = SubUnit(self, StormravenGunship(parent=None))
                self.models.append(self.raven)

        def get_transport(self):
            return self.DCTransport(self)

        def check_rules(self):
            super(DeathCompany, self).check_rules()
            show_vehicles = not self.opt.any
            self.transport.options.set_visible([m for m in self.transport.models
                                                if m != self.transport.raven], show_vehicles)

    def __init__(self):
        super(DeathCompanyStrikeForce, self).__init__()
        UnitType(self, DeathCompanyChaplain, min_limit=1, max_limit=1)
        UnitType(self, self.ForceDeathCompany, min_limit=3, max_limit=3)
        UnitType(self, DeathCompanyDreadnought, min_limit=1, max_limit=3)


class GoldenHost(Formation, BloodAngelsCommon):
    army_name = 'Golden Host'
    army_id = 'blood_angels_v3_golden'

    def __init__(self):
        super(GoldenHost, self).__init__()
        lords = [
            UnitType(self, Dante),
            UnitType(self, Sanguinor)
        ]
        self.add_type_restriction(lords, 1, 1)
        UnitType(self, SanguinaryGuards, min_limit=2, max_limit=5)


class Ancients(volume2.SpaceMarinesLegaciesFormation,
               BloodAngelsCommon):
    army_name = 'Chapter Ancients'
    army_id = 'blood_angels_v3_ancents'

    def __init__(self):
        super(Ancients, self).__init__()
        dreads = [
            UnitType(self, LibrarianDreadnought),
            UnitType(self, Dreadnought),
            UnitType(self, FuriosoDreadnought)
        ]
        self.add_type_restriction(dreads, 3, 5)


class StormravenSquadron(volume2.SpaceMarinesLegaciesFormation):
    army_name = 'Stormraven Squadron'
    army_id = 'blood_angels_v3_ravens'

    class Wing(Stormravens):
        unit_min = 2

    def __init__(self):
        super(StormravenSquadron, self).__init__()
        UnitType(self, self.Wing, min_limit=1, max_limit=1)


class LuciferTaskForce(volume2.SpaceMarinesLegaciesFormation,
                       BloodAngelsCommon):
    army_id = 'blood_angels_v3_lucifer'
    army_name = 'Lucifer Armoured Task Force'

    class FastLR(LandRaider):
        engines = True

    class FastLRC(LandRaiderCrusader):
        angines = True

    class FastLRR(LandRaiderRedeemer):
        engines = True

    class FastPredators(Predators):
        class FastPredator(Predator):
            engines = True

        unit_class = FastPredator

    def __init__(self):
        super(LuciferTaskForce, self).__init__()
        UnitType(self, Techmarine, min_limit=1, max_limit=1)
        preds = [
            UnitType(self, self.FastPredators),
            UnitType(self, BaalPredators)
        ]
        self.add_type_restriction(preds, 3, 5)
        lrs = [
            UnitType(self, self.FastLR),
            UnitType(self, self.FastLRC),
            UnitType(self, self.FastLRR)
        ]
        self.add_type_restriction(lrs, 1, 3)


class AngelBlade(Wh40k7ed, volume2.SpaceMarinesLegaciesRoster,
                 BloodAngelsCommon):
    army_name = "Angel's Blade Strike Force"
    army_id = 'blood_angels_v3_blade'

    class Lords(volume2.SpaceMarinesLegaciesFormation,
                BloodAngelsCommon):
        army_name = 'Leaders of the Angelic Host'
        army_id = 'blood_angels_v3_blade_lords'

        def __init__(self):
            super(AngelBlade.Lords, self).__init__()
            lords = [
                UnitType(self, TerminatorCaptain),
                UnitType(self, Captain),
                UnitType(self, Tycho),
                UnitType(self, Librarian),
                UnitType(self, Mephiston),
                UnitType(self, Priest),
                UnitType(self, Corbulo)
            ]
            self.add_type_restriction(lords, 1, 1)
            UnitType(self, CommandSquad, max_limit=1)
            UnitType(self, StormravenGunship, max_limit=1)

    class Command(Detachment):
        def __init__(self, parent):
            super(AngelBlade.Command, self).__init__(
                parent, 'command', 'Command',
                [GoldenHost, AngelBlade.Lords, Ancients], 0, 5)

    class Core(Detachment):
        def __init__(self, parent):
            super(AngelBlade.Core, self).__init__(
                parent, 'core', 'Core',
                [BattleDemiCompany, ArchangelDemiCompanyBlade], 1, None)

    class RapidAssault(volume2.SpaceMarinesLegaciesFormation,
                       BloodAngelsCommon):
        army_id = 'blood_angels_b3_blade_rapid'
        army_name = 'Rapid Assault Force'

        def __init__(self):
            super(AngelBlade.RapidAssault, self).__init__()
            units = [
                UnitType(self, AssaultSquad),
                UnitType(self, SpaceMarineBikers),
                UnitType(self, AttackBikeSquad),
                UnitType(self, LandSpeederSquadron)
            ]
            self.add_type_restriction(units, 1, 3)

    class FireSupport(volume2.SpaceMarinesLegaciesFormation,
                      BloodAngelsCommon):
        army_id = 'blood_angels_v3_blade_fire'
        army_name = 'Fire Support Force'

        def __init__(self):
            super(AngelBlade.FireSupport, self).__init__()
            units = [
                UnitType(self, Devastators),
                UnitType(self, Vindicators),
                UnitType(self, Whirlwinds)
            ]
            self.add_type_restriction(units, 1, 3)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(AngelBlade.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [ArchangelInterventionForce,
                 AmbushForce,
                 DeathCompanyStrikeForce,
                 LuciferTaskForce,
                 StormravenSquadron,
                 AngelBlade.RapidAssault,
                 AngelBlade.FireSupport], 1, None)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class LostBrotherhood(Wh40k7ed, volume2.SpaceMarinesLegaciesRoster,
                      BloodAngelsCommon):
    army_name = "Lost Brotherhood Strike Force"
    army_id = 'blood_angels_v3_lost'

    class Lords(volume2.SpaceMarinesLegaciesFormation,
                BloodAngelsCommon):
        army_name = 'Death Company Command'
        army_id = 'blood_angels_v3_lost_lords'

        @property
        def supplement(self):
            return 'death_company'

        def __init__(self):
            super(LostBrotherhood.Lords, self).__init__()
            lords = [
                UnitType(self, Astorath),
                UnitType(self, Lemartes),
                UnitType(self, DeathCompanyChaplain)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(LostBrotherhood.Command, self).__init__(
                parent, 'command', 'Command',
                [LostBrotherhood.Lords], 1, 1)

    class Core(Detachment):
        def __init__(self, parent):
            super(LostBrotherhood.Core, self).__init__(
                parent, 'core', 'Core',
                [DeathCompanyStrikeForce], 1, None)

    class TenthSupport(volume2.SpaceMarinesLegaciesFormation,
                       BloodAngelsCommon):
        army_id = 'blood_angels_v3_lost_scouts'
        army_name = '10th Company Support'

        def __init__(self):
            super(LostBrotherhood.TenthSupport, self).__init__()
            units = [
                UnitType(self, ScoutSquad),
                UnitType(self, ScoutBikers)
            ]
            self.add_type_restriction(units, 3, 5)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(LostBrotherhood.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [ArchangelInterventionForce,
                 LostBrotherhood.TenthSupport,
                 LuciferTaskForce,
                 StormravenSquadron,
                 AngelBlade.RapidAssault], 0, 5)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class BattleCompany(volume2.SpaceMarinesLegaciesFormation,
                    BloodAngelsCommon):
    army_id = 'blood_angels_v3_battle'
    army_name = 'Blood Angels Battle Company'

    def __init__(self):
        super(BattleCompany, self).__init__()
        UnitType(self, Captain, min_limit=1, max_limit=1)
        UnitType(self, Chaplain, min_limit=1, max_limit=1)
        UnitType(self, CommandSquad, min_limit=1, max_limit=1)
        UnitType(self, TacticalSquad, min_limit=6, max_limit=6)
        UnitType(self, AssaultSquad, min_limit=2, max_limit=2)
        UnitType(self, Devastators, min_limit=2, max_limit=2)
        UnitType(self, DeathCompany, min_limit=1, max_limit=1)
        UnitType(self, Dreadnought, min_limit=1, max_limit=1)
        UnitType(self, FuriosoDreadnought, min_limit=1, max_limit=1)


class CarmineHost(volume2.SpaceMarinesLegaciesFormation, BloodAngelsCommon):
    army_id = 'blood_angels_v3_carmine_host'
    army_name = 'Blood Angels Carmine Host'

    def __init__(self):
        super(CarmineHost, self).__init__()
        self.cap = UnitType(self, Captain, min_limit=1, max_limit=1)
        UnitType(self, TacticalSquad, min_limit=1, max_limit=1)
        UnitType(self, BaalPredator, min_limit=1, max_limit=1)

    def check_rules(self):
        super(CarmineHost, self).check_rules()
        for unit in self.cap.units:
            if not unit.armour.is_tda():
                self.error("{} must be equipped with Terminator Armour".format(unit.name))


class BloodAngelsV3Base(volume2.SpaceMarinesRelicRoster,
                        BloodAngelsCommon):
    army_id = 'blood_angels_v3_base'
    army_name = 'Blood Angels'

    def __init__(self):

        self.hq = HQwithCypher(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(BloodAngelsV3Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self), lords=LordsOfWar(parent=self)
        )

    def has_keeper(self):
        return self.hq.reclusiarch.count > 0

    def check_rules(self):
        super(BloodAngelsV3Base, self).check_rules()
        if self.hq.phoros.count:
            if self.hq.chapter_master_count() + (self.lords.chapter_master_count() if self.lords is not None else 0) > 1:
                self.error("Malakim Phoros must be the only Chapter Master in detachment")
            if not type(self.parent.parent) is PrimaryDetachment:
                self.error("Malakim Phoros must be your army\'s Warlord")


class BloodAngelsV3CAD(BloodAngelsV3Base, CombinedArmsDetachment):
    army_id = 'blood_angels_v3_cad'
    army_name = 'Blood Angels (Combined Arms detachment)'


class BloodAngelsV3AD(BloodAngelsV3Base, AlliedDetachment):
    army_id = 'blood_angels_v3_ad'
    army_name = 'Blood Angels (Allied detachment)'


class BloodAngelsV3PA(BloodAngelsV3Base, PlanetstrikeAttacker):
    army_id = 'blood_angels_v3_pa'
    army_name = 'Blood Angels (Planetstrike attacker detachment)'


class BloodAngelsV3PD(BloodAngelsV3Base, PlanetstrikeDefender):
    army_id = 'blood_angels_v3_pd'
    army_name = 'Blood Angels (Planetstrike defender detachment)'


class BloodAngelsV3SA(BloodAngelsV3Base, SiegeAttacker):
    army_id = 'blood_angels_v3_sa'
    army_name = 'Blood Angels (Siege War attacker detachment)'


class BloodAngelsV3SD(BloodAngelsV3Base, SiegeDefender):
    army_id = 'blood_angels_v3_sd'
    army_name = 'Blood Angels (Siege War defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'blood_angels_v3_asd'
    army_name = 'Blood Angels (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class BloodAngelsV3KillTeam(Wh40kKillTeam):
    army_id = 'blood_angels_v3_kt'
    army_name = 'Blood Angels'
    ia_enabled = False

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(BloodAngelsV3KillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [TacticalSquad], lambda u: [u.transport.droppod])
            self.scout_squad = UnitType(self, ScoutSquad)
            UnitType(self, RaphenDeathCompany)

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(BloodAngelsV3KillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                SternguardVeteranSquad,
                DeathCompany,
                VanguardVeteranSquad,
                CommandSquad
            ], lambda u: [u.transport.droppod])
            UnitType(self, Lemartes)

    class KTFast(FastSection):
        def __init__(self, parent):
            super(BloodAngelsV3KillTeam.KTFast, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                AssaultSquad
            ], lambda u: [u.transport.droppod])
            Wh40kKillTeam.process_vehicle_units(self, [
                Rhino, Razorback, LandSpeederSquadron])
            UnitType(self, AssaultSquad)
            UnitType(self, SpaceMarineBikers)
            UnitType(self, AttackBikeSquad)
            UnitType(self, ScoutBikers)

    def __init__(self):
        super(BloodAngelsV3KillTeam, self).__init__(
            self.KTTroops(self), self.KTElites(self), self.KTFast(self))


faction = 'Blood Angels'


class BloodAngelsV3(Wh40k7ed, Wh40kImperial):
    army_id = 'blood_angels_v3'
    army_name = 'Blood Angels'
    faction = faction

    def __init__(self):
        super(BloodAngelsV3, self).__init__([BloodAngelsV3CAD,
                                             FlierDetachment,
                                             BattleCompany,
                                             BaalStrikeForce,
                                             FleshTearersStrikeForce,
                                             AngelBlade,
                                             LostBrotherhood,
                                             ArchangelInterventionForceBaal,
                                             ArchangelStrikeForce,
                                             ArchangelSanguineWing,
                                             ArchangelDemiCompany,
                                             Archangels,
                                             BloodedDemiCompany,
                                             Mortalis,
                                             AvengingHost,
                                             VanguardStrikeForce,
                                             ReliefForce,
                                             CatherdumDefenders,
                                             RazorwindForce,
                                             WrathIntervention,
                                             FurySpearhead,
                                             Deathstorm,
                                             CarmineHost,
                                             ArchangelInterventionForce,
                                             ArchangelDemiCompanyBlade,
                                             BattleDemiCompany,
                                             AmbushForce,
                                             DeathCompanyStrikeForce,
                                             GoldenHost,
                                             Ancients,
                                             StormravenSquadron,
                                             LuciferTaskForce
                                             ])


class BloodAngelsV3Missions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'blood_angels_v3_mis'
    army_name = 'Blood Angels'
    faction = faction

    def __init__(self):
        super(BloodAngelsV3Missions, self).__init__([
            BloodAngelsV3CAD,
            BloodAngelsV3PA,
            BloodAngelsV3PD,
            BloodAngelsV3SA,
            BloodAngelsV3SD,
            FlierDetachment,
            BattleCompany,
            BaalStrikeForce,
            FleshTearersStrikeForce,
            AngelBlade,
            LostBrotherhood,
            ArchangelInterventionForceBaal,
            ArchangelStrikeForce,
            ArchangelSanguineWing,
            ArchangelDemiCompany,
            Archangels,
            BloodedDemiCompany,
            Mortalis,
            AvengingHost,
            VanguardStrikeForce,
            ReliefForce,
            CatherdumDefenders,
            RazorwindForce,
            WrathIntervention,
            FurySpearhead,
            Deathstorm,
            CarmineHost,
            ArchangelInterventionForce,
            ArchangelDemiCompanyBlade,
            BattleDemiCompany,
            AmbushForce,
            DeathCompanyStrikeForce,
            GoldenHost,
            Ancients,
            StormravenSquadron,
            LuciferTaskForce
        ])


detachments = [
    BloodAngelsV3CAD,
    BloodAngelsV3PA,
    BloodAngelsV3PD,
    BloodAngelsV3SA,
    BloodAngelsV3SD,
    BloodAngelsV3AD,
    FlierDetachment,
    BattleCompany,
    BaalStrikeForce,
    FleshTearersStrikeForce,
    AngelBlade,
    LostBrotherhood,
    ArchangelInterventionForceBaal,
    ArchangelStrikeForce,
    ArchangelSanguineWing,
    ArchangelDemiCompany,
    Archangels,
    BloodedDemiCompany,
    Mortalis,
    AvengingHost,
    VanguardStrikeForce,
    ReliefForce,
    CatherdumDefenders,
    RazorwindForce,
    WrathIntervention,
    FurySpearhead,
    Deathstorm,
    CarmineHost,
    ArchangelInterventionForce,
    ArchangelDemiCompanyBlade,
    BattleDemiCompany,
    AmbushForce,
    DeathCompanyStrikeForce,
    GoldenHost,
    Ancients,
    StormravenSquadron,
    LuciferTaskForce
]


unit_types = [
    Captain, Tycho, DCTycho, Sanguinor, Librarian, Mephiston,
    Priest, Corbulo, Chaplain, Astorath, Techmarine,
    LibrarianDreadnought, Karlaen, TerminatorCaptain,
    DeathCompanyChaplain,
    CommandSquad, DeathCompany, Lemartes, SanguinaryGuards,
    Dreadnought, DeathCompanyDreadnought, FuriosoDreadnought, TerminatorSquad,
    TerminatorAssaultSquad, VanguardVeteranSquad, SternguardVeteranSquad,
    SquadAlphaeus,
    TacticalSquad, ScoutSquad, RaphenDeathCompany, Cassor,
    AssaultSquad, SpaceMarineBikers, ScoutBikers,
    AttackBikeSquad, LandSpeederSquadron,
    Devastators,
    Stormravens, BaalPredators,
    Predators, Vindicators, Whirlwinds,
    DropPod, Rhino, Razorback, LandRaider, LandRaiderCrusader,
    LandRaiderRedeemer,
    Dante, Seth
]
