/**
 * Created by dante on 4/23/14.
 */

var Node = function() {};

Node.prototype.setupNode = function(parent, data) {
    this.id = data.id;
    this.parent = parent;
    this.active = ko.observable(data.active);
    this.visible = ko.observable(data.visible);
};

Node.prototype.genericPush = function(data) {
    if ('active' in data)
        this.active(data.active);
    if ('visible' in data)
        this.visible(data.visible);
};
