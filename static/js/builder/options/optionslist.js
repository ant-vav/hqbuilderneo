/**
 * Created by dante on 4/24/14.
 */

var ListSingleOption = function(parent, data) {
    var self = this;
    this.setupNode(parent, data);
    this.name = data.name;
    this.is_top = !data.low_level;
    this.value = ko.observable(data.value);
    this.value.subscribe(function(o) {
        self.parent.update({id: self.id, value: o});
    });
};

ListSingleOption.prototype = new Node();

ListSingleOption.prototype.pushChanges = function(data) {
    this.genericPush(data);
    if ('value' in data)
        this.value(data.value);
};


var OptionsList = function(opt) {
    var self = this;
    this.setupNode(opt.parent, opt.data);
    this.type = 'options_list';
    this.section = opt.data.name;
    this.optionsList = opt.data.options.map(function(opt) { return new ListSingleOption(self, opt); });

    this.options = ko.observableArray(this.getOptions());
};

OptionsList.prototype = new Node();

OptionsList.prototype.update = function(obj) {
    this.parent.update({id: this.id, options: [obj]});
};

OptionsList.prototype.getOptions = function() {
    return this.optionsList.filter(function(val) { return val.visible(); });
};

OptionsList.prototype.pushChanges = function(data) {
    var self = this;
    this.genericPush(data);
    if (data.options)
    {
        $.each(data.options, function(n, option_data) {
            $.each(self.optionsList, function(n, option) {
                if (option_data.id == option.id)
                    option.pushChanges(option_data);
            });
        });
        this.options(this.getOptions());
    }
};
