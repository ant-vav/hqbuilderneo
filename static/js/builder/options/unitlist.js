/**
 * Created by dromanow on 4/25/14.
 */


var UnitList = function(opt) {
    var self = this;
    this.setupNode(opt.parent, opt.data);
    this.type = 'unit_list';

    this.units = ko.observableArray(opt.data.units.map(function(unit_data) {
        var unit = new Unit({parent: self, data: unit_data});
        unit.canSplit = ko.observable($.inArray(unit.id, opt.data.can_split) != -1);
        return unit;
    }));
    this.canDelete = ko.observable(opt.data.can_delete);
    this.canAdd = ko.observable(opt.data.can_add);
};

UnitList.prototype = new Node();

UnitList.prototype.updateUnit = function(obj) {
    this.parent.update({id: this.id, units: [obj]});
};

UnitList.prototype.addUnit = function(unit) {
    this.parent.update({id: this.id, add: unit.id});
};

UnitList.prototype.deleteUnit = function(unit) {
    this.parent.update({id: this.id, 'delete': unit.id});
};

UnitList.prototype.splitUnit = function(unit) {
    this.parent.update({id: this.id, split: unit.id});
};

UnitList.prototype.pushChanges = function(data) {
    var self = this;
    this.genericPush(data);
    if (data.new_unit)
    {
        var unit = new Unit({parent: self, data: data.new_unit});
        unit.canSplit = ko.observable($.inArray(unit.id, data.can_split) != -1);
        self.units.push(unit);
    }
    if (data.deleted_unit)
        self.units.remove(function(unit) { return unit.id == data.deleted_unit; });
    if (data.can_add !== undefined)
        self.canAdd(data.can_add);
    if (data.can_delete !== undefined)
        self.canDelete(data.can_delete);
    if (data.can_split !== undefined)
        $.each(self.units(), function(n, unit) {
            unit.canSplit($.inArray(unit.id, data.can_split) != -1);
        });

    if (data.units)
        $.each(data.units, function (n, unit_data) {
            $.each(self.units(), function(n, unit) {
                if (unit.id == unit_data.id)
                    unit.pushChanges(unit_data);
            });
        });
};
