"""add link lookup table

Revision ID: 24756c4ba754
Revises: 1788125b7018
Create Date: 2017-01-13 19:33:38.630312

"""

# revision identifiers, used by Alembic.
revision = '24756c4ba754'
down_revision = '1788125b7018'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'lookup',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('faction', sa.String(64), nullable=False),
        sa.Column('name', sa.String(64), nullable=False),
        sa.Column('alias', sa.String(64)),
        sa.Column('link', sa.String(256), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('lookup')
