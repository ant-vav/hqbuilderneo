__author__ = 'dante'

import uuid
from flask.ext.login import LoginManager as LoginManagerBase, login_user, UserMixin
from db.models import User, db
from db.crypto import verify, hash_password
from sqlalchemy import func


class LoginUser(User, UserMixin):

    def is_active(self):
        return self.active

    def verify_password(self, password):
        return verify(password, self.password)

    def update_settings(self, settings):
        if not self.verify_password(settings['password']):
            raise RuntimeError("Wrong password")
        if settings['new_password'] != settings['repeat_password']:
            raise RuntimeError('Passwords does not match')
        email = settings['email'].lower()
        if email != self.email:
            if LoginUser.query.filter_by(email=email.lower()).count() > 0:
                raise RuntimeError('This email already in use')
            self.email = email
        name = settings['name']
        if name != self.name:
            if LoginUser.query.filter_by(name=name).count() > 0:
                raise RuntimeError('This name already in use')
            self.name = name
        if settings['new_password']:
            self.password = hash_password(settings['new_password'])
        db.session.commit()
        return 'Settings have been successfully saved'


class LoginManager(LoginManagerBase):

    def __init__(self, app):
        super(LoginManager, self).__init__(app=app)
        self.login_view = "auth.signin_page"

        self.user_loader(self.get_user)
        self.mailer = app.mailer

    @staticmethod
    def get_user(user_id):
        if user_id.isdigit():
            user = LoginUser.query.filter_by(id=int(user_id)).first()
        else:
            # TODO: this is temporary user login with mongodb key
            user = LoginUser.query.filter_by(legacy_key=user_id).first()
            if user:
                login_user(user)
        return user

    @staticmethod
    def get_user_by_email(email):
        return LoginUser.query.filter(func.lower(User.email) == func.lower(email)).first()

    @staticmethod
    def create_user(email, name, password, repeat_password):
        if password != repeat_password:
            raise RuntimeError('Passwords does not match')
        if LoginManager.get_user_by_email(email):
            raise RuntimeError('This email already in use')
        if LoginUser.query.filter_by(name=name).count() > 0:
            raise RuntimeError('This user name already in use')
        user = LoginUser(email=email.lower(), name=name, password=hash_password(password))
        db.session.add(user)
        db.session.commit()
        login_user(user, True)

    @staticmethod
    def process_login_user(email, password, remember):
        user = LoginManager.get_user_by_email(email)
        if not user or not user.verify_password(password):
            raise RuntimeError("Wrong email or password")
        login_user(user, remember)

    def create_recovery_key(self, email):
        user = LoginManager.get_user_by_email(email)
        if user is None:
            raise RuntimeError('User not found')
        user.recovery = uuid.uuid4().hex
        db.session.commit()
        self.mailer.send_recovery_key(user.email, user.recovery)
        return 'Please, check your mail box and follow instructions'

    def process_recovery_key(self, recovery_key):
        user = LoginUser.query.filter_by(recovery=recovery_key).first()
        if not recovery_key or user is None:
            raise RuntimeError('Recovery key not found')
        password = uuid.uuid4().hex
        user.recovery = None
        user.password = hash_password(password)
        db.session.commit()
        self.mailer.send_new_password(user.email, password)
        return 'Please, check your mail box for new password'
