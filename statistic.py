__author__ = 'Denis Romanow'

from db.models import Roster
from hq import Hq
from datetime import datetime, timedelta

hq = Hq(True)
# hq = Hq()
hq.test_request_context().push()

# from collections import Counter
# c = Counter(roster.data.get('army_id') for roster in Roster.query.filter(Roster.created > datetime.utcnow()
#                                                                          - timedelta(days=30)))

num = 0
c = {}
for roster in Roster.query.filter(Roster.created > datetime.utcnow() - timedelta(days=30)):
    num += 1
    army = roster.data.get('army_id')
    c[army] = 1 + c.get(army, 0)
    if not num % 100:
        print num

from builder.games import Games
games = Games(False, None)

rosters = [dict(name=games.get_roster_name(k), count=count) for k, count in c.iteritems()]
rosters.sort(key=lambda v: v['count'], reverse=True)
total = sum(v['count'] for v in rosters)
for v in rosters:
    v['percent'] = float(v['count']) / float(total) * 100.0


for roster in rosters:
    print "{}; {}; {:.2f}; {};".format(roster['name'][0], roster['name'][1], roster['percent'], roster['count'])
