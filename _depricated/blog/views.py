from _depricated.blog import post_list

__author__ = 'dante'

from flask import Blueprint, render_template, jsonify, request, url_for
from flask.ext.login import current_user
import json

module = Blueprint('blog', __name__)


@module.route('/post/<post_id>')
def post(post_id):
    return render_template("../templates/blog/post.html", data=post_utils.get_single_post(post_id))


@module.route('/_update_post', methods=['POST'])
def update_post():
    if not current_user.is_authenticated():
        return jsonify(redirect=url_for('auth.signin_page'))
    return jsonify(result=post_utils.update_post(json.loads(request.form.get('data'))))

#
# @module.route('/_post_list_data', methods=['POST'])
# def post_list_data():
#     data = json.loads(request.form.get('data'))
#
#     def safe_get(key):
#         return data[key] if key in data.keys() else None
#
#     return jsonify(result=posts_list_data(safe_get('page'), safe_get('tag'), safe_get('user')))

@module.route('/')
@module.route('/<int:page>')
@module.route('/blog/<string:author>')
@module.route('/blog/<string:author>/<int:page>')
@module.route('/blog/<string:author>/tag/<string:tag>')
@module.route('/blog/<string:author>/tag/<string:tag>/<int:page>')
@module.route('/tag/<string:tag>')
@module.route('/tag/<string:tag>/<int:page>')
def main(page=0, author=None, tag=None):
    return render_template("blog/main.html", data=post_list(page=page, author=author, tag=tag))
