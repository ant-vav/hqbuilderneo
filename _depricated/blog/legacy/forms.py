import datetime

from django import forms
from django.contrib import auth
from waaagh.multiblog.models import User, UserProfile
from _depricated.blog.consts import max_avatar_size
from _depricated.blog.legacy import messages


class UserProfileForm(forms.Form):
    e_mail = forms.EmailField()
    country = forms.CharField(required=False)
    city = forms.CharField(required=False)
    birth_day = forms.ChoiceField(required=False,
        choices=[('','')]+[(i, i) for i in range(1, 32)])
    birth_month = forms.ChoiceField(required=False,
        choices=[('','')]+[(i, messages.months[i]) for i in range(1, 13)])
    birth_year = forms.ChoiceField(required=False,
        choices=[('','')]+[(i, i) for i in range(1900, datetime.date.today().year)])

    def clean_password_again(self):
        if self.cleaned_data['password'] != self.cleaned_data['password_again']:
            raise forms.ValidationError(messages.error_passwords_not_match)
        return self.cleaned_data['password_again']

    def clean_birth_year(self):
        try:
            cd = self.cleaned_data
            if cd['birth_year'] != '' and cd['birth_month'] != '' and cd['birth_day'] != '':
                datetime.date(year=int(cd['birth_year']), month=int(cd['birth_month']), day=int(cd['birth_day']))
        except Exception:
            raise forms.ValidationError(messages.error_date)
        return cd['birth_year']


class NewUserForm(UserProfileForm):
    name = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())
    password_again = forms.CharField(widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        forms.Form.__init__(self, *args, **kwargs)

    def clean_name(self):
        name = self.cleaned_data['name']
        if User.objects.filter(username = name).count():
            raise forms.ValidationError(messages.error_username_exist)
        return name

    def clean_e_mail(self):
        e_mail = self.cleaned_data['e_mail']
        if User.objects.filter(email = e_mail).count():
            raise forms.ValidationError(messages.error_email_exist)
        return e_mail


class EditUserForm(UserProfileForm):
    old_password = forms.CharField(widget=forms.PasswordInput())
    password = forms.CharField(widget=forms.PasswordInput(), required=False)
    password_again = forms.CharField(widget=forms.PasswordInput(), required=False)

    def __init__(self, *args, **kwargs):
        self.current_user = kwargs.pop('current_user', None)
        user = User.objects.get(username=self.current_user)
        initial = {
            'e_mail': user.email,
            'city': user.get_profile().city,
            'country' : user.get_profile().country
        }
        if user.get_profile().birth_date is not None:
            initial.update({
                'birth_day' : str(user.get_profile().birth_date.day),
                'birth_month' : str(user.get_profile().birth_date.month),
                'birth_year' : str(user.get_profile().birth_date.year)
            })
        UserProfileForm.__init__(self, initial=initial, *args, **kwargs)

    def clean_old_password(self):
        auth_user = auth.authenticate(username=self.current_user,
            password=self.cleaned_data['old_password'])
        if auth_user is not None and auth_user.is_active:
            return self.cleaned_data['old_password']
        raise forms.ValidationError(messages.error_access_denied)


class LoginForm(forms.Form):
    name = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

    def clean_password(self):
        auth_user = auth.authenticate(username=self.cleaned_data['name'],
            password=self.cleaned_data['password'])
        if auth_user is not None and auth_user.is_active:
            return self.cleaned_data['password']
        raise forms.ValidationError(messages.error_access_denied)


class AmnesiaForm(forms.Form):
    e_mail = forms.EmailField()

    def clean_e_mail(self):
        e_mail = self.cleaned_data['e_mail']
        if User.objects.filter(email = e_mail).count():
            return e_mail
        raise forms.ValidationError(messages.error_email_not_found)

class PrivateMessageForm(forms.Form):
    to_user = forms.CharField()
    subject = forms.CharField()
    text = forms.CharField(widget=forms.Textarea())

    def __init__(self, *args, **kwargs):
        self.current_user = kwargs.pop('current_user', None)
        forms.Form.__init__(self, *args, **kwargs)

    def clean_to_user(self):
        try:
            user = User.objects.get(username=self.cleaned_data['to_user'])
        except Exception:
            raise forms.ValidationError(messages.error_user_not_found)
        if user.username == self.current_user:
            raise forms.ValidationError(messages.error_self_mail)
        return self.cleaned_data['to_user']

class AvatarForm(forms.Form):
#    blog_title = forms.CharField(max_length=120, required=False)
#    blog_description = forms.CharField(max_length=500, required=False)
    avatar = forms.ImageField(required=False)
#    new_messages_notify = forms.BooleanField(required=False)
#    new_bookmarked_comments_notify = forms.BooleanField(required=False)
#    keep_bookmarks = forms.IntegerField(required=False, min_value=1, max_value=30)
#
#    def __init__(self, *args, **kwargs):
#        user = kwargs.pop('user', None)
#        initial = {}
#        if user is not None:
#            user_profile = user.get_profile()
#            initial = {'blog_title': user_profile.blog_title,
#                   'blog_description': user_profile.blog_description,
#                   'new_messages_notify': user_profile.new_messages_notify,
#                   'new_bookmarked_comments_notify': user_profile.new_bookmarked_comments_notify,
#                   'keep_bookmarks': user_profile.keep_bookmarks }
#        forms.Form.__init__(self, initial=initial, *args, **kwargs)

    def clean_avatar(self):
        avatar_file = self.cleaned_data['avatar']
        if avatar_file and avatar_file.size > max_avatar_size * 1024:
            raise forms.ValidationError(messages.error_max_avatar_size % max_avatar_size)
        return avatar_file
