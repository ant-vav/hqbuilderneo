__author__ = 'dromanov'

from datetime import datetime, timedelta

online_users = {}

def add_online_user(user):
    online_users[user] = datetime.now()

def get_online_users():
    for u in online_users.keys():
        if online_users[u] < datetime.now() - timedelta(minutes=5):
            online_users.pop(u)
    return online_users.keys()

if __name__ == '__main__':
    import time
    add_online_user('Dante')
    print get_online_users()

    time.sleep(2)
    add_online_user('Waaagh')
    print get_online_users()

    time.sleep(2)
    print get_online_users()

    time.sleep(2)
    print get_online_users()
