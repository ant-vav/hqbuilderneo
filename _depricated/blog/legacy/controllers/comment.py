__author__ = 'dante'

from django.core.mail import EmailMessage
from waaagh.multiblog.models import Comment, PostHit, Bookmark
from waaagh.multiblog.tools.mail import send_email
from waaagh.multiblog.tools.text import auto_mark_links, normalize_line_breaks
from waaagh.multiblog.messages import new_comment_msg, new_comment_subj

def edit_comment(comment, text, proc_ref=True, proc_images=True, proc_lines=True):
    comment.text = get_comment_text(text, proc_ref, proc_images, proc_lines)
    comment.save()

def new_comment(author, post, parent, text, proc_ref=True, proc_images=True, proc_lines=True):
    comment = Comment(
        text=get_comment_text(text, proc_ref, proc_images, proc_lines),
        author=author,
        post=post)
    if parent is not None:
        comment.parent = parent
        comment.level = parent.level + 1
    comment.save()
    post.comments = Comment.objects.filter(post=post).count()
    post.save()

    try:
        hit = PostHit.objects.get(post=post, user=author)
        hit.hit_comment_count += 1
        hit.save()
    except Exception:
        pass

    try:
        bm = Bookmark.objects.get(post=post, user=author)
        bm.hit_comment_count += 1
    except Exception:
        bm = Bookmark(post=post, user=author, hit_comment_count=post.comments)
    bm.save()

    to = []
    for notify in Bookmark.objects.filter(post=post):
        if notify.user.get_profile().new_bookmarked_comments_notify and notify.user.username != author.username:
            to.append(notify.user.email)
    if len(to) is not 0:
        send_email(EmailMessage(new_comment_subj,
            new_comment_msg % {'author':author.username,
                               'text': text,
                               'title': post.title,
                               'post_id': post.id}, bcc=to))

    return comment

def get_comment_text(text, process_ref, process_images, process_lines):
    if process_ref or process_images:
        text = auto_mark_links(text, process_ref, process_images)
    if process_lines:
        text = normalize_line_breaks(text)
    return text
